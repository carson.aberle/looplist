//
//  InputTextfield.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/1/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation

class InputTextField: UITextField {

    
    let padding = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30);
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        var originalRect = super.caretRect(for: position)
        originalRect.size.width = 1.0
        return originalRect
    }
    
    
    
    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.height / 2.0
        //        self.backgroundColor = UIColor.whiteColor()
//        self.layer.borderWidth = 0.5
//        self.layer.borderColor = UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1.0).cgColor
    }

}
