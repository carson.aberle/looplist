//
//  ShadowButton.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/27/17.
//  Copyright © 2017 Looplist. All rights reserved.
//
//
//import UIKit
//
//class ShadowButton: UIButton {
//    
//    var shadowLayer: CAShapeLayer!
//    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        
//        if shadowLayer == nil {
//            shadowLayer = CAShapeLayer()
//            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: self.frame.height).cgPath
//            shadowLayer.fillColor = UIColor.white.cgColor
//            shadowLayer.shadowColor = UIColor.darkGray.cgColor
//            shadowLayer.shadowPath = shadowLayer.path
//            shadowLayer.shadowOffset = CGSize(width: 2.0, height: 2.0)
//            shadowLayer.shadowOpacity = 0.8
//            shadowLayer.shadowRadius = 2
//            
//            layer.insertSublayer(shadowLayer, at: 0)
//            //layer.insertSublayer(shadowLayer, below: nil) // also works
//        }        
//    }
//    
//}
