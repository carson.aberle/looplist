if (webkit) {
    setInterval(function() {
                var data = getData();
                webkit.messageHandlers.callbackHandler.postMessage(data);
                }, 1000);
}


function getData() {
    var data = [];
    var temp = [];
    
    var imageNodes = document.querySelectorAll("img");
    for (var i = 0; i < imageNodes.length; i++) {
        var imageNode = imageNodes[i];
        
        if (isImageValid(imageNode) && checkImageIfNotGIF(imageNode.src)) {
            var price = getPriceFromParents(imageNode);
            var description = getDescriptionFromParents(imageNode);
            
            var webProduct = {
            image: imageNode.src,
            price: price,
            node: imageNode,
            description:description
            };
            
            temp.push(webProduct);
        }
    }
    
    data = eliminateRepeatingImages(temp);
    function compare(a,b) {
        var aRes = a.node.naturalWidth * a.node.naturalHeight;
        var bRes = b.node.naturalWidth * b.node.naturalHeight;
        
        if (aRes > bRes)
            return -1;
        if (aRes < bRes)
            return 1;
        return 0;
    }
    
    data.sort(compare);
    
    for (var i = 0, len = data.length; i < len; i++) {
        data[i].node = undefined;
    }
    
    function compare(a,b) {
        var aRes = a.naturalWidth * a.naturalHeight;
        var bRes = b.naturalWidth * b.naturalHeight;
        
        if (aRes < bRes)
            return -1;
        if (aRes > bRes)
            return 1;
        return 0;
    }
    
    data.sort(compare);
    console.dir("Data", data)
    return data;
}


/*
 * Checks to make sure image is large enough size and does not have an empty src.
 */
function isImageValid(imageNode) {
    return imageNode.naturalWidth > 250 && imageNode.naturalHeight > 250 && imageNode.src != "";
}

/*
 * Checks the src url to make sure is not gif
 */
function checkImageIfNotGIF(url) {
    var string1 = "gif";
    var string2 = "GIF";
    var ext = url.substring(url.lastIndexOf(".")+1);
    
    if (ext != string1 && ext != string2) {
        return true;
    } else {
        return false;
    }
}

/*
 * Checks the data array to make sure image src is not already present.
 */
function isImagePresentIn(arr, imageNode) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].image && arr[i].image == imageNode.src) {
            return true;
        }
    }
    
    return false;
}

/*
 * Iterates through array and counts how many times an item occures in the array.  removes Repeating elements.
 */
function eliminateRepeatingImages(arr) {
    var a = [], b = [], prev;
    
    arr.sort();
    for ( var i = 0; i < arr.length; i++ ) {
        if ( arr[i].image !== prev ) {
            a.push(arr[i]);
            b.push(1);
        } else {
            b[b.length-1]++;
        }
        prev = arr[i].image;
    }
    
    for (var i = b.length - 0; i >= 0; i--){
        if (b[i] >= 3){
            a.splice(i, 1);
        }
    }
    
    return a;
}
//
//function eliminateRepeatingImages(arr) {
//    console.log("array",arr)
//    return arr.sort().reduce(function(accumulator,value){
//        if(accumulator.indexOf(value) == -1){
//         accumulator.push(value)
//        }
//    }, [])
//}

/*
 * Iterates through node's parents and returns the nearest description. Returns undefined if none are found.
 */
function getDescriptionFromParents(node) {
    while(node.parentNode) {
        var description = getActualDescriptionFromNode(node);
        if (description) {
            //            console.log(node.innerText);
            //            return price;
        }
        
        node = node.parentNode;
    }
    
    return undefined;
}

/*
 * Finds the price in a string and converts it to an integer. Returns undefined if no prices are found.
 */


function getActualDescriptionFromNode(node) {
    
    //    console.log("Node",node.className);
    //    var str = node.className;
    var descriptionClassName = node.innerText.match(/desc/gi); // logic
    console.log("description class name",descriptionClassName);
    if (descriptionClassName){
        return node.innerText
    }
}



/*
 * Iterates through node's parents and returns the nearest price. Returns undefined if none are found.
 */
function getPriceFromParents(node) {
    while(node.parentNode) {
        var price = getActualPriceFromNode(node.innerText);
        if (price) {
            //            console.log(node.innerText);
            return price;
        }
        
        node = node.parentNode;
    }
    
    return undefined;
}

/*
 * Finds the price in a string and converts it to an integer. Returns undefined if no prices are found.
 test.match(/[\$|USD]\s*([\d\,]+\.\d{0,2})/);
 */
function getActualPriceFromNode(str) {
    str = str.match(/\$[0-9\.,]+/);
    
    if ( !str ) {
        return undefined;
    }
    
    str = str[0];
    str = str.replace(/[^\d.-]/g, '');
    
    if (str.indexOf(".") >= 0) {
        str = str.replace(".", "");
    } else {
        str += "00";
    }
    
    str = parseInt(str);
    
    return str;
}
