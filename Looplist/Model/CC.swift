//
//  CC.swift
//  Looplist
//
//  Created by Arun Sivakumar on 10/3/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation

class CC{
    
    // MARK: -  --------------------------------------------------------   VARIABLES  -----------------------------------------------------------------------
    
    var objectId:String?
    var brand:String?
    var last4:String?
    
    var name:String?
    
    // CURRENT USER DEFAULTS INIT
    init(input:Dictionary<String,AnyObject>){
        if let objectId = input["_id"] as? String, let last4 = input["last4"] as? String ,let brand = input["brand"] as? String{
            self.objectId = objectId
            self.last4 = last4
            self.brand = brand
        }
        
        if let name = input["name"] as? String{
            self.name = name
        }
        
    }
    
//    func assignCC(_ input:Dictionary<String,AnyObject>){
//
//  
//    }
    
}
