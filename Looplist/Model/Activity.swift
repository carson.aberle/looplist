//
//  Activity.swift
//  Looplist
//
//  Created by Carson Aberle on 8/25/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation

class Activity{ // FINAL
    
    // MARK: -  --------------------------------------------------------   VARIABLES  -----------------------------------------------------------------------

    var activityType:ActivityType?
    var user:User?
    var product:Product?
    var date:Date?
    var read:Bool?
    var objectId:String?
    var attributes:NSDictionary?
    
    // MARK: -  //////////////////////////////////////////////////////   INITILIZER   /////////////////////////////////////////////////////////////////////////

    init?(input:Dictionary<String,AnyObject>){
        
        print(input)
        guard let objectId = input["_id"] as? String,
              let activityTypeString = input["activityType"]as? String,let activityType = ActivityType(rawValue: activityTypeString),
              let date = input["createdAt"] as? String,
            let read = input["read"] as? Bool
        else {
                return nil
        }
        
        if let productAttributes = input["product"] as? Dictionary<String, AnyObject>{
            if let product = Product(input: productAttributes){
                self.product = product
            }else{
                return nil
            }
        }
        
        if activityType == .likedYourRepostedProduct || activityType == .someoneAddedYourProduct {
            if let userFromActivity =  input["associatedUser"] as? Dictionary<String, AnyObject>{
                let user = User(input:userFromActivity)
                self.user = user
            }else{
                return nil
            }
           
        }else if activityType == .follow {
            if let userFromActivity =  input["following"] as? Dictionary<String, AnyObject>{
                let user = User(input:userFromActivity)
                self.user = user
            }else {
               return nil
            }
        
        }else if let userFromActivity =  input["user"] as? Dictionary<String, AnyObject>{ // activityType == .followRequest or user
            let user = User(input:userFromActivity)
            self.user = user
        }   else {
            return nil
        }
        
        self.objectId = objectId
        self.activityType = activityType
        self.date = HelperDate.activityStringToDate(date)
        self.read = read
        
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////

    
}
