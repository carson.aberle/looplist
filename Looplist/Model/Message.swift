//
//  Message.swift
//  Looplist
//
//  Created by Arun Sivakumar on 8/28/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation


class Message{
    
    // MARK: -  --------------------------------------------------------   VARIABLES  -----------------------------------------------------------------------

    var fromId:String?
    var textMessage:String?
    var listing:Listing?
    var product:Product?
    
    var chatId:String?
    var date:Date?

    var headerMessage:String?
    
    // MARK: -  //////////////////////////////////////////////////////   INITIALIZER   /////////////////////////////////////////////////////////////////////////

    init( fromId:String? = nil,chatId:String? = nil,textMessage:String? = nil,listing:Listing? = nil, product:Product? = nil,date:Date? = nil){
        
        if let fromId  = fromId{
              self.fromId = fromId
        }
        
        if let chatId = chatId{
        
            self.chatId = chatId
        }
        
        if let date = date{
            self.date = date
        }
        
        self.textMessage = textMessage
        self.listing = listing
        self.product = product

    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////

    func setProduct(_ product:Product?){
        if let product = product{
            self.product = product
        }
    }
    
    func setMessageBasedOnSender(_ sender:Bool,receiverName:String){
        if sender{
            self.headerMessage = "You started a conversation with" + " " + receiverName
        }else{
            self.headerMessage = receiverName + " " + "started a conversation with you"
        }
    }
}
