//
//  MessagingInbox.swift
//  Looplist
//
//  Created by Arun Sivakumar on 8/12/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation
import UIKit

class MessageConversation:NSObject{
 
    // MARK: -  --------------------------------------------------------   VARIABLES  -----------------------------------------------------------------------

    var user:User?
    var userLastMessage:String?
    var chatId:String?
    var date:Date?
    var read:Bool?
//    var userProfilePicture:UIImage?
    var product:Product?
    
    
    // MARK: -  //////////////////////////////////////////////////////   INITILIZER   /////////////////////////////////////////////////////////////////////////

    init( user:User?,chatId:String?,userLastMessage:String?,date:Date?,read:Bool?){
        if let user  = user{
            self.user = user
        }
        if let chatId = chatId{
            self.chatId = chatId
        }
        
        if let userLastMessage = userLastMessage , let date = date{
            self.userLastMessage = userLastMessage
            self.date = date
        }
        if let read = read{
            self.read = read
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////

//    func setProduct(_ product:Product){
//        self.product = product
//    }
}
