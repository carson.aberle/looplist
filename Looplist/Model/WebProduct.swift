//
//  AddProduct.swift
//  Looplist
//
//  Created by Arun Sivakumar on 11/3/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation


struct WebProduct{
    let productUrl:String? // M
    let productTitle:String? // M
    let productImageUrl:String? //M
    let productPrice:Int?


    init?(productUrl:String?,productTitle:String?,productImageUrl:String?,productPrice:Int?){
        guard let productUrl = productUrl , let productTitle = productTitle, let productImageUrl = productImageUrl else{return nil}
        
        
            self.productUrl = productUrl
            self.productTitle = productTitle
            self.productImageUrl = productImageUrl
            self.productPrice = productPrice ?? nil
        
    }

}
