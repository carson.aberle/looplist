//
//  User.swift
//  Looplist
//
//  Created by Arun Sivakumar on 2/14/16.
//  Copyright © 2016 Looplist.Inc. All rights reserved.
//

import Foundation
import Kingfisher
import Stripe

class User{
    
    // MARK: -  ----------------------------------------------------    MANDATORY   ----------------------------------------------------------------------
    
    var objectId: String?
    var userName: String?
    var fullName: String?{
        get{
            if let firstName = self.firstName, let lastName = self.lastName {
                return firstName + " " + lastName
            }else{
                return " "
            }
        }
    }
    
    var firstName: String?
    var lastName: String?  // current user
    var gender:String?
    var profileImageUrl: String?
    //var profileImage: UIImage? //messaging
    
    // MARK: -  ----------------------------------------------------     USER DETAILS   ---------------------------------------------------------------------
    
    var email: String? // current user
    var bio: String?
    var location: String?

    var addedProducts:[Product] = [Product]()
    // MARK: -   -----------------------------------------------------    COUNT   -----------------------------------------------------
    
    
    var followersCount: Int?
    var followingCount: Int?
    var likesCount: Int?
    var collectionsCount: Int?
    
    
    // MARK: -   -----------------------------------------------------      COLLECTIONS     -----------------------------------------------------
    
    
    var collectionsIds:[String]? = [String]()
    var collections:[Collection]? = [Collection]()
    
    
    // MARK: -   -----------------------------------------------------      ACTIONS - other User   -----------------------------------------------------
    
    var isProfilePrivate = false
    var followRequestStatus =  FollowStatus.follow(false)

    
    // MARK: -  -----------------------------------------------------    NOTIFICATIONS   - current user  -----------------------------------------------------
    
    var followNotifications:Bool?
    var messageNotifications:Bool?
    var followRequestsNotifications:Bool?
    var acceptedfollowRequestsNotifications:Bool?
    
    
    // MARK: -   -----------------------------------------------------      SHIPPING   - current user   -----------------------------------------------------
    
    var shipping:[Shipping]?
    var defaultShippingAddressObjectId: String?
    
    // MARK: -   -----------------------------------------------------      CC    - current user   -----------------------------------------------------
    
    var cc:[CC]?
    var defaultPaymentMethodObjectId: String?

    
    // MARK: -  ////////////////////////////////////////////////////    INIT   ///////////////////////////////////////////////////////////////////////////
    
    
    // INIT FOR ALL USERS AND CURRENT USER
    init(input:Dictionary<String,AnyObject>) {
        self.assignUserData(input)
    }
    
    // ASSIGN USER DATA
    func assignUserData(_ input:Dictionary<String,AnyObject>){
        if let objectId = input["_id"] as? String,let userName = input["username"] as? String,let firstName = input["firstName"] as? String,let lastName = input["lastName"] as? String,let gender = input["gender"] as? String{
            
            self.objectId = objectId
            self.userName = userName
            self.firstName = firstName
            self.lastName = lastName
            self.gender = gender
        }
        
        if let _ = self.objectId{
            
            if let email = input["email"] as? String{
                self.email = email
            }
            
            if let photoUrl = input["profileImageUrl"] as? String{
                self.profileImageUrl = photoUrl
                /*
                if let url = URL(string: photoUrl){
                    let sampleImageView = UIImageView()
                    sampleImageView.kf.setImage(with: url, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                    sampleImageView.kf.setImage(with: url, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: { (image, error, cache, url) in
                        if let image = image{
                            self.profileImage = image

                        }
                    })
                }
                */
            }
            
            if let quote = input["quote"] as? String{
                self.bio = quote
            }
            
            if let location = input["location"] as? String{
                self.location = location
            }
            
            if let collectionsIds = input["collections"] as? [String]{
                
                self.collectionsIds = collectionsIds
            }
            
            if let isProfilePrivate = input["isPrivate"] as? Bool {
                self.isProfilePrivate = isProfilePrivate
            }

            
            if let isRequestedByCurrentUser = input["flags"]?["isRequestedByUser"] as? Bool,let isFollowedByCurrentUser = input["flags"]?["isFollowedByUser"] as? Bool {
                
                if self.isProfilePrivate == true && !self.isCurrentUser() && !isFollowedByCurrentUser{
                    if isRequestedByCurrentUser == true{
                        self.followRequestStatus = .request(true)
                    }else{
                        self.followRequestStatus = .request(false)
                    }
                }else{
                    if isFollowedByCurrentUser == true{
                         self.followRequestStatus = .follow(true)
                    }else{
                         self.followRequestStatus = .follow(false)
                    }
                }
            }
            
            
            if let addedProductsArray = input["products"] as? NSArray{
                for addedProductElement in addedProductsArray{
                    if let addedProductDictionary = addedProductElement as? Dictionary<String,AnyObject>{
                        if let product = Product(input: addedProductDictionary){
                            self.addedProducts.append(product)
                        }
                    }
                }
            }
            
            if let likeC = input["productsLikesCount"] as? Int{
                self.likesCount = likeC
            }
            
            if let collectionsC = input["collectionsCount"] as? Int{
                self.collectionsCount = collectionsC
            }
            
            if let followersC = input["followersCount"] as? Int{
                self.followersCount = followersC
            }
            
            if let followingC = input["followingCount"] as? Int{
                self.followingCount = followingC
            }
            
            if let notifications = input["notifications"] as? NSDictionary{ // SETTINGS
                if let follows = notifications["follows"] as? Bool{
                    self.followNotifications = follows
                }
                if let messages = notifications["messages"] as? Bool {
                    self.messageNotifications = messages
                }
                if let followRequestsNotifications = notifications["followRequests"] as? Bool{
                    self.followRequestsNotifications = followRequestsNotifications
                }
                if let acceptedfollowRequestsNotifications = notifications["acceptedFollowRequests"] as? Bool {
                    self.acceptedfollowRequestsNotifications = acceptedfollowRequestsNotifications
                }
            }
            
            if let shipping = input["shipping"] as? Dictionary<String,AnyObject>{ // SHIPPING
                if let primaryAddress = shipping["primary"]{
                    self.defaultShippingAddressObjectId = primaryAddress as? String
                }
            }
            
            if let paymentMethod = input["payment"] as? Dictionary<String,AnyObject>{ //CC
                if let primaryAddress = paymentMethod["primary"]{
                    self.defaultPaymentMethodObjectId = primaryAddress as? String
                }
            }
            
        }
    }
    
    
    // MARK: -  /////////////////////////////////////////////////////    UPDATE USER    /////////////////////////////////////////////////////////////////////////

    func updateUser(_ completion:@escaping (_ success:Bool)-> Void ){
        if let objectId = self.objectId{
            let path = "users/\(objectId)"
            DBHelper.callAPIGet(path, completion: { (result, error) in
                if error == nil {
                    if let user = result?["user"] as? Dictionary<String, AnyObject>{
                        self.assignUserData(user)
                        completion(true)
                    }
                    
                } else {
                    completion(false)
                }
            })
        }
    }
    
    // GET COLLECTIONS BY THIS USER
    
    func getCurrentUserCollectionsWithoutLikes(_ initialLoad:Bool,product:Product?, skip:Int?,limit:Int?,completion:@escaping (_ success: Bool, _ collections: [Collection]?) -> Void){
        if let objectId = self.objectId,let skip = skip, let limit = limit, let productId = product?.objectId{
            
            let path = "collections"
            
            
            DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(["where":["users":objectId, "isLikesCollection":false],"sort":"-updatedAt", "flags":["containsProduct":productId],"skip":skip, "limit":limit, "populate":[["path": "products"]]])) { (result, error) in
                if error == nil{
                    if let collectionsDictionaries:NSArray = result?["collections"] as? NSArray{
                        var collections = [Collection]()

                        
                        for dictionary in collectionsDictionaries{
                            let tempCollection = Collection(input: dictionary as! [String:AnyObject])
                            
                            collections.append(tempCollection)
                        }
                        for index in 0..<collections.count{
                            if collections[index].isAddedProductCollection == true{
                                let addedProductsCollection = collections[index]
                                collections.remove(at: index)
                                collections.append(addedProductsCollection)
                            }
                        }
                        completion(true, collections)
                    } else {
                        completion(false, nil)
                    }
                } else {
                    completion(false, nil)
                }
            }
        } else if let objectId = self.objectId,let skip = skip, let limit = limit{
            
            //NO PRODUCT
            let path = "collections"
            
            
            DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(["where":["users":objectId],"skip":skip, "limit":limit, "populate":[["path": "products", "options":["sort":"-updatedAt"]]]])) { (result, error) in
                if error == nil{
                    if let collectionsDictionaries:NSArray = result?["collections"] as? NSArray{
                        
                        var collections = [Collection]()

                        
                        for dictionary in collectionsDictionaries{
                            let tempCollection = Collection(input: dictionary as! [String:AnyObject])
                            
                            collections.append(tempCollection)

                        }
                        
                        completion(true, collections)
                    } else {
                        completion(false, nil)
                    }
                } else {
                    completion(false, nil)
                }
            }
        }
    }
    
    
    // MARK: -  /////////////////////////////////////////////////////    SHIPPING    /////////////////////////////////////////////////////////////////////////
    
    func findAndGetShippingAddress(_ currentShippingAddress:Shipping?,completion:@escaping (_ shipping:Shipping?) -> Void){
        
        if let currentShippingAddress = currentShippingAddress, let shipping = self.shipping{
            var foundShipping = false
            for shippingAddress in shipping{
                if shippingAddress.address1 == currentShippingAddress.address1
                    && shippingAddress.city == currentShippingAddress.city
                    && shippingAddress.state == currentShippingAddress.state
                    && shippingAddress.zip == currentShippingAddress.zip{
                    completion(shippingAddress)
                    foundShipping = true
                    break
                }
            }
            if !foundShipping{
                self.postShipping(false, shippingAddress: currentShippingAddress, completion: { (shipping) in
                    if let shippingAddress = shipping{
                        completion(shippingAddress)
                    }else{
                        completion(nil)
                    }
                })
            }
        }else{
            completion(nil)
        }
    }
    
    // MARK: -  ----------    POST ADDRESS   -------------
    
    func postShipping(_ setDefaultAddress: Bool?, shippingAddress: Shipping?, completion:@escaping (_ shipping:Shipping?) -> Void){
        if let _ = self.objectId, let shippingAddress = shippingAddress{
            let parameters: [String:AnyObject] = ConvertToJSON.shipping(shippingAddress)
            
            DBHelper.callAPIPost("addresses", params: parameters, completion: { (result, error) in
                if error == nil{
                    if let addressId = result?["_id"] as? String{
                        Analytics.sharedInstance.actionsArray.append(Action(context:["createAddress": addressId], forScreen:nil, withActivityType:"Add Address"))
                    }
                    if let address = result?["address"] as? Dictionary<String, AnyObject>{
                        
                        let shippingAddress = Shipping(input:address)
                        self.shipping?.append(shippingAddress)
                        
                        if setDefaultAddress == true {
                            self.setUserDefaultShippingAddress(shippingAddress.objectId, completion: { (result) in
                                if result == true{
                                    completion(shippingAddress )
                                } else{
                                    completion(nil)
                                }
                            })
                        } else {
                            completion(shippingAddress)
                        }
                    }
                }else if let _ = error{
//                    if error.code != -1009 &&  error.code != -1001 {
                        ErrorHandling.unexpectedErrorHandler()
//                    }
                    completion(nil)
                }
            })
        }
    }
    
    
    // MARK: -  ----------    UPDATE ADDRESS   -------------
    
    func updateShippingAddress(_ addressId: String?, shipping: Shipping?, completion:@escaping (_ success: Bool) -> Void){
        if let shipping = shipping, let objectId = shipping.objectId {
            
            let path = "addresses/\(objectId)"
            let parameters: [String:AnyObject] = ConvertToJSON.shipping(shipping)
            
            DBHelper.callAPIPut(path, params: parameters, completion: { (result, error) in
                if error == nil{
                    Analytics.sharedInstance.actionsArray.append(Action(context:["updateAddress": objectId], forScreen:nil, withActivityType:"Edit Address"))
                    if let address = result?["address"] as? Dictionary<String, AnyObject>{
                        
                        shipping.assignShippingAddresss(address)
                        completion(true)
                    }else{
                        completion(false)
                    }
                }else if let _ = error{
//                    if error.code != -1009 &&  error.code != -1001 {
                        ErrorHandling.unexpectedErrorHandler()
//                    }
                    completion(false)
                }
            })
        }
    }
    
    // MARK: -  ----------    GET ADDRESS   -------------
    
    
    // GET DEFAULT SHIPPING ADDRESS
    
    func getDefaultShippingAddress() -> Shipping? {
        
        if let shipping = self.shipping, let defaultShippingObjectId = self.defaultShippingAddressObjectId {
            for shippingAddress in shipping{
                if let objectId = shippingAddress.objectId, objectId == defaultShippingObjectId {
                    return shippingAddress
                }
            }
        }
        return nil
    }
    
    
    //GET USER SHIPPING ADDRESS
    
    func getUserShippingAddresses(_ initialLoad:Bool,skip:Int?,limit:Int?,completion:@escaping (_ success: Bool) -> Void){
        if let skip = skip, let limit = limit{
            let queryString = "{\"sort\":\"-createdAt\",\"skip\":\(skip),\"limit\":\(limit)}"
            DBHelper.callAPIGet("addresses", queryString: queryString, completion: { (result, error) in
                if let result = result {
                    if let addresses = result["addresses"] as? NSArray{
                        
                        if initialLoad{
                            self.shipping = [Shipping]()
                        }
                        
                        for values in addresses{
                            if let address = values as? Dictionary<String, AnyObject> {
                                let shippingAddress = Shipping(input:address)
                                self.shipping?.append(shippingAddress)
                            }
                        }
                        
                        if self.getShippingAddresesCount() > 0{
                            completion(true)
                        }else{
                            completion(false)
                        }
                    }
                } else {
                    completion(false)
                }
            })
        }
    }
    
    // MARK: -  ----------    DELETE ADDRESS   -------------
    
    
    func deleteShippingAddressFromUserArray(_ objectId:String?){
        if let shippingAddresses = self.shipping{
            for (index,currentShipping) in shippingAddresses.enumerated(){
                if currentShipping.objectId == objectId{
                    shipping?.remove(at: index)
                }
            }
            checkAndDeleteDefaultAddress(objectId)
        }
        
    }
    
    
    
    func checkAndDeleteDefaultAddress(_ objectId:String?){
        if let objectId = objectId, let defaultShippingAddressObjectId = CurrentUser.sharedInstance.currentUser?.defaultShippingAddressObjectId, objectId == defaultShippingAddressObjectId{
            CurrentUser.sharedInstance.currentUser?.defaultShippingAddressObjectId = nil
        }
    }
    
    func deleteShippingAddress(_ objectId: String?, completion:@escaping (_ success: Bool) -> Void){
        if let id = objectId {
            DBHelper.callAPIDelete("addresses/\(id)", completion: { (result, error) in
                if error == nil {
                    Analytics.sharedInstance.actionsArray.append(Action(context:["deleteAddress": id], forScreen:nil, withActivityType:"Delete Address"))
                    self.deleteShippingAddressFromUserArray(objectId)
                    completion(true)
                } else {
                    completion(false)
                }
            })
        }
    }
    
    
    
    // MARK: -  ----------    DEFAULT ADDRESS   -------------
    
    func toggleUserDefaultShippingAddress(_ objectId: String?,completion:@escaping (_ success: Bool) -> Void){
        if let objectId = objectId{
            if let defaultShippingAddressObjectId = CurrentUser.sharedInstance.currentUser?.defaultShippingAddressObjectId, objectId == defaultShippingAddressObjectId{
                // UNSET DEFAULT
                
                self.unsetUserDefaultShippingAddress(objectId, completion: { (success) in
                    if success{
                        completion(true)
                    }else{
                        completion(false)
                    }
                })
                
                // SET DEFAULT
            }else{
                self.setUserDefaultShippingAddress(objectId, completion: { (success) in
                    if success{
                        completion(true)
                    }else{
                        completion(false)
                    }
                })
            }
            
        }
        
    }
    
    func setUserDefaultShippingAddress(_ objectId: String?,completion:@escaping (_ success: Bool) -> Void){
        if let objectId = objectId,let userId = CurrentUser.sharedInstance.currentUser?.objectId{
            let path = "users/\(userId)/shipping/primary/\(objectId)"
            DBHelper.callAPIPut(path, completion: { (result, error) in
                
                if error == nil{
                    Analytics.sharedInstance.actionsArray.append(Action(context:["defaultAddress": objectId], forScreen:nil, withActivityType:"Updated Settings")) // $AS
                    self.defaultShippingAddressObjectId = objectId
                    
                    completion(true)
                }else if let _ = error{
//                    if error.code != -1009 &&  error.code != -1001 {
                        ErrorHandling.unexpectedErrorHandler()
//                    }
                    completion(false)
                }
            })
        }else{
            
        }
    }
    
    
    func unsetUserDefaultShippingAddress(_ objectId: String?,completion:@escaping (_ success: Bool) -> Void){
        if let objectId = objectId,let userId = CurrentUser.sharedInstance.currentUser?.objectId{
            let path = "users/\(userId)/shipping/primary/\(objectId)"
            DBHelper.callAPIDelete(path, completion: { (result, error) in
                if error == nil{
                    self.defaultShippingAddressObjectId = nil
                    completion(true)
                }else if let _ = error{
//                    if error.code != -1009 &&  error.code != -1001 {
                        ErrorHandling.unexpectedErrorHandler()
//                    }
                    completion(false)
                }
            })
        }else{
            
        }
    }
    
    
    
    // MARK: -  //////////////////////////////////////////////////////////    CC    ///////////////////////////////////////////////////////////////////////////
    
    
    func getDefaultPaymentMethod() -> CC? {
        
        if let cc = self.cc, let defaultPaymentMethodObjectId = self.defaultPaymentMethodObjectId {
            for paymentMethod in cc{
                if let objectId = paymentMethod.objectId, objectId == defaultPaymentMethodObjectId {
                    return paymentMethod
                }
            }
        }
        return nil
    }
    
    
    func getUserCC(_ initialLoad:Bool,skip:Int?,limit:Int?,completion:@escaping (_ success: Bool) -> Void){
        if let skip = skip, let limit = limit{
            let queryString = "{\"sort\":\"-createdAt\",\"skip\":\(skip),\"limit\":\(limit)}"
            DBHelper.callAPIGet("paymentMethods", queryString: queryString, completion: { (result, error) in
                if let result = result {
                    
                    if let paymentMethods = result["paymentMethods"] as? NSArray{
                        if initialLoad{
                            self.cc = [CC]()
                        }
                        
                        for value in paymentMethods{
                            if let paymentMethod = value as? Dictionary<String, AnyObject> {
                                let cc = CC(input:paymentMethod)
                                self.cc?.append(cc)
                            }
                        }
                        
                        if self.getPaymentMethodCount() > 0{
                            completion(true)
                        }else{
                            completion(false)
                        }
                    }
                } else if error != nil{
                    
                    completion(false)
                }
            })
        }
    }
    
    
    func postUserCC(_ setDefaultPaymentMethod:Bool?,token:STPToken?,name:String?,completion:@escaping (_ cc:CC?) -> Void){
        if let token = token{
            //            let path =  "stripe/cards"
            let path =  "paymentMethods"
            var params:[String:AnyObject] = ["token": token.tokenId as AnyObject]
            if let name = name{
                params = ["token": token.tokenId as AnyObject,"name":name as AnyObject]
            }
            
            
            
            DBHelper.callAPIPost(path, params: params, completion: { (result, error) in
                if error == nil{
                    if let result = result?["paymentMethod"] as? Dictionary<String, AnyObject>{
                        
                        let cc = CC(input: result)
                        
                        self.cc?.append(cc)
                        
                        if setDefaultPaymentMethod == true {
                            self.setUserDefaultPaymentMethod(cc.objectId, completion: { (result) in
                                if result == true{
                                    completion(cc )
                                } else{
                                    completion(nil)
                                }
                            })
                        } else {
                            completion(cc )
                        }
                        
                    }
                } else {
//                    DropDownAlert.customErrorMessage("Error saving card!")
                    ErrorHandling.customErrorHandler(message: "Error saving card!")
                    completion(nil)
                }
            })
        }else{
            completion(nil)
            
        }
    }
    
    
    // MARK: -  ----------    DEFAULT PAYMENT   -------------
    
    func toggleUserDefaultPaymentMethod(_ objectId: String?,completion:@escaping (_ success: Bool) -> Void){
        if let objectId = objectId{
            if let defaultPaymentMethodObjectId = CurrentUser.sharedInstance.currentUser?.defaultPaymentMethodObjectId, objectId != defaultPaymentMethodObjectId{
                // SET DEFAULT
                
                self.setUserDefaultPaymentMethod(objectId, completion: { (success) in
                    if success{
                        completion(true)
                    }else{
                        completion(false)
                    }
                })
            }
            
        }
        
    }
    
    func setUserDefaultPaymentMethod(_ objectId: String?,completion:@escaping (_ success: Bool) -> Void){
        if let objectId = objectId,let userId = CurrentUser.sharedInstance.currentUser?.objectId{
            let path = "users/\(userId)/payment/primary/\(objectId)"
            DBHelper.callAPIPut(path, completion: { (result, error) in
                
                if error == nil{
                    self.defaultPaymentMethodObjectId = objectId
                    completion(true)
                }else if let _ = error{
//                    if error.code != -1009 &&  error.code != -1001 {
                        ErrorHandling.unexpectedErrorHandler()
//                    }
                    completion(false)
                }
            })
        }else{
            
        }
    }
    
    
    // MARK: -  ----------    DELETE PAYMENT   -------------
    
    func deletePaymentMethod(_ objectId: String?, completion:@escaping (_ success: Bool) -> Void){
        if let id = objectId {
            DBHelper.callAPIDelete("paymentMethods/\(id)", completion: { (result, error) in
                if error == nil {
                    Analytics.sharedInstance.actionsArray.append(Action(context:["deletePaymentMetod": id], forScreen:nil, withActivityType:"Delete Payment Method"))
                    self.deletePaymentMethodFromUserArray(objectId)
                    completion(true)
                } else {
                    completion(false)
                }
            })
        }
    }
    
    func deletePaymentMethodFromUserArray(_ objectId:String?){
        if let paymentMethods = self.cc{
            for (index,currentPaymentMethod) in paymentMethods.enumerated(){
                if currentPaymentMethod.objectId == objectId{
                    cc?.remove(at: index)
                }
            }
            checkAndDeleteDefaultPaymentMethod(objectId)
        }
        
    }
    
    
    func checkAndDeleteDefaultPaymentMethod(_ objectId:String?){
        if let objectId = objectId, let defaultPaymentMethodObjectId = CurrentUser.sharedInstance.currentUser?.defaultPaymentMethodObjectId, objectId == defaultPaymentMethodObjectId{
            CurrentUser.sharedInstance.currentUser?.defaultPaymentMethodObjectId = nil
        }
    }
    
    
    // MARK: -  /////////////////////////////////////////////////////    GET COUNTS    /////////////////////////////////////////////////////////////////////////
    
    // returns the followers count for Table view controller
    func getFollowersCount() -> Int{
        return self.followersCount ?? 0
    }
    
    // returns the following count for Table view controller
    func getFollowingCount() -> Int{
        return self.followingCount ?? 0
    }
    
    // returns the product count for Collection view controller
    func getLikesCount()-> Int{
        return self.likesCount ?? 0
    }
    
    // returns the collection count for Collection view controller
    func getCollectionsCount()-> Int{
        return self.collections?.count ?? 0
    }
    
    // returns the shipping addresses count
    func getShippingAddresesCount()-> Int {
        return self.shipping?.count ?? 0
    }
    
    // returns the shipping addresses count
    func getPaymentMethodCount()-> Int {
        return self.cc?.count ?? 0
    }
    
    func isCurrentUser()->Bool{
        if let currentUserObjectId = CurrentUser.sharedInstance.currentUser?.objectId,self.objectId == currentUserObjectId{
            return true
        }else{
            return false
        }
    }
}

//            if let currentUser = CurrentUser.sharedInstance.currentUser, let currentUserObjectId = currentUser.objectId,objectId == currentUserObjectId{
//                self.isCurrentUser = true
//            }

//    var isCurrentUser = false

//    var isUserFollowedByCurrentUser = false

//    var isFollowedByCurrentUser:Bool?
//    var isRequestedByCurrentUser:Bool? // Current user

//    var emailVerified: Bool?
//    var phoneNumber: String?
//    var password: String?
//    var selectedIndex = 0
//    var facebookId:String?
//    var twitterId:String?
//    var userImageView:UIImageView = UIImageView()

// MARK: -   -----------------------------------------------------      Others    -----------------------------------------------------

//    var updatedAt: Date?
//    var createdAt: Date?



// MARK: -   -----------------------------------------------------      OTHERS     -----------------------------------------------------

//    var isWaitlisted:Bool?

//    var followingId: [String]?
//    var followersId: [String]?

//    var following:[User]? = [User]()
//    var requestId: [String]?

// MARK: -   -----------------------------------------------------      PRODUCTS     -----------------------------------------------------

//    var likesId: [String]?
//    var products:[Product]? = [Product]()

//    var repostIds:[String] = [String]()



//
//            if let isRequestedByCurrentUser = input["isRequestedByCurrentUser"] as? Bool {
//                self.isRequestedByCurrentUser = isRequestedByCurrentUser
//            } else {
//                self.isRequestedByCurrentUser = false
//            }
//
//            if let isFollowedByCurrentUser = input["isFollowedByCurrentUser"] as? Bool {
//                self.isFollowedByCurrentUser = isFollowedByCurrentUser
//            } else {
//                self.isFollowedByCurrentUser = false
//            }



//            if let isWaitlisted = input["isWaitlisted"] as? Bool{
//                self.isWaitlisted = isWaitlisted
//            }

//            if let createdAt = input["createdAt"] as? Date{
//                self.createdAt = createdAt
//            }
//
//            if let updatedAt = input["updatedAt"] as? Date{
//                self.updatedAt = updatedAt
//            }

//            if let emailVerified = input["emailVerified"] as? Bool{
//                self.emailVerified = emailVerified
//            }
//
//
//            if let phoneNumber = input["phone"] as? String{
//                self.phoneNumber = phoneNumber
//            }



//            if let following = input["following"] as? [String]{
//                self.followingId = following
//            }
//
//            if let followers = input["followers"] as? [String]{
//                self.followersId = followers
//            }

//            if let requestId = input["requests"] as? [String]{ // check
//                self.requestId = requestId
//                self.userAlreadyRequestedFollow = false
//                if let currentRequests = CurrentUser.sharedInstance.currentUser?.requestId{
//                    for object in currentRequests{
//                        if let objectId = self.objectId, object == objectId{
//                            self.userAlreadyRequestedFollow = true
//                        }
//                    }
//
//                } else {
//                    self.userAlreadyRequestedFollow = false
//                }
//            } else {
//                self.userAlreadyRequestedFollow = false
//            }



//            if let facebookId = input["facebookId"] as? String{
//                self.facebookId = facebookId
//            }
//
//            if let twitterId = input["twitterId"] as? String{
//                self.twitterId = twitterId
//            }
//


//            if let repostIds = input["reposts"]?["products"] as? [String]{
//                self.repostIds = repostIds
//            }

//            if let likes = input["likes"], let products = likes["products"] as? [String]{
//                self.likesId = products
//            }



//            self.findIfCurrentUserIsFollowingThisUser()





// MARK: -  /////////////////////////////////////////////////////    USER ACTIONS    /////////////////////////////////////////////////////////////////////////


// CHECK IF THE CURRENT USER IS FOLLOWING THIS USER

//    func findIfCurrentUserIsFollowingThisUser(){
//        if let currentUserFollowings = CurrentUser.sharedInstance.currentUser?.followingId{
//            for value in currentUserFollowings{
//                if self.objectId == value{
//                    self.isUserFollowedByCurrentUser = true
//                    break
//                }
//            }
//        }
//    }
//


// UNFOLLOW THIS IS USER BY CURRENT USER

//    func unFollowUser(_ completion:@escaping (_ success: Bool) -> Void){
//        if let userToUnFollowId = self.objectId,let userId = CurrentUser.sharedInstance.currentUser?.objectId{
//
//            let path = "users/\(userId)/following/\(userToUnFollowId)"
//
//            DBHelper.callAPIDelete(path, completion: { (result, error) in
//                if error == nil {
//
//                    Analytics.sharedInstance.actionsArray.append(Action(context: ["unfollowedUser":userToUnFollowId], forScreen: nil, withActivityType:"Unfollow"))
//
//                    CurrentUser.sharedInstance.currentUser?.updateUser({ (success) in
//                        self.isUserFollowedByCurrentUser = false
//                        self.userAlreadyRequestedFollow = false
//                        completion(true)
//                    })
//                } else {
//
//                }
//            })
//        }else{
//
//        }
//    }
//
//    // FOLLOW THIS USER BY CURRENT USER
//
//    func followUser(_ completion:@escaping (_ success: Bool) -> Void){
//        if let userToFollowId = self.objectId,let userId = CurrentUser.sharedInstance.currentUser?.objectId{
//
//            let path = "users/\(userId)/following/\(userToFollowId)"
//
//            DBHelper.callAPIPost(path, completion: { (result, error) in
//                if error == nil {
//
//                    Analytics.sharedInstance.actionsArray.append(Action(context: ["followedUser":userToFollowId], forScreen: nil, withActivityType:"Follow"))
//
//                    if self.isProfilePrivate == true && self.userAlreadyRequestedFollow == false{
//                        self.userAlreadyRequestedFollow = true
//                    } else {
//                        self.isUserFollowedByCurrentUser = true
//                    }
//                    CurrentUser.sharedInstance.currentUser?.updateUser({ (success) in
//                        completion(true)
//                    })
//
//                } else {
//
//                }
//            })
//        }else{
//
//        }
//    }

// MARK: -  /////////////////////////////////////////////////////    GET USER DATA    /////////////////////////////////////////////////////////////////////////

// GET LIKES FOR THIS USER
//
//    func getProductLikes(_ initialLoad:Bool,skip:Int?,limit:Int?,completion:@escaping (_ success: Bool, _ count: Int) -> Void){
//        if let objectId = self.objectId,let skip = skip, let limit = limit{
//
//            let path = "products"
//            let queryString = "{\"where\":{\"likes\":\"\(objectId)\"},\"sort\":{\"createdAt\":-1},\"skip\":\(skip),\"limit\":\(limit)}"
//
//            DBHelper.callAPIGet(path,queryString: queryString, completion: { (result, error) in
//                if error == nil {
//                    if let products = result?["products"] as? NSArray{
//                        if initialLoad{
//                            self.products = [Product]()
//                        }
//                        for values in products{
//
//                            if let product = values as? NSDictionary {
//                                if let newProduct = Product(input:(product) as! Dictionary<String, AnyObject>){
//                                    self.products?.append(newProduct)
//                                }
//                            }
//                        }
//                        completion(true, self.getLikesCount())
//                    }
//                } else {
//                    completion(false, 0)
//                }
//            })
//        }
//    }

// GET USER FOLLOWERS
//
//    func getUserFollowers(_ initialLoad:Bool,skip:Int?,limit:Int?,completion:@escaping (_ success: Bool, _ count: Int) -> Void){
//        if let objectId = self.objectId,let skip = skip, let limit = limit{
//
//            let path = "users"
//            let queryString = "{\"where\":{\"following\":\"\(objectId)\"},\"skip\":\(skip),\"limit\":\(limit)}"
//
//            DBHelper.callAPIGet(path, queryString: queryString, completion: { (result, error) in
//
//                if let _ = result{
//                }
//                if error == nil {
//                    if let users = result?["users"] as? NSArray{
//
//                        if initialLoad{
//                            self.followers = [User]()
//                        }
//
//                        for values in users{
//
//                            if let user = values as? NSDictionary {
//                                let newUser = User(input:(user) as! Dictionary<String, AnyObject>)
//                                self.followers?.append(newUser)
//                            }
//                        }
//                        completion(true, self.getFollowersCount())
//                    }
//                } else {
//                    completion(false, 0)
//                }
//            })
//        }
//    }

// GET USER FOLLOWING

//    func getUserFollowings(_ initialLoad:Bool,skip:Int?,limit:Int?,completion:@escaping (_ success: Bool, _ count: Int) -> Void){
//        if let objectId = self.objectId,let skip = skip, let limit = limit{
//
//            let path = "users"
//            let queryString = "{\"where\":{\"followers\":\"\(objectId)\"},\"skip\":\(skip),\"limit\":\(limit)}"
//
//            DBHelper.callAPIGet(path, queryString: queryString, completion: { (result, error) in
//                if error == nil {
//                    if let users = result?["users"] as? NSArray{
//
//                        if initialLoad{
//                            self.following = [User]()
//                        }
//
//                        for values in users{
//
//                            if let user = values as? NSDictionary {
//                                let newUser = User(input:(user) as! Dictionary<String, AnyObject>)
//                                self.following?.append(newUser)
//                            }
//                        }
//
//                        completion(true, self.getFollowingCount())
//                    }
//                } else {
//                    completion(false, 0)
//                }
//            })
//        }
//    }
