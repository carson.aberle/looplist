//
//  Collection.swift
//  Looplist
//
//  Created by Carson Aberle on 8/16/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class Collection{
    
    // MARK: -  --------------------------------------------------------   VARIABLES  -----------------------------------------------------------------------
    
    var users:[String]?
    var objectId:String?
    var name:String?
    var description:String?
    var products:[Product]?
    var collectionItemsCount:String{
        get{
            if collectionItemsCountInt == 1{
                return "\(collectionItemsCountInt) item"
            }else if collectionItemsCountInt > 1{
                return "\(collectionItemsCountInt) items"
                
            }else{
                return "0 items"
            }
        }
    }
    var owner:User?
    var isPrivate:Bool?
    var containsProduct:Bool?
    var isLikesCollection:Bool?
    var isAddedProductCollection:Bool?
    var collectionItemsCountInt:Int
    
    // MARK: -  //////////////////////////////////////////////////////   INITIALIZER   /////////////////////////////////////////////////////////////////////////
    
    init(inputProductImagesOnly:Dictionary<String,AnyObject>){
        
        
        if let objectId:String = inputProductImagesOnly["_id"] as? String{
            self.objectId = objectId
            
        }
        if let users:[String] = inputProductImagesOnly["users"] as? [String] {
            self.users = users
        }
        if let name:String = inputProductImagesOnly["name"] as? String {
            self.name = name
        }
        
        if let description:String = inputProductImagesOnly["description"] as? String {
            self.description = description
        }
        if let isPrivate:Bool = inputProductImagesOnly["isPrivate"] as? Bool {
            self.isPrivate = isPrivate
        }
        
        if let isLikesCollection:Bool = inputProductImagesOnly["isLikesCollection"] as? Bool{
            self.isLikesCollection = isLikesCollection
        } else {
            self.isLikesCollection = false
        }
        
        if let isAddedProductCollection:Bool = inputProductImagesOnly["isAddedProductCollection"] as? Bool{
            self.isAddedProductCollection = isAddedProductCollection
        } else {
            self.isAddedProductCollection = false
        }
        
        if let productsDictionary:NSArray = inputProductImagesOnly["products"] as? NSArray{
            var productsArray:[Product] = [Product]()
            for product in productsDictionary{
                if let product = product as? Dictionary<String, AnyObject>{
                    if let product = Product(inputImageOnly: product){
                        productsArray.append(product)
                    }
                }
            }
//            productsArray.reverse()
            self.products = productsArray
            
//            if let count = self.products?.count{
//                if count == 1{
//                    collectionItemsCount = "\(count) item"
//                }else{
//                    collectionItemsCount = "\(count) items"
//                }
//            }
            
        } else {
            self.products = [Product]()
//            self.collectionItemsCount = "0 items"
        }
        
        if let containsProduct:Bool = inputProductImagesOnly["containsProduct"] as? Bool{
            self.containsProduct = containsProduct
        } else {
            self.containsProduct = false
        }
        
        if let ownerDictionary = inputProductImagesOnly["owner"] as? Dictionary<String, AnyObject>{
            self.owner = User(input: ownerDictionary)
        }
        
//        
//        if let count = inputProductImagesOnly["productsCount"] as? Int{
//            if count == 1{
//                collectionItemsCount = "\(count) item"
//            }else{
//                collectionItemsCount = "\(count) items"
//            }
//        }else{
//            self.collectionItemsCount = "0 items"
//        }
        
        if let count = inputProductImagesOnly["productsCount"] as? Int{
            self.collectionItemsCountInt = count
        }else{
            self.collectionItemsCountInt = 0
        }
        
    }
    
    init(input:Dictionary<String,AnyObject>){
        if let objectId:String = input["_id"] as? String{
            self.objectId = objectId
        }
        if let users:[String] = input["users"] as? [String] {
            self.users = users
        }
        if let name:String = input["name"] as? String {
            self.name = name
        }
        
        if let isLikesCollection:Bool = input["isLikesCollection"] as? Bool{
            self.isLikesCollection = isLikesCollection
        } else {
            self.isLikesCollection = false
        }
        
        if let isAddedProductCollection:Bool = input["isAddedProductCollection"] as? Bool{
            self.isAddedProductCollection = isAddedProductCollection
        } else {
            self.isAddedProductCollection = false
        }
        
        if let containsProduct:Bool = input["flags"]?["containsProduct"] as? Bool{
            self.containsProduct = containsProduct
        } else {
            self.containsProduct = false
        }
        
        if let description:String = input["description"] as? String {
            self.description = description
        }
        if let isPrivate:Bool = input["isPrivate"] as? Bool {
            self.isPrivate = isPrivate
        }
        if let productsDictionary:NSArray = input["products"] as? NSArray{
            var productsArray:[Product] = [Product]()
            for product in productsDictionary{
                if let product = product as? Dictionary<String, AnyObject>{
                    if let product = Product(input: product){
                        productsArray.append(product)
                    }
                }
            }
            self.products = productsArray
            
//            let count = productsDictionary.count
//            if count == 1{
//                collectionItemsCount = "\(count) item"
//            }else{
//                collectionItemsCount = "\(count) items"
//            }
            
        } else {
            self.products = [Product]()
//            self.collectionItemsCount = "0 items"
        }
        
        if let ownerDictionary = input["owner"] as?  Dictionary<String, AnyObject>{
            self.owner = User(input: ownerDictionary)
        }
        
        if let count = input["productsCount"] as? Int{
            self.collectionItemsCountInt = count
        }else{
            self.collectionItemsCountInt = 0
        }
//            if count == 1{
//                collectionItemsCount = "\(count) item"
//            }else{
//                collectionItemsCount = "\(count) items"
//            }
//        }else{
//            self.collectionItemsCount = "0 items"
//        }
    }
    
   
    
    func getCollectionItemsCount() -> String{
        
        return collectionItemsCount
    }
}
