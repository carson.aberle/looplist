//
//  Transactions.swift
//  Looplist
//
//  Created by Roman Wendelboe on 6/5/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct Transaction {
    
    // MARK: -  --------------------------------------------------------   VARIABLES  -----------------------------------------------------------------------

    let objectId:String?
    let payment:Payment?
    let shipping:Shipping?
    let listing: Listing?
//    var listing:String?
//    let email:String?
    let createdAt:Date?
    
    let method:String? // for transaction
    
    // MARK: -  //////////////////////////////////////////////////////   INITILIZERS   /////////////////////////////////////////////////////////////////////////

    // EMPTY INITILIZER
//    init(){}
    
    // INITIALIZER WITH PARAMETERS
    
//    init(payment:Payment?,shipping:Shipping?,listing:String?){
//        if let user = CurrentUser.sharedInstance.currentUser?.objectId, let payment = payment, let shipping = shipping,let listing = listing, let email = CurrentUser.sharedInstance.currentUser?.email{
//            
//            self.user = user
//            self.payment = payment
//            self.shipping = shipping
//            self.listing = listing
//            self.email = email
//        }
//    }
    
    
//    init?(objectId:String?,payment:Payment?,shipping:Shipping?,listing:Listing?,method:Int?){
//        if let objectId = objectId,let payment = payment, let shipping = shipping,let listing = listing,let method = method{
//            
//            self.objectId = objectId
//            self.payment = payment
//            self.shipping = shipping
//            self.listing = listing
//          
//        }else{
//            return nil
//        }
//    }
    
    // INIT from Backend
    
    init?(input:[String:AnyObject]){
        
        if let objectId = input["_id"] as? String{
            self.objectId = objectId
        } else {
            return nil
        }
        
        if let payment = input["payment"], let method = payment["method"] as? String{
            let paymentObject = Payment(input:payment as! NSDictionary)
            //paymentObject.listingId = self.listing!
            self.payment = paymentObject
            self.method = method
        }else{
             return nil
        }
        
        
        if let shipping = input["shipping"] as? Dictionary<String, AnyObject>{
            self.shipping = Shipping(input: shipping )
        }else{
             return nil
        }

        if let listing = input["listing"] as? Dictionary<String, AnyObject>{
            self.listing = Listing(input: listing)
        }else{
            return nil
        }
        
        if let date = input["createdAt"] as? String{
            self.createdAt = HelperDate.formatDataBaseDate(date)
        }else {
            self.createdAt = nil
        }
    }
}
