//
//  Payment.swift
//  Looplist
//
//  Created by Arun Sivakumar on 7/26/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation

struct Payment{
    
    // MARK: -  --------------------------------------------------------   VARIABLES  -----------------------------------------------------------------------

    var subtotal:String?
    var service:String?
    var shipping :String?
    var tax :String?
    var total:String?
    
    var method:String? // for transaction
    // MARK: -  //////////////////////////////////////////////////////   INITIALIZER   /////////////////////////////////////////////////////////////////////////

    init?(input:NSDictionary){
        if let subtotal = input["subtotal"] as? Int,let service = input["service"] as? Int,let shipping = input["shipping"] as? Int,let tax = input["tax"] as? Int,let total = input["total"] as? Int{ //### change to integer
            self.subtotal = HelperPrice.convertPricefromIntToString(subtotal)
            self.service = HelperPrice.convertPricefromIntToString(service)
            self.shipping = HelperPrice.convertPricefromIntToString(shipping)
            self.tax = HelperPrice.convertPricefromIntToString(tax)
            self.total  = HelperPrice.convertPricefromIntToString(total)
        
        }else{
            return nil
        }
        
//        if let method = input["method"] as? String{
//            self.method = method
//        }
    }
}
