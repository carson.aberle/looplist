//
//  Shipping.swift
//  Looplist
//
//  Created by Arun Sivakumar on 7/26/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class Shipping{
    
    // MARK: -  --------------------------------------------------------   VARIABLES  -----------------------------------------------------------------------
    
    var objectId:String?
    var name :String?
    var address1:String?
    var city :String?
    var state :String?
    var zip:String?
    var phone:String?
    var address2:String? // Optional
    var instructions:String? // Optional
    
    
    var cityStateZip:String?{
        get{
            if let city = self.city, let state = self.state, let zip = self.zip{
                return "\(city), \(state) \(zip)"
            }else{
                return ""
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   INITILIZERS   /////////////////////////////////////////////////////////////////////////
    
    // DEFAULT INIT
    init(name:String,address1:String,address2:String?,city:String,state:String,zip:String,phone:String,instructions:String?){
        self.name = name
        self.address1 = address1
        self.city = city
        self.state = state
        self.zip = zip
        self.phone = phone
        
        if let address2 = address2{
            self.address2 = address2
        }
        
        if let instructions = instructions {
            self.instructions = instructions
        }
    }
    
    // CURRENT USER DEFAULTS INIT
    init(input:Dictionary<String,AnyObject>){
        assignShippingAddresss(input)
        
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    // UPDATE ADDRESS
    func updateShippingAddress(_ name:String,address1:String,address2:String?,city:String,state:String,zip:String,phone:String,instructions:String?){
        
        self.name = name
        self.address1 = address1
        self.city = city
        self.state = state
        self.zip = zip
        self.phone = phone
        
        if let address2 = address2{
            self.address2 = address2
        }
        if let instructions = instructions {
            self.instructions = instructions
        }
    }
    func assignShippingAddresss(_ input:Dictionary<String,AnyObject>){
        if let name = input["name"] as? String ,let address1 = input["address1"] as? String  ,let city = input["city"] as? String ,let state = input["state"] as? String ,let zip = input["zip"] as? String ,let phone = input["phone"] as? String{
            self.name = name
            self.address1 = address1
            self.city = city
            self.state = state
            self.zip = zip
            self.phone = phone
        }
        
        if let id = input["_id"] as? String{
            self.objectId = id
        }
        
        if let address2 = input["address2"] as? String{
            self.address2 = address2
        }
        if let instructions = input["instructions"] as? String{
            self.instructions = instructions
        }
    }
    
//    func toObject() -> Dictionary<String,AnyObject>{
//        
//        var shipping:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
//        
//        // object id not required because of the API
//        shipping["name"] = self.name as AnyObject?
//        shipping["address1"] = self.address1 as AnyObject?
//        shipping["city"] = self.city as AnyObject?
//        shipping["state"] = self.state as AnyObject?
//        shipping["zip"] = self.zip as AnyObject?
//        shipping["phone"] = self.phone as AnyObject?
//        
//        if let address2 = self.address2 {
//            shipping["address2"] = address2 as AnyObject?
//        }
//        
//        if let instructions = self.instructions {
//            shipping["instructions"] = instructions as AnyObject?
//        }
//        return shipping
//    }
}
