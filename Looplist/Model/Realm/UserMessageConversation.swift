//
//  UserMessageConversation.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/20/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import Foundation
import RealmSwift

class UserMessageConversation:Object{
    var user:User?
    var userLastMessage:String?
    var chatId:String?
    var date:Date?
    var read:Bool?
    
    override class func primaryKey() -> String? { // set so that data can be overridden
        return "chatId"
    }
}
