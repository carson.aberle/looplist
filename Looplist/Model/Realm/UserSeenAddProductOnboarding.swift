//
//  UserSeenAddProductOnboarding.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/12/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import Foundation


import RealmSwift


class UserSeenAddProductOnboarding:Object{
    dynamic var seen:String = "seen"
    dynamic var onBoardingSeen:Bool = false
    dynamic var modificationDate = NSDate()
    
    override class func primaryKey() -> String? { // set so that data can be overridden
        return "seen"
    }
}
