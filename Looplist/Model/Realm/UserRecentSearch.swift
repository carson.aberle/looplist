//
//  SearchRecent.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/12/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import RealmSwift


class UserRecentSearch:Object{
    dynamic var searchTerm = ""// Recent Search
    dynamic var searchTermCount = 0 // Recent Search Count
    dynamic var modificationDate = NSDate()
    
    override class func primaryKey() -> String? { // set so that data can be overridden
        return "searchTerm"
    }
    
}   
