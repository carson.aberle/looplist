//
//  Interest.swift
//  Looplist
//
//  Created by Carson Aberle on 1/28/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import Foundation
import SwiftyJSON

class Interest{
    
    // MARK: -  --------------------------------------------------------   MANDATORY  -----------------------------------------------------------------------

    var objectId:String?
    var name:String?
    var siblings:[Interest] = [Interest]()
    
    var siblingsId:[String] = [String]()
    var isActive:Bool?
    
    var childrenIds:[String] = [String]()
    var children:[Interest] = [Interest]()
    
    var isInUsersInterests:Bool = false
    
    // MARK: -  //////////////////////////////////////////////////////   INITILIZER   /////////////////////////////////////////////////////////////////////////
    
    init?(input:Dictionary<String,AnyObject>){
        if let objectId = input["_id"] as? String{
            self.objectId = objectId
        } else {
            return nil
        }
        
        if let name = input["name"] as? String{
            self.name = name
        } else {
            return nil
        }
        
        if let isActive = input["isActive"] as? Bool{
            self.isActive = isActive
        }
        
        if let siblingsArray = input["siblings"] as? NSArray{
            for siblingElement in siblingsArray{
                if let siblingDictionary = siblingElement as? Dictionary<String, AnyObject>{
                    if let sibling = Interest(input: siblingDictionary){
                        self.siblings.append(sibling)
                    }
                }
                if let siblingId = siblingElement as? String{
                    self.siblingsId.append(siblingId)
                }
            }
        }
        
        if let childrenArray = input["children"] as? NSArray{
            for childElement in childrenArray{
                if let childDictionary = childElement as? Dictionary<String, AnyObject>{
                    if let child = Interest(input: childDictionary){
                        self.children.append(child)
                    }
                }
                if let childId = childElement as? String{
                    self.childrenIds.append(childId)
                }
            }
        }
        
        if let flags = input["flags"] as? NSDictionary, let isInUsersInterests = flags["isInUsersInterests"] as? Bool{
            self.isInUsersInterests = isInUsersInterests
        }

    }

}
