//
//  Action.swift
//  Looplist
//
//  Created by Carson Aberle on 9/21/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation

class Action: NSObject{
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////

    var timestamp:String?
    var duration:Double?
    var context:NSDictionary?
    var actionType:String?
    var source:String?
    var productId:String?
    var version:String?
    
    // MARK: -  //////////////////////////////////////////////////////   INITIALIZERS   /////////////////////////////////////////////////////////////////////////

    //SCREEN TIME INITIALIZER
    internal init(timeOnScreen:Double, withParameters:NSMutableDictionary, andActionType:String){
        super.init()
        self.timestamp = String(Date().timeIntervalSince1970 * 1000)
        self.duration = timeOnScreen
        self.source = "iOS"
        
        self.context = withParameters
        
        self.actionType = andActionType
        let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
        self.version = nsObject as? String
    }
    
    //ACTION INITIALIZER
    internal init(context:NSDictionary, forScreen:String?, withActivityType:String){
        super.init()
        self.timestamp = String(Date().timeIntervalSince1970 * 1000)
        self.context = context
        self.source = "iOS"
        self.actionType = withActivityType
        let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
        self.version = nsObject as? String
    }
    
    //HOMEFEED PRODUCT TIME INITIALIZER
    internal init(timeOnProduct:Double, productId:String, fromScreen:String){
        super.init()
        self.timestamp = String(Date().timeIntervalSince1970 * 1000)
        self.source = "iOS"
        self.duration = timeOnProduct
        self.productId = productId
        self.actionType = "Saw Product"
        self.context = ["TechnicalName":fromScreen, "ScreenName":AnalyticsConstants.getViewControllerAnalyticsName(fromScreen)]
        let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
        self.version = nsObject as? String
    }
}
