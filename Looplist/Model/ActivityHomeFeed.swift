//
//  Activity.swift
//  Looplist
//
//  Created by Carson Aberle on 8/25/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation

class ActivityHomeFeed{ // FINAL
    
    // MARK: -  --------------------------------------------------------   VARIABLES  -----------------------------------------------------------------------
    
    var activityTypeHomeFeed:ActivityTypeHomeFeed?
    var users:[User]?
    var product:Product?
    var date:Date?
    var read:Bool?
    var objectId:String?
    var attributes:NSDictionary?
    
    // MARK: -  //////////////////////////////////////////////////////   INITILIZER   /////////////////////////////////////////////////////////////////////////
    
    init?(input:Dictionary<String,AnyObject>){
        
        
//        guard let objectId = input["_id"] as? String,
//            let activityTypeString = input["activityType"]as? String,let activityType = ActivityTypeHomeFeed(rawValue: activityTypeString),
//            let date = input["createdAt"] as? String,
//            let read = input["read"] as? Bool
//            else {
//                return nil
//        }
        
//        print(input)
        if  let date = input["createdAt"] as? String{
            self.date = HelperDate.activityStringToDate(date)
        }
        
        guard let activityTypeString = input["activityType"]as? String,let activityTypeHomeFeed = ActivityTypeHomeFeed(rawValue: activityTypeString)
        else {
                return nil
        }
        
        if let productAttributes = input["product"] as? Dictionary<String, AnyObject>{
            if let product = Product(input: productAttributes){
//                if let isLikedByCurrentUser = input["isLikedByUser"] as? Bool, let isRepostedByCurrentUser = input["isRepostedByUser"] as? Bool{
//                    product.setFlags(isLikedByCurrentUser: isLikedByCurrentUser, isRepostedByCurrentUser: isRepostedByCurrentUser)
//                    
//                }
                self.product = product

            }else{
                return nil
            }
            
        }
        
        if let usersFromActivity = input["users"] as? NSArray{
            var users = [User]()
            for userFromActivity in usersFromActivity{
                if let userFromActivity =  userFromActivity as? Dictionary<String, AnyObject>{
                    let user = User(input:userFromActivity)
                    users.append(user)
                }
            }
            if users.count > 0{
                self.users = users
            }
        }
    
//        self.objectId = objectId
        self.activityTypeHomeFeed = activityTypeHomeFeed
     
//        self.read = read
        
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    func getUser()->User?{ // returns the first user in the user array
        if let users = self.users, users.count > 0{
            return users[0] // implement bounds checkng
        }else{
            return nil
        }
    }
    
    func getUserDescription()->String{
        if let users = self.users,let firstName = users[0].firstName{
            let count = users.count-1
            
            if count > 1{
                return firstName + " +\(count) others"
            }else if count == 1{
                return firstName + " +\(count) other"
            }else if count == 0{
                return firstName
            }
        }
        return ""
    }
    
}
