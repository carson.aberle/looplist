//
//  EmptyDataSet.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/23/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import Foundation


enum EmptyDataSet{
    case Loading
    case Search
    case Empty
}
