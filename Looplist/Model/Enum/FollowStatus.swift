//
//  FollowStatus.Swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/5/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import Foundation

enum FollowStatus{
    case follow(Bool)
    case request(Bool)
}

extension FollowStatus:Equatable{
    static func ==(lhs: FollowStatus, rhs: FollowStatus) -> Bool {
        
        switch (lhs, rhs) {
        case (let .request(value1), let .request(value2)):
            if value1 == value2{
                return true
            }else{
                return false
            }
        default:
            return false
        }
        
    }
    
}
