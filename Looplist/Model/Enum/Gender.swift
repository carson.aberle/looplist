//
//  Gender.swift
//  Looplist
//
//  Created by Carson Aberle on 1/16/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import Foundation


enum Gender:String{
    case notApplicable = "N/A"
    case male = "Male"
    case female = "Female"
}
