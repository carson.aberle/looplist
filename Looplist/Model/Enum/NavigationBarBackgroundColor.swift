//
//  NavigationBarBackgroundColor.swift
//  Looplist
//
//  Created by Carson Aberle on 2/6/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

enum NavigationBarBackgroundColor:String{
    case clear = "Clear"
    case clearNoGradient = "Clear No Gradient"
    case whiteTransparent = "White"
    case whiteOpaque = "Transparent White"
}
