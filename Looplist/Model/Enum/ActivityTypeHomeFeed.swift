//
//  ActivityType.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/10/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

//
//switch self{
//case .follow:
//case .comment:
//case .likeRepost:
//case .likeCollection:
//case .likeProduct:
//case .repostProduct:
//case .repostCollection:
//case .followRequest:
//}


import Foundation


enum ActivityTypeHomeFeed:String{
//    case likeRepost = "Like Repost"
    case aggLikeProduct = "Agg Like Product"
    case aggRepostProduct = "Agg Repost Product"
}

extension ActivityTypeHomeFeed:Equatable{
    static func ==(lhs: ActivityTypeHomeFeed, rhs: ActivityTypeHomeFeed) -> Bool {
        
        if lhs.rawValue == rhs.rawValue{
            return true
        }else{
            return false
        }
    }
}
