//
//  ExternalAccount.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/10/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import Foundation

enum ExternalAccount:String{
    case facebook = "Facebook"
    case twitter = "Twitter"
    case sms = "SMS"
}
