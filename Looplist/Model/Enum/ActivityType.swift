//
//  ActivityType.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/10/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

//
//switch self{
//case .follow:
//case .comment:
//case .likeRepost:
//case .likeCollection:
//case .likeProduct:
//case .repostProduct:
//case .repostCollection:
//case .followRequest:
//}


import Foundation


enum ActivityType:String{
    
    //1. User profile
    
    //case comment = "Comment"
//    case likeRepost = "Like Repost"
    //case likeCollection = "Like Collection"
    case likeProduct = "Like Product"
    case repostProduct = "Repost Product"
    //case repostCollection = "Repost Collection"
    
    
    // 2.Notifications
    case follow = "Follow" //following
    case followRequest = "Follow Request" //following
    case acceptedFollowRequest = "Accepted Follow Request" //user
    case likedYourRepostedProduct = "Liked your Reposted Product" //associatedUser
    case someoneAddedYourProduct = "Someone added your Product" //associatedUser
    
    
    func getDescription(callingClass:UIViewController)->String{
        if let _ = callingClass as? UserProfileViewController{
            switch self{
//            case .follow:
//                return "STARTED_FOLLOWING".localized
//            case .comment:
//                return "COMMENTED_ON_REPOST_COLLECTION".localized
//            case .likeRepost:
//                return "LIKED_YOUR_REPOST".localized
//            case .likeCollection:
//                return "LIKED_A_COLLECTION".localized
            case .likeProduct:
                return "LIKED_A_PRODUCT".localized

            case .repostProduct:
                return "REPOSTED_A_PRODUCT".localized
            default:
                return ""
//            case .repostCollection:
//                return "REPOSTED_A_COLLECTION".localized
//            case .followRequest:
//                return "REQUESTED_TO_FOLLOW".localized
//                
//            case.acceptedFollowRequest:
//                return ""
//            case .likedYourRepostedProduct:
//                return ""
//            case .someoneAddedYourProduct:
//                return ""
            }
        } else {
            switch self{

//            case .comment:
//                return "COMMENTED_ON_YOUR_REPOST_COLLECTION".localized
//            case .likeRepost:
//                return "LIKED_YOUR_REPOST".localized
//            case .likeCollection:
//                return "LIKED_YOUR_COLLECTION".localized
//            case .likeProduct:
//                return "LIKED_A_PRODUCT_YOU_ADDED".localized
//            case .repostProduct:
//                return "REPOSTED_A_PRODUCT_YOU_ADDED".localized
//            case .repostCollection:
//                return "REPOSTED_YOUR_COLLECTION".localized
            case .follow:
                return "STARTED_FOLLOWING_YOU".localized
            case .followRequest:
                return "REQUESTED_TO_FOLLOW_YOU".localized
            case.acceptedFollowRequest:
                return " accepted your follow request"
            case .likedYourRepostedProduct:
                return " liked the product you reposted"
            case .someoneAddedYourProduct:
                return " added your product to a collection"
            default:
                return ""
            }
        }
        
    }
}

extension ActivityType:Equatable{
    static func ==(lhs: ActivityType, rhs: ActivityType) -> Bool {
        
        if lhs.rawValue == rhs.rawValue{
            return true
        }else{
            return false
        }
    }
}
