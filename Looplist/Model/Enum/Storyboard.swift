//
//  Storyboard.swift
//  Looplist
//
//  Created by Arun Sivakumar on 2/7/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import Foundation


import Foundation

enum Storyboard:String{
    case main = "Main"
    case messaging = "Messaging"
    case search = "Search"
    case loginAndRegistration = "LoginAndRegistration"
    case product = "Product"
    case addProduct = "AddProduct"
    case purchase = "Purchase"
    case settings = "Settings"
    case editProfile = "EditProfile"
    case creditCard = "CreditCard"
    case shipping = "Shipping"
    case collection = "Collection"
}
