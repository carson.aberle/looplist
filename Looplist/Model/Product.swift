//
//  Product.swift
//  Looplist
//
//  Created by Arun on 5/25/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import Foundation
import SwiftyJSON
import HTMLAttributedString

class Product{
    
    // MARK: -  ----------------------------------------------------    MANDATORY   ----------------------------------------------------------------------
    
    
    var objectId:String?
    var images:[String]?
    
    var brandName:String = "" //Computed
    var priceRange:String = "" //Computed
    
    // MARK: -  ----------------------------------------------------    VARIABLES  -----------------------------------------------------------------------
    
    var attributes:Dictionary<String,AnyObject>? // Filters
    var descriptionAttributedString:NSAttributedString?
    var keywords:String = ""
    var productVariations:[Product] = [Product]()
    var likesCount:Int?
    var status:String? // $AS
    
    var isLikedByCurrentUser:Bool?
    var isRepostedByCurrentUser:Bool?
    var isInUsersCollections:Bool?
    //    var activity:Activity?
    //    var currentUsersCollectionsIdsIn:[String] = [String]()
    //    var productImage:UIImage?
    
    
     init?(collectionInput:Dictionary<String,AnyObject>){
        if let objectId = collectionInput["_id"] as? String,let images = collectionInput["images"] as? [String]{
            self.objectId  = objectId
            self.images = images
        }else{
            return nil
        }
        
    }
        //
    
    init?(inputImageOnly:Dictionary<String,AnyObject>){
        if let objectId = inputImageOnly["_id"] as? String, let images = inputImageOnly["images"] as? [String]{
            
            self.objectId  = objectId
            self.images = images
            
            if let isLikedByCurrentUser = inputImageOnly["flags"]?["isLikedByUser"] as? Bool{
                self.isLikedByCurrentUser = isLikedByCurrentUser
            } else {
                self.isLikedByCurrentUser = false
            }
            
            if let isRepostedByCurrentUser = inputImageOnly["flags"]?["isRepostedByUser"] as? Bool{
                self.isRepostedByCurrentUser = isRepostedByCurrentUser
            } else {
                self.isRepostedByCurrentUser = false
            }
            
            if let isInUsersCollections = inputImageOnly["flags"]?["isInUsersCollections"] as? Bool{
                self.isInUsersCollections = isInUsersCollections
            } else {
                self.isInUsersCollections = false
            }
        }else{
            return nil
        }
    }
    
    init?(input:Dictionary<String,AnyObject>){
        //print(input)
        if let objectId = input["_id"] as? String, let images = input["images"] as? [String], let name = input["name"] as? String{
            
            self.objectId  = objectId
            self.images = images
            
            if let brandName = input["brandName"] as? String{
                self.brandName = brandName.capitalized + ": " + name
            }else{
                self.brandName = name.capitalized
            }
            
            if let min = input["minPrice"] as? Int, let max = input["maxPrice"] as? Int{
                self.priceRange = HelperPrice.convertPriceFromMinMaxPrice(min: min, max: max)
            }
            
        }else{
            return nil
        }
        self.populateProduct(input)
    }
    
    
    func populateProduct(_ input:Dictionary<String,AnyObject>){ //### check
        //        self.findIfUserLikedThisProduct()
        
        if let keywords = input["keywords"] as? String{
            self.keywords = keywords
        }
        
        if let isLikedByCurrentUser = input["flags"]?["isLikedByUser"] as? Bool{
            self.isLikedByCurrentUser = isLikedByCurrentUser
        } else {
            self.isLikedByCurrentUser = false
        }
        
        if let isRepostedByCurrentUser = input["flags"]?["isRepostedByUser"] as? Bool{
            self.isRepostedByCurrentUser = isRepostedByCurrentUser
        } else {
            self.isRepostedByCurrentUser = false
        }
        
        if let isInUsersCollections = input["flags"]?["isInUsersCollections"] as? Bool{
            self.isInUsersCollections = isInUsersCollections
        } else {
            self.isInUsersCollections = false
        }
        
        if let description = input["description"] as? String{
            
            // for HTML attributed String - $AS
            if let font: UIFont = UIFont(name: ConstantsFont.fontDefault, size: 13){
                if let attributedString = HTMLAttributedString.init(html: description, andBodyFont: font){
                    self.descriptionAttributedString = attributedString.attributedString
                }
            }
        }
        
        if let status = input["status"] as? String{
            self.status = status
        }
        
        if let attributes = input["attributes"] as? Dictionary<String,AnyObject>{
            self.attributes = attributes
        }
        
        if let likesCount = input["likesCount"] as? Int, likesCount > 0{
            self.likesCount = likesCount
        }
        
    }
    
//    func setFlags(isLikedByCurrentUser:Bool,isRepostedByCurrentUser:Bool){
//        self.isLikedByCurrentUser = isLikedByCurrentUser
//        self.isRepostedByCurrentUser = isRepostedByCurrentUser
//    }
    

    
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    
    
    //    func findIfUserLikedThisProduct(){
    //        if let userLikedProducts = CurrentUser.sharedInstance.currentUser?.likesId{
    //            for value in userLikedProducts{
    //                if self.objectId! == value{
    //                    self.isProductLiked = true
    //                    break
    //                }
    //            }
    //        }
    //    }
    
    // RETURNS PRODUCT VARIATIONS COUNT
    //    func getProductVariationCount()-> Int{
    //        return self.productVariations.count+1
    //    }
    //    
}
