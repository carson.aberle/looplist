//
//  UserSelf.swift
//  Looplist
//
//  Created by Arun Sivakumar on 7/11/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation

class CurrentUser{
    
    // MARK: VARIABLES

    var currentUser:User?
    static var sharedInstance = CurrentUser()
    var count = 0;
    
    // MARK: - INIT

    fileprivate init(){}
    
    // MARK: -  FUNCTIONS

    //ASSIGN CURRENT USER
    
    func assignCurrentUser(_ user:User){
        self.currentUser = user
//        self.currentUser?.isCurrentUser = true
    }

    // RESET CURRENT USER
    func resetCurrentUser(){
        self.currentUser = nil
    }

}
