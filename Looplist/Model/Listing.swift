//
//  Listing.swift
//  Looplist
//
//  Created by Arun on 5/25/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Listing{
    
    // MARK: -  --------------------------------------------------------   MANDATORY  -----------------------------------------------------------------------
    
    //    var updatedAt:Date?
    //    var createdAt:Date?

    var objectId:String?
    var brandName:String?
    var attributes:String? // filters
    var price:String?
    var images:[String]?

    
    // MARK: -  --------------------------------------------------------   OPTIONAL  -----------------------------------------------------------------------

    
    // Listing Screen
    var payment:Payment? // Lazy load
    var description:String? // ### listing screen // Lazy populate Change API call
    var optionalNotes:String = "" //  for pending product
//    var keywords:String? // ### check where its used
    var stock:Int? // -1 if no stock
    
    var product:Product?    // ### added for messaging
    var location = "Online"

    //    var storeId:String? // not yet
    //    var urls:[String]? // ### no idea // use from product
    //    var productId:String?

    
    // MARK: -  //////////////////////////////////////////////////////   INITILIZER   /////////////////////////////////////////////////////////////////////////
    
    init?(input:Dictionary<String,AnyObject>){
        if let objectId = input["_id"] as? String{
            self.objectId = objectId
        } else {
            return nil
        }
        
        if let images = input["images"] as? [String]{
            self.images = images
        }else{
            return nil
        }
        
        
        if let price = input["price"] as? Int{
            self.price =  HelperPrice.convertPricefromIntToString(price)
        } else {
            return nil
        }
        
        if let attributes = input["attributes"] as? NSDictionary{
            
            var valueArray = [String]()
            for value in attributes{
                valueArray.append((value.value as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
            }
            if valueArray.count > 0{
               self.attributes = valueArray.joined(separator: ", ")
            }
        }
        
        
        //Dont return nil for pending products - Carson
        
        
//        if let keywords = input["keywords"] as? String{
//            self.keywords = keywords
//            
//        }
        
        if let product = input["product"] as? [String:AnyObject]{ // for transactions
            self.product = Product(input: product)
            self.setDefaultProductValuesToListing()
        } else {
//            return nil
        }
        
        if let stock = input["stock"] as? Int{
            self.stock = stock
        }
        
//        if let updatedAt = input["updatedAt"] as? Date{
//            self.updatedAt = updatedAt
//        } else {
//            self.updatedAt = Date()
//        }
//        
//        if let createdAt = input["createdAt"] as? Date{
//            self.createdAt = createdAt
//        } else {
//            self.createdAt = Date()
//        }
        
//        if let urls = input["urls"] as? [String]{
//            self.urls = urls
//        }
        
//        if let storeId = input["store"] as? String{
//            self.storeId = storeId
//            
//        }
        

//        if let productId = input["product"] as? String {
//            self.productId = productId
//        }
        
//        self.optionalNotes = ""
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    // GET DATA FROM PRODUCT
    func setDefaultProductValuesToListing(){
//        
//        if self.urls == nil, let productUrl = self.product?.urls {
//            self.urls = productUrl
//        }
        if self.brandName == nil, let brandName = self.product?.brandName{
            self.brandName = brandName.capitalized
        }
        if self.description == nil, let description = self.product?.descriptionAttributedString{
            self.description = description.string
        }
//        if self.keywords == nil, let keywords = self.product?.identifier{
//            self.keywords = product.keywords
//        } 
        
//        ### change keywords
        if self.images != nil{
            if self.images!.count == 0, let images = self.product?.images{
                self.images = images
            }
        }
    }
}

// MARK: -  //////////////////////////////////////////////////////   EXTENSION   /////////////////////////////////////////////////////////////////////////

