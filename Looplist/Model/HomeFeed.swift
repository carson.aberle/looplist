//
//  Likes.swift
//  Looplist
//
//  Created by Arun Sivakumar on 5/25/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class HomeFeed{
    
    // MARK: -  --------------------------------------------------------   VARIABLES  -----------------------------------------------------------------------

    var products:[Product]? = [Product]()
    
    // MARK: -  //////////////////////////////////////////////////////   INITILIZER   /////////////////////////////////////////////////////////////////////////

    init(){}
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////

    // RETURNS PRODUCTS FOR THE HOMEFEED TABLE
    func queryForHomeFeed(_ initialLoad:Bool,skip:Int?,limit:Int?,completion:@escaping (_ success: Bool) -> Void){
        let jsonQuery = ["skip": skip!, "limit": limit!]
        let path = "misc/homefeed"
        DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(jsonQuery as NSDictionary)) { (result, error) in
            if let result = result {
                if let activities = result["activities"] as? NSArray{
                    if initialLoad{
                        self.products = [Product]()
                    }
                    for values in activities{
                        if let activityDict = values as? NSDictionary {
                            let activity = Activity(input: activityDict as! Dictionary<String, AnyObject>)
                            if let productDict = activity.attributes?["product"] as? NSDictionary{
                                if let product = Product(input: productDict as! Dictionary<String, AnyObject>){
                                    if activity.user?.profileImageUrl != nil{
                                        product.activity = activity
                                    }
//                                    if product.status == ConstantPlaceHolders.productPendingStatus{
//                                        continue
//                                    }else {
//                                        self.products?.append(product)
//                                    }
                                    self.products?.append(product)

                                }
                            }
                        }
                    }
                    if self.getProductCount() > 0{
                        //TEMPORARY TO GET LIKES AND REPOSTS WORKING FROM HOME FEED
                        completion(true)
                    } else {
                        completion(false)
                    }
                }
            } else if error != nil{
                completion(false)
            }
        }
    }
    
    // RETURN PRODUCT COUNT
    func getProductCount()-> Int{
        if let _ = self.products{
            return self.products!.count
        }
        return 0
    }
}
