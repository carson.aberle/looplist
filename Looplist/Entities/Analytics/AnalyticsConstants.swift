//
//  AnalyticsConstants.swift
//  Looplist
//
//  Created by Carson Aberle on 11/29/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit


struct AnalyticsConstants{

    static func getViewControllerAnalyticsName(_ viewControllerClassName:String) -> String{
        switch(viewControllerClassName){
        case "UserRegistrationEmailFinalViewController":
            return "Registration Username"
        case "UserRegistrationEmailInitialViewController":
            return "Registration Email"
        case "UserRegistrationProfilePhotoViewController":
            return "Registration Photo"
        case "ResetPasswordViewController":
            return "Reset Password"
        case "UserSignInViewController":
            return "Sign In"
        case "AddCollectionPopupViewController":
            return "Add To Collection Popup"
        case "AddCollectionViewController":
            return "New Collection"
        case "RepostPopupViewController":
            return "Repost Popup"
        case "HomeFeedViewController":
            return "Home Feed"
        case "NotificationsTableViewController":
            return "Notifications"
        case "FollowRequestTableViewController":
            return "Follow Request"
        case "MessagingInboxTableViewController":
            return "Messaging Inbox"
        case "MessagingNewMessageViewController":
            return "New Message"
        case "MessagingViewController":
            return "Message Conversation"
        case "DiscoverViewController":
            return "Discovery"
        case "SearchDisplayViewController":
            return "Search"
        case "FiltersViewController":
            return "Filters"
        case "ProductPreviewViewController":
            return "Product Preview"
        case "AddProductViewController":
            return "Add Product Web Page"
        case "AddProductDisplayViewController":
            return "Add Product Collection"
        case "AddProductWebViewController":
            return "Add Product User Selection"
        case "ProductPendingBuyTableViewController":
            return "Pending Product Buy"
        case "ProductBuyRetailerTableViewController":
            return "Retailer Product Buy"
        case "ProductTableViewController":
            return "Product"
        case "ProductLikingUsersTableViewController":
            return "Product Liking Users"
        case "ProductBuyViewController":
            return "Product Buy"
        case "ReturnPolicyTableViewController":
            return "Return Policy"
        case "OrderViewController":
            return "Checkout"
        case "ConfirmationNumberViewController":
            return "Checkout Confirmation"
        case "CollectionEditTableViewController":
            return "Edit Collection"
        case "CollectionLikesCollectionViewController":
            return "View Collection"
        case "UserFollowersViewController":
            return "Followers"
        case "UserFollowingViewController":
            return "Following"
//        case "UserProfileSortTableViewController":
//            return "Sort Collections Popup"
        case "UserProfileViewController":
            return "User Profile"
        case "SettingsHelpViewController":
            return "Help"
        case "SettingsLicensesTableViewController":
            return "Licenses"
        case "SettingsOrderHistoryTableViewController":
            return "Order History"
        case "SettingsPrivacyPolicyViewController":
            return "Privacy Policy"
        case "SettingsExternalAccountsTableViewController":
            return "External Accounts"
        case "SettingsPushNotificationsViewController":
            return "Edit Push Notifications"
        case "SettingsShippingAddressesViewController":
            return "Edit Shipping"
        case "SettingsViewController":
            return "Settings"
        case "SettingsTermsAndConditionsViewController":
            return "Terms and Conditions"
        case "EditProfilePasswordViewController":
            return "Edit Profile Password"
        case "EditProfileViewController":
            return "Edit Profile"
        case "CCViewController":
            return "Credit Card"
        case "CCAddViewController":
            return "Add Credit Card"
        case "ShippingAddressesViewController":
            return "Shipping Address"
        case "ShippingAddressAddUpdateViewController":
            return "Add Shipping Address"
        case "DiscoverViewController":
            return "Discover"
            
            //These Ones are in the docs, but not being sent because of no access token issue...
        
        default:
            return "Error on: " + viewControllerClassName
        }
    }
}
