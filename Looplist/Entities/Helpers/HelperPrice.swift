//
//  HelperPrice.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/27/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation


struct HelperPrice{
    
    static func convertPricefromIntToString(_ priceInInt:Int) -> String{
        
        
        let remainder = priceInInt%100
        
        switch remainder{
            
        case 0..<10:
            return "$\(priceInInt/100).0\(remainder)"

        default:
            return "$\(priceInInt/100).\(remainder)"
            
        }

//        return "$\(formatter.string(from: NSNumber(value:(priceInInt/100)))!)"+"."+"\(formatter.string(from: NSNumber(value:(priceInInt%100)))!)"
    }
    
    
    
    static func convertPriceFromMinMaxPrice(min:Int,max:Int) -> String{
        
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        
        if min == max {
            if min == 0 {
                return "N/A"
            } else if min > 99999{
                return "$\(formatter.string(from: NSNumber(value:(min/100000)))!)K"
            } else {
                
                return "$\(formatter.string(from: NSNumber(value:(min/100)))!)"
                
            }
        } else if max > 99999{
            
            if min > 99999{
                return "$\(formatter.string(from: NSNumber(value:(min/100000)))!)K - $\(formatter.string(from: NSNumber(value:(max/100000)))!)K"
            } else {
                return "$\(formatter.string(from: NSNumber(value:min/100))!) - $\(formatter.string(from: NSNumber(value:(max/100000)))!)K"
            }
            
        } else {
            return "$\(min/100).\(min%100) - $\(max/100).\(max%100)"
        }
        
        
    }
}


//    static func getPriceForApplePay(_ priceInInt:Int) -> String{
//        return  "\(priceInInt/100).\(priceInInt%100)"
//    }
