//
//  HelperPushNotification.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/22/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation


struct HelperPushNotification{
    
    static func registerForRemoteNotification(){
        //Register for Push Notifications
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
        let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
        
        UIApplication.shared.registerUserNotificationSettings(pushNotificationSettings)
        UIApplication.shared.registerForRemoteNotifications()
        
    }
}
