//
//  HelperClass.swift
//  Looplist
//
//  Created by Arun Sivakumar on 5/31/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import UIKit

class HelperDate{
    
    // MARK: -  //////////////////////////////////////////////////////    FUNCTIONS    //////////////////////////////////////////////////////////////////////////
    
    
    
    static func homeFeedGetDate(_ date:Date)-> String{
        
        var interval = (Calendar.current as NSCalendar).components(.year, from: date, to: Date(), options: []).year
        
        if let interval = interval, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "year" :
                "\(interval)" + " " + "years"
        }
        //
        //        interval = NSCalendar.currentCalendar().components(.Month, fromDate: date, toDate: NSDate(), options: []).month
        //        if interval > 0 {
        //            return interval == 1 ? "\(interval)" + " " + "mon" :
        //                "\(interval)" + " " + "mons"
        //        }
        
        interval = (Calendar.current as NSCalendar).components(.day, from: date, to: Date(), options: []).day
        if  let interval = interval, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "day" :
                "\(interval)" + " " + "days"
        }
        
        interval = (Calendar.current as NSCalendar).components(.hour, from: date, to: Date(), options: []).hour
        if  let interval = interval, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "hr" :
                "\(interval)" + " " + "hrs"
        }
        
        interval = (Calendar.current as NSCalendar).components(.minute, from: date, to: Date(), options: []).minute
        if  let interval = interval, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "min" :
                "\(interval)" + " " + "mins"
        }
        
        return "a moment ago"
    }
    
    
    //    Function that takes a NSDate and returns HH:MM if today, Day of week if this week, or DD:MM:YY otherwise as NSString
    static func getTimeInString(_ date: Date) -> String {
        let cal: Calendar = Calendar.current
        let componentsToday: DateComponents = (cal as NSCalendar).components(([.era, .year, .month,.weekOfYear, .day]), from: Date())
        let today: Date = cal.date(from: componentsToday)!
        let componentsPassedDate = (cal as NSCalendar).components(([.era, .year, .month,.weekOfYear, .day]), from: date)
        let otherDate: Date = cal.date(from: componentsPassedDate)!
        
        let timeFormat: DateFormatter = DateFormatter()
        timeFormat.dateFormat = "h:mm a"
        let weekdayFormatter: DateFormatter = DateFormatter()
        weekdayFormatter.dateFormat = "EEEE, h:mm a"
        let dateFormat: DateFormatter = DateFormatter()
        dateFormat.dateFormat = "MMMM d, YYYY h:mm a"
        if today == otherDate {
            return "Today, " + timeFormat.string(from: date)
        } else if componentsToday.weekOfYear == componentsPassedDate.weekOfYear && componentsToday.year == componentsPassedDate.year {
            return weekdayFormatter.string(from: date)
        } else {
            return dateFormat.string(from: date)
        }
    }
    
    //    Function that takes a NSDate and returns HH:MM if today, Day of week if this week, or DD:MM:YY otherwise as NSString
    static func getTimeStringInbox(_ date: Date) -> String {
        let cal: Calendar = Calendar.current
        let componentsToday: DateComponents = (cal as NSCalendar).components(([.era, .year, .month,.weekOfYear, .day]), from: Date())
        let today: Date = cal.date(from: componentsToday)!
        let componentsPassedDate = (cal as NSCalendar).components(([.era, .year, .month,.weekOfYear, .day]), from: date)
        let otherDate: Date = cal.date(from: componentsPassedDate)!
        
        let timeFormat: DateFormatter = DateFormatter()
        timeFormat.dateFormat = "h:mm a"
        let weekdayFormatter: DateFormatter = DateFormatter()
        weekdayFormatter.dateFormat = "EEE"
        let dateFormat: DateFormatter = DateFormatter()
        dateFormat.dateFormat = "M/d/yy"
        if today == otherDate {
            return timeFormat.string(from: date)
        } else if componentsToday.weekOfYear == componentsPassedDate.weekOfYear && componentsToday.year == componentsPassedDate.year {
            return weekdayFormatter.string(from: date)
        } else {
            return dateFormat.string(from: date)
        }
    }
    
    static func messagingCompareTwoDates(_ date1: Date,date2: Date) -> Bool{
        let calendar = Calendar.current
        let components1 = (calendar as NSCalendar).components([.day , .month , .year], from: date1)
        let components2 = (calendar as NSCalendar).components([.day , .month , .year], from: date2)
        
        let year1 =  components1.year
        let month1 = components1.month
        let day1 = components1.day
        
        let year2 =  components2.year
        let month2 = components2.month
        let day2 = components2.day
        
        if (year1 == year2 && month1 == month2 && day1 == day2){
            return true
        }else{
            return false
        }
    }
    
    static func formatDataBaseDate(_ serializedDate:String?) -> Date{ // all inits
        var date:Date = Date()
        if let serializedDate = serializedDate{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            date = dateFormatter.date(from: serializedDate)!
            
        }
        return date
    }
    
    static func activityStringToDate(_ date:String) -> Date { // Activity model
        let formatter = DateFormatter()
        
        // Format 1
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let parsedDate = formatter.date(from: date) {
            return parsedDate
        }
        
        // Format 2
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:SSSZ"
        if let parsedDate = formatter.date(from: date) {
            return parsedDate
        }
        
        // Couldn't parsed with any format. Just get the date
        let splitedDate = date.components(separatedBy: "T")
        if splitedDate.count > 0 {
            formatter.dateFormat = "yyyy-MM-dd"
            if let parsedDate = formatter.date(from: splitedDate[0]) {
                return parsedDate
            }
        }
        return Date()
    }
    
    
    
    //    static func offsetFrom(_ date:Date) -> String {
    //        let currentDate = Date()
    //
    //        let dayHourMinuteSecond: NSCalendar.Unit = [.day, .hour, .minute, .second]
    //        let difference = (Calendar.current as NSCalendar).components(dayHourMinuteSecond, from: date, to: currentDate, options: [])
    //
    //        let seconds = "\(difference.second)s"
    //        let minutes = "\(difference.minute)m" + " " + seconds
    //        let hours = "\(difference.hour)h" + " " + minutes
    //        let days = "\(difference.day)d" + " " + hours
    //
    //        if difference.day!    > 0 { return days }
    //        if difference.hour!   > 0 { return hours }
    //        if difference.minute! > 0 { return minutes }
    //        if difference.second! > 0 { return seconds }
    //        return ""
    //    }
    
    //    static func offsetFromDays(_ date:Date) -> Int {
    //        let currentDate = Date()
    //
    //        let dayHourMinuteSecond: NSCalendar.Unit = [.day]
    //        let difference = (Calendar.current as NSCalendar).components(dayHourMinuteSecond, from: date, to: currentDate, options: [])
    //
    //        let days = difference.day
    //
    //        if difference.day!    > 0 { return days! }
    //        return 0
    //    }
}
