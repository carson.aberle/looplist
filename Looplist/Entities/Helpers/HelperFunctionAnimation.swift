//
//  HelperFunctionAnimation.swift
//  Looplist
//
//  Created by Arun Sivakumar on 2/7/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import Foundation


struct HelperFunctionAnimation{
    
    
    static func getLooplistGradient(insideFrame view:UIView) -> CAGradientLayer{
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [ConstantsColor.loginGradientStartColor.cgColor, ConstantsColor.loginGradientEndColor.cgColor]
        gradient.locations = nil
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: view.frame.size.width, height: view.frame.size.height)

        return gradient
    }
    
}
