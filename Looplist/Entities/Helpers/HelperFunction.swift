//
//  HelperFunctions.swift
//  Looplist
//
//  Created by Arun on 5/17/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import Foundation
import Branch

struct HelperFunction {
    
    // MARK: -------------------------------------------    VARIABLES     ------------------------------------------------------
    
    static let loginBackgroundImage = UIImage(named:"Login Background")!
    static let loginBackgroundImageExtra = UIImage(named:"Login Background")!
    static let backgroundImageView1 = UIImageView(image: loginBackgroundImage )
    static let backgroundImageView2 = UIImageView(image: loginBackgroundImageExtra )
    static var hasSetFrame:Bool = false
    
      // MARK: ------------------------------------------   FUNCTIONS     -------------------------------------------------------

        
    static func validateEmail (_ email : String) -> Bool{
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }
    
    static func checkIfOnlyWhitespace(_ input: String) -> Bool {
        let whitespace: CharacterSet = CharacterSet.whitespacesAndNewlines
        return (input.trimmingCharacters(in: whitespace) == "")
    }
    
    static func checkIfAlphanumericDotUnderscore(_ input: String) -> Bool {
        let allowedSet: NSMutableCharacterSet = NSMutableCharacterSet(charactersIn: "_.@")
        allowedSet.formUnion(with: CharacterSet.alphanumerics)
        return (input.trimmingCharacters(in: allowedSet as CharacterSet) != "")
    }
    
    static func getBranchLink(title:String, forUser:String?, forProduct:String?, withInviteCode:String?, withUrl:String?, andContentDescription:String, onChannel:String,  completion: @escaping (_ result: String?, _ error: Error?) -> Void) {
        let branchUniversalObject = BranchUniversalObject(canonicalIdentifier: "item/12345")
        branchUniversalObject.title = title
        branchUniversalObject.contentDescription = andContentDescription.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
        if let imageUrl = withUrl{
            branchUniversalObject.imageUrl = imageUrl
        }
        if let inviteCode = withInviteCode{
            branchUniversalObject.addMetadataKey("inviteCode", value: inviteCode)
        }
        if let productId = forProduct{
            branchUniversalObject.addMetadataKey("productId", value: productId)
        }
        if let userId = forUser{
            branchUniversalObject.addMetadataKey("userId", value: userId)
        }
        
        let linkProperties = BranchLinkProperties()
        linkProperties.feature = "sharing"
        linkProperties.channel = onChannel
        
        branchUniversalObject.getShortUrl(with: linkProperties) { (url, error) in
            completion(url,error)
        }
    }
    
    static func getResultString(count:Int?)->String{
        if let count = count{
            let result = (count > 1) ? "RESULTS".localized : "RESULT".localized
            return "\(count) " + result
            
        }
        return "" // fallback
    }
    
}
