//
//  ErrorHandling.swift
//  Looplist
//
//  Created by Arun Sivakumar on 6/5/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import UIKit


//protocol ErrorHandlerProtocol:class{
//     func customErrorMessage(_ message:String)
//     func customSuccessMessage(_ message:String)
//}

struct ErrorHandling {
    // MARK: -  //////////////////////////////////////////////////////    VARIABLES    //////////////////////////////////////////////////////////////////////////
    
    
    //ERROR CHECKING
    static let transactionsError = "Transaction failed!"
    static let transactionsErrorShipping = "Please select a Shipping Address"
    static let transactionsErrorPaymentMethod = "Please select a valid Credit Card"
    static let transactionsErrorShippingAndPaymentMethod = "Please select Shipping Address & Credit Card"
    
    
    static let ResetPasswordSuccess = "We have sent you a link. Check your email to reset your password"
    static let ResetPasswordErrorValidEmail = "Please enter a valid e-mail"
    static let ResetPasswordErrorRegisteredEmail = "Please enter your registered e-mail"
    

    static let MandatoryErrorDefaultMessage  = "All Fields are mandatory"
    
    static let ApplePayErrorHandling = "Apple Pay not available"

    static func unexpectedErrorHandler() {

    }


    
    static func customErrorHandler(message:String) {
         DropDownAlert.customErrorMessage(message)
    }
    
    static func customErrorHandlerWithTitle(title:String,message:String) {
        DropDownAlert.customErrorMessage(title+"\n"+message)
    }
    
    static func customSuccessHandler(message:String) {
        DropDownAlert.customSuccessMessage(message)
    }
    
    static func customSuccessHandlerWithTitle(title:String,message:String) {
        DropDownAlert.customSuccessMessage(message)
    }


    
    static func tokenErrorHandler(_ error: NSError) {
        DropDownAlert.customErrorMessage(error.localizedDescription)
    }

    
    static func defaultApplePayErrorHandler() {
        DropDownAlert.customErrorMessage(ApplePayErrorHandling)
    }
    
    static func mandatoryFieldsrrorHandler() {
        DropDownAlert.customErrorMessage(MandatoryErrorDefaultMessage)
    }
}
