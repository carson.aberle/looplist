//
//  DropDownAlert.swift
//  Looplist
//
//  Created by Arun Sivakumar on 8/10/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import SwiftyDrop

// MARK: -  //////////////////////////////////////////////////////    OVERRIDE    //////////////////////////////////////////////////////////////////////////

enum Custom: DropStatable {
    case error
    case success
//    case neutral
    
        
    var backgroundColor: UIColor? {
        switch self {
        case .error: return ConstantsColor.dropDownErrorColor
        case .success: return ConstantsColor.dropDownSuccessColor
//        case .neutral: return ConstantsColor.looplistColor
        }
    }
    
    var font: UIFont? {
        switch self {
            default: return UIFont(name: ConstantsFont.dropDownFont, size: ConstantsFont.dropDownFontSize)
        }
    }
    
    var textColor: UIColor? {
        switch self {
        default: return .white
        }
    }
    
    var blurEffect: UIBlurEffect? {
        switch self {
            default: return UIBlurEffect(style: .light)
        }
    }
}

// MARK: -  //////////////////////////////////////////////////////    CUSTOM FUNCTIONS    //////////////////////////////////////////////////////////////////////////



struct DropDownAlert{
    
    
    static func customErrorMessage(_ message:String){
        Drop.down(message, state: Custom.error)
    }

    static func customSuccessMessage(_ message:String){
        Drop.down(message, state: Custom.success)
    }

}
