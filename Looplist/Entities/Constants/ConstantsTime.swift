//
//  ConstantsTime.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/30/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import Foundation


struct ConstantsTime{
    static let indicatorMinimumTime = 0.2
    static let indicatorMinimumTimeShort = 0.2
    
}
