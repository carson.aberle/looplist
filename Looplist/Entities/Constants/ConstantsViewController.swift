//
//  ConstantsViewControllers.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/15/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation

struct ConstantsViewController{
// VIEW CONTROLLERS

static let viewControllerWaitlist = "WaitlistViewController"
static let viewControllerInitial = "TutorialNavigationController"
static let viewControllerTabBar = "looplistTabBarController"
static let viewControllerSignIn = "TutorialNavigationController"


}
