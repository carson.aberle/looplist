//
//  ConstantsObjects.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/15/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation


struct ConstantsObjects{
    
    
    // BUTTON CONFIGURATION
    static let defaultButtonCornerRadius = CGFloat(5.0)
    
    // IMAGE CONFIGURATION
    static let defaultImageCornerRadius = CGFloat(3.0)
    
    // VIEW CONFIGURATION
    static let defaultPopUpViewCornerRadius = CGFloat(2.0)
    
    
    // SPINNER
    
    static let spinnerTime = 2.0
    
    // POPUP
    
    static let REPOST_POPUP_BACKGROUND_TAG = 8888
    static let REPOST_POPUP_SLIDER_TAG = 8889
//    static let REPOST_POPUP_TRANSLATION_HEIGHT = CGFloat(286)
    
    static let REPOST_POPUP_TRANSLATION_HEIGHT = CGFloat(370)
    static let REPOST_POPUP_HEIGHT = CGFloat(REPOST_POPUP_TRANSLATION_HEIGHT + 6)
    
    static let FILTER_POPUP_BACKGROUND_TAG = 1234
    static let FILTER_POPUP_SLIDER_TAG = 1235
    static let FILTER_POPUP_TRANSLATION_HEIGHT = CGFloat(300)
    static let FILTER_POPUP_HEIGHT = CGFloat(308)

    
    static let ADD_TO_COLLECTION_POPUP_BACKGROUND_TAG = 3234
    static let ADD_TO_COLLECTION_POPUP_SLIDER_TAG = 3235
    static let ADD_TO_COLLECTION_POPUP_TRANSLATION_HEIGHT = CGFloat(322)
    static let ADD_TO_COLLECTION_POPUP_HEIGHT = CGFloat(ADD_TO_COLLECTION_POPUP_TRANSLATION_HEIGHT + 8)
    
    static let EXPAND_WIDTH_VIEW_TAG = 873

    
}
