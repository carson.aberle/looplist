//
//  Constants.swift
//  alphalooplist
//
//  Created by Arun on 3/4/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import UIKit

struct Constants{
    
    // ASSETS
    static let defaultFemaleImage = "Default Female"
    static let defaultMaleImage = "Default Male"
    static let defaultGenderImage = "Default Gender"
    static let mavenImage = "Maven"
    static let addImage = "Add"
    static let checkmarBlueImage = "Checkmark Blue"
    static let backImage = "Back"
    static let carrotDownImage = "Caret Down"
    static let carrotDownGreyImage = "Caret Down Grey"
    static let carrotUpGreyImage = "Caret Up Grey"
    static let repostImage = "Repost"
    static let repostedImage = "Reposted"
    static let peekArrowImage = "Peek Arrow"
    static let peekCheckedImage = "Peek Checked"
    static let closeSmallImage = "Close Small"
    static let closeLargeImage = "Close Large"
    static let productScreenOverlayImage = "Product Screen Overlay"
    static let collectionsPlaceholderImage = "Collections Img Placeholder"
    static let followImage = "Follow"
    static let repostActivityImage = "Activity Repost"
    static let activityAddedProductImage = "Activity Added Product"
    static let likeImage = "Like"
    static let commentImage = "Comment"
    static let settingsImage = "Settings"
    static let tutorialUserProfileImage = "Tutorial User Profile"
    static let tutorialMavenPageImage = "Tutorial Maven Page"
    static let tutorialListingPageImage = "Tutorial Listing Page"
    
    
    static let notificationLikeImage = "Notification Like"
    static let notificationRepostImage = "Notification Repost"
    static let notificationFollowImage = "Notification Follow"
    static let notificationAddProductImage = "Notification Add Product"
    
    
    static let imageSearchLabelActive = "Search Label Active"
    static let imageSearchLabelInactive = "Search Label Inactive"
    
    static let looplistBannerImage = "Looplist Banner"
    static let likedHeartsActive = "Heart Big Active"
    static let likedHeartsInActive = "Heart Big Inactive"
    static let placeholder = "Placeholder"
    static let assetsUserProfileNavBar = "Nav Bar User Profile"
    
    
    static let addBlue = "Add Blue"
    static let addGrey = "Add"
    
    static let activityRepost = "Activity Repost"
    static let activityLike = "Activity Like"

    
    static let sadEmojiImage = "Sad Face Dark"
    static let smileyEmojiImage = "Smiley Face Dark"
    static let lockedProfileImage = "Locked Profile"
    
    // WEB NAVIGATION DEFAULT IMAGES
    
    static let WebForwardImageActive = "Forward Arrow Dark"
    static let WebForwardImageInActive = "Forward Arrow Light"
    static let WebBackImageActive = "Back Arrow Dark"
    static let WebBackImageInActive = "Back Arrow Light"
    static let WebShareImage = "Share Arrow Dark"
    
    
    static let MessagingTabBarNotification = "Messaging Tab Bar Notification"
    static let MessagingTabBar = "Messaging Tab Bar"
    

    
    // PAYMENT
    
//    static let paymentTypeApplePay = "Apple Pay"
    
    static let YELLOW_BUTTON_TAG = 5000
    static let RED_BUTTON_TAG = 5001
    static let BLUE_BUTTON_TAG = 5002
    static let GREEN_BUTTON_TAG = 5003
    static let ORANGE_BUTTON_TAG = 5004
    static let PURPLE_BUTTON_TAG = 5005
    static let PINK_BUTTON_TAG = 5006
    static let WHITE_BUTTON_TAG = 5007
    static let LIGHT_GREY_BUTTON_TAG = 5008
    static let DARK_GREY_BUTTON_TAG = 5009
    static let BLACK_BUTTON_TAG = 5010
    static let TAN_BUTTON_TAG = 5011
    static let BROWN_BUTTON_TAG = 5012

    }
