//
//  Identification.swift
//  Looplist
//
//  Created by Arun Sivakumar on 5/31/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import Foundation

struct ConstantsIdentification{
    
    static let ApplePayMerchantID = "merchant.com.looplist"
    
    // STRIPE
    
//      static let StripeKey = "pk_test_I22dcxNHUjMrIzVNoTG7yHIC"
        static let StripeKey = "sk_live_UK7x5K2BqlMK6tZu22oDAMZU"
    
//    
//    static let defaultsFirstName = "firstName"
//    static let defaultsLastName = "lastName"
//    static let defaultsUsername = "username"
//    static let defaultsIsWaitlisted = "isWaitlisted"
//    static let defaultsPhotoUrl = "profileImageUrl"
//    static let defaultsAccessToken = "accessToken"
//    static let defaultsGender = "gender"
//    static let defaultsLocation = "location"
//    static let defaultsQuote = "quote"
    
    
    static let defaultsDeviceId = "deviceId"
    static let defaultsObjectId = "objectId"

}
