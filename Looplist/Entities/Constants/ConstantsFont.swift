//
//  ConstantsFont.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/15/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation

enum FontStyle{
    case MessagingReadSmall
    case MessagingUnreadSmall
    case MessagingReadBig
    case MessagingUnreadBig
}

struct ConstantsFont{
    
    
    // FONTS
    
    static let fontRegular = "SFUIDisplay-Regular"
    static let fontDefault = "SFUIDisplay-Light"
    static let fontDefaultBold = "SFUIDisplay-Bold"
    static let fontDefaultBlack = "SFUIDisplay-Black"
    static let fontThin = "SFUIDisplay-Thin"
    
    
    //DROP DOWN
    
    static let dropDownFont = fontRegular
    static let dropDownFontSize:CGFloat = 15

//    static let searchDefault = "SFUIText-Light"

    static let navBarFont = UIFont(name: ConstantsFont.fontDefault, size: 20)!
    static let navBarFontBold = UIFont(name: ConstantsFont.fontDefaultBold, size: 16)!


    static func getFont(fontStyle:FontStyle)->UIFont{
        switch  fontStyle{
        case .MessagingReadBig:
           return UIFont(name: ConstantsFont.fontThin, size: 15.0)!
        case .MessagingReadSmall:
            return UIFont(name: ConstantsFont.fontThin, size: 12.0)!
        case .MessagingUnreadBig:
           return UIFont(name: ConstantsFont.fontRegular, size: 15.0)!
        case .MessagingUnreadSmall:
            return UIFont(name: ConstantsFont.fontRegular, size: 12.0)!
        }
    }
}
