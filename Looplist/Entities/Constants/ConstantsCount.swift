//
//  ConstantsCount.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/22/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation


struct ConstantsCount{

    
    static let searchResultCount = 25
    static let paginationlimitNumber = 25
    static let paginationCurrentPage = 0
    static let homeFeedPollTimeInterval = 5.0

}
