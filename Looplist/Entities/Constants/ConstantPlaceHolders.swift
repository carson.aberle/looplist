//
//  ConstantPlaceHolders.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/15/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation


struct ConstantPlaceHolders{
    
    // MESSAGING
    
    static let messagingTextViewPlaceHolderMaven = "Ask MΛVEN"
    static let messagingTextViewPlaceHolderDefault = "message..."
    static let messagingReceiverMavenImage = "Maven"
    static let maven = "MΛVEN"
    
    
    static let productPendingStatus = "pending"
    static let productActiveStatus = "active"
}
