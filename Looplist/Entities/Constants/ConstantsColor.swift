//
//  ConstantsImages.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/15/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation


struct ConstantsColor{
    
    
    // COLORS
    // LOGIN
    
    static let loginGradientStartColor = UIColor(red:0/255, green:210/255, blue:255/255, alpha:1.0)
    static let loginGradientEndColor = UIColor(red:58/255, green:125/255, blue:213/255, alpha:1.0)

    
    static let looplistColor = UIColor(red: 37/255, green: 199/255, blue: 255/255, alpha: 1.0)
    static let looplistGreyColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
    static let borderDarkColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
    static let addButtonColor = UIColor(red: 106/255, green: 165/255, blue: 92/255, alpha: 1.0)
    static let pullToRefreshColor = UIColor(red: 225/255, green: 225/255, blue: 225/255, alpha: 1.0)
    static let messagingInboxUnreadCellColor = UIColor(red: 37/255, green: 199/255, blue: 255/255, alpha: 0.1)
    static let messagingInboxReadCellColor = UIColor.white
    static let messagingInputToolbarPlaceholderColor = UIColor(red: 189/255, green: 189/255, blue: 189/255, alpha: 1.0)
    
    static let shippingMakeDefaultButtonColor = UIColor(red: 231/255, green: 232/255, blue: 232/255, alpha: 1.0)
    static let shippingMakeDefaultButtonTextColor = UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0)
    
    static let notificationColor = UIColor(red: 168/255, green: 255/255, blue: 80/255, alpha: 1.0)

    static let FILTERS_YELLOW_COLOR = UIColor(red:0.98, green:0.98, blue:0.00, alpha:1.0)
    static let FILTERS_RED_COLOR = UIColor(red:0.96, green:0.31, blue:0.31, alpha:1.0)
    static let FILTERS_BLUE_COLOR = UIColor(red:0.33, green:0.60, blue:0.95, alpha:1.0)
    static let FILTERS_GREEN_COLOR = UIColor(red:0.30, green:0.77, blue:0.30, alpha:1.0)
    static let FILTERS_ORANGE_COLOR = UIColor(red:1.00, green:0.42, blue:0.00, alpha:1.0)
    static let FILTERS_PURPLE_COLOR = UIColor(red:0.69, green:0.00, blue:1.00, alpha:1.0)
    static let FILTERS_PINK_COLOR = UIColor(red:1.00, green:0.58, blue:1.00, alpha:1.0)
    static let FILTERS_WHITE_COLOR = UIColor(red:1.00, green:1.0, blue:1.00, alpha:1.0)
    static let FILTERS_BLACK_COLOR = UIColor(red:0.0, green:0.0, blue:0.0, alpha:1.0)
    static let FILTERS_LIGHT_GREY_COLOR = UIColor(red:0.52, green:0.52, blue:0.52, alpha:1.0)
    static let FILTERS_DARK_GREY_COLOR = UIColor(red:0.30, green:0.30, blue:0.30, alpha:1.0)
    static let FILTERS_TAN_COLOR = UIColor(red:0.35, green:0.28, blue:0.35, alpha:1.0)
    static let FILTERS_BROWN_COLOR = UIColor(red:0.55, green:0.34, blue:0.16, alpha:1.0)
    
    
    static let FILTERS_YELLOW_HEX = "#fbfb00"
    static let FILTERS_RED_HEX = "#f65050"
    static let FILTERS_BLUE_HEX = "#559af1"
    static let FILTERS_GREEN_HEX = "#4dc54d"
    static let FILTERS_ORANGE_HEX = "#ff6c01"
    static let FILTERS_PURPLE_HEX = "#b100ff"
    static let FILTERS_PINK_HEX = "#ff94ff"
    static let FILTERS_WHITE_HEX = "#ffffff"
    static let FILTERS_BLACK_HEX = "#000000"
    static let FILTERS_LIGHT_GREY_HEX = "#858585"
    static let FILTERS_DARK_GREY_HEX = "#4c4c4c"
    static let FILTERS_TAN_HEX = "#5a4759"
    static let FILTERS_BROWN_HEX = "#8b572a"
    
    // DROP DOWN
    
    static let dropDownErrorColor = UIColor(red: 241/255, green: 110/255, blue: 110/255, alpha: 0.7)
    static let dropDownSuccessColor = UIColor(red:129/255, green:210/255, blue:103/255, alpha:0.7)
    
}
