//
//  RealmHelper.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/12/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import RealmSwift

class RealmHelper{
    
    static let propertyModificationDate = "modificationDate"
    
    
    // Search Display VC
    
    static func addToUserRecentSearch(_ searchTerm:String,searchTermCount:Int){
        
        
        let userRecentSearch = UserRecentSearch()
        userRecentSearch.searchTerm = searchTerm
        userRecentSearch.searchTermCount = searchTermCount
        userRecentSearch.modificationDate = NSDate()
        
        
        // Get the default Realm
        
        if let realm = try? Realm(){
            do{
                try realm.write {
                    realm.add(userRecentSearch, update:true)
                }
            }catch{
                //cannot save data
            }
        }
        
    }
    
    static func getUserRecentSearch()->Results<UserRecentSearch>?{
        
        // Get the default Realm
            if let realm = try? Realm(){
                let userRecentSearchResults = realm.objects(UserRecentSearch.self).sorted(byKeyPath: propertyModificationDate, ascending: false)
                
                return userRecentSearchResults
            }else{
                
                return nil
            }
    }
    

    
    // Add product onboarding seen by user
    
    static func setUserSeenAddProductOnboarding(){
        
        let userSeenAddProductOnboarding = UserSeenAddProductOnboarding()
        userSeenAddProductOnboarding.onBoardingSeen = true
        
        // Get the default Realm
        
        if let realm = try? Realm(){
            do {
                try realm.write {
                    realm.add(userSeenAddProductOnboarding, update:true)
                }
            }catch{
                //error
            }
        }
    }


    static func getOnBoardingStatus() -> Bool?{
        
        if let realm = try? Realm(){
            let userSeenAddProductOnboarding = realm.objects(UserSeenAddProductOnboarding.self).sorted(byKeyPath: propertyModificationDate, ascending: false)
            if userSeenAddProductOnboarding.count > 0{
                let status = userSeenAddProductOnboarding[0].onBoardingSeen
                return status
            }
        }
        return nil

    }

}

// Add product Screen VC
//
//    static func assignLastVisitedUrl(_ url:String?){
//        if let url = url {
//            let userLastVisitedURL = UserLastVisitedURL()
//            userLastVisitedURL.lastVisitedUrlByUser = url
//
//            // Get the default Realm
//            let realm = try! Realm()
//
//            try! realm.write {
//                realm.add(userLastVisitedURL, update:true)
//            }
//        }else{
//            return
//        }
//    }
//
//    static func getLastVisitedUrl() -> String?{
//
//        let realm = try! Realm()
//        let userLastVisitedURL = realm.objects(UserLastVisitedURL.self).sorted(byProperty: propertyModificationDate, ascending: false)
//        if userLastVisitedURL.count > 0{
//            let url = userLastVisitedURL[0].lastVisitedUrlByUser
//            return url
//        }else{
//            return nil
//        }
//    }
