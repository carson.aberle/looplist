//
//  DBHelper.swift
//  Looplist
//
//  Created by Carson Aberle on 6/28/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
//import SwiftyDrop

struct DBHelper{
    
    // MARK: -  //////////////////////////////////////////////////////    VARIABLES    //////////////////////////////////////////////////////////////////////////
    
    //0 = Development
    //1 = Staging
    //2 = Production
    
    static let RUNNING_CONFIG = 3 // IF 3 switch stripe key in ConstantsIdentification
    static var VERSION_NUMBER:String = "v1/"
    
    // MARK: -  //////////////////////////////////////////////////////    FUNCTIONS    //////////////////////////////////////////////////////////////////////////
    
    static func getAPIHost() -> String {
        switch(self.RUNNING_CONFIG) {
        case 0:
            return "https://dev-api.looplist.com/"
        case 1:
            return "https://testing-api.looplist.com/"
        case 2:
            return "https://staging-api.looplist.com/"
        case 3:
            return "https://api.looplist.com/"
        default:
            return "https://api.looplist.com/"
        }
    }
    
    static func getHeader() ->[String: String]{
        return ["authorization":"Bearer \(getAccessToken())", "accept": "application/json", "content-type": "application/json"] //  for DEV
    }
    
    static func getAccessToken() -> String{
        let MyKeychainWrapper = KeychainWrapper()
        if let accessToken = MyKeychainWrapper.myObject(forKey: kSecValueData) as? String{
            return accessToken
        } else {
            return ""
        }
    }
    
    static func getPath(_ path:String?)->String{
        var completePath = self.getAPIHost() + self.VERSION_NUMBER
        if let path = path{
            completePath += path
        }
        return completePath
    }
     
    static func setUserDefaults(){
        UserDefaults.standard.set(CurrentUser.sharedInstance.currentUser?.objectId, forKey: ConstantsIdentification.defaultsObjectId)
    }
    
    
    static func logoutAndClearDefaults(){
        // signout
        let appDomain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        //Clear Keychain
        let MyKeychainWrapper = KeychainWrapper()
        MyKeychainWrapper.mySetObject(nil, forKey:kSecValueData)
        MyKeychainWrapper.writeToKeychain()
        
        // reset Singletons
        CurrentUser.sharedInstance.resetCurrentUser()
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate{
            appDelegate.showScreen(ConstantsViewController.viewControllerSignIn)
        }
    }
    
    static func getCurrentUser(_ completion:@escaping (_ success:Bool)->Void){
        if let objectId = UserDefaults.standard.string(forKey: ConstantsIdentification.defaultsObjectId),  self.getAccessToken() != ""{
            let path = "users/" + objectId
            
            //Used to for pre login checks so they don't throw error
            let queryString = DBHelper.getJSONFromDictionary(["populate":[["path":"payment.paymentMethods"]]])
            
            
            DBHelper.callAPIGet(path, queryString: queryString, completion: { (result, error) in
                
                if let result = result{
                    if let userObject = result["user"], let user = userObject as? Dictionary<String, AnyObject> {
                        CurrentUser.sharedInstance.assignCurrentUser(User(input:user))
                        // GET USER SHIPPING ADDRESS - APP DELEGATE
                        if CurrentUser.sharedInstance.currentUser?.objectId != nil{
                            
                            completion(true)
                        }
                    }else{
                        completion(false)
                    }
                } else if let _ = error{
//                    self.logoutAndClearDefaults()
                    completion(false)
                }
            })
        }
    }
    

    
    // ANALYTICS
    static func callAPIPostForDefaultAnalytics(_ path:String, params:[String:AnyObject], completion: @escaping (_ result: NSDictionary?, _ error: NSError?) -> Void){
        Alamofire.request((self.getAPIHost() + self.VERSION_NUMBER + "actions"), method: .post, parameters: params, encoding: JSONEncoding.default, headers: ["authorization":"Bearer \"000000000000000000000000\"", "accept": "application/json", "content-type": "application/json"]).validate().responseJSON(completionHandler: { (response) in
            completion(NSDictionary(), nil)
        })
    }
    
    static func callAPIPostForAnalytics(_ path:String, params:[String: AnyObject?], completion: @escaping (_ result: NSDictionary?, _ error: NSError?) -> Void) {
        Alamofire.request(self.getAPIHost() + self.VERSION_NUMBER + path, method: .post, parameters: params, encoding: JSONEncoding.default, headers: DBHelper.getHeader()).validate().responseJSON(completionHandler: { (response) in
            self.getResultAndErrorForAnalytics(response) { (result, error) in
                completion(result, error)
            }
        })
    }
    
    static func getResultAndErrorForAnalytics(_ response:DataResponse<Any>, completion: (_ result: NSDictionary?, _ error: NSError?) -> Void){
        
        if let statuscode = response.response?.statusCode{
            switch statuscode{
            case 200...299:
                if let resultJSON = response.result.value {
                    completion(resultJSON as? NSDictionary, nil)
                }
            default:
                completion(nil, nil)
                //                break
            }
        }else if let error = response.result.error {
            completion(nil, error as NSError?)
        }
    }
    
    // GENERAL
    
    static func callAPIPost(_ path:String, params:[String: AnyObject?], completion: @escaping (_ result: NSDictionary?, _ error: NSError?) -> Void) {
        Alamofire.request(self.getAPIHost() + self.VERSION_NUMBER + path, method: .post, parameters: params, encoding: JSONEncoding.default, headers: DBHelper.getHeader()).validate().responseJSON(completionHandler: { (response) in
            self.getResultAndError(response) { (result, error) in
                completion(result, error)
            }
        })
    }
    
    static func callAPIPost(_ path:String, completion: @escaping (_ result: NSDictionary?, _ error: NSError?) -> Void) {
        Alamofire.request(self.getAPIHost() + self.VERSION_NUMBER + path, method: .post,  encoding: JSONEncoding.default, headers: DBHelper.getHeader()).validate().responseJSON(completionHandler: { (response) in
            self.getResultAndError(response) { (result, error) in
                completion(result, error)
                
            }
        })
    }
    
    static func callAPIGet(_ path:String, queryString: String, completion: @escaping (_ result: NSDictionary?, _ error: NSError?) -> Void){
        let pathFinal:String = (self.getAPIHost() + self.VERSION_NUMBER + path + "?query=" + queryString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)
        Alamofire.request(pathFinal, method: .get,  encoding: JSONEncoding.default, headers: DBHelper.getHeader()).validate().responseJSON(completionHandler: { (response) in
            self.getResultAndError(response) { (result, error) in
                
                completion(result, error)
            }
        })
        
        
    }
    
    static func callAPIPut(_ path:String, params: [String:AnyObject], completion: @escaping (_ result: NSDictionary?, _ error: NSError?) -> Void) {
        Alamofire.request(self.getAPIHost() + self.VERSION_NUMBER + path, method: .put, parameters: params, encoding: JSONEncoding.default, headers: DBHelper.getHeader()).validate().responseJSON(completionHandler: { (response) in
            self.getResultAndError(response) { (result, error) in
                completion(result, error)
            }
        })
    }
    
    static func callAPIDelete(_ path:String, queryString: String, completion: @escaping (_ result: NSDictionary?, _ error: NSError?) -> Void) {
        Alamofire.request(self.getAPIHost() + self.VERSION_NUMBER + path + "?query=" + queryString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!, method: .delete, headers: DBHelper.getHeader()).validate().responseJSON(completionHandler: { (response) in
            self.getResultAndError(response) { (result, error) in
                completion(result, error)
            }
        })
    }
    
    static func callAPIDelete(_ path:String,  params:[String: AnyObject?], completion: @escaping (_ result: NSDictionary?, _ error: NSError?) -> Void) {
        Alamofire.request(self.getAPIHost() + self.VERSION_NUMBER + path, method: .delete, parameters: params, encoding: JSONEncoding.default, headers: DBHelper.getHeader()).validate().responseJSON(completionHandler: { (response) in
            self.getResultAndError(response) { (result, error) in
                completion(result, error)
            }
        })
    }
    
    static func callAPIGet(_ path:String, completion: @escaping (_ result: NSDictionary?, _ error: NSError?) -> Void){
        Alamofire.request(self.getAPIHost() + self.VERSION_NUMBER + path, method: .get,  encoding: JSONEncoding.default, headers: DBHelper.getHeader()).validate().responseJSON(completionHandler: { (response) in
            self.getResultAndError(response) { (result, error) in
                completion(result,error)
            }
        })
    }
    
    static func callAPIPut(_ path:String, completion: @escaping (_ result: NSDictionary?, _ error: NSError?) -> Void) {
        Alamofire.request(self.getAPIHost() + self.VERSION_NUMBER + path, method: .put, encoding: JSONEncoding.default, headers: DBHelper.getHeader()).validate().responseJSON(completionHandler: { (response) in
            self.getResultAndError(response) { (result, error) in
                completion(result, error)
            }
        })
    }
    
    static func callAPIDelete(_ path:String,  completion: @escaping (_ result: NSDictionary?, _ error: NSError?) -> Void) {
        Alamofire.request(self.getAPIHost() + self.VERSION_NUMBER + path, method: .delete, headers: DBHelper.getHeader()).validate().responseJSON(completionHandler: { (response) in
            self.getResultAndError(response) { (result, error) in
                completion(result, error)
            }
        })
        
    }
    
    static func getResultAndError(_ response:DataResponse<Any>, completion: (_ result: NSDictionary?, _ error: NSError?) -> Void){
        
        
        if let statuscode = response.response?.statusCode{
            switch statuscode{
                
            case 200...299:
                if let resultJSON = response.result.value {
                    completion(resultJSON as? NSDictionary, nil)
                } else {
                    completion(NSDictionary(), nil)
                }
            case 400:
                
                if let resultJSON = response.result.value {
                    let json = JSON(resultJSON)
                    if let errorMessage =  json["error"]["errors"]["email"]["message"].string{
                        let errorDict: [AnyHashable: Any] = [NSLocalizedDescriptionKey :  NSLocalizedString("message", value: errorMessage, comment: "")]
                        let err = NSError(domain: "LooplistAPIError", code: statuscode, userInfo:errorDict)
                        ErrorHandling.customErrorHandler(message: "Please enter a valid e-mail")
                        
                        completion(nil, err)
                    } else if let errorMessage =  json["error"]["errors"]["username"]["message"].string{
                        let errorDict: [AnyHashable: Any] = [NSLocalizedDescriptionKey :  NSLocalizedString("message", value: errorMessage, comment: "")]
                        let err = NSError(domain: "LooplistAPIError", code: statuscode, userInfo:errorDict)
                        ErrorHandling.customErrorHandler(message: "Please enter a valid username")
                        
                        completion(nil, err)
                    } else if let errorMessage = json["error"]["message"].string{
                        let errorDict: [AnyHashable: Any] = [NSLocalizedDescriptionKey :  NSLocalizedString("message", value: errorMessage, comment: "")]
                        let err = NSError(domain: "LooplistAPIError", code: statuscode, userInfo:errorDict)
                        ErrorHandling.customErrorHandler(message: "Error")
                        completion(nil, err)
                    } else {
                        let errorDict: [AnyHashable: Any] = [NSLocalizedDescriptionKey :  NSLocalizedString("message", value: "Error", comment: "")]
                        let err = NSError(domain: "LooplistAPIError", code: statuscode, userInfo:errorDict)
                        ErrorHandling.customErrorHandler(message: "Error")
                        completion(nil, err)
                    }
                } else {
                    let errorDict: [AnyHashable: Any] = [NSLocalizedDescriptionKey :  NSLocalizedString("message", value: "Error", comment: "")]
                    let err = NSError(domain: "LooplistAPIError", code: statuscode, userInfo:errorDict)
                    ErrorHandling.customErrorHandler(message: "Error")
                    completion(nil, err)
                }
            case 401:
                DropDownAlert.customErrorMessage("UNAUTHORIZED".localized)
                let errorDict: [AnyHashable: Any] = [NSLocalizedDescriptionKey :  NSLocalizedString("message", value: "Error", comment: "")]
                let err = NSError(domain: "LooplistUnauthorizedError", code: statuscode, userInfo:errorDict)
                completion(nil, err)
                //self.logoutAndClearDefaults()
                break;
            case 404:
                let errorDict: [AnyHashable: Any] = [NSLocalizedDescriptionKey :  NSLocalizedString("message", value: "404: Page Not Found", comment: "")]
                let err = NSError(domain: "LooplistAPIError", code: (response.response?.statusCode)!, userInfo:errorDict)
//                ErrorHandling.customErrorHandler(message: "404: Not Found!")
//                ErrorHandling.customErrorHandler(message: "Error!")

                completion(nil, err)
            case 502:
                let errorDict: [AnyHashable: Any] = [NSLocalizedDescriptionKey :  NSLocalizedString("message", value: "Bad Gateway", comment: "")]
                let err = NSError(domain: "LooplistAPIError", code: (response.response?.statusCode)!, userInfo:errorDict)
//                ErrorHandling.customErrorHandler(message: "Bad Gateway!")
                //ErrorHandling.customErrorHandler(message: "Error!")
                
                
                completion(nil, err)
            case 500:
                let errorDict: [AnyHashable: Any] = [NSLocalizedDescriptionKey :  NSLocalizedString("message", value: "Internal Server Error", comment: "")]
                let err = NSError(domain: "LooplistAPIError", code: (response.response?.statusCode)!, userInfo:errorDict)
//                ErrorHandling.customErrorHandler(message: "Internal Server Error!")
//                ErrorHandling.customErrorHandler(message: "Error!")
                completion(nil, err)
            default:
                break
            }
        }else if let error = response.result.error {
            
            switch error._code{
            case -1001:
                completion(nil, nil)
                ErrorHandling.customErrorHandler(message: "Slow Internet Connection!")
            case -1009:
                completion(nil, nil)
                ErrorHandling.customErrorHandler(message: "No Internet Connection!")
                
            default:
                //                let errorDict: [AnyHashable: Any] = [NSLocalizedDescriptionKey :  NSLocalizedString("message", value: "Default Error!", comment: "")]
                //                let err = NSError(domain: "LooplistAPIError", code: -100000, userInfo:errorDict)
                completion(nil, error as NSError?)
                //                DropDownAlert.customErrorMessage("Error!")
                //                ErrorHandling.customErrorHandler(message: "Error!")
                
            }
        }else{
            completion(nil, nil)
        }
    }
    
    static func getUserFromId(_ id:String, completion: @escaping (_ user: User?, _ error: NSError?) -> Void){
        self.callAPIGet(("users/" + id)) { (result, error) in
            if(error == nil) {
                completion(User(input: (result?["user"] as? Dictionary<String, AnyObject>)!), nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
    static func getJSONFromDictionary(_ input:NSDictionary) -> String {
        var theJSONText:NSString = ""
        do {
            let theJSONData = try JSONSerialization.data(withJSONObject: input, options: JSONSerialization.WritingOptions(rawValue: 0))
            theJSONText = NSString(data: theJSONData,
                                   encoding: String.Encoding.ascii.rawValue)!
        } catch{
        }
        return String(theJSONText)
    }
}
