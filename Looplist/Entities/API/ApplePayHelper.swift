////
////  ApplePayHelper.swift
////  Looplist
////
////  Created by Arun Sivakumar on 1/3/17.
////  Copyright © 2017 Looplist. All rights reserved.
////
//
//import Foundation
//import Stripe
//
//
//struct ApplePayHelper{
//
//
//    
////     weak var vc:UIViewController? = nil
////     weak var listing:Listing? = nil
////     weak var shipping:Shipping? = nil
////    
////    init(listing:Listing,vc:UIViewController){
////        super.init()
////        self.listing = listing
////        self.vc = vc
//////        self.loadPaymentForApplePay()
////    }
//    
////    func showApplePay(listing:Listing?,vc:UIViewController){
////        
//////        self.vc = vc
//////        self.listing = listing
////        
////        if let listing = listing{
////            APIHelperProduct.getPaymentData(listing){(success) in
////                if success{
////                    self.loadPaymentForApplePay(listing: listing,vc: vc)
////                }
////            }
////        }
////    }
//    
//    static func getPayment(listing:Listing?,completion:@escaping(_ success:Bool)->Void){
//        if let listing = listing{
//        APIHelperProduct.getPaymentData(listing){(success) in
//            if success{
//                completion(true)
//            }else{
//                completion(false)
//
//            }
//        }
//        }else{
//            completion(false)
//        }
//    }
//
//    
//    // SHOW APPLE PAY SCREEN
//    static func loadPaymentForApplePay(listing:Listing?,completion: @escaping (PKPaymentAuthorizationViewController?) -> ()){
//        
//        
//        if let listing = listing{
//            
//            self.getPayment(listing: listing, completion: { (success) in
//                if success{
//                    if let payment = listing.payment{
//                        if let subtotal = payment.subtotal,let tax = payment.tax, let service = payment.service,let shipping = payment.shipping, let total = payment.total{
//                            
//                            let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: ConstantsIdentification.ApplePayMerchantID)
//                            
//                            var summaryItems = [PKPaymentSummaryItem]()
//                            
//                            paymentRequest.requiredShippingAddressFields = PKAddressField.all
//                            
//                            summaryItems.append(PKPaymentSummaryItem(label: "Sub Total", amount: NSDecimalNumber(string:"\(subtotal.substringFromIndex(1)))")))
//                            summaryItems.append(PKPaymentSummaryItem(label: "Tax", amount: NSDecimalNumber(string:"\(tax.substringFromIndex(1))")))
//                            summaryItems.append(PKPaymentSummaryItem(label: "Services", amount: NSDecimalNumber(string:"\(service.substringFromIndex(1))")))
//                            summaryItems.append(PKPaymentSummaryItem(label: "Shipping", amount: NSDecimalNumber(string:"\(shipping.substringFromIndex(1))")))
//                            summaryItems.append(PKPaymentSummaryItem(label: "Total", amount: NSDecimalNumber(string:"\(total.substringFromIndex(1))")))
//                            
//                            paymentRequest.paymentSummaryItems = summaryItems
//                            
//                            if (Stripe.canSubmitPaymentRequest(paymentRequest)) {
//                                let paymentController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
//                                completion(paymentController)
//                                //                    paymentController.delegate = self
//                                //                    self.present(paymentController, animated: true, completion: nil)
//                                
//                                //                    vc?.present(paymentController, animated: true, completion: nil)
//                                
//                            } else {
//                                // Show the user your own credit card form (see options 2 or 3)
//                                ErrorHandling.defaultApplePayErrorHandler()
//                            }
//                        }
//                    }
//                }
//            })
//        }
//    }
//    
//
////    // handle the PKPayment that the payment authorization controller returns by implementing this protocol
////     func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
////        /*
////         We'll implement this method below in 'Creating a single-use token'.
////         Note that we've also been given a block that takes a
////         PKPaymentAuthorizationStatus. We'll call this function with either
////         PKPaymentAuthorizationStatusSuccess or PKPaymentAuthorizationStatusFailure
////         after all of our asynchronous code is finished executing. This is how the
////         PKPaymentAuthorizationViewController knows when and how to update its UI.
////         */
////        
////        
////        handlePaymentAuthorizationWithPayment(payment, completion: completion)
////    }
//    
//
//    
//    // create token
//     static func handlePaymentAuthorizationWithPayment(_ payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> ()) {
//        STPAPIClient.shared().createToken(with: payment) { (token, error) -> Void in
//            if error != nil {
//                completion(PKPaymentAuthorizationStatus.failure)
//                return
//            }
//            /*
//             We'll implement this below in "Sending the token to your server".
//             Notice that we're passing the completion block through.
//             See the above comment in didAuthorizePayment to learn why.
//             */
//            self.assignAppleShippingInfoToCurrentShipping(payment, completion: { (success) in
//                if (success != nil){
//                    self.createBackendChargeWithToken(token!, completion: completion)
//                }
//            })
//            
//        }
//    }
//    
////     func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
////                vc?.dismiss(animated: true, completion: nil)
////    }
//    
//     static func assignAppleShippingInfoToCurrentShipping(_ payment: PKPayment,completion:@escaping (_ shipping:Shipping?)->Void){
//        
//        if let firstName = payment.shippingContact?.name?.givenName, let lastName =  payment.shippingContact?.name?.familyName, let address1 = payment.shippingContact?.postalAddress?.street, let city = payment.shippingContact?.postalAddress?.city, let state = payment.shippingContact?.postalAddress?.state,let zip = payment.shippingContact?.postalAddress?.postalCode,  let phone =  payment.shippingContact?.phoneNumber {
//            
//            // Check if shipping address if available in DB ..if available get the object..
//            let shipping = Shipping(name: firstName + " " + lastName, address1: address1, address2: "", city: city, state: state, zip: zip, phone: phone.stringValue, instructions: " ")
//            
//            if let user = CurrentUser.sharedInstance.currentUser{
//                user.findAndGetShippingAddress(shipping, completion: { (shipping) in
//                    if let shipping = shipping{
////                        self.shipping = shipping
//                        completion(shipping)
//                    }else{
//                        completion(nil)
//                    }
//                })
//                
//            }
//            
//        }
//        //        currentShippingAddress = Shipping(name:(payment.shippingContact?.name?.givenName)! + (payment.shippingContact?.name?.familyName)!,address1:(payment.shippingContact?.postalAddress?.street)!,address2: "",city:(payment.shippingContact?.postalAddress?.city)!,state: (payment.shippingContact?.postalAddress?.state)!,zip: (payment.shippingContact?.postalAddress?.postalCode)!, phone: (payment.shippingContact?.phoneNumber?.stringValue)!, instructions: "")
//    }
//    
//    // MARK: -  /////////////////////////////////////////////////////    TRANSACTION    /////////////////////////////////////////////////////////////////////////
//    
//    //## backend charge
//    static func createBackendChargeWithToken(_ token: STPToken,listing:Listing,shipping:Shipping, completion: @escaping (PKPaymentAuthorizationStatus) -> ()) {
//        
//        //        if let listing = self.listing?.objectId, let shipping = self.shipping?.objectId, let listingNotes = self.listing?.optionalNotes{
//        if let listing = listing.objectId, let shipping = shipping.objectId{
//            
//            
//            let path =  "stripe/charge"
//            
//            DBHelper.callAPIPost(path, params:(["token": token.tokenId as AnyObject, "listing": listing as AnyObject, "transaction":["shipping":["address": shipping],"customerNotes":""]] as AnyObject) as! [String : AnyObject?], completion: { (result, error) in
//                
//                if let result = result{
//                    
//                    Analytics.sharedInstance.actionsArray.append(Action(context:["purchaseListingApplePay": listing], forScreen:nil, withActivityType:"Purchase Apple Pay")) // $AS
//                    
//                    
//                    completion(PKPaymentAuthorizationStatus.success)
//                    
//                    if let transactionDictionary = result["transaction"] as? NSDictionary{
//                        if let transactionId = transactionDictionary["_id"] as? String{
//                            
//                            self.showOrderConfirmationPage(transactionId)
//                        }
//                    }
//                    
//                }else if error != nil{
//                    
//                    completion(PKPaymentAuthorizationStatus.failure)
//                }
//            })
//            
//        }else{
//            completion(PKPaymentAuthorizationStatus.failure)
//        }
//    }
//    
////    func showOrderConfirmationPage(_ transationId:String){
////        
////        let confirmationNumberVC:ConfirmationNumberViewController = ConfirmationNumberViewController.instanceFromStoryboard("Purchase")
////        confirmationNumberVC.listing = self.listing
////        confirmationNumberVC.shipping = self.shipping
////        confirmationNumberVC.cc = nil
////        confirmationNumberVC.transactionId = transationId
////        self.vc?.navigationController?.pushViewController(confirmationNumberVC, animated: true)
////    }
//    
//}
