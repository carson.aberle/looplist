//
//  ConvertToJSON.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/28/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation

struct ConvertToJSON{
    
    static func shipping(_ address: Shipping) -> [String: AnyObject]{
        var shipping:[String: AnyObject] = [String: AnyObject]()
        shipping["name"] = address.name as AnyObject?
        shipping["address1"] = address.address1 as AnyObject?
        
        if let address2 = address.address2{
            shipping["address2"] = address2 as AnyObject?
        }
        shipping["city"] = address.city as AnyObject?
        shipping["state"] = address.state as AnyObject?
        shipping["zip"] = address.zip as AnyObject?
        shipping["phone"] = address.phone as AnyObject?
        if let instructions = address.instructions{
            shipping["instructions"] = instructions as AnyObject?
        }
        shipping["phone"] = address.phone as AnyObject?
        
        return shipping
    }
}
