//
//  APIHelper.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/22/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation
import Kingfisher

struct APIHelper{
    
    // MARK: -  //////////////////////////////////////////////////////   SEARCH   /////////////////////////////////////////////////////////////////////////
    
    // RETURNS THE QUERY FOR PRODUCT TYPED IN THE SEARCH FIELD
    // changed from searchTerm:String? to String 29 dec, 2016
    
    static func searchGetProductsCount(_ searchTerm:String,skip:Int,limit:Int,completion:@escaping (_ productCount:Int?,_ error:NSError?) -> Void){
        let path = "search/products"
        DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(["query":searchTerm,"skip":skip,"limit":limit])) { (result, error) in
            
            guard let result = result,let count = result["totalResults"] as? Int else {completion(nil,error); return }
            completion(count,nil)
        }
    }

    
    
    
    static func searchGetProducts(_ searchTerm:String,skip:Int,limit:Int,minPrice:Int?, maxPrice:Int?, colors:NSArray?,completion:@escaping (_ products:[Product]?,_ productAttributes:NSDictionary?,_ productCount:Int?,_ error:NSError?) -> Void){
        let path = "search/products"
        let dictionary = NSMutableDictionary()
        if let minPrice = minPrice{
            dictionary.setValue(minPrice, forKey: "minPrice")
        }
        if let maxPrice = maxPrice{
            dictionary.setValue(maxPrice, forKey: "maxPrice")
        }
        if let colors = colors{
            let stringRepresentation = colors.componentsJoined(by: " ")
            dictionary.setValue(stringRepresentation.replacingOccurrences(of: "#", with: ""), forKey: "colors")
        }
        dictionary.setValue(searchTerm, forKey: "query")
        dictionary.setValue(skip, forKey: "skip")
        dictionary.setValue(limit, forKey: "limit")
        DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(dictionary)) { (result, error) in
            
            guard let result = result,let productsArray = result["products"] as? NSArray else {completion(nil,nil,nil,error); return }
            var products = [Product]()
            var productAttributes:NSDictionary? = nil
            var count:Int? = nil
            
            
            for values in productsArray{
                
                if let product = values as? Dictionary<String, AnyObject> {
                    if let newProduct = Product(input:(product)){
                        products.append(newProduct)
                    }
                }
            }
            
            if let resultCount = result["totalResults"] as? Int{
                count = resultCount
            }
            if skip == 0{
                if let attributes = result["attributes"] as? NSDictionary{
                    productAttributes = attributes as NSDictionary?
                    
                }
            }
            completion(products,productAttributes,count,nil)

        }
    }
    
    static func discovery(skip:Int,limit:Int,completion:@escaping (_ products:[Product]?,_ productAttributes:NSDictionary?,_ productCount:Int?,_ error:NSError?) -> Void){
        if let objectId = CurrentUser.sharedInstance.currentUser?.objectId{
            let path = "users/\(objectId)/discovery"
            DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(["skip":skip,"limit":limit])) { (result, error) in
                
                guard let result = result,let productsArray = result["products"] as? NSArray else {completion(nil,nil,nil,error); return } // handle error
                
                var products = [Product]()
                var count:Int? = nil
                var productAttributes:NSDictionary? = nil
                
                for values in productsArray{
                    
                    if let product = values as? Dictionary<String, AnyObject> {
                        if let newProduct = Product(input:(product)){
                            products.append(newProduct)
                        }
                    }
                }
                
                if let resultCount = result["totalResults"] as? Int{
                    count = resultCount
                }
                if skip == 0{
                    if let attributes = result["attributes"] as? NSDictionary{
                        productAttributes = attributes as NSDictionary?
                        
                    }
                }
                completion(products,productAttributes,count,nil)
                
            }
        }
    }
    
    
    
    // RETURNS THE QUERY FOR USER TYPED IN THE SEARCH FIELD
    // changed from searchTerm:String? to String

    static func searchGetUsers(_ searchTerm:String,skip:Int,limit:Int,completion:@escaping (_ users:[User]?,_ error:NSError?) -> Void){
        
       let path = "search/users"

        DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(["query":searchTerm,"skip":skip,"limit":limit, "flags":["isFollowedByUser":CurrentUser.sharedInstance.currentUser!.objectId!, "isRequestedByUser":CurrentUser.sharedInstance.currentUser!.objectId!]])) { (result, error) in
            if let result = result{
                
                var users = [User]()
                
                if let usersArray = result["users"] as? NSArray{
                    
                    
                    for values in usersArray{
                        
                        if let user = values as?  Dictionary<String, AnyObject>{
                            let newUser = User(input:(user))
                            users.append(newUser)
                        }
                    }
                    completion(users,nil)
                }
                
            }else if let error = error{
                
                completion(nil,error)
            }
        }
    }
    
    
    // RETURNS PRODUCTS FOR THE HOMEFEED TABLE
    static func homeFeedQuery(skip:Int,limit:Int,completion:@escaping (_ activity:[ActivityHomeFeed]?,_ error:NSError?) -> Void){
        
        let jsonQuery = ["skip": skip, "limit": limit]
        let path = "misc/homefeed"
        
        DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(jsonQuery as NSDictionary)) { (result, error) in
            if let result = result{
                if let activities = result["activities"] as? NSArray{
                    
                    var userActivity = [ActivityHomeFeed]()
                    for (index,value) in activities.enumerated(){
                        if let activityDict = value as? Dictionary<String, AnyObject>,let activity = ActivityHomeFeed(input: activityDict) {
                            if let featuredImage = activity.product?.images?[0], let nsurl = URL(string:(featuredImage)){
                                if skip == 0 && (index == 0 || index == 1){
                                let imageView = UIImageView()
                                imageView.kf.setImage(with:nsurl, placeholder: UIImage(named: Constants.placeholder))
                                }
                                
                            }

                            userActivity.append(activity)
                            
                        }
                    }
                    completion(userActivity, nil)
                }
            }else if let error = error{
                completion(nil, error)
            }
        }
    }
    
    // RETURNS PRODUCTS FOR THE HOMEFEED TABLE
    static func homeFeedPoll(completion:@escaping (_ activity:ActivityHomeFeed?,_ error:NSError?) -> Void){
        
        let jsonQuery = ["select":"_id","skip": 0, "limit": 1,"sort":"-createdAt"] as [String : Any]
        let path = "misc/homefeed"
        
        DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(jsonQuery as NSDictionary)) { (result, error) in
            guard let result = result, let activities = result["activities"] as? NSArray, activities.count > 0,let activity = activities[0] as? Dictionary<String, AnyObject>, let userActivity = ActivityHomeFeed(input: activity)
                else { if let error = error{print(error)}; completion(nil, nil); return }
            completion(userActivity, nil)
            return
        }
    }
    
    
    static func addProductToCollection(collection:Collection, product:Product, completion:@escaping (_ success:Bool?) -> Void){
        if   let collectionId = collection.objectId, let productId = product.objectId, let collectionName = collection.name{
            DBHelper.callAPIPost("collections/\(collectionId)/products/\(productId)", completion: { (result, error) in
                if error == nil {
                    Analytics.sharedInstance.actionsArray.append(Action(context:["addProductToCollection": productId, "collection":collectionId], forScreen:nil, withActivityType:"Added Product To Collection"))
                    DropDownAlert.customSuccessMessage("SUCCESSFULLY_ADDED".localized + " \(product.brandName)" + "TO".localized + " \(collectionName)")
                    collection.containsProduct = true
                    APIHelperProduct.updateProduct(product, { (success) in
                        completion(true)
                    })
                } else {
                    completion(false)
                }
            })
        }
    }
    
    static func removeProductFromCollection(collection:Collection, product:Product, completion:@escaping (_ success:Bool?) -> Void){
        if   let collectionId = collection.objectId, let productId = product.objectId, let collectionName = collection.name{
            DBHelper.callAPIDelete("collections/\(collectionId)/products/\(productId)", completion: { (result, error) in
                
                if error == nil{
                    Analytics.sharedInstance.actionsArray.append(Action(context:["deleteCollection": collectionId], forScreen:nil, withActivityType:"Delete Collection"))
                    DropDownAlert.customSuccessMessage("\(product.brandName) " + "REMOVED_FROM".localized + " \(collectionName)!")
                    collection.containsProduct = false
                    APIHelperProduct.updateProduct(product, { (success) in
                        completion(true)
                    })
                } else {
                    completion(false)
                }
                
            })

        }
        
    }
    
    static func getRecommenderUsers(completion:@escaping (_ users:[User]?,_ error:NSError?) -> Void){
        DBHelper.callAPIGet("users", queryString: "{\"flags\":{\"isRequestedByUser\":\"\(CurrentUser.sharedInstance.currentUser!.objectId!)\",\"isFollowedByUser\":\"\(CurrentUser.sharedInstance.currentUser!.objectId!)\"},\"sort\":\"-followersCount\",\"limit\":25, \"where\":{\"_id\":{\"$ne\":\"\(CurrentUser.sharedInstance.currentUser!.objectId!)\"},\"authentication.level\":{\"$ne\":3}}}") { (result, error) in
            var users = [User]()
            
            if let result = result{
                if let usersArray = result["users"] as? NSArray{
                    for value in usersArray{
                        if let userObject = value as? Dictionary<String, AnyObject>{
                            let user = User(input:userObject)
                            users.append(user)
                        }
                    }
                    completion(users,nil)
                    
                }else if let error = error{
                    completion(nil, error)
                }
            }
        }
    }
}
