//
//  MessageManager.swift
//  Looplist
//
//  Created by Arun Sivakumar on 8/31/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation
import SwiftyJSON

class MessagingManager{
    
    // MARK: -  ////////////////////////////////////////////    MESSAGE READ/ UNDREAD/ COUNT    ////////////////////////////////////////////////////////////////
    
    static func getUnreadMessageCount(_ chatConversations:[String],completion:@escaping (_ count:Int?)->Void){
        if let userId = CurrentUser.sharedInstance.currentUser?.objectId{
            let query = "{\"where\":{\"chat\":{\"$in\":\(chatConversations)},\"readBy\":{\"$ne\":\"\(userId)\"}},\"count\":true}"
            let path = "messages"
            DBHelper.callAPIGet(path, queryString: query, completion: { (result, error) in
                if let result = result{
                    let data = JSON(result)
                    
                    if let count = data["messages"].int{
                        completion(count)
                    }else{
                        completion(nil)
                    }
                    
                }else{
                    completion(nil)
                }
            })
        }else{
            completion(nil)
        }
        
        
    }
    
    static func findMessageUnreadCount(_ completion:@escaping (_ count:Int?)->Void){
        
        if let userId = CurrentUser.sharedInstance.currentUser?.objectId{
            let query = "{\"select\":\"\(userId)\"}"
            let path = "chats"
            DBHelper.callAPIGet(path, queryString: query, completion: { (result, error) in
                if let result = result{
                    let data = JSON(result)
                    
                    if let messages = data["chats"].array, messages.count > 0{
                        
                        var chatConversations = [String]()
                        for values in messages{
                            if let chatId = values["_id"].string{
                                chatConversations.append(chatId)
                            }
                        }
                        self.getUnreadMessageCount(chatConversations, completion: { (count) in
                            if let count = count{
                                completion(count)
                            }else{
                                completion(nil)
                            }
                        })
                    }else{
                        completion(nil)
                    }
                }else{
                    completion(nil)
                }
            })
        }else{
            completion(nil)
        }
        
    }
    
    static func markMessageAsRead(_ chatId:String?,completion:@escaping (_ count:Int?) -> Void){
        
        if let chatId = chatId{
            let path = "chats/\(chatId)/read"
            DBHelper.callAPIPut(path, completion: { (result, error) in
                if let result = result{
                    
                    let data = JSON(result)
                    
                    if let count = data["count"].int, count >= 0 {
                        
                        self.findMessageUnreadCount({ (count) in
                            if let count = count{
                                completion(count)
                            }else{
                                completion(nil)
                            }
                        })
                    }else{
                        completion(nil)
                    }
                }else {
                
                    completion(nil)
                }
            })
        }
    }
    
    // MARK: -  ///////////////////////////////////////////////////    CHAT FUNCTIONS    ///////////////////////////////////////////////////////////////////////
    
    static func createChat(_ userId:String?, message:String?,listingId:String?,productId:String?,completion:@escaping (_ chatId:String?) -> Void){
        
        if let userId = userId{
            let users = [userId]
            
            var data:[String:String]? = nil
            
            if let message = message{
                data = ["message":message]
            }else if let productId = productId{
                data = ["product":productId]
            }
            
            if let data = data{
                let path = "chats"
                let messageDic:[String:AnyObject] = ["users":users as AnyObject,"message":data as AnyObject]
                
                DBHelper.callAPIPost(path, params: messageDic, completion: { (result, error) in
                    if let result = result{
                     
                        let data = JSON(result)
                        if let chatId = data["chat"]["_id"].string {
                            completion(chatId)
                        }else{
                            completion(nil)
                        }
                    }else {
                        completion(nil)
                    }
                    
                })
                
            }else{
                completion(nil)
            }
        }
    }
    
    static func sendMessage(_ chatId:String?,message:String?,listingId:String?,productId:String?,completion:@escaping (_ message:Message?) -> Void){
        
        var messageDic:[String:AnyObject] = [String:AnyObject]()
        
        if let chatId = chatId{
            if let message = message{
                messageDic = ["chat":chatId as AnyObject,"message":message as AnyObject]
                
            }else if let productId = productId{
                messageDic = ["chat":chatId as AnyObject,"product":productId as AnyObject]
                
            }
            
            if !messageDic.isEmpty{
                let path = "messages"
                DBHelper.callAPIPost(path, params: messageDic, completion: { (result, error) in
                    if let result = result{
                        Analytics.sharedInstance.actionsArray.append(Action(context: messageDic as NSDictionary, forScreen: nil, withActivityType:"Sent Message"))
                        var messageSent = JSON(result)
                        
                        if let messages = messageSent["message"].dictionary{
                            
                            let value = JSON(messages)
                            
                            if let message = self.convertToMessage(value){
                                completion(message)
                            }
                        }else{
                            completion(nil)
                        }
                    }else{
                        completion(nil)
                    }
                })
                
            }else{
                completion(nil)
            }
        }
    }
    
    // MARK: -  ///////////////////////////////////////////////////    GET  FUNCTIONS    ///////////////////////////////////////////////////////////////////////
    
    static func getChatWithUser(_ userId:String?,completion:@escaping (_ chatId:String?) -> Void){
        
        var query = ""
        if let userId = userId{
            query = "{\"where\":{\"users\":\"\(userId)\"},\"select\":\"_id\", \"flags\":{\"isFollowedByUser\":\"\(CurrentUser.sharedInstance.currentUser!.objectId!)\",\"isRequestedByUser\":\"\(CurrentUser.sharedInstance.currentUser!.objectId!)\"}}"
        }else if userId == nil{
            query = "{\"where\":{\"users\":{\"$size\":1}},\"select\":\"_id\", \"flags\":{\"isFollowedByUser\":\"\(CurrentUser.sharedInstance.currentUser!.objectId!)\",\"isRequestedByUser\":\"\(CurrentUser.sharedInstance.currentUser!.objectId!)\"}}"
        }
        
        if query != ""{
            
            let path = "chats"
            
            DBHelper.callAPIGet(path, queryString: query) { (result, error) in
                
                if let result = result{
                    let result = JSON(result)
                    
                    
                    if let chatId = result["chats"][0]["_id"].string{
                        completion(chatId)
                    }else{
                        completion(nil)
                    }
                }
            }
        }else{
            completion(nil)
        }
    }
    
    
   
    
    //GET MESSAGE FROM PUSH NOTIFICATION
    
     static func getMessageFromPushNotification(messageId:String?,completion:@escaping (_ messages:Message?) -> Void){
        guard let messageId = messageId else { completion(nil); return }

            let path = "messages/\(messageId)"
            let query = "{\"populate\":[{\"path\":\"from\",\"select\":\"_id firstName profileImageUrl\"},{\"path\":\"listing\",\"populate\":[{\"path\":\"product\"}]},{\"path\":\"product\"},{\"path\":\"chat\",\"select\":\"createdAt\"}]}"
        
        DBHelper.callAPIGet(path, queryString: query) { (result, error) in
            guard let result = result else{completion(nil); return}
            let receivedMessage = JSON(result["message"] as Any)
            guard let message = self.convertHistoryMessageToMessage(receivedMessage) else{completion(nil); return}
            //print(message)
            completion(message)
        }
    }

 // GET HISTORY MESSAGES 

    static func getMessages(_ skip:Int?,limit:Int?,chatId:String?,messageId:String?,completion:@escaping (_ messages:[Message]?,_ messageCount:Int?) -> Void){
        
        var query = ""
        var path = ""
        if let chatId = chatId, let skip = skip, let limit = limit{
        
            path = "messages"
            query = "{\"skip\":\(skip),\"limit\":\(limit), \"flags\":{\"isFollowedByUser\":\"\(CurrentUser.sharedInstance.currentUser!.objectId!)\",\"isRequestedByUser\":\"\(CurrentUser.sharedInstance.currentUser!.objectId!)\"},\"where\":{\"chat\":\"\(chatId)\"},\"sort\":\"-createdAt\",\"populate\":[{\"path\":\"from\",\"select\":\"_id firstName profileImageUrl\"},{\"path\":\"listing\",\"populate\":[{\"path\":\"product\"}]},{\"path\":\"product\"},{\"path\":\"chat\",\"select\":\"createdAt\"}]}"
        }else if let messageId = messageId{
            path = "messages/\(messageId)"
            query = "{\"populate\":[{\"path\":\"from\",\"select\":\"_id firstName profileImageUrl\"},{\"path\":\"listing\",\"populate\":[{\"path\":\"product\"}]},{\"path\":\"product\"},{\"path\":\"chat\",\"select\":\"createdAt\"}]}"
        }
        
        if query != "" && path != ""{
            
            
            DBHelper.callAPIGet(path, queryString: query) { (result, error) in
                if let result = result{
                    //print(result)
                    var messageHistory = JSON(result)
                    if let messages = messageHistory["messages"].array, messages.count > 0{
                        
                        
                        var messageData = [Message]()
                        
                        for values in messages{
                            
                            if let message = self.convertHistoryMessageToMessage(values){
                                messageData.insert(message, at: 0)
                            }
                            
                        }
                        completion(messageData,messages.count)
                    }else{
                        completion(nil,nil)
                    }
                    
                    
                }else{
                    completion(nil,nil)
                }
                
            }
        }else{
            completion(nil,nil)
        }
    }
    
    static func getConversations(_ getMavenConversation:Bool,chatId:String?,skip:Int?,limit:Int?,completion:@escaping (_ conversation:[MessageConversation]?) -> Void){
        
        var query = ""
        
        guard let _ = CurrentUser.sharedInstance.currentUser?.objectId else{
            completion(nil);return
        }
        if getMavenConversation{
            //Maven
             query = "{\"where\":{\"isMavenChat\":true},\"populate\":[{\"path\":\"lastMessage\",\"populate\":[{\"path\":\"from\"}, {\"path\":\"product\"},{\"path\":\"listing\"}]}], \"sort\":\"-updatedAt\"}"

        }else if let skip = skip, let limit = limit{
            //Pagination
            query = "{\"where\":{\"isMavenChat\":false},\"populate\":[{\"path\":\"lastMessage\",\"populate\":[{\"path\":\"from\"}, {\"path\":\"product\"}]},{\"path\":\"users\"}], \"sort\":\"-updatedAt\",\"skip\":\(skip),\"limit\":\(limit)}"
        }else{
            completion(nil);return
        }
        
        if query != ""{
            let path = "chats"
            
            DBHelper.callAPIGet(path, queryString: query) { (result, error) in
            
                if let result = result{
                 //print(result)
                    var chatHistory = JSON(result)
                    // list of conversations
                    var messageConversations = [MessageConversation]()
                    
                    if let conversations = chatHistory["chats"].array, conversations.count > 0{
                        
                        for values in conversations{
                            
                            if let messageConversation = self.convertMessageConversation(values){
                                      messageConversations.append(messageConversation)
                            }
                        }
                        // send back the list of conversations
                        completion(messageConversations)
                        
                    }else{
                        completion(messageConversations)
                    }
                }else {
                  
                    completion(nil)
                }
            }
        }else{
            completion(nil)
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////    CONVERT MESSAGES    ///////////////////////////////////////////////////////////////////////
    
    // SEND MASSAGE
    static func convertToMessage(_ result:JSON) -> Message?{
        
        
        if let serializedDate = result["createdAt"].string,let chatId = result["chat"].string {
            
            let date = HelperDate.formatDataBaseDate(serializedDate)
            var fromId:String? = nil
            
            // sender
            if let objectId = result["from"].string{
                fromId = objectId
            }
            
            // message text/listing/product/picture
            
            if let textMessage = result["message"].string{
                let message = Message(fromId: fromId, chatId: chatId, textMessage: textMessage, listing: nil, product: nil, date: date)
                return message
            }else if result["listing"].string != nil{
                
                let message = Message(fromId: fromId, chatId:  chatId, textMessage: nil, listing: nil, product: nil, date: date)
                return message
                
            }else if result["product"].string != nil{
                
                let message = Message(fromId: fromId, chatId: chatId, textMessage: nil, listing: nil, product: nil, date: date)
                return message
                
            }
        }
        return nil
        
    }
    
    // MESSAGE CONVERSATION
    static func convertHistoryMessageToMessage(_ result:JSON) -> Message?{
        
        if let serializedDate = result["createdAt"].string,let chatId = result["_id"].string {
            
            let date = HelperDate.formatDataBaseDate(serializedDate)
            var fromId:String? = nil
            
            // sender
            if let objectId = result["from"]["_id"].string{
                fromId = objectId
            }
            
            // message text/listing/product/picture
            if let product = result["product"].dictionaryObject{
                if let product = Product(input: product as Dictionary<String, AnyObject>){
                    //                if let _ =  product.objectId{
                    let message = Message(fromId: fromId, chatId: chatId, textMessage: nil, listing: nil, product: product, date: date)
                    return message
                }
            }else if let listing = result["listing"].dictionaryObject{
                if let listing = Listing(input: listing as Dictionary<String, AnyObject>){
//                if let _ =  listing.objectId{
                
                    let message = Message(fromId: fromId, chatId:  chatId, textMessage: nil, listing: listing, product: nil, date: date)
                    return message
                }
            }else  if let textMessage = result["message"].string{
                let message = Message(fromId: fromId, chatId: chatId, textMessage: textMessage, listing: nil, product: nil, date: date)
                return message
            }
        }
        return nil
        
    }
    
    // INBOX
    static func convertMessageConversation(_ values:JSON)-> MessageConversation?{
        guard let currentUserObjectId = CurrentUser.sharedInstance.currentUser?.objectId else {return nil}
        //print(values)
        
        // 1. Get User
        
        // NIL for MAVEN
        var receivedUser:User? = nil
        
        if let users = values["users"].array, let userType = values["isMavenChat"].int{
    
            if  users.count == 1 &&  userType == 1{
                // Maven
                receivedUser = nil
            }else if users.count == 2 && userType == 0{
                
                // Others
                if let userObject = users[1].dictionaryObject{
                    let user = User(input: userObject as Dictionary<String, AnyObject>)
                    
                    if let objectId = user.objectId, objectId != currentUserObjectId{
                        receivedUser = user
                    }else if let userObject = users[0].dictionaryObject{
                        let user = User(input: userObject as Dictionary<String, AnyObject>)
                        if let objectId = user.objectId, objectId != currentUserObjectId{
                            receivedUser = user
                        }
                    }
                }
                
            }else {
                return nil
            }
        }
        
        
        // 2. Get chat
        var receivedChatId:String?
        
        if let chatId = values["_id"].string{
            receivedChatId = chatId
        }
        
        // 3. Get message (text or listing or product)
        var receivedLastMessage:String? = ""
        
        if let _ = values["lastMessage"]["product"].dictionaryObject{
            if let name = values["lastMessage"]["product"]["name"].string{
                receivedLastMessage = name
            }else{
                receivedLastMessage = "Product"

            }
        }else if let _ = values["lastMessage"]["listing"].dictionaryObject{ //### chck for MAVEN
            
            if let name = values["lastMessage"]["listing"]["product"]["name"].string{
                receivedLastMessage = name
            }else{
                receivedLastMessage = "Listing"
            }
        }else if let lastMessage = values["lastMessage"]["message"].string{
            receivedLastMessage = lastMessage
        }
        if receivedLastMessage == nil{
            receivedLastMessage = "Message"
        }

        
        // 4.Get messageDate
        var date:Date?
        
        if let serializedDate = values["lastMessage"]["createdAt"].string{
            date = HelperDate.formatDataBaseDate(serializedDate)
        }
        
        // 5.read unread
        var read = false
        
        if let objectId =  values["lastMessage"]["readBy"][0].string, objectId == currentUserObjectId{
            read = true
        }else  if let objectId =  values["lastMessage"]["readBy"][1].string, objectId == currentUserObjectId{
            read = true
        }
        
        
        // init message conversation
        if let receivedChatId = receivedChatId, let receivedLastMessage = receivedLastMessage,let date = date{
            let messageConversation = MessageConversation(user: receivedUser,chatId:receivedChatId,userLastMessage:receivedLastMessage,date: date, read: read)
            return messageConversation
        }
        return nil
    }
}
