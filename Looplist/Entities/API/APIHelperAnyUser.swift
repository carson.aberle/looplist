//
//  APIHelperAnyUser.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/28/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation


struct APIHelperAnyUser{
    
    
    static func getGuestUser(_ objectId:String?,completion:@escaping (_ user:User?)->Void){
        if let objectId = objectId{
            let path = "users/" + objectId
            //            Alamofire.request(DBHelper.getPath(path), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: DBHelper.getHeader()).validate().responseJSON(completionHandler: { (response) in
            //                switch response.result {
            //                case .success(_):
            //                    if let value = response.result.value {
            //                        let result = JSON(value)
            //                        if let user = result["user"].dictionaryObject {
            //                            let user = User(input:user as Dictionary<String, AnyObject>)
            //                            completion(user)
            //                        }
            //                    }
            //                case .failure(_):
            //                    completion(nil)
            //                }
            //            })
            
            DBHelper.callAPIGet(path, completion: { (result, error) in //### carson test this
                if let result = result{
                    if let user = result["user"] as? Dictionary<String, AnyObject> {
                        let user = User(input:user)
                        completion(user)
                    }
                }else if let _ = error{
                    
                }
            })
        }
    }
    
    static func getUserFollowers(_ fromFollowers:Bool = true,user:User,searchFollower:String?,skip:Int,limit:Int,completion:@escaping (_ users:[User]?) -> Void){
        
        if let userId = user.objectId{
            
            var path:String?
            var queryString:String?
            
            if fromFollowers{
                path = "search/users/\(userId)/followers"
                
                if let searchFollower = searchFollower, searchFollower == "",let currentUserObjectId = CurrentUser.sharedInstance.currentUser?.objectId{
                    queryString = DBHelper.getJSONFromDictionary(["query":"","skip":skip,"flags":["isRequestedByUser":currentUserObjectId, "isFollowedByUser":currentUserObjectId],"limit":limit])
                    
                }else{
                    queryString = DBHelper.getJSONFromDictionary(["query":"\(searchFollower)","skip":skip,"flags":["isRequestedByUser":userId, "isFollowedByUser":userId],"limit":limit])
                }
            }else{
                
                if let searchFollower = searchFollower, searchFollower != ""{
                    path = "search/users"
                    queryString = DBHelper.getJSONFromDictionary(["query":searchFollower,"flags":["isRequestedByUser":userId, "isFollowedByUser":userId],"skip":skip,"limit":limit])
                }else{
                    path = "search/users/\(userId)/followers"
                    queryString = DBHelper.getJSONFromDictionary(["query":"","skip":skip,"flags":["isRequestedByUser":userId, "isFollowedByUser":userId],"limit":limit])
                }
            }
            
            if let path = path, let queryString = queryString{
                DBHelper.callAPIGet(path,queryString:queryString) { (result, error) in
                    
                    guard let result = result, let users = result["users"] as? NSArray else {
                        //handle error
                        completion(nil)
                        return
                    }
                    
                    var followers = [User]()
                    for values in users{
                        
                        if let user = values as? Dictionary<String, AnyObject> {
                            let newUser = User(input:(user))
                            followers.append(newUser)
                        }
                    }
                    completion(followers)
                }
            }else{
                completion(nil)
            }
            
        }
    }
    
    
    
    static func getUserFollowings(_ user:User,searchFollower:String?,skip:Int,limit:Int,completion:@escaping (_ users:[User]?) -> Void){
        
        if let objectId = user.objectId{
            
            
            let path = "search/users/\(objectId)/following"
            var queryString = ""
            if let userId = user.objectId,let currentUserObjectId = CurrentUser.sharedInstance.currentUser?.objectId{
                if let searchFollower = searchFollower, searchFollower == ""{
                    queryString = DBHelper.getJSONFromDictionary(["query":"","skip":skip,"flags":["isRequestedByUser":currentUserObjectId, "isFollowedByUser":currentUserObjectId],"limit":limit])
                }else{
                    queryString = DBHelper.getJSONFromDictionary(["query":"\(searchFollower)","skip":skip,"flags":["isRequestedByUser":userId, "isFollowedByUser":userId],"limit":limit])
                }
            }
            
            
            DBHelper.callAPIGet(path,queryString:queryString) { (result, error) in
                
                guard let result = result, let users = result["users"] as? NSArray else {
                    //handle error
                    completion(nil)
                    return
                }
                
                var followers = [User]()
                for values in users{
                    
                    if let user = values as? Dictionary<String, AnyObject> {
                        let newUser = User(input:(user))
                        followers.append(newUser)
                    }
                }
                completion(followers)
                
            }
        }
    }
    
    //    static func getUserFollowers(user:User,skip:Int,limit:Int,completion:@escaping (_ followers:[User]?) -> Void){
    //
    //        if let objectId = user.objectId{
    //
    //            let path = "users"
    //            let queryString = "{\"where\":{\"following\":\"\(objectId)\"},\"skip\":\(skip),\"limit\":\(limit),\"sort\":\"firstName\"}"
    //
    //            DBHelper.callAPIGet(path, queryString: queryString){ (result, error) in
    //
    //                guard let result = result, let users = result["users"] as? NSArray else {
    //                    //handle error
    //                    completion(nil)
    //                    return
    //                }
    //
    //                var followers = [User]()
    //                for values in users{
    //
    //                    if let user = values as? NSDictionary {
    //                        let newUser = User(input:(user) as! Dictionary<String, AnyObject>)
    //                        followers.append(newUser)
    //                    }
    //                }
    //                completion(followers)
    //            }
    //        }
    //    }
    
    
    
    //    static func getUserFollowings(user:User,skip:Int,limit:Int,completion:@escaping (_ following:[User]?) -> Void){
    //
    //        if let objectId = user.objectId{
    //
    //            let path = "users"
    //            let queryString = "{\"where\":{\"followers\":\"\(objectId)\"},\"skip\":\(skip),\"limit\":\(limit),\"sort\":\"firstName\"}"
    //
    //            DBHelper.callAPIGet(path, queryString: queryString) { (result, error) in
    //                guard let result = result, let users = result["users"] as? NSArray else {
    //                    // Handle Error
    //                    completion(nil)
    //                    return
    //                }
    //                var followings = [User]()
    //                for values in users{
    //
    //                    if let user = values as? NSDictionary {
    //                        let newUser = User(input:(user) as! Dictionary<String, AnyObject>)
    //                        followings.append(newUser)
    //                    }
    //                }
    //                completion(followings)
    //
    //            }
    //        }
    //    }
    
    
    static func getUserLikes(user:User,skip:Int,limit:Int,completion:@escaping (_ collections:[Product]?) -> Void){
        
        print(skip)
        print(limit)
        
        var productsArray = [Product]()
        
        
        if let objectId = user.objectId,let currentUserObjectId = CurrentUser.sharedInstance.currentUser?.objectId{
            
            let path = "collections"
            
            
            DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(["where":["users":objectId, "isLikesCollection":true],"flags":["isInUsersCollections":currentUserObjectId,"isLikedByUser":currentUserObjectId, "isRepostedByUser":currentUserObjectId], "populate":[ ["path": "products","options":["skip":skip,"limit":limit]],["path": "owner"]]])) { (result, error) in
                guard let result  = result, let collectionsArray:NSArray = result["collections"] as? NSArray else { completion(productsArray); return}
                
                
                for dictionary in collectionsArray{
                    let collection = Collection(input: dictionary as! [String:AnyObject])
                    if let products = collection.products {
                        for product in products{
                            productsArray.append(product)
                        }
                    }
                }
//                productsArray.reverse()
                completion(productsArray)
                
            }
        }else{
            completion(nil)
        }
    }
    
    // GET COLLECTIONS BY THIS USER
    
    static func getUserCollections(user:User,skip:Int,limit:Int,completion:@escaping (_ collections:[Collection]?) -> Void){
        
        if let objectId = user.objectId{
            
            let path = "collections"
            
           //  DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(["where":["users":objectId, "isLikesCollection":false],"sort":"-updatedAt","skip":skip,"limit":limit, "populate":[["path": "owner"], ["path": "products", "select":"images"]]])) { (result, error) in
            
            DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(["where":["users":objectId, "isLikesCollection":false],"skip":skip,"limit":limit, "populate":[["path": "owner"], ["path": "products", "select":"images"]]])) { (result, error) in
                
                var collections = [Collection]()
                
                guard let result  = result, let collectionsArray:NSArray = result["collections"] as? NSArray else{ completion(collections); return}
                
//                print(result)
                for dictionary in collectionsArray{
                    let collection = Collection(inputProductImagesOnly: dictionary as! [String:AnyObject])
                    collections.append(collection)
                }
                for index in 0..<collections.count{ // makes added products collection to be the last one - refactor the code
                    if collections[index].isAddedProductCollection == true{
                        let addedProductsCollection = collections[index]
                        collections.remove(at: index)
                        collections.append(addedProductsCollection)
                    }
                }
                completion(collections)
                
                
                
            }
        }else{
            completion(nil)
        }
    }
    
    static func getUserCollectionsFull(userId: String, collectionId: String, skip:Int,limit:Int,completion:@escaping (_ collection:Collection?) -> Void){
            let path = "collections/\(collectionId)"

            DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(["flags":["isInUsersCollections":CurrentUser.sharedInstance.currentUser!.objectId!,"isLikedByUser":userId, "isRepostedByUser":userId], "populate":[["path": "products","options":["limit":limit, "skip":skip]]]])) { (result, error) in
        
//        DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(["flags":["isInUsersCollections":CurrentUser.sharedInstance.currentUser!.objectId!,"isLikedByUser":userId, "isRepostedByUser":userId],"skip":skip,"limit":limit, "populate":[["path": "products"]]])) { (result, error) in

                guard let result  = result, let dictionary = result["collection"] as? NSDictionary else{
                    completion(nil);
                    return
                }
                print(result)
                let collection = Collection(input: dictionary as! [String:AnyObject])
                for values in collection.products!{
                    print(values.brandName)
                }
                completion(collection)
            }
    }
    
    static func getUserActivities(user:User,skip:Int,limit:Int,completion:@escaping (_ collections:[Activity]?) -> Void){
        
        var activities = [Activity]()
        
        if let objectId = user.objectId,let currentUserObjectId = CurrentUser.sharedInstance.currentUser?.objectId{
            
            let path = "activities"
            
            
            DBHelper.callAPIGet(path, queryString: DBHelper.getJSONFromDictionary(["flags":["isRequestedByUser":currentUserObjectId, "isFollowedByUser":currentUserObjectId, "isInUsersCollections":currentUserObjectId,"isLikedByUser":currentUserObjectId, "isRepostedByUser":currentUserObjectId],"skip":skip,"limit":limit, "where":["user":objectId, "activityType":["$in": ["Like Product", "Repost Product"]]],"sort":"-updatedAt", "populate":[["path":"user"],["path": "product", "options":["sort":"-updatedAt"]]]])) { (result, error) in
                
                guard let result  = result, let activitiesArray:NSArray = result["activities"] as? NSArray else {completion(activities); return}
                
                for dictionary in activitiesArray{
                    let activity = Activity(input: dictionary as! [String:AnyObject])
                    if let unwrappedActivity = activity{
                        activities.append(unwrappedActivity)
                    }
                }
                completion(activities)
                
            }
        }else{
            completion(nil)
        }
        
    }
}
