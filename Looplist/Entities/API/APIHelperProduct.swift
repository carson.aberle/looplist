 //
//  APIHelperProduct.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/27/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation


struct APIHelperProduct{
    
    // ADD PRODUCT
    static func addProduct(webProduct:WebProduct?,_ completion:@escaping (_ product:Product?,_ error: NSError?) -> Void){
        
        if let webProduct = webProduct{
            if let productUrl = webProduct.productUrl , let productTitle = webProduct.productTitle, let productImageUrl = webProduct.productImageUrl{
                
                let path = "products/add"
                
                var parameter:[String:AnyObject] = ["url":productUrl as AnyObject,"imageUrl":productImageUrl as AnyObject,"name":productTitle as AnyObject]
                
                if let productPrice = webProduct.productPrice{
                    parameter = ["url":productUrl as AnyObject,"imageUrl":productImageUrl as AnyObject,"name":productTitle as AnyObject,"price":productPrice as AnyObject]
                }
                
                DBHelper.callAPIPost(path, params: parameter, completion: { (result, error) in
                    if let result = result{
                        if let product = Product(input: result["product"] as! Dictionary<String,AnyObject>){
                            completion(product,nil)
                        }else{
                            completion(nil,nil)
                        }
                        
                    }else if let error = error{
                        completion(nil,error)
                    }
                })
            }
        }
    }
    
    // LISTING
    static func getPaymentData(_ listing:Listing,_ completion:@escaping (_ success: Bool) -> Void){ // Lazy load
        
        if let listingId = listing.objectId{
            
            let path = "misc/price"
            let queryString = "{\"listingId\":\"\(listingId)\"}"
            
            DBHelper.callAPIGet(path, queryString: queryString, completion: { (result, error) in
                if let result = result{
                    if let payment = Payment(input:result){
                        listing.payment = payment
                        completion(true)
                    }else{
                        // cannot init Analytics
                        completion(false)
                        
                    }
                }else if let _ = error{
                    // cannot get payment Handle errror
                    completion(false)
                }
            })
        }
    }
    
    // PRODUCT
    
    static func likeProduct(_ product:Product,_ completion:@escaping (_ success: Bool) -> Void){
        
        if let productId = product.objectId,let userId = CurrentUser.sharedInstance.currentUser?.objectId{
            
            let path = "users/\(userId)/likes/products/\(productId)"
            
            DBHelper.callAPIPost(path, completion: { (result, error) in
                if error == nil {
                    Analytics.sharedInstance.actionsArray.append(Action(context: ["product":productId], forScreen: nil, withActivityType:"Liked"))
                    product.isLikedByCurrentUser = true
                    completion(true)
                } else {
                    product.isLikedByCurrentUser = true
                    completion(true)
                }
            })
        }
    }
    
    static func unlikeProduct(_ product:Product,_ completion:@escaping (_ success: Bool) -> Void){
        
        if let productId = product.objectId,let userId = CurrentUser.sharedInstance.currentUser?.objectId{
            
            let path = "users/\(userId)/likes/products/\(productId)"
            
            DBHelper.callAPIDelete(path, completion: { (result, error) in
                if error == nil {
                    Analytics.sharedInstance.actionsArray.append(Action(context: ["product":productId], forScreen: nil, withActivityType:"Unliked"))
                    product.isLikedByCurrentUser = false
                    completion(true)
                } else {
                    completion(true)
                    product.isLikedByCurrentUser = false
                }
            })
        }
    }
    
    static func repostProduct(_ product:Product,_ completion:@escaping (_ success: Bool) -> Void){
        
        if let productId = product.objectId,let userId = CurrentUser.sharedInstance.currentUser?.objectId{
            
            let path = "users/\(userId)/reposts/products/\(productId)"
            
            DBHelper.callAPIPost(path, completion:{ (result, error) in
                if error == nil{
                    product.isRepostedByCurrentUser = true
                    Analytics.sharedInstance.actionsArray.append(Action(context: ["product":productId], forScreen: nil, withActivityType:"Reposted"))
                    completion(true)
                } else {
                    completion(false)
                }
            })
        }
    }
    
    static func removeRepostProduct(_ product:Product,_ completion:@escaping (_ success: Bool) -> Void){
        
        if let productId = product.objectId,let userId = CurrentUser.sharedInstance.currentUser?.objectId{
            
            let path = "users/\(userId)/reposts/products/\(productId)"
            
            DBHelper.callAPIDelete(path, completion:{ (result, error) in
                if error == nil{
                    product.isRepostedByCurrentUser = false
                    Analytics.sharedInstance.actionsArray.append(Action(context: ["product":productId], forScreen: nil, withActivityType:"Unreposted"))
                    completion(true)
                } else {
                    completion(false)
                }
            })
        }
    }
    
    static func getVariations(_ product:Product,_ completion:@escaping (_ success: Bool) -> Void){
        
        if let productId = product.objectId{
            
            let path = "products/\(productId)"
            let queryString = DBHelper.getJSONFromDictionary(["select":"variations","populate":[["path":"variations"]]])
            
            DBHelper.callAPIGet(path,queryString: queryString){
                (result, error) in
                
                guard let result = result,
                    let input = result["product"] as? Dictionary<String,AnyObject>,
                    let variations = input["variations"] as? [Dictionary<String,AnyObject>]
                    else{
                        completion(false); return
                }
                print(result)
                product.productVariations.removeAll()
                for (value) in variations{
                    if let productNew = Product(input: value){
                        product.productVariations.append(productNew)
                    }else{
                        // Analytics
                    }
                }
                product.productVariations.count > 0 ?  completion(true) : completion(false)
            }
        }else{
            completion(false)
        }
    }
    static func updateProduct(_ product:Product,_ completion:@escaping (_ success: Bool) -> Void){
        
        if let productId = product.objectId{
            
            let path = "products/\(productId)"
            
            DBHelper.callAPIGet(path,queryString: DBHelper.getJSONFromDictionary(["flags":["isInUsersCollections":CurrentUser.sharedInstance.currentUser!.objectId!,"isLikedByUser":CurrentUser.sharedInstance.currentUser!.objectId, "isRepostedByUser":CurrentUser.sharedInstance.currentUser!.objectId!]])){
                (result, error) in
                
                guard let result = result,
                    let input = result["product"] as? Dictionary<String,AnyObject>
                else{
                    completion(false); return
                }
                print(result)
                product.populateProduct(input)
                completion(true)
            } // end of API call
        }else{
            completion(false)
        }
    }
    
    static func getUsersWhoLikedProduct(_ productId: String, _ completion:@escaping (_ productLiked:[String]?, _ success: Bool) -> Void){
        if productId != "" {
            DBHelper.callAPIGet("products/\(productId)", queryString: DBHelper.getJSONFromDictionary(["populate":[["path":"likes", "sort": "-followersCount"]],"limit":4])) { (result, error) in
                if error == nil{
                    if let product = result?["product"] as? NSDictionary{
                        if let users = product["likes"] as? NSArray{
                            var userlikingUsersFirstFourURLs = [String]()
                            for user in users{
                                if let userDic = user as? Dictionary<String, AnyObject>{
                                    let user = User(input: userDic)
                                    if user.userName != "mcurator" && user.userName != "fcurator" && user.userName != "nacurator" && user.userName != CurrentUser.sharedInstance.currentUser?.userName{ // ### $AS added this check remove this in the future
                                        if let url = user.profileImageUrl{
                                            userlikingUsersFirstFourURLs.append(url)
                                        }
                                    }
                                }
                            }
                            completion(userlikingUsersFirstFourURLs, true)
                        }
                        completion([], false)
                    }
                    completion([], false)
                } else {
                    completion([], false)
                }
            }
            
        } else {
            completion([], false)
        }
    }
    
    static func getProductListingsWithParameters(_ params: String, _ product: Product?, _ completion:@escaping (_ listings:[Listing], _ success: Bool) -> Void){
        DBHelper.callAPIGet("listings", queryString: params, completion: { (result, error) in
            if error == nil{
                if let listings = result?["listings"] as? NSArray{
                    var allListings = [Listing]()
                    for listing in listings {
                        if let aListing = listing as? Dictionary<String, AnyObject>{
                            let theListing = Listing(input:aListing)
                            if let finalListing = theListing{
                                finalListing.product = product
                                finalListing.setDefaultProductValuesToListing()
                                allListings.append(finalListing)
                            }
                        }
                    }
                    
                    completion(allListings, true)
                } else {
                    completion([], false)
                }
            } else {
                completion([], false)
            }
        })
    }
    
    static func createBackendChargeWithToken(_ params: [String : AnyObject?]?, _ completion:@escaping (_ transactionID: String, _ success: Bool) -> Void){
        let path =  "stripe/charge"
        if let parameters = params {
            var listingId = ""
            if let id = parameters["listing"] as! String? {
                listingId = id
            }
            DBHelper.callAPIPost(path, params: parameters, completion: { (result, error) in
                if let result = result{
                    if let transactionId = result["transaction"] as? String{
                        Analytics.sharedInstance.actionsArray.append(Action(context:["listing": listingId,"paymentMethod":"Apple Pay"], forScreen:nil, withActivityType:"Purchased Product")) // $AS
                        completion(transactionId, true)
                    } else {
                        completion("", false)
                    }
                }else if error != nil{
                    completion("", false)
                }
            })
        } else {
            completion("", false)
        }
    }
}
