//
//  ObjectStateHelper.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/28/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit


struct ObjectStateHelper{
    
    
    static func userActionForFollowerFollowingButton(user:User?,button:UIButton?,grey:Bool){
     
        if let user = user, let button = button{
            
            button.isEnabled = false
            
            switch user.followRequestStatus{
                
            case .follow(let value):
                if value == true{ //UnFollow
                    
                    
                    APIHelperUser.unFollowUser(user:user,{ (success) in
                        if success{
//                            if grey{
                                //Set as grey fro user profile
                                user.followRequestStatus = .follow(false) //
                                button.setTitle("Follow", for: UIControlState())
                                button.setTitleColor(UIColor.darkGray, for: UIControlState())
                                button.backgroundColor = UIColor.clear
                                button.layer.borderColor = UIColor.darkGray.cgColor
                                button.isEnabled = true
//                            } else {
                                //Set as blue for lists
//                                user.followRequestStatus = .follow(false) //
//                                button.setTitle("Follow", for: UIControlState())
//                                button.setTitleColor(ConstantsColor.looplistColor, for: UIControlState())
//                                button.backgroundColor = UIColor.clear
//                                button.layer.borderColor = ConstantsColor.looplistColor.cgColor
//                                button.isEnabled = true
//                            }
                            
                        }else{
                            button.isEnabled = true
                        }
                    })
                    
                    
                    
                    
                }else{      //Follow
                    
                    APIHelperUser.followUser(user:user,{ (success) in
                        if success{
//                            if grey{
                                //Set as grey fro user profile
                                user.followRequestStatus = .follow(true) //
                                button.setTitle("Following", for: UIControlState())
                                button.setTitleColor(UIColor.white, for: UIControlState())
                                button.backgroundColor = UIColor.darkGray
                                button.layer.borderColor = UIColor.darkGray.cgColor
                                button.isEnabled = true
//                            } else {
                                //Set as blue for lists
//                                user.followRequestStatus = .follow(true) //
//                                button.setTitle("Following", for: UIControlState())
//                                button.setTitleColor(UIColor.white, for: UIControlState())
//                                button.backgroundColor = ConstantsColor.looplistColor
//                                button.layer.borderColor = ConstantsColor.looplistColor.cgColor
//                                button.isEnabled = true
//                            }
                            
                        }else{
                            button.isEnabled = true
                        }
                    })
                    
                }
                
            case .request(let value):
                if value == true{    //Request
                    
                    APIHelperUser.unFollowUser(user:user,{ (success) in
                        if success{
//                            if grey{
                                //Set as grey fro user profile
                                user.followRequestStatus = .request(false) //
                                button.setTitle("Request to follow", for: UIControlState())
                                button.setTitleColor(UIColor.darkGray, for: UIControlState())
                                button.backgroundColor = UIColor.clear
                                button.layer.borderColor = UIColor.darkGray.cgColor
                                button.isEnabled = true
//                            } else {
                                //Set as blue for lists
//                                user.followRequestStatus = .request(false) //
//                                button.setTitle("Request to follow", for: UIControlState())
//                                button.setTitleColor(ConstantsColor.looplistColor, for: UIControlState())
//                                button.backgroundColor = UIColor.clear
//                                button.layer.borderColor = ConstantsColor.looplistColor.cgColor
//                                button.isEnabled = true
//                            }
                            
                        }else{
                            button.isEnabled = true
                        }
                    })
                    
                }else{  //Un-Request
                    
                    APIHelperUser.followUser(user:user,{ (success) in
                        if success{
//                            if grey{
                                //Set as grey fro user profile
                                user.followRequestStatus = .request(true) //
                                button.setTitle("Request sent", for: UIControlState())
                                button.setTitleColor(UIColor.white, for: UIControlState())
                                button.backgroundColor = UIColor.darkGray
                                button.layer.borderColor = UIColor.darkGray.cgColor
                                button.isEnabled = true
//                            } else {
                                //Set as blue for lists
//                                user.followRequestStatus = .request(true) //
//                                button.setTitle("Request sent", for: UIControlState())
//                                button.setTitleColor(UIColor.white, for: UIControlState())
//                                button.backgroundColor = ConstantsColor.looplistColor
//                                button.layer.borderColor = ConstantsColor.looplistColor.cgColor
//                                button.isEnabled = true
//                            }
                            
                        }else{
                            button.isEnabled = true
                        }
                    })
                    
                    
                }
            }
        }
    }
    
    static func getStateForFollowerFollowingButton(user:User?,button:UIButton?){
        
        if let user = user, let button = button{ // current user
            if user.objectId == CurrentUser.sharedInstance.currentUser?.objectId{
                button.isHidden = true
            }else{
                button.isHidden = false
                
                switch user.followRequestStatus{
                    
                case .follow(let value):
                    if value == true{
                        button.setTitle("Following", for: UIControlState())
                        button.setTitleColor(UIColor.white, for: UIControlState())
                        button.backgroundColor = UIColor.darkGray
                        button.layer.borderColor = UIColor.darkGray.cgColor
                    }else{
                        button.setTitle("Follow", for: UIControlState())
                        button.setTitleColor(UIColor.darkGray, for: UIControlState())
                        button.backgroundColor = UIColor.clear
                        button.layer.borderColor = UIColor.darkGray.cgColor
                    }
                    
                case .request(let value):
                    if value == true{
                        button.setTitle("Request sent", for: UIControlState())
                        button.setTitleColor(UIColor.white, for: UIControlState())
                        button.backgroundColor = UIColor.darkGray
                        button.layer.borderColor = UIColor.darkGray.cgColor
                    }else{
                        button.setTitle("Request to follow", for: UIControlState())
                        button.setTitleColor(UIColor.darkGray, for: UIControlState())
                        button.backgroundColor = UIColor.clear
                        button.layer.borderColor = UIColor.darkGray.cgColor
                    }
                }
            }
        }
    }
    
    
    static func getStateForUserProfileHeaderButton(user:User?,button:UIButton?){
        
        if let user = user, let button = button{ // current user
            if user.objectId == CurrentUser.sharedInstance.currentUser?.objectId{
                button.setTitle("Edit Profile", for: UIControlState())
            }else{
                button.isHidden = false
                switch user.followRequestStatus{
                    
                case .follow(let value):
                    if value == true{
                        button.setTitle("Following", for: UIControlState())
                        button.backgroundColor = UIColor.darkGray
                        button.layer.borderColor? = UIColor.darkGray.cgColor
                        button.setTitleColor(UIColor.white, for: .normal)
                    }else{
                        button.setTitle("Follow", for: UIControlState())
                        button.backgroundColor = UIColor.clear
                        button.layer.borderColor? = UIColor.darkGray.cgColor
                    }
                    
                case .request(let value):
                    if value == true{
                        button.setTitle("Request sent", for: UIControlState())
                        button.setTitleColor(UIColor.white, for: UIControlState())
                        button.backgroundColor = UIColor.darkGray
                        button.layer.borderColor = UIColor.darkGray.cgColor
                    }else{
                        button.setTitle("Request to follow", for: UIControlState())
                        button.setTitleColor(UIColor.darkGray, for: UIControlState())
                        button.backgroundColor = UIColor.clear
                        button.layer.borderColor = UIColor.darkGray.cgColor
                    }
                }
            }
        }
    }
    
    static func getUserProfileImage(user:User?,imageView:UIImageView?){
        
        if let user = user, let imageView = imageView{
            
            if let profileImageUrl = user.profileImageUrl{
                if let url = URL(string: profileImageUrl){
                    imageView.kf.setImage(with:(url))
                }
            } else if let gender = user.gender, gender == "Female"{
                imageView.image = UIImage(named: Constants.defaultFemaleImage)
            } else {
                imageView.image = UIImage(named: Constants.defaultMaleImage)
            }
            
        }
    }
    
    
}
