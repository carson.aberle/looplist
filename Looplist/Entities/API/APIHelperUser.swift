//
//  APIHelperUser.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/26/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation



struct APIHelperUser{
    

    
    // UPDATE CURRENT USER IMAGE
    static func updateCurrentUserProfilePicture(_ profile64:String,completion:@escaping (_ success: Bool) -> Void){
        
        if let currentUser = CurrentUser.sharedInstance.currentUser{
        if  let objectId = currentUser.objectId{
            let path = "users/\(objectId)"
            let parameters = ["profile64":profile64]
            
            DBHelper.callAPIPut(path, params: parameters as [String : AnyObject] , completion: { (result, error) in
                if let result = result {
                    if let user = result["user"] as? Dictionary<String, AnyObject> {
                        currentUser.assignUserData(user)
                        Analytics.sharedInstance.actionsArray.append(Action(context:["url": currentUser.profileImageUrl!], forScreen:nil, withActivityType:"Changed Profile Photo"))
//                        currentUser.isCurrentUser = true
                        if let _ = CurrentUser.sharedInstance.currentUser?.objectId {
                            completion(true)
                        }
                    }
                } else if let _ = error{
                        ErrorHandling.customErrorHandlerWithTitle(title:"Oops!", message: "Something went wrong")
                        completion(false)
                }
            })
        }
        }
    }
    
    // UPDATE CURRENT USER BIO
    static func updateCurrentUserDetails(_ parameters:[String:AnyObject],completion:@escaping (_ success: Bool) -> Void){
        if let currentUser = CurrentUser.sharedInstance.currentUser,let objectId = currentUser.objectId{
        
            let path = "users/\(objectId)"
            DBHelper.callAPIPut(path, params: parameters, completion: { (result, error) in
                if let result = result {
                    if let username = parameters["username"] as? String, let firstName = parameters["firstName"] as? String, let quote = parameters["quote"] as? String,let gender = parameters["gender"] as? String,let email = parameters["email"] as? String,let location = parameters["location"] as? String,let lastName = parameters["lastName"] as? String{
                        Analytics.sharedInstance.actionsArray.append(Action(context:["username":username, "firstName":firstName, "quote":quote, "gender":gender,"email":email, "location":location, "lastName":lastName], forScreen:nil, withActivityType:"Edited User Profile")) // $AS

                    }
                    if let user = result["user"] as? Dictionary<String, AnyObject>{
                        currentUser.assignUserData(user)
                        if let _ = CurrentUser.sharedInstance.currentUser?.objectId {
                            completion(true)
                        }
                    }
                } else if let _ = error{
                        ErrorHandling.customErrorHandlerWithTitle(title:"Oops!", message: "Something went wrong")
                        completion(false)

                }
            })
            
        }
    }

    
    // TRANSACTIONS
    
    static func getCurrentUserTransactions(skip:Int,limit:Int,completion:@escaping (_ transactions: [Transaction]?) -> Void){
        
        let path = "transactions"
        let queryString = "{\"populate\":[{\"path\":\"listing\",\"populate\":[{\"path\":\"product\"}]}],\"skip\":\(skip),\"limit\":\(limit), \"sort\":\"-createdAt\"}"
        
        DBHelper.callAPIGet(path,queryString: queryString, completion: { (result, error) in
            if let result = result{
                var transactions = [Transaction] ()
                
                if let transactionsArray = result["transactions"] as? NSArray {
                    for transactionDictionary in transactionsArray{
                        if let transactionValue = transactionDictionary as? NSDictionary {
                            if let transaction = Transaction(input:transactionValue as! Dictionary<String,AnyObject>){
                                transactions.append(transaction)
                            }
                        }
                    }
                    completion(transactions)
                }else{
                    completion(nil)
                }
            } else if let _ = error{
                
                // Handle Error
                completion(nil)
            }
        })
        
    }
    
    
    static func changePassword(username:String){
        
        let path = "authentication/local/reset-password"
        DBHelper.callAPIPost(path, params: ["username":username as Optional<AnyObject>], completion: { (result, error) in
            if let _ = result{
                ErrorHandling.customSuccessHandlerWithTitle(title:"Success!", message: ErrorHandling.ResetPasswordSuccess)
            }else if let error = error{
                ErrorHandling.customErrorHandler(message: error.localizedDescription)
            }
        })
    }
    
    
    static func deactivateUser(){
        
        
    }
    
    
    
    // UNFOLLOW THIS IS USER BY CURRENT USER
    
    static func unFollowUser(user:User,_ completion:@escaping (_ success: Bool) -> Void){
        
        
        if let userToUnFollowId = user.objectId,let userId = CurrentUser.sharedInstance.currentUser?.objectId{
            
            let path = "users/\(userId)/following/\(userToUnFollowId)"
            
            DBHelper.callAPIDelete(path, completion: { (result, error) in
                if let _ = result {
                    
                    Analytics.sharedInstance.actionsArray.append(Action(context: ["user":userToUnFollowId], forScreen: nil, withActivityType:"Unfollowed User"))
                    
                    completion(true)
                    
                } else if let _ = error{
                    // Handle error
                    completion(false)
                }
            })
        }else{
            completion(false)
        }
    }

    
    // FOLLOW THIS USER BY CURRENT USER
    
    static func followUser(user:User,_ completion:@escaping (_ success: Bool) -> Void){
        if let userToFollowId = user.objectId,let userId = CurrentUser.sharedInstance.currentUser?.objectId{
            
            let path = "users/\(userId)/following/\(userToFollowId)"
            
            DBHelper.callAPIPost(path, completion: { (result, error) in
                
                if let  _ = result {
                    
                    Analytics.sharedInstance.actionsArray.append(Action(context: ["user":userToFollowId], forScreen: nil, withActivityType:"Followed User"))
                    completion(true)
                } else if let _ = error{
                    // Handle Error
                    completion(false)
                }
            })
        }else{
            completion(false)
        }
    }
    
    // CHANGE PRIVACY STATUS
    
    static func changePrivacyStatus(collectionId: String, status: Bool,_ completion:@escaping (_ success: Bool) -> Void){
        DBHelper.callAPIPut("collections/\(collectionId)", params: ["isPrivate":status as AnyObject], completion: { (result, error) in
            if error == nil{
                
                Analytics.sharedInstance.actionsArray.append(Action(context:["collection": collectionId], forScreen:nil, withActivityType:"Edited Collection"))
                DropDownAlert.customSuccessMessage("SUCCESSFULLY_UPDATED_COLLECTION".localized)
                completion(true)
            } else {
                completion(false)
            }
        })

    }
    
    // DELETE COLLECTION 
    
    static func deleteProductFromCollecton(collection:Collection?,product:Product?, _ completion:@escaping (_ success: Bool) -> Void){
        
        if let productId = product?.objectId, let collectionId = collection?.objectId{
            
            DBHelper.callAPIDelete("collections/\(collectionId)/products/\(productId)", completion:{ (result, error) in
                if error == nil{
                    Analytics.sharedInstance.actionsArray.append(Action(context:["product": productId], forScreen:nil, withActivityType:"Deleted Product from collection"))
                    completion(true)
                } else {
                    completion(false)
                }
            })
        }
    }
    
    static func deleteCollecton(collectionId: String, collectionName: String, _ completion:@escaping (_ success: Bool) -> Void){
        DBHelper.callAPIDelete("collections/\(collectionId)", completion:{ (result, error) in
            if error == nil{
                Analytics.sharedInstance.actionsArray.append(Action(context:["collection": collectionId], forScreen:nil, withActivityType:"Deleted Collection"))
                DropDownAlert.customSuccessMessage("SUCCESSFULLY_DELETED".localized + " \(collectionName)!")
                completion(true)
            } else {
                DropDownAlert.customErrorMessage("ERROR_DELETING".localized + " \(collectionName) :/")
                completion(false)
            }
        })
    }
    
    // SAVE COLLECTION
    
    static func saveCollection(collectionId: String, isCollectionPrivate: Bool, collectionName: String, collectionDescription: String, _ completion:@escaping (_ success: Bool) -> Void){
        DBHelper.callAPIPut("collections/\(collectionId)", params: ["isPrivate": isCollectionPrivate as AnyObject, "name": collectionName as AnyObject, "description": collectionDescription as AnyObject], completion: { (result, error) in
            if error == nil{
                Analytics.sharedInstance.actionsArray.append(Action(context:["collection": collectionId], forScreen:nil, withActivityType:"Edited Collection"))
                DropDownAlert.customSuccessMessage("SUCCESSFULLY_UPDATED_COLLECTION".localized)
                
                completion(true)
            } else {
                completion(false)
            }
        })
    }

    
    
//    static func saveCollecton(collectionId: String, isCollectionPrivate: Bool, collectionTitle: String, collectionDescription: String, _ completion:@escaping (_ success: Bool) -> Void){
//        DBHelper.callAPIPut("collections/\(collectionId)", params: ["isPrivate": isCollectionPrivate as AnyObject, "name": collectionTitle as AnyObject, "description": collectionDescription as AnyObject], completion: { (result, error) in
//            if error == nil{
//                Analytics.sharedInstance.actionsArray.append(Action(context:["collection": collectionId], forScreen:nil, withActivityType:"Edited Collection"))
//                DropDownAlert.customSuccessMessage("SUCCESSFULLY_UPDATED_COLLECTION".localized)
//                
//                completion(true)
//            } else {
//                completion(false)
//            }
//        })
//    }
}
