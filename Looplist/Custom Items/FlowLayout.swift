//
//  FlowLayout.swift
//  Looplist
//
//  Created by Roman Wendelboe on 12/25/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class FlowLayout: UICollectionViewFlowLayout {
    // MARK: -   -----------------------------------------------------      VARIABLES   -----------------------------------------------------
    var itemHeight: CGFloat = 375
    
    // MARK: -   -----------------------------------------------------      INITIALIZER    -----------------------------------------------------
    init(height: CGFloat) {
        super.init()
        itemHeight = height
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    // MARK: -   -----------------------------------------------------      FUNCTIONS    -----------------------------------------------------
    
    /**
     Sets up the layout for the collectionView. 0 distance between each cell, and vertical layout
     */
    func setupLayout() {
        minimumInteritemSpacing = 0
        minimumLineSpacing = 0
        scrollDirection = .horizontal
    }
    
    func itemWidth() -> CGFloat {
        return collectionView!.frame.width
    }
    
    override var itemSize: CGSize {
        set {
            self.itemSize = CGSize(width: itemWidth(), height: itemHeight)
        }
        get {
            return CGSize(width: itemWidth(), height: itemHeight)
        }
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }
}
