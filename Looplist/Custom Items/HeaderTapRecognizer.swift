//
//  HeaderTapRecognizer.swift
//  Looplist
//
//  Created by Roman Wendelboe on 1/20/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class HeaderTapRecognizer: UITapGestureRecognizer {
    let section: Int;
    
    init(section: Int, target: AnyObject, action: Selector) {
        self.section = section;
        super.init(target: target, action: action);
    }
}
