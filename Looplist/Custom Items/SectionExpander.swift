//
//  SectionExpander.swift
//  Looplist
//
//  Created by Roman Wendelboe on 1/20/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

open class SectionExpander {
    open var expandedSections = Set<Int>()
    fileprivate var previousSectionHeader = 0
    open var multipleExpansionEnabled = false
    fileprivate var previousHeader: ProductBuyCollectionSectionHeader!
    
    open func toggleExpansionForSectionAtIndex(_ section: Int, inCollectionView collectionView: UICollectionView, withDuration duration: TimeInterval?) {
        if previousSectionHeader != section {
            if let dataSource = collectionView.dataSource {
                let itemCount = dataSource.collectionView(collectionView, numberOfItemsInSection: section)
                var itemsToInsert = [IndexPath]()
                var itemsToDelete = [IndexPath]()
                let currentHeader = collectionView.supplementaryView(forElementKind: UICollectionElementKindSectionHeader, at: IndexPath(item: 0, section: section)) as! ProductBuyCollectionSectionHeader
                self.previousHeader = collectionView.supplementaryView(forElementKind: UICollectionElementKindSectionHeader, at: IndexPath(item: 0, section: previousSectionHeader)) as! ProductBuyCollectionSectionHeader
                
                if self.expandedSections.contains(section) {
                    self.expandedSections.remove(section)
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        currentHeader.sectionImageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(M_PI)) * 180.0)
                    })
                    
                    if previousHeader != nil {
                        UIView.animate(withDuration: 0.5, animations: {
                            self.previousHeader.sectionImageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(M_PI)) / 180.0)
                        })
                    }
                    
                    for i in 0 ..< itemCount {
                        itemsToDelete.append(IndexPath(item: i, section: section))
                    }
                } else {
                    UIView.animate(withDuration: 0.5, animations: {
                        currentHeader.sectionImageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(M_PI)) / 180.0)
                    })
                    
                    if previousHeader != nil {
                        UIView.animate(withDuration: 0.5, animations: {
                            self.previousHeader.sectionImageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(M_PI)) * 180.0)
                        })
                    }
                    
                    if !self.multipleExpansionEnabled {
                        for expandedSection in self.expandedSections {
                            let itemCount = dataSource.collectionView(collectionView, numberOfItemsInSection: expandedSection)
                            
                            for i in 0 ..< itemCount {
                                itemsToDelete.append(IndexPath(item: i, section: expandedSection))
                            }
                        }
                        self.expandedSections.removeAll(keepingCapacity: true)
                    }
                }
                
                previousSectionHeader = section
                
                UIView.animate(withDuration: duration ?? 0.25, animations: { () -> Void in
                    collectionView.performBatchUpdates({ () -> Void in
                        collectionView.deleteItems(at: itemsToDelete)
                    }, completion: { (finished: Bool) -> Void in
                        self.expandedSections.insert(section)
                        
                        let itemCount = dataSource.collectionView(collectionView, numberOfItemsInSection: section)
                        
                        for i in 0 ..< itemCount {
                            itemsToInsert.append(IndexPath(item: i, section: section))
                        }
                        
                        UIView.animate(withDuration: duration ?? 0.25, delay: 0, options: .curveEaseInOut, animations: {
                            collectionView.insertItems(at: itemsToInsert)
                        }, completion: { (true) in
                            if let firstInsert = itemsToInsert.first, firstInsert.section < collectionView.numberOfSections &&
                                firstInsert.item < collectionView.numberOfItems(inSection: firstInsert.section) {
                                if collectionView.numberOfSections > firstInsert.section {
                                    collectionView.scrollToItem(at: firstInsert, at: UICollectionViewScrollPosition.centeredVertically, animated: true)
                                }
                            }
                        })
                    })
                })
            }
        }
    }
    
    open func sectionIsExpandedAtIndex(_ section: Int) -> Bool {
        return self.expandedSections.contains(section)
    }
}
