//
//  UIViewController+CWPopup.m
//  CWPopupDemo
//
//  Created by Cezary Wojcik on 8/21/13.
//  Copyright (c) 2013 Cezary Wojcik. All rights reserved.
//

#import "UIViewController+CWPopup.h"
#import <objc/runtime.h>
#import <QuartzCore/QuartzCore.h>
#import "Looplist-Swift.h"

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
@import Accelerate;
#endif
#import <float.h>

#define ANIMATION_TIME 0.5f
#define STATUS_BAR_SIZE 22

NSString const *CWPopupKey = @"CWPopupkey";
NSString const *CWBlurViewKey = @"CWFadeViewKey";
NSString const *CWUseBlurForPopup = @"CWUseBlurForPopup";
NSString const *CWPopupViewOffset = @"CWPopupViewOffset";

@implementation UIViewController (CWPopup)

@dynamic popupViewController, useBlurForPopup, popupViewOffset;



#pragma mark - present/dismiss


-(void) closeButtonPressed{
    [self dismissPopupViewControllerAnimated:true completion:nil];
}

- (void)presentPopupViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {
    if (self.popupViewController == nil) {
        // initial setup
        self.popupViewController = viewControllerToPresent;
        self.popupViewController.view.autoresizesSubviews = NO;
        self.popupViewController.view.autoresizingMask = UIViewAutoresizingNone;
        
        
        if( [self isKindOfClass:[ProductTableViewController class]]){
            ProductTableViewController *productVC = (ProductTableViewController *) self;
            [productVC.tableView setScrollEnabled:false];

        }
        
     
        
        viewControllerToPresent.view.layer.cornerRadius = 5;
        [viewControllerToPresent.view setClipsToBounds:YES];
        [self addChildViewController:viewControllerToPresent];
        CGRect finalFrame = [self getPopupFrameForViewController:viewControllerToPresent];
        
        UIInterpolatingMotionEffect *interpolationHorizontal = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
        interpolationHorizontal.minimumRelativeValue = @-10.0;
        interpolationHorizontal.maximumRelativeValue = @10.0;
        UIInterpolatingMotionEffect *interpolationVertical = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
        interpolationHorizontal.minimumRelativeValue = @-10.0;
        interpolationHorizontal.maximumRelativeValue = @10.0;
        [self.popupViewController.view addMotionEffect:interpolationHorizontal];
        [self.popupViewController.view addMotionEffect:interpolationVertical];

        // background view
        UIView *fadeView = [UIImageView new];
        fadeView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height + 64);

        fadeView.backgroundColor = [UIColor blackColor];
        [fadeView setUserInteractionEnabled:YES];
        fadeView.alpha = 0.0;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closePopup)];
        [fadeView addGestureRecognizer:tapGesture];
        [self.view addSubview:fadeView];
        objc_setAssociatedObject(self, &CWBlurViewKey, fadeView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        UIView *blurView = objc_getAssociatedObject(self, &CWBlurViewKey);
        [viewControllerToPresent beginAppearanceTransition:YES animated:flag];
        
        // setup
        if (flag) { // animate
            CGRect initialFrame = CGRectMake(finalFrame.origin.x, [UIScreen mainScreen].bounds.size.height + viewControllerToPresent.view.frame.size.height/2, finalFrame.size.width, finalFrame.size.height);
            viewControllerToPresent.view.frame = initialFrame;
            [self.view addSubview:viewControllerToPresent.view];
            [UIView animateWithDuration:ANIMATION_TIME delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                viewControllerToPresent.view.frame = finalFrame;
                blurView.alpha = 0.7;
            } completion:^(BOOL finished) {
                [self.popupViewController didMoveToParentViewController:self];
                [self.popupViewController endAppearanceTransition];
                [completion invoke];
            }];
        } else { // don't animate
            viewControllerToPresent.view.frame = finalFrame;
            [self.view addSubview:viewControllerToPresent.view];
            [self.popupViewController didMoveToParentViewController:self];
            [self.popupViewController endAppearanceTransition];
            [completion invoke];
        }
        // if screen orientation changed
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(screenOrientationChanged) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

-(void) closePopup{
    [self dismissPopupViewControllerAnimated:true completion:nil];
//    NSLog(@"%@",[[self]);

//
//    if( [self isKindOfClass:[AddProductWebViewController class]]){
//        AddProductWebViewController *addProductVC = (AddProductWebViewController *) self;
//        [addProductVC goBack];
//        
//    }
}

- (void)dismissPopupViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    UIView *blurView = objc_getAssociatedObject(self, &CWBlurViewKey);
    
    if( [self isKindOfClass:[ProductTableViewController class]]){
        ProductTableViewController *productVC = (ProductTableViewController *) self;
        [productVC.tableView setScrollEnabled:true];
    
    }
    
    
    
    [self.popupViewController willMoveToParentViewController:nil];
    
    [self.popupViewController beginAppearanceTransition:NO animated:flag];
    if (flag) { // animate
        CGRect initialFrame = self.popupViewController.view.frame;
        [UIView animateWithDuration:ANIMATION_TIME delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.popupViewController.view.frame = CGRectMake(initialFrame.origin.x, [UIScreen mainScreen].bounds.size.height + initialFrame.size.height/2, initialFrame.size.width, initialFrame.size.height);
            // uncomment the line below to have slight rotation during the dismissal
            // self.popupViewController.view.transform = CGAffineTransformMakeRotation(M_PI/6);
            blurView.alpha = 0.0f;
        } completion:^(BOOL finished) {
            [self.popupViewController removeFromParentViewController];
            [self.popupViewController endAppearanceTransition];
            [self.popupViewController.view removeFromSuperview];
            [blurView removeFromSuperview];
            self.popupViewController = nil;
            [completion invoke];
        }];
    } else { // don't animate
        [self.popupViewController removeFromParentViewController];
        [self.popupViewController endAppearanceTransition];
        [self.popupViewController.view removeFromSuperview];
        [blurView removeFromSuperview];
        self.popupViewController = nil;
        blurView = nil;
        [completion invoke];
    }
    // remove observer
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
}

#pragma mark - handling screen orientation change

- (CGRect)getPopupFrameForViewController:(UIViewController *)viewController {
    CGRect frame = viewController.view.frame;
    CGFloat x;
    CGFloat y;
    if (UIDeviceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) || NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
        x = ([UIScreen mainScreen].bounds.size.width - frame.size.width)/2;
        y = ([UIScreen mainScreen].bounds.size.height - frame.size.height)/2;
    } else {
        x = ([UIScreen mainScreen].bounds.size.height - frame.size.width)/2;
        y = ([UIScreen mainScreen].bounds.size.width - frame.size.height)/2;
    }
    return CGRectMake(x + viewController.popupViewOffset.x, y + viewController.popupViewOffset.y, frame.size.width, frame.size.height);
}

- (void)screenOrientationChanged {
    // make blur view go away so that we can re-blur the original back
    UIView *blurView = objc_getAssociatedObject(self, &CWBlurViewKey);
    [UIView animateWithDuration:ANIMATION_TIME animations:^{
        self.popupViewController.view.frame = [self getPopupFrameForViewController:self.popupViewController];
        if (UIDeviceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) || NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
            blurView.frame = [UIScreen mainScreen].bounds;
        } else {
            blurView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width);
        }
    }];
}

#pragma mark - popupViewController getter/setter

- (void)setPopupViewController:(UIViewController *)popupViewController {
    objc_setAssociatedObject(self, &CWPopupKey, popupViewController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIViewController *)popupViewController {
    return objc_getAssociatedObject(self, &CWPopupKey);
    
}

- (void)setUseBlurForPopup:(BOOL)useBlurForPopup {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0 && useBlurForPopup) {
        NSLog(@"ERROR: Blur unavailable prior to iOS 7");
        objc_setAssociatedObject(self, &CWUseBlurForPopup, [NSNumber numberWithBool:NO], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    } else {
        objc_setAssociatedObject(self, &CWUseBlurForPopup, [NSNumber numberWithBool:useBlurForPopup], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}

- (BOOL)useBlurForPopup {
    NSNumber *result = objc_getAssociatedObject(self, &CWUseBlurForPopup);
    return [result boolValue];
    
}

- (void)setPopupViewOffset:(CGPoint)popupViewOffset {
    objc_setAssociatedObject(self, &CWPopupViewOffset, [NSValue valueWithCGPoint:popupViewOffset], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CGPoint)popupViewOffset {
    NSValue *offset = objc_getAssociatedObject(self, &CWPopupViewOffset);
    return [offset CGPointValue];
}

@end
