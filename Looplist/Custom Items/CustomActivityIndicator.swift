//
//  CustomActivityIndicator.swift
//  Looplist
//
//  Created by Roman Wendelboe on 1/27/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

open class CustomActivityIndicator {
    private var container: UIView = UIView()
    private var loadingView: UIView = UIView()
    private var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var isLoading = false
    
    // Show indicator
    open func showActivityIndicator(uiView: UIView, offset: CGFloat) {
        self.isLoading = true
        container.frame = uiView.frame
        container.center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2 - offset)
    
        container.backgroundColor = UIColorFromHex(rgbValue: 0xffffff, alpha: 0.0)
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColorFromHex(rgbValue: 0x444444, alpha: 0.8)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        
        loadingView.addSubview(activityIndicator)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        activityIndicator.startAnimating()
    }
    
    // Hide indicator
    open func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
        self.isLoading = false
    }
    
    // Define UIColor from hex value
    private func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
}
