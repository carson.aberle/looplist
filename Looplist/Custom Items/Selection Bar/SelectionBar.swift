//
//  SelectionBar.swift
//  Looplist
//
//  Created by Carson Aberle on 8/12/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class SelectionBar {
    
    // MARK: -  //////////////////////////////////////////////////////    VARIABLES    //////////////////////////////////////////////////////////////////////////
    
    static var callingClass:UIViewController?
    
    // MARK: -  //////////////////////////////////////////////////////    FUNCTIONS    //////////////////////////////////////////////////////////////////////////
    
    internal class func addProductToCollection(_ callingClass:UIViewController, product:Product, sender:UIButton){
        self.callingClass = callingClass
        UIViewController.showAddToCollectionPopup(callingClass: callingClass,product: product, sender: sender)
    }
    
    internal class func reportProduct(_ callingClass:UIViewController, product:Product, sender:UIButton){
        (sender).isEnabled = false
        let alertController = UIAlertController(title: "Report Product", message: "Would you like to report this product?", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            (sender).isEnabled = true
        }
        
        alertController.addAction(cancelAction)
        
        let reportAction = UIAlertAction(title: "Report", style: .destructive) { (action) in
            if let objectId = CurrentUser.sharedInstance.currentUser?.objectId,let productObjectId = product.objectId{
            DBHelper.callAPIPost("users/\(objectId)/reports/\(productObjectId)", completion: { (result, error) in
                if error == nil{
                    Analytics.sharedInstance.actionsArray.append(Action(context: ["product":productObjectId], forScreen: nil, withActivityType:"Reported"))
//                    DropDownAlert.customSuccessMessage("Successfully reported \(product.brandName)")
                    ErrorHandling.customSuccessHandler(message: "Successfully reported \(product.brandName)")
                    sender.isEnabled = true
                } else {
//                    DropDownAlert.customSuccessMessage("Error reporting \(product.brandName)")
                    ErrorHandling.customErrorHandler(message: "Error reporting \(product.brandName)")
                    sender.isEnabled = true
                }
            })
            }
            
        }
        alertController.addAction(reportAction)
        
        callingClass.present(alertController, animated: true, completion: nil)
    }
    

    
    class func repostProduct(_ callingClass:UIViewController, product:Product, sender:UIButton){
        if product.isRepostedByCurrentUser == true{
            APIHelperProduct.removeRepostProduct(product,{ (success) in
                if success == true {
                    if let productVC = callingClass as? ProductTableViewController{
                        productVC.isRepostingProduct = false
                    } else if let homeFeedVC = callingClass as? HomeFeedViewController{
                        homeFeedVC.isRepostingProduct = false
                    }
                    product.isRepostedByCurrentUser = false
                    CurrentUser.sharedInstance.currentUser!.updateUser({ (success) in
                        DropDownAlert.customSuccessMessage("Successfully removed repost \(product.brandName )")
                        sender.setImage(UIImage(named:Constants.repostImage), for: UIControlState())
                        UIViewController.dismissRepostPopup()
                    })
                } else {
                    DropDownAlert.customErrorMessage("Error reposted \(product.brandName )! :/")
                }
            })
            
        } else {
            APIHelperProduct.repostProduct(product,{ (success) in
                if success == true{
                    if let productVC = callingClass as? ProductTableViewController{
                        productVC.isRepostingProduct = false
                    } else if let homeFeedVC = callingClass as? HomeFeedViewController{
                        homeFeedVC.isRepostingProduct = false
                    }
                    product.isRepostedByCurrentUser = true
                    CurrentUser.sharedInstance.currentUser!.updateUser({ (success) in
                        DropDownAlert.customSuccessMessage("Successfully reposted \(product.brandName )")
                        sender.setImage(UIImage(named:Constants.repostedImage), for: UIControlState())
                        UIViewController.dismissRepostPopup()
                    })
                } else {
                    DropDownAlert.customErrorMessage("Error reposting \(product.brandName )! :/")
                }
            })
        }
    }
    
    class func shareProduct(_ callingClass:UIViewController, product:Product, sender:UIButton){

        UIViewController.showSharePopup(callingClass: callingClass, product:product, sender:sender)
    }
    
    internal class func likeProduct(_ callingClass:UIViewController, product:Product, sender:UIButton){
        if product.isLikedByCurrentUser == false{
            APIHelperProduct.likeProduct(product,{ (success) in
                if success{
                    if let homeFeedVC = callingClass as? HomeFeedViewController{
                        homeFeedVC.isLikingProduct = false
                    } else if let productVC = callingClass as? ProductTableViewController{
                        productVC.product?.isLikedByCurrentUser = false
                        productVC.isLikingProduct = false
                    }
                    product.isLikedByCurrentUser = true
                    if let heartBigActive = UIImage(named: Constants.likedHeartsActive){
                        sender.isEnabled = true
                        sender.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
                        sender.setImage(heartBigActive, for: UIControlState())
                        UIView.animate(withDuration: 0.7,
                            delay: 0,
                            usingSpringWithDamping: 0.4,
                            initialSpringVelocity: 6.0,
                            options: UIViewAnimationOptions.allowUserInteraction,
                            animations: {
                                sender.transform = CGAffineTransform.identity
                            }, completion: nil)
                    }
                }else{
                    sender.isEnabled = true
                }
            })
        }else{
            APIHelperProduct.unlikeProduct(product,{ (success) in
                if success{
                    if let homeFeedVC = callingClass as? HomeFeedViewController{
                        homeFeedVC.isLikingProduct = false
                    } else if let productVC = callingClass as? ProductTableViewController{
                        productVC.product?.isLikedByCurrentUser = false
                        productVC.isLikingProduct = false
                    }
                    product.isLikedByCurrentUser = false
                    if let heartBigActive = UIImage(named: Constants.likedHeartsInActive){
                        sender.isEnabled = true
                        sender.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
                        sender.setImage(heartBigActive, for: UIControlState())
                        UIView.animate(withDuration: 0.7,
                            delay: 0,
                            usingSpringWithDamping: 0.4,
                            initialSpringVelocity: 6.0,
                            options: UIViewAnimationOptions.allowUserInteraction,
                            animations: {
                                sender.transform = CGAffineTransform.identity
                            }, completion: nil)
                    }
                }else{
                  
                    sender.isEnabled = true
                }
            })
        }
    }
}
