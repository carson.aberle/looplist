//
//  TabBarController.swift
//  Looplist
//
//  Created by Arun Sivakumar on 12/13/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit


var dot = UIView()

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tabBar.tintColor = UIColor.white
        if #available(iOS 10.0, *) {
            self.tabBar.unselectedItemTintColor = UIColor.white
        } else {
            // Fallback on earlier versions
        }
        //         self.tabBar.barTintColor = UIColor.white
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(TabBarController.hideMessagingNotificationBadge), name: NSNotification.Name(rawValue: "hideMessagingNotificationBadge"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TabBarController.showMessagingNotificationBadge), name: NSNotification.Name(rawValue: "showMessagingNotificationBadge"), object: nil)
        
        //        let blurEffect = UIBlurEffect(style:.dark)
        //        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //
        //        blurEffectView.frame = CGRect(x: 0.0,y: 0.0,width: self.view.frame.width,height: 40.0)
        //        self.tabBar.addSubview(blurEffectView)
        
        //        dot.frame = CGRect(x:(self.view.frame.size.width / 5), y:self.view.frame.size.height - 27, width:100 , height:100)
        //        dot.center.x = self.view.frame.size.width / 5  + ((self.view.frame.size.width / 5) / 2) + 6
        dot.frame = CGRect(x:0, y:0, width:6 , height:6)
        dot.center.x = self.view.frame.size.width / 5  + ((self.view.frame.size.width / 5) / 2) + 6
        dot.center.y = 16
        dot.backgroundColor = ConstantsColor.notificationColor
        dot.layer.cornerRadius = dot.frame.size.width / 2
        dot.isHidden = true
        self.tabBar.addSubview(dot)
//        let tabBarItem2 = self.tabBar.items![1] as UITabBarItem
//        tabBarItem2.addSubview(dot)
        //        self.tabBar.insertSubview(dot, at: self.tabBar.subviews.count)
        
        
        
         self.tabBar.barTintColor = UIColor.black
//        self.tabBar.tintColor = UIColor.black
        self.tabBar.isTranslucent = true
        self.tabBar.barStyle = .black
        self.tabBar.alpha = 0.95
        
//        let subViews = self.tabBar.subviews
        
//        if {
//            for view in subViews{
//                if let visualEffectView = view as? UIVisualEffectView{
////                    visualEffectView.
//                }
//            }
//        }
        
//        
//        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
//        let blurEffectView = UIVisualEffectView(effect: blurEffect)
//        blurEffectView.frame = self.tabBar.frame
//        
//        self.tabBar.addSubview(blurEffectView)
//        self.tabBar.bringSubview(toFront: blurEffectView)
        //        self.tabBarItem.selectedImage = UIImage(named:Constants.likedHeartsActive)
        
        self.tabBar.bringSubview(toFront: dot)
        
    }
    
    override func viewWillLayoutSubviews() {
        
        var tabFrame = self.tabBar.frame
        tabFrame.size.height = 40
        tabFrame.origin.y = self.view.frame.size.height - 40
        self.tabBar.frame = tabFrame
        //         self.tabBar.bringSubview(toFront: dot)
        
    }
    
    func hideMessagingNotificationBadge(){
        dot.isHidden = true
        self.tabBar.items?[1].image = UIImage(named:Constants.MessagingTabBar)
        self.tabBar.items?[1].selectedImage = UIImage(named:Constants.MessagingTabBar)
        
    }
    func showMessagingNotificationBadge(){
        self.tabBar.items?[1].image = UIImage(named:Constants.MessagingTabBarNotification)
        self.tabBar.items?[1].selectedImage = UIImage(named:Constants.MessagingTabBarNotification) // Image if there is any notification (broken image)
        
        dot.isHidden = false
         self.tabBar.bringSubview(toFront: dot)
        
    }
    
}
