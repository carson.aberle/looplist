//
//  FilterPopup+UIViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 12/5/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation

extension UIViewController {

    
    public class func showFiltersPopup(callingClass:UIViewController, filtersDictionary:NSDictionary?){
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanFilter))
        let closeTap = UITapGestureRecognizer(target: self, action: #selector(dismissFilterPopupTap))
        panGesture.require(toFail: closeTap)
        let backgroundOverlay = UIView(frame:UIScreen.main.bounds)
        backgroundOverlay.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        backgroundOverlay.tag = ConstantsObjects.FILTER_POPUP_BACKGROUND_TAG
        backgroundOverlay.addGestureRecognizer(closeTap)
        backgroundOverlay.addGestureRecognizer(panGesture)

        UIApplication.shared.keyWindow?.addSubview(backgroundOverlay)
        let height:CGFloat = 0

        let bounds = CGRect(x:5,y:UIScreen.main.bounds.size.height + 8,width:UIScreen.main.bounds.size.width - 10,height: UIScreen.main.bounds.size.height + 16)
        
        let bundle = Bundle(for: type(of: callingClass))
        let nib = UINib(nibName: "FilterPopupView", bundle: bundle)
        let popupBackgroundView = nib.instantiate(withOwner: self, options: nil)[0] as! FilterPopupView

        popupBackgroundView.preSelectedFilters = filtersDictionary
        if let discoverVC = callingClass as? DiscoverViewController{
            popupBackgroundView.discoverVC = discoverVC
        }
        popupBackgroundView.frame = bounds
        popupBackgroundView.backgroundColor = UIColor(red:0.956, green:0.956, blue:0.956, alpha:1.0)
        popupBackgroundView.clipsToBounds = true
        popupBackgroundView.tag = ConstantsObjects.FILTER_POPUP_SLIDER_TAG
        popupBackgroundView.addGestureRecognizer(panGesture)
        popupBackgroundView.layer.cornerRadius = 8
        backgroundOverlay.alpha = 0.0
        popupBackgroundView.layer.shadowColor = UIColor.black.cgColor
        popupBackgroundView.layer.masksToBounds = false
        popupBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 8)
        popupBackgroundView.layer.shadowRadius = 5
        popupBackgroundView.layer.shadowOpacity = 1.0
        UIApplication.shared.keyWindow?.addSubview(popupBackgroundView)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseOut, animations: {
            let up = CGAffineTransform(translationX: 0, y: -ConstantsObjects.FILTER_POPUP_TRANSLATION_HEIGHT - height)
            backgroundOverlay.alpha = 1.0
            popupBackgroundView.transform = up
        }, completion: { finished in
            if let colors = popupBackgroundView.preSelectedFilters?["colors"] as? NSArray {
                for colorElement in colors{
                    if let color = colorElement as? String{
                        let colorTag = popupBackgroundView.getTagFromHexValue(hex: color)
                        popupBackgroundView.selectColorWithTag(tag: colorTag, sender: popupBackgroundView.getButtonFromTag(tag: colorTag))
                    }
                }
                popupBackgroundView.filtersDictionary.setValue(colors, forKey: "colors")
            }

        })
    }
    
    class func handlePanFilter (_ pan:UIPanGestureRecognizer){
        if let view = UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.FILTER_POPUP_SLIDER_TAG) as? FilterPopupView{
            let translation = pan.translation(in: view)
            
            if pan.state == .ended{
                if view.frame.origin.y <= UIScreen.main.bounds.size.height - ConstantsObjects.FILTER_POPUP_TRANSLATION_HEIGHT - 60{

                } else if view.frame.origin.y >= UIScreen.main.bounds.size.height - ConstantsObjects.FILTER_POPUP_TRANSLATION_HEIGHT + 60{
                    self.dismissFilterPopup(popupView:view, callingClass: view.discoverVC!)
                } else {
                    view.frame.origin.y =  UIScreen.main.bounds.size.height - ConstantsObjects.FILTER_POPUP_TRANSLATION_HEIGHT
                }
            } else {
                if translation.y > 0{
                    view.frame.origin.y = UIScreen.main.bounds.size.height - ConstantsObjects.FILTER_POPUP_TRANSLATION_HEIGHT + translation.y
                }
            }
        }
    }
    
    public class func dismissFilterPopupTap(){
        if let view = UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.FILTER_POPUP_SLIDER_TAG) as? FilterPopupView{
            dismissFilterPopup(popupView: view, callingClass: view.discoverVC!)
        }
    }
    
    public class func dismissFilterPopup(popupView:FilterPopupView, callingClass:UIViewController){
        if let discoverVC = callingClass as? DiscoverViewController{
            discoverVC.getListingsWithFilters(filtersDictionary: popupView.filtersDictionary)
        }
        UIView.animate(withDuration: 0.20, delay: 0.0, options: .curveEaseOut, animations: {
            let up = CGAffineTransform(translationX: 0, y: 316)
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.FILTER_POPUP_BACKGROUND_TAG)?.alpha = 0.0
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.FILTER_POPUP_SLIDER_TAG)?.transform = up
        }, completion: { finished in
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.FILTER_POPUP_BACKGROUND_TAG)?.removeFromSuperview()
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.FILTER_POPUP_SLIDER_TAG)?.removeFromSuperview()
        })

    }
    
    class func showSharePopup(callingClass:UIViewController, product:Product, sender:UIButton){
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanRepost))
        let closeTap = UITapGestureRecognizer(target: self, action: #selector(dismissRepostPopup))
        panGesture.require(toFail: closeTap)
        let backgroundOverlay = UIView(frame:UIScreen.main.bounds)
        backgroundOverlay.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        backgroundOverlay.tag = ConstantsObjects.REPOST_POPUP_BACKGROUND_TAG
        backgroundOverlay.addGestureRecognizer(closeTap)
        backgroundOverlay.addGestureRecognizer(panGesture)

        UIApplication.shared.keyWindow?.addSubview(backgroundOverlay)
        let bounds = CGRect(x:10,y:UIScreen.main.bounds.size.height + 8,width:UIScreen.main.bounds.size.width - 20,height:UIScreen.main.bounds.size.height)
        
        let bundle = Bundle(for: type(of: callingClass))
        let nib = UINib(nibName: "RepostPopupView", bundle: bundle)
        let popupBackgroundView = nib.instantiate(withOwner: self, options: nil)[0] as! RepostPopupView
        if let homeFeedVC = callingClass as? HomeFeedViewController{
            popupBackgroundView.homeFeedVC = homeFeedVC
        } else if let productVC = callingClass as? ProductTableViewController{
            popupBackgroundView.productVC = productVC
        }
        popupBackgroundView.product = product
        popupBackgroundView.addGestureRecognizer(panGesture)
        popupBackgroundView.frame = bounds
        popupBackgroundView.backgroundColor = UIColor(red:0.956, green:0.956, blue:0.956, alpha:1.0)
        popupBackgroundView.clipsToBounds = true
        popupBackgroundView.tag = ConstantsObjects.REPOST_POPUP_SLIDER_TAG
        popupBackgroundView.layer.cornerRadius = 8
        backgroundOverlay.alpha = 0.0
        popupBackgroundView.layer.shadowColor = UIColor.black.cgColor
        popupBackgroundView.layer.masksToBounds = false
        popupBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 8)
        popupBackgroundView.layer.shadowRadius = 5
        popupBackgroundView.layer.shadowOpacity = 1.0
        UIApplication.shared.keyWindow?.addSubview(popupBackgroundView)
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseOut, animations: {
            let up = CGAffineTransform(translationX: 0, y: -ConstantsObjects.REPOST_POPUP_TRANSLATION_HEIGHT)
            backgroundOverlay.alpha = 1.0
            popupBackgroundView.transform = up
        }, completion: { finished in
        })
        
    }
    
    
    class func handlePanRepost (_ pan:UIPanGestureRecognizer){
        if let view = UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.REPOST_POPUP_SLIDER_TAG){
            let translation = pan.translation(in: view)
            let location = pan.location(in: UIApplication.shared.keyWindow!)
            if pan.state == .ended{
                if translation.y > 0{
                    self.dismissRepostPopup()
                } else {
                    UIView.animate(withDuration: 0.2, animations: {
                       view.frame.origin.y = UIScreen.main.bounds.size.height - ConstantsObjects.REPOST_POPUP_TRANSLATION_HEIGHT
                    })
                }
            } else {
                if location.y > (UIScreen.main.bounds.size.height - ConstantsObjects.REPOST_POPUP_TRANSLATION_HEIGHT){
                    view.frame.origin.y = location.y
                }
            }
        }
    }
    
    
    public class func dismissRepostPopup(){
        if let view = UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.REPOST_POPUP_SLIDER_TAG){
            view.endEditing(true)
        }
        UIView.animate(withDuration: 0.40, delay: 0.0, options: .curveEaseOut, animations: {
            let up = CGAffineTransform(translationX: 0, y: 286)
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.REPOST_POPUP_BACKGROUND_TAG)?.alpha = 0.0
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.REPOST_POPUP_SLIDER_TAG)?.transform = up
        }, completion: { finished in
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.REPOST_POPUP_BACKGROUND_TAG)?.removeFromSuperview()
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.REPOST_POPUP_SLIDER_TAG)?.removeFromSuperview()
        })
    }
    
    
    
    class func showAddToCollectionPopup(callingClass:UIViewController, product:Product, sender:UIButton){
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanAddToCollection))
        let closeTap = UITapGestureRecognizer(target: self, action: #selector(dismissAddToCollectionPopup))
        panGesture.require(toFail: closeTap)
        let backgroundOverlay = UIView(frame:UIScreen.main.bounds)
        backgroundOverlay.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        backgroundOverlay.tag = ConstantsObjects.ADD_TO_COLLECTION_POPUP_BACKGROUND_TAG
        backgroundOverlay.addGestureRecognizer(closeTap)
        backgroundOverlay.addGestureRecognizer(panGesture)
        
        UIApplication.shared.keyWindow?.addSubview(backgroundOverlay)
        let bounds = CGRect(x:10,y:UIScreen.main.bounds.size.height + 8,width:UIScreen.main.bounds.size.width - 20,height:UIScreen.main.bounds.size.height + 16)
        
        let bundle = Bundle(for: type(of: callingClass))
        let nib = UINib(nibName: "AddToCollectionPopupView", bundle: bundle)
        let popupBackgroundView = nib.instantiate(withOwner: self, options: nil)[0] as! AddToCollectionPopupView
        if let homeFeedVC = callingClass as? HomeFeedViewController{
            popupBackgroundView.homeFeedVC = homeFeedVC
        } else if let productVC = callingClass as? ProductTableViewController{
            popupBackgroundView.productVC = productVC
        }
        popupBackgroundView.product = product
        
        popupBackgroundView.layer.shadowColor = UIColor.black.cgColor
        popupBackgroundView.layer.masksToBounds = false
        popupBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 8)
        popupBackgroundView.layer.shadowRadius = 5
        popupBackgroundView.layer.shadowOpacity = 1.0
        popupBackgroundView.addGestureRecognizer(panGesture)
        popupBackgroundView.frame = bounds
        popupBackgroundView.backgroundColor = UIColor(red:0.956, green:0.956, blue:0.956, alpha:1.0)
        popupBackgroundView.tag = ConstantsObjects.ADD_TO_COLLECTION_POPUP_SLIDER_TAG
        popupBackgroundView.layer.cornerRadius = 8
        backgroundOverlay.alpha = 0.0
        UIApplication.shared.keyWindow?.addSubview(popupBackgroundView)
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseOut, animations: {
            let up = CGAffineTransform(translationX: 0, y: -ConstantsObjects.ADD_TO_COLLECTION_POPUP_TRANSLATION_HEIGHT)
            backgroundOverlay.alpha = 1.0
            popupBackgroundView.transform = up
        }, completion: { finished in
        })
        
    }
    
    
    class func handlePanAddToCollection (_ pan:UIPanGestureRecognizer){
        if let view = UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.ADD_TO_COLLECTION_POPUP_SLIDER_TAG) as? AddToCollectionPopupView{
            let translation = pan.translation(in: view)
            
            if pan.state == .ended{
                if view.collections.count > 3{
                    if view.frame.origin.y <= UIScreen.main.bounds.size.height - ConstantsObjects.ADD_TO_COLLECTION_POPUP_TRANSLATION_HEIGHT - 60{
                        if let productBuyVC = view.productVC{
                            productBuyVC.expandAddToCollectionPopup(view: view)
                        } else if let homeFeedVC = view.homeFeedVC{
                            homeFeedVC.expandAddToCollectionPopup(view: view)
                        }
                    } else if view.frame.origin.y >= UIScreen.main.bounds.size.height - ConstantsObjects.ADD_TO_COLLECTION_POPUP_TRANSLATION_HEIGHT + 60 {
                        self.dismissAddToCollectionPopup()
                    } else {
                        view.frame.origin.y =  UIScreen.main.bounds.size.height - ConstantsObjects.ADD_TO_COLLECTION_POPUP_TRANSLATION_HEIGHT
                    }
                } else {
                    if view.frame.origin.y >= UIScreen.main.bounds.size.height - ConstantsObjects.ADD_TO_COLLECTION_POPUP_TRANSLATION_HEIGHT + 60 {
                        self.dismissAddToCollectionPopup()
                    } else {
                        view.frame.origin.y =  UIScreen.main.bounds.size.height - ConstantsObjects.ADD_TO_COLLECTION_POPUP_TRANSLATION_HEIGHT
                    }
                }
                
                
            } else {
                if view.collections.count > 3{
                    view.frame.origin.y = UIScreen.main.bounds.size.height - ConstantsObjects.ADD_TO_COLLECTION_POPUP_TRANSLATION_HEIGHT + translation.y
                } else {
                    if translation.y > 0{
                        view.frame.origin.y = UIScreen.main.bounds.size.height - ConstantsObjects.ADD_TO_COLLECTION_POPUP_TRANSLATION_HEIGHT + translation.y
                    }
                }
                
            }
        }
    }

    
    
    public class func dismissAddToCollectionPopup(){
        UIView.animate(withDuration: 0.40, delay: 0.0, options: .curveEaseOut, animations: {
            let up = CGAffineTransform(translationX: 0, y: 286)
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.ADD_TO_COLLECTION_POPUP_BACKGROUND_TAG)?.alpha = 0.0
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.ADD_TO_COLLECTION_POPUP_SLIDER_TAG)?.transform = up
        }, completion: { finished in
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.ADD_TO_COLLECTION_POPUP_BACKGROUND_TAG)?.removeFromSuperview()
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.ADD_TO_COLLECTION_POPUP_SLIDER_TAG)?.removeFromSuperview()
        })
    }
}
