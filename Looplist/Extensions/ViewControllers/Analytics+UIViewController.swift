//
//  Analytics+UIViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 9/21/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

private var timeOnScreenAssociationKey: UInt8 = 123

extension UIViewController {
    
    // MARK: -  /////////////////////////////////////////////////////    TIME ANALYTICS FUNCTIONS    /////////////////////////////////////////////////////////////////////////
    
    public class var screenStartTime: Date {
        get {
            if let date = objc_getAssociatedObject(self, &timeOnScreenAssociationKey) as? Date {
                return date
            } else {
                return Date.distantPast as Date
            }
            //return objc_getAssociatedObject(self, &timeOnScreenAssociationKey) as! Date
        }
        set(newValue) {
            objc_setAssociatedObject(self, &timeOnScreenAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    public class func startTimer(){
        self.screenStartTime = Date()
    }
    
    public class func stopTimer(_ parameters:NSDictionary, withActionType:String){
        let mutableParameters:NSMutableDictionary = parameters.mutableCopy() as! NSMutableDictionary
        let timePassed:Double = abs(self.screenStartTime.timeIntervalSinceNow)
        mutableParameters.setValue(AnalyticsConstants.getViewControllerAnalyticsName(self.getClassName()), forKey: "ScreenName")
        mutableParameters.setValue(self.getClassName(), forKey: "TechnicalName")

        let action:Action = Action(timeOnScreen: timePassed, withParameters: mutableParameters, andActionType: withActionType)

        Analytics.sharedInstance.actionsArray.append(action)
    }
    
//    public class func addAction(_ actionDictionary:NSDictionary){
//        let action:Action = Action(context:actionDictionary, forScreen:self.getClassName())
//        Analytics.sharedInstance.actionsArray.append(action)
//    }
    
    public class func getClassName() ->String{
        return NSStringFromClass(self.classForCoder()).components(separatedBy: ".").last!
    }
}
