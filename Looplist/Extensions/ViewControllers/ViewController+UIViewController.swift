//
//  ViewController+UIViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 7/18/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
//fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
//  switch (lhs, rhs) {
//  case let (l?, r?):
//    return l < r
//  case (nil, _?):
//    return true
//  default:
//    return false
//  }
//}


extension UIViewController {
    
    // MARK: -  /////////////////////////////////////////////////////    POP UP CONTROLLERS    /////////////////////////////////////////////////////////////////////////

    public class func instanceFromStoryboard<T>(_ storyboardName:String) -> T {
        return UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: String(describing: T.self)) as! T
    }
    
//    public class func getRepostPopup<T:UIViewController>(_ callingClass:UIViewController) -> T{
//        let popupVC:RepostPopupViewController = RepostPopupViewController.instanceFromStoryboard("Main")
//        
//        popupVC.view.frame = CGRect(x: 0, y: 10, width: UIScreen.main.bounds.width - 40, height: 232)
//        popupVC.callingClass = callingClass
//        
//        return popupVC as! T
//    }
//    
//    public class func getSelectAddressPopup<T:UIViewController>(callingClass:UIViewController) -> T{
//        let popupVC:ShippingAddressesViewController = ShippingAddressesViewController.instanceFromStoryboard("Shipping")
//        let maxPopupHeight = UIScreen.mainScreen().bounds.height - 220
//        var popupHeight:CGFloat
//        var tableHeight:CGFloat = CGFloat(120.0)
//        if let count = CurrentUser.sharedInstance.currentUser?.getShippingAddresesCount() where count != 0{
//          tableHeight = CGFloat((120 * count))
//        }
//        
//        if(tableHeight > maxPopupHeight){
//            popupHeight = maxPopupHeight
//        } else {
//            popupHeight = tableHeight
//        }
//        
//        popupVC.view.frame = CGRectMake(0,(UIScreen.mainScreen().bounds.height - popupHeight) / 2.0 , UIScreen.mainScreen().bounds.width - 40, popupHeight)
//        popupVC.callingClass = callingClass
//        
//        return popupVC as! T
//        
//    }
//    
//    
//    
//    public class func getSelectPaymentMethodPopup<T:UIViewController>(callingClass:UIViewController) -> T{
//        let popupVC:CCViewController = CCViewController.instanceFromStoryboard("CreditCard")
//        let maxPopupHeight = UIScreen.mainScreen().bounds.height - 220
//        var popupHeight:CGFloat
//        var tableHeight:CGFloat = CGFloat(75.0)
//        if let count = CurrentUser.sharedInstance.currentUser?.getPaymentMethodCount() where count != 0{
//            tableHeight = CGFloat((120 * count))
//        }
//        
//        if(tableHeight > maxPopupHeight){
//            popupHeight = maxPopupHeight
//        } else {
//            popupHeight = tableHeight
//        }
//        
//        popupVC.view.frame = CGRectMake(0,(UIScreen.mainScreen().bounds.height - popupHeight) / 2.0 , UIScreen.mainScreen().bounds.width - 40, popupHeight)
//        //popupVC.view.layer.cornerRadius = CGFloat(5.0)
//        popupVC.callingClass = callingClass
//        
//        return popupVC as! T
//        
//    }
    
    
    
    class func getDelegate() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    class func setNavigationBar(_ color:UIColor, font:UIFont, navigationController:UINavigationController, statusBar:UIStatusBarStyle, backgroundColor:NavigationBarBackgroundColor){
        navigationController.navigationBar.superview?.viewWithTag(99)?.removeFromSuperview()
        navigationController.navigationBar.viewWithTag(99)?.removeFromSuperview()
        navigationController.navigationBar.superview?.superview?.viewWithTag(99)?.removeFromSuperview()

        switch(backgroundColor){
        case .whiteOpaque:
            navigationController.navigationBar.isTranslucent = false
            let bounds = CGRect(x:0.0,y:-20.0,width:navigationController.navigationBar.frame.width,height:84.0)
            let backgroundView = UIView(frame: bounds)
            backgroundView.tag = 99
            backgroundView.backgroundColor = UIColor.white
            navigationController.navigationBar.superview?.insertSubview(backgroundView, at: 1)
            navigationController.navigationBar.backgroundColor = UIColor.white
            navigationController.navigationBar.barTintColor = UIColor.white
            navigationController.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            navigationController.navigationBar.shadowImage = UIImage()
            break;
        case .whiteTransparent:
            navigationController.navigationBar.isTranslucent = true
            navigationController.navigationBar.shadowImage = UIImage()
            navigationController.navigationBar.backgroundColor = UIColor.clear
            navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)

            let bounds = CGRect(x:0.0,y:-20.0,width:navigationController.navigationBar.frame.width,height:84.0)

            let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .light)) as UIVisualEffectView
            visualEffectView.backgroundColor = UIColor.white.withAlphaComponent(0.55)
            visualEffectView.frame = bounds
            visualEffectView.tag = 99
            visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            visualEffectView.isUserInteractionEnabled = false
            visualEffectView.tintColor = UIColor.clear
            navigationController.navigationBar.superview?.insertSubview(visualEffectView, at: 1)
            
            break;
        case .clear:
            navigationController.navigationBar.isTranslucent = true
            let navBarBackground  = UIImageView(frame:CGRect(x: 0, y: -10, width: UIScreen.main.bounds.size.width, height: 240));
            navBarBackground.image = UIImage(named: Constants.assetsUserProfileNavBar)
            navBarBackground.tag = 99
            navigationController.navigationBar.superview?.insertSubview(navBarBackground, at: 1)
            navigationController.navigationBar.shadowImage = UIImage()
            navigationController.navigationBar.backgroundColor = UIColor.clear
            navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
            break;
        case .clearNoGradient:
            navigationController.navigationBar.isTranslucent = true
            navigationController.navigationBar.barTintColor = UIColor.clear
            navigationController.navigationBar.backgroundColor = UIColor.clear
            navigationController.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            navigationController.navigationBar.shadowImage = UIImage()
            break;
        }
        
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = statusBar
        
        if DBHelper.RUNNING_CONFIG == 0{
            navigationController.navigationBar.tintColor = UIColor.red
            navigationController.navigationBar.titleTextAttributes = [NSFontAttributeName: font,NSForegroundColorAttributeName: UIColor.red]
        
//            navigationController.navigationBar.tintColor = color
//            navigationController.navigationBar.titleTextAttributes = [NSFontAttributeName: font,NSForegroundColorAttributeName: color]
            
            
        } else if DBHelper.RUNNING_CONFIG == 1{
            navigationController.navigationBar.tintColor = UIColor.yellow
            navigationController.navigationBar.titleTextAttributes = [NSFontAttributeName: font,NSForegroundColorAttributeName: UIColor.yellow]
        } else if DBHelper.RUNNING_CONFIG == 2{
            navigationController.navigationBar.tintColor = UIColor.orange
            navigationController.navigationBar.titleTextAttributes = [NSFontAttributeName: font,NSForegroundColorAttributeName: UIColor.orange]
        } else {
            navigationController.navigationBar.tintColor = color
            navigationController.navigationBar.titleTextAttributes = [NSFontAttributeName: font,NSForegroundColorAttributeName: color]
        }
    }
    
    class func setNavigationBarStatusAndFont(_ color:UIColor, font:UIFont, navigationController:UINavigationController, statusBar:UIStatusBarStyle){
        
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = statusBar
        
        if DBHelper.RUNNING_CONFIG == 0{
            navigationController.navigationBar.tintColor = UIColor.red
            navigationController.navigationBar.titleTextAttributes = [NSFontAttributeName: font,NSForegroundColorAttributeName: UIColor.red]
//
//            navigationController.navigationBar.tintColor = color
//            navigationController.navigationBar.titleTextAttributes = [NSFontAttributeName: font,NSForegroundColorAttributeName: color]
            
        } else if DBHelper.RUNNING_CONFIG == 1{
            navigationController.navigationBar.tintColor = UIColor.yellow
            navigationController.navigationBar.titleTextAttributes = [NSFontAttributeName: font,NSForegroundColorAttributeName: UIColor.yellow]
        } else if DBHelper.RUNNING_CONFIG == 2{
            navigationController.navigationBar.tintColor = UIColor.orange
            navigationController.navigationBar.titleTextAttributes = [NSFontAttributeName: font,NSForegroundColorAttributeName: UIColor.orange]
        } else {
            navigationController.navigationBar.tintColor = color
            navigationController.navigationBar.titleTextAttributes = [NSFontAttributeName: font,NSForegroundColorAttributeName: color]
        }
    }

    class func popSlideMotionOn(_ navigationController:UINavigationController) {
        navigationController.interactivePopGestureRecognizer?.isEnabled = true
        navigationController.interactivePopGestureRecognizer?.delegate = nil
    }
    
    class func popSlideMotionOff(_ navigationController:UINavigationController) {
        navigationController.interactivePopGestureRecognizer?.isEnabled = false
        navigationController.interactivePopGestureRecognizer?.delegate = nil
    }
}
