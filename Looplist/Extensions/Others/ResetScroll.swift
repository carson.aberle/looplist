//
//  ResetScroll.swift
//  Looplist
//
//  Created by Roman Wendelboe on 8/24/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

extension UIScrollView {
    /// Sets content offset to the top.
    func resetScrollPositionToTop() {
        self.contentOffset = CGPoint(x: -contentInset.left, y: -contentInset.top)
    }
}
