//
//  Localization+UIString.swift
//  Looplist
//
//  Created by Carson Aberle on 1/5/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
