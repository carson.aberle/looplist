
//
//  String+NSNumber.swift
//  Looplist
//
//  Created by Carson Aberle on 8/3/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

//extension Double {
//    func stringToDecimalPlaces(_ fractionDigits:Int) -> String {
//        let formatter = NumberFormatter()
//        formatter.minimumFractionDigits = fractionDigits
//        formatter.maximumFractionDigits = fractionDigits
//        return formatter.string(from: NSNumber(value:self)) ?? "\(self)"
//    }
//}
//
//extension Float {
//    func stringToDecimalPlaces(_ fractionDigits:Int) -> String {
//        let formatter = NumberFormatter()
//        formatter.minimumFractionDigits = fractionDigits
//        formatter.maximumFractionDigits = fractionDigits
//        return formatter.string(from: NSNumber(value:self)) ?? "\(self)"
//    }
//}

extension String {
    func substringFromIndex(_ index: Int) -> String {
        if (index < 0 || index > self.characters.count) {
            return ""
        }
        return self.substring(from: self.characters.index(self.startIndex, offsetBy: index))
    }
}
