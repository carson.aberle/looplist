//
//  NavigationBarBlue.swift
//  Looplist
//
//  Created by Arun Sivakumar on 11/6/16.
//  Copyright © 2016 Looplist. All rights reserved.
//
//
//import UIKit
//
//extension UINavigationBar {
//    
//    func setBottomBorderColor() {
//        let bottomBorderRect = CGRect(x: 0, y: frame.height, width: frame.width, height: 1.0)
//        let bottomBorderView = UIView(frame: bottomBorderRect)
//        bottomBorderView.backgroundColor = ConstantsColor.looplistColor
//        addSubview(bottomBorderView)
//    }
//    
//    func setBottomBorderClearColor() {
//        let bottomBorderRect = CGRect(x: 0, y: frame.height, width: frame.width, height: 1.0)
//        let bottomBorderView = UIView(frame: bottomBorderRect)
//        bottomBorderView.backgroundColor = UIColor.clear
//        addSubview(bottomBorderView)
//    }
//    
//
//    
//}
