//
//  FakeNavBar.swift
//  Looplist
//
//  Created by Roman Wendelboe on 1/26/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

open class FakeNavBar{

    private var fakeNavBarView:  UIView!
    private var visualEffectView: UIVisualEffectView!
    
    open func removeFakeNavBarView(_ kind: String){
        switch kind {
        case "White":
            if let fNavBar = self.fakeNavBarView {
                fNavBar.removeFromSuperview()
            }
        default:
            if let vToRemove = self.visualEffectView {
                vToRemove.removeFromSuperview()
            }
        }
    }

    open func addFakeNavBarView(_ kind: String, uView: UIView, offset: CGFloat){
        switch kind {
        case "White":
            self.fakeNavBarView = UIView(frame: CGRect(x: 0, y: offset, width: uView.frame.width, height: 64.0))
            fakeNavBarView.backgroundColor = UIColor.white
            
            uView.addSubview(fakeNavBarView)
            uView.bringSubview(toFront: fakeNavBarView)
        default:
            let bounds = CGRect(x:0.0, y: offset, width:uView.frame.width, height: 64.0)
            
            self.visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
            visualEffectView.backgroundColor = UIColor.white.withAlphaComponent(0.55)
            visualEffectView.frame = bounds
            visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            visualEffectView.isUserInteractionEnabled = false
            visualEffectView.tintColor = UIColor.clear
            uView.addSubview(visualEffectView)
            uView.bringSubview(toFront: visualEffectView)
        }
    }
}
