//
//  NavigationController.swift
//  Looplist
//
//  Created by Arun Sivakumar on 11/23/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation


extension UINavigationController{
    
    func showProductScreen(_ product:Product){
        self.popToRootViewController(animated: false)
        let vc:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
        vc.product = product
        self.pushViewController(vc, animated: true)
    }

}
