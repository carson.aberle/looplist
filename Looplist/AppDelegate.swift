//
//  AppDelegate.swift
//  Alpha
//
//  Created by Kade France on 11/23/15.
//  Copyright © 2015 Kade France. All rights reserved.
//

import UIKit
import Stripe
import CoreSpotlight
import MobileCoreServices
import FacebookCore
import FBSDKLoginKit
import Fabric
import Crashlytics
import Fabric
import TwitterKit
import Branch

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UINavigationControllerDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        // Stripe Configuration
        Stripe.setDefaultPublishableKey(ConstantsIdentification.StripeKey)
        
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: UIFont(name: ConstantsFont.fontDefault, size: 32)!]
        
        
        
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
        
        UITextField.appearance().keyboardAppearance = .dark
        UISearchBar.appearance().keyboardAppearance = .dark
        
        let font: UIFont = UIFont(name: ConstantsFont.fontDefault, size: 22)!
        let color = UIColor.darkGray
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: font,NSForegroundColorAttributeName: color]
        
        //        UITabBar.appearance().tintColor = UIColor.white
        //        UITabBar.appearance().isTranslucent = true
        //        UITabBar.appearance().barStyle = .black
        //        UITabBar.appearance().alpha = 0.7
        
        //        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
        //        let pushNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
        
        //        application.registerUserNotificationSettings(pushNotificationSettings)
        //        application.registerForRemoteNotifications()
        
        let branch: Branch = Branch.getInstance()
        branch.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: {params, error in
            if error == nil {
                // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
                // params will be empty if no data found
                if let productId = params?["productId"] as? String{
                    DBHelper.callAPIGet("products/\(productId)", completion: { (result, error) in
                        if error == nil{
                            let product = Product(input: result?["product"] as! Dictionary<String,AnyObject>)
                            if let tabBarController = self.window?.rootViewController as? UITabBarController{
                                
                                let navigationController = tabBarController.selectedViewController as! UINavigationController
                                navigationController.popToRootViewController(animated: false)
                                let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
                                productVC.product = product
                                navigationController.pushViewController(productVC, animated: false)
                            } else {
                                DBHelper.getCurrentUser { (success) in
                                    if success{
                                        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let showScreen = storyboard.instantiateViewController(withIdentifier: ConstantsViewController.viewControllerTabBar) as! UITabBarController
                                        self.window = UIWindow(frame:UIScreen.main.bounds)
                                        let navigationController = showScreen.selectedViewController as! UINavigationController
                                        navigationController.popToRootViewController(animated: false)
                                        let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
                                        productVC.product = product
                                        navigationController.pushViewController(productVC, animated: false)
                                        self.window!.rootViewController = showScreen
                                        self.window!.makeKeyAndVisible()
                                    } else{
                                        DBHelper.logoutAndClearDefaults()
                                        self.showScreen(ConstantsViewController.viewControllerInitial)
                                        
                                    }
                                }
                            }
                        }
                    })
                    
                } else  if let userId = params?["userId"] as? String {
                    APIHelperAnyUser.getGuestUser(userId, completion: { (user) in
                        if let tabBarController = self.window?.rootViewController as? UITabBarController{
                            
                            let navigationController = tabBarController.selectedViewController as! UINavigationController
                            navigationController.popToRootViewController(animated: false)
                            let userVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
                            userVC.currentUser = user
                            navigationController.pushViewController(userVC, animated: false)
                        } else {
                            DBHelper.getCurrentUser { (success) in
                                if success{
                                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let showScreen = storyboard.instantiateViewController(withIdentifier: ConstantsViewController.viewControllerTabBar) as! UITabBarController
                                    self.window = UIWindow(frame:UIScreen.main.bounds)
                                    let navigationController = showScreen.selectedViewController as! UINavigationController
                                    navigationController.popToRootViewController(animated: false)
                                    let userVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
                                    userVC.currentUser = user
                                    navigationController.pushViewController(userVC, animated: false)
                                    self.window!.rootViewController = showScreen
                                    self.window!.makeKeyAndVisible()
                                } else{
                                    DBHelper.logoutAndClearDefaults()
                                    self.showScreen(ConstantsViewController.viewControllerInitial)
                                    
                                }
                            }
                        }
                    })
                }
            }
        })
        
        loginIntoLooplist(application)
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        
        Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.sendAnalytics), userInfo: nil, repeats: true)
        
        //        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
        //        let pushNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
        //        application.registerUserNotificationSettings(pushNotificationSettings)
        //        application.registerForRemoteNotifications()
        Fabric.with([STPAPIClient.self, Crashlytics.self, Twitter.self])
        // TODO: Move this to where you establish a user session
        self.logUser()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        
        // Handle quick actions
        completionHandler(Handle3DTouch(shortcutItem: shortcutItem))
        
    }
    
    enum Shortcut: String {
        case openUserProfile = "openUserProfile"
        case newMessage = "newMessage"
        case search = "search"
    }
    
    func Handle3DTouch(shortcutItem: UIApplicationShortcutItem) -> Bool {
        
        var actionHandled = false
        
        let type = shortcutItem.type.components(separatedBy: ".").last!
        //        let type = shortcutItem.type.componentsSeparatedByString(".").last!
        if let shortcutType = Shortcut.init(rawValue: type) {
            if let tabBarController = self.window?.rootViewController as? UITabBarController{
                
                switch shortcutType {
                case .openUserProfile:
                    //                    let vc:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
                    //                     navigationController.pushViewController(vc, animated: true)
                    if let navController = tabBarController.viewControllers?[4] as? UINavigationController{
                        tabBarController.selectedIndex = 4
                        navController.popToRootViewController(animated: false)
                    }
                case .newMessage:
                    let vc:MessagingNewMessageViewController = MessagingNewMessageViewController.instanceFromStoryboard("Messaging")
                    //                     ViewControllerExtensions.
                    //                        .pushViewController(vc, animated: true)
                    //                    tabBarController.selectedIndex = 1
                    
                    if let navController = tabBarController.viewControllers?[1] as? UINavigationController{
                        tabBarController.selectedIndex = 1
                        navController.pushViewController(vc, animated: true)
                    }
                case .search:
                    //                    let vc:DiscoverViewController = DiscoverViewController.instanceFromStoryboard("Main")
                    //                     navigationController.pushViewController(vc, animated: true)
                    if let navController = tabBarController.viewControllers?[2] as? UINavigationController{
                        tabBarController.selectedIndex = 2
                        navController.popToRootViewController(animated: false)
                    }
                }
                
                
                actionHandled = true
                
                
            }else{
                // Handle in a different way
            }
        }
        
        return actionHandled
    }
    
    
    
    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        if let user = CurrentUser.sharedInstance.currentUser, let email = user.email, let uid = user.objectId,let userName = user.userName{
            Crashlytics.sharedInstance().setUserEmail(email)
            Crashlytics.sharedInstance().setUserIdentifier(uid)
            Crashlytics.sharedInstance().setUserName(userName)
        }
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    func sendAnalytics(){
        if Analytics.sharedInstance.actionsArray.count > 0 {
            var array = [NSMutableDictionary]()
            for action in Analytics.sharedInstance.actionsArray{
                let tempActionDict = NSMutableDictionary()
                tempActionDict.setValue(Double(action.timestamp!), forKey: "timestamp")
                if (action.context != nil){
                    tempActionDict.setValue(action.context, forKey: "context")
                }
                if (action.duration != nil){
                    tempActionDict.setValue(action.duration, forKey: "duration")
                }
                if (action.productId != nil){
                    tempActionDict.setValue(action.productId, forKey: "productId")
                }
                tempActionDict.setValue(action.actionType, forKey: "actionType")
                tempActionDict.setValue(action.source, forKey: "source")
                tempActionDict.setValue(action.version, forKey: "version")
                array.append(tempActionDict)
            }
            Analytics.sharedInstance.actionsArray.removeAll()
            
            if(DBHelper.getAccessToken() == ""){
                DBHelper.callAPIPostForDefaultAnalytics("actions", params: ["actions":array as AnyObject], completion: { (result, error) in
                    if error == nil{
                        
                    } else {
                        
                    }
                })
            } else {
                DBHelper.callAPIPostForAnalytics("actions", params: ["actions":array as Optional<AnyObject>], completion: { (result, error) in
                    if error == nil{
                        
                    } else {
                        
                    }
                })
            }
            
            
        }
    }
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        Branch.getInstance().handleDeepLink(url);
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //Send Device Token to API
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var tokenString = ""
        
        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        
        if let objectId = CurrentUser.sharedInstance.currentUser?.objectId, let identifierForVendor = UIDevice.current.identifierForVendor{
            let uuid = identifierForVendor.uuidString
            DBHelper.callAPIPost("devices", params: ["deviceType":"ios" as Optional<AnyObject>, "pushToken": tokenString as Optional<AnyObject>, "user": objectId as Optional<AnyObject>, "uuid": uuid as Optional<AnyObject>]) { (result, error) in
                if error == nil{
                    if let device = result?["device"] as? NSDictionary{
                        if let deviceId = device["_id"] as? String {
                            UserDefaults.standard.set(deviceId, forKey: ConstantsIdentification.defaultsDeviceId)
                            
                        }
                    }
                }
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
    //    func registerForRemoteNotification(application:UIApplication){
    //                let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
    //                let pushNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
    //
    //                application.registerUserNotificationSettings(pushNotificationSettings)
    //                application.registerForRemoteNotifications()
    //
    ////                UIApplication.sharedApplication().registerUserNotificationSettings(pushNotificationSettings)
    ////                UIApplication.sharedApplication().registerForRemoteNotifications()
    //
    //    }
    
    
    func loginIntoLooplist(_ application:UIApplication){
        
        if let _ = UserDefaults.standard.string(forKey: ConstantsIdentification.defaultsObjectId){
            
            DBHelper.getCurrentUser { (success) in
                if success{
                    self.addLikesToSpotlight()
                    //                    self.registerForRemoteNotification(application)
                    self.findUnreadMessages()
                    self.loadShippingAddresses()
                    self.loadCC()
                    self.showScreen(ConstantsViewController.viewControllerTabBar)
                    
                } else{
                    DBHelper.logoutAndClearDefaults()
                    self.showScreen(ConstantsViewController.viewControllerInitial)
                    
                }
            }
            
        }else{
            self.showScreen(ConstantsViewController.viewControllerInitial)
        }
    }
    
    func loadShippingAddresses(){
        DispatchQueue.global(qos: .background).async {
             if let user = CurrentUser.sharedInstance.currentUser{
                user.getUserShippingAddresses(true, skip: 0, limit: 100, completion: { (success) in})
             }
        }
    }
    
    func loadCC(){
         DispatchQueue.global(qos: .background).async {
            if let user = CurrentUser.sharedInstance.currentUser{
                user.getUserCC(true, skip: 0, limit: 100, completion: { (success) in})
            }
        }
    }
    
    func findUnreadMessages(){
        DispatchQueue.global(qos: .background).async {
            MessagingManager.findMessageUnreadCount({ (count) in
                if let count = count, count > 0 {
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "showMessagingNotificationBadge"), object:nil)
                }
            })
        }
        
    }
    
    func showScreen(_ screen:String){
        var storyboard:UIStoryboard
        switch(screen){
        case ConstantsViewController.viewControllerSignIn:
            storyboard = UIStoryboard(name: "LoginAndRegistration", bundle: nil)
        case ConstantsViewController.viewControllerInitial:
            storyboard = UIStoryboard(name: "LoginAndRegistration", bundle: nil)
        default:
            storyboard = UIStoryboard(name: "Main", bundle: nil)
            break
        }
        let showScreen = storyboard.instantiateViewController(withIdentifier: screen)
        self.window = UIWindow(frame:UIScreen.main.bounds)
        self.window!.rootViewController = showScreen
        self.window!.makeKeyAndVisible()
    }
    
    func addLikesToSpotlight(){
        DispatchQueue.global(qos: .background).async {
            
            DBHelper.callAPIGet("activities", queryString: DBHelper.getJSONFromDictionary(["where":["activityType":"Like Product"], "populate":[["path":"product"]]])) { (result, error) in
                if error == nil{
                    if let activitiesArrayResult = result?["activities"] as? NSArray{
                        CSSearchableIndex.default().deleteAllSearchableItems(completionHandler: { (error) in
                            for index in 0..<activitiesArrayResult.count{
                                let activity = Activity(input: activitiesArrayResult[index] as! Dictionary<String, AnyObject>)
                                if let productDictionary = activity?.attributes?["product"] as? Dictionary<String, AnyObject>{
                                    if let product = Product(input: productDictionary){
                                        
                                        let attributeSet = CSSearchableItemAttributeSet(itemContentType: kUTTypeText as String)
                                        attributeSet.title = product.brandName
                                        //                                ### changed to optional chaining - Arun
                                        //                                    if let brand = product.brand{
                                        //
                                        //                                    }
                                        attributeSet.contentDescription = product.brandName
                                        if (product.images?.count)! > 0{
                                            if let imageURLString = product.images?[0]{
                                                let url = URL(string: imageURLString)
                                                if url != nil{
                                                    attributeSet.thumbnailURL = url
                                                    attributeSet.thumbnailData = try? Data(contentsOf: url!)
                                                }
                                            }
                                        }
                                        //                                ### changed to optional chaining - Arun
                                        
                                        if let description = product.descriptionAttributedString{
                                            attributeSet.keywords = [description.string, product.brandName,"shopping"]
                                        }
                                        
                                        let item = CSSearchableItem(uniqueIdentifier: product.objectId ?? "", domainIdentifier: "com.looplist.looplist", attributeSet: attributeSet)
                                        CSSearchableIndex.default().indexSearchableItems([item], completionHandler: { (error) in
                                            
                                        })
                                    }
                                    
                                }
                            }
                            
                        })
                    }
                } else {
                    
                }
            }
        }
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        
        Branch.getInstance().continue(userActivity)
        
        if userActivity.activityType == CSSearchableItemActionType {
            if let uniqueIdentifier = userActivity.userInfo?[CSSearchableItemActivityIdentifier] as? String {
                DBHelper.callAPIGet("products/\(uniqueIdentifier)", completion: { (result, error) in
                    if error == nil{
                        let product = Product(input: result?["product"] as! Dictionary<String,AnyObject>)
                        if let tabBarController = self.window?.rootViewController as? UITabBarController{
                            //                            if let searchVC = tabBarController.presentedViewController as? SearchNavigationController{
                            //                                searchVC.dismissViewControllerAnimated(false, completion: nil)
                            //                            }
                            let navigationController = tabBarController.selectedViewController as! UINavigationController
                            navigationController.popToRootViewController(animated: false)
                            let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
                            productVC.product = product
                            navigationController.pushViewController(productVC, animated: false)
                        } else {
                            DBHelper.getCurrentUser { (success) in
                                if success{
                                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let showScreen = storyboard.instantiateViewController(withIdentifier: ConstantsViewController.viewControllerTabBar) as! UITabBarController
                                    self.window = UIWindow(frame:UIScreen.main.bounds)
                                    let navigationController = showScreen.selectedViewController as! UINavigationController
                                    navigationController.popToRootViewController(animated: false)
                                    let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
                                    productVC.product = product
                                    navigationController.pushViewController(productVC, animated: false)
                                    self.window!.rootViewController = showScreen
                                    self.window!.makeKeyAndVisible()
                                } else{
                                    DBHelper.logoutAndClearDefaults()
                                    self.showScreen(ConstantsViewController.viewControllerInitial)
                                    
                                }
                            }
                        }
                    }
                })
            }
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        self.showNotification(userInfo as! [String:AnyObject], application:application)
        
        application.applicationIconBadgeNumber = 0;
        
    }
    
    
    internal func showPages(_ notification:[String:AnyObject],application:Int){
        let tabBarController = self.window?.rootViewController as! UITabBarController
        if notification["type"] as! String == "message"{
            if let chatId = notification["chatId"] as? String{
                if let messageId = notification["messageId"] as? String{
                    
                    if tabBarController.selectedIndex == 1{ // inbox or conversation screen
                        
                        let index = tabBarController.selectedIndex
                        if let navigationController = tabBarController.viewControllers?[index] as? UINavigationController{
                            
                            
                            if let activeViewController = navigationController.topViewController as? MessagingViewController{
                                if let currentChatId = activeViewController.messageConversation?.chatId, currentChatId == chatId{ // current conversation
                                    
                                    MessagingManager.getMessageFromPushNotification(messageId: messageId, completion: { (message) in
                                        if let message = message{
                                            activeViewController.handleIncomingMessages(message)
                                        }
                                    })
                                } // else ignore the message
                                
                            }else if let activeViewController = navigationController.topViewController as? MessagingInboxTableViewController{
                                activeViewController.reload()
                            }
                        }
                        
                    }else{ // in app notification and increment tab bar badge count
                        MessagingManager.getConversations(false, chatId: chatId, skip: nil, limit: nil, completion: { (conversation) in //### check chat
                            
                            
                            if let conversation = conversation, conversation.count >= 1{
                                let messageConversation = conversation[0]
                                
                                
                                let index = tabBarController.selectedIndex
                                if let navigationController = tabBarController.viewControllers?[index] as? UINavigationController{
                                    // check
                                    if messageConversation.userLastMessage != nil{
                                        
                                        //var sender = ""
                                        //var senderProfileImage = UIImage(named: Constants.defaultMaleImage)
                                        
                                        if let user = messageConversation.user, let _ = user.fullName{ // user exists
                                            //sender = userfullName
                                            if let profileImageUrl = user.profileImageUrl,let url = URL(string: profileImageUrl){
                                                
                                                let userProfilePictureImage = UIImageView()
                                                
                                                userProfilePictureImage.kf.setImage(with:(url), placeholder: nil, options: nil, progressBlock: nil, completionHandler:{ (image, error, cacheType, imageURL) -> () in
                                                    if let image = image{
                                                        //user.profileImage = image
                                                        //senderProfileImage = image
                                                    }
                                                })
                                                
                                            }else if let gender = user.gender, gender == "Female"{
                                                //senderProfileImage = UIImage(named: Constants.defaultFemaleImage)
                                            }
                                        }else{ // Maven
                                            //sender = ConstantPlaceHolders.messagingMaven
                                            //senderProfileImage = UIImage(named: Constants.mavenImage)
                                        }
                                        
                                        
                                        if application == 0{ // In app Notification background
                                            
                                            //###$AS Turned Off - Rob's suggestion - Dec 15, 2015
                                            //                                            let announcement = Announcement(title: sender, subtitle: message, image: senderProfileImage, duration: 2.0, action: {
                                            //
                                            //                                                let messagingVC:MessagingViewController = MessagingViewController.instanceFromStoryboard("Messaging")
                                            //                                                messagingVC.messageConversation = messageConversation

                                            //                                                navigationController.pushViewController( messagingVC, animated: true)
                                            //
                                            //                                            })
                                            //                                            show(shout: announcement, to: navigationController, completion: {
                                            //
                                            //                                            })
                                        }else if application ==  1{ //foreground
                                            let messagingVC:MessagingViewController = MessagingViewController.instanceFromStoryboard("Messaging")
                                            messagingVC.messageConversation = messageConversation
                                            navigationController.pushViewController( messagingVC, animated: true)
                                        }
                                    }
                                }
                            }
                        })
                        //                        CurrentUser.sharedInstance.unreadMessageCount += 1
                        //                        tabBarController.tabBar.items?[1].badgeValue = "\(CurrentUser.sharedInstance.unreadMessageCount)"
                        
                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "showMessagingNotificationBadge"), object:nil)
                    }
                }
            }
        }
    }
    
    internal func showNotification(_ notification:[String:AnyObject], application:UIApplication){
        if let tabBarController = self.window?.rootViewController as? UITabBarController{
            //print(notification)
            if notification.count != 0{
                
                if let notificationType = notification["type"] as? String,notificationType == "collectionCreated"{
                    if let aps = notification["aps"] as? NSDictionary{
                        if let alert = aps["alert"] as? String{
                            if application.applicationState == UIApplicationState.active{
                                ErrorHandling.customSuccessHandler(message: alert)
                            }else {
                                if let collectionId = notification["collectionId"] as? String{ // take to collection View Controller
                                    DBHelper.callAPIGet("collections/\(collectionId)",queryString:DBHelper.getJSONFromDictionary(["populate":[["path":"owner"]]]), completion: { (result, error) in
                                        if let result = result{
                                            if let collectionDictionary = result["collection"] as? Dictionary<String,AnyObject>{
                                                let collection = Collection(input:collectionDictionary)
                                                let collectionLikesVC:CollectionLikesCollectionViewController = CollectionLikesCollectionViewController.instanceFromStoryboard("Main")
                                                collectionLikesVC.collection = collection
                                                let navigationController = tabBarController.viewControllers![0] as! UINavigationController
                                                navigationController.pushViewController(collectionLikesVC, animated: true)
                                            }
                                        }else if let error = error{
                                            print(error)
                                        }
                                    })
                                    
                                }
                            }
                        }
                    }
                }
            
                if let userId = notification["userId"] as? String {
                    if notification["type"] as! String == "follow"{
                        Analytics.sharedInstance.actionsArray.append(Action(context:["user": userId], forScreen:nil, withActivityType:"Received Follow Notification"))
                        
                        APIHelperAnyUser.getGuestUser(userId, completion: { (user) in
                            if application.applicationState == UIApplicationState.active{
                                //                                DropDownAlert.customSuccessMessage("\(user!.firstName!) \(user!.lastName!) followed you!")
                                if let firstName = user?.firstName, let lastName = user?.lastName{
                                    ErrorHandling.customSuccessHandler(message: "\(firstName) \(lastName) followed you!")
                                }
                            } else {
                                let guestUserVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
                                guestUserVC.currentUser = user
                                let navigationController = tabBarController.viewControllers![0] as! UINavigationController
                                navigationController.pushViewController(guestUserVC, animated: true)
                            }
                        })
                    }
                } else if notification["type"] as! String == "message"{
                    if let chatId = notification["chatId"] as? String{
                        Analytics.sharedInstance.actionsArray.append(Action(context:["messageId": chatId], forScreen:nil, withActivityType:"Received Message Notification"))
                    }
                    self.showPages(notification,application: application.applicationState.rawValue)
                    
                }else if notification["type"] as! String == "productApproved"{
                    if let productId = notification["productId"] as? String {
                        Analytics.sharedInstance.actionsArray.append(Action(context:["product": productId], forScreen:nil, withActivityType:"Received Product Approval Notification"))
                        
                        let path = "products/\(productId)"
                        DBHelper.callAPIGet(path, completion: { (result, error) in
                            if let result = result, let input = result["product"] as? Dictionary<String,AnyObject>{
                                
                                let product = Product(input:input)
                                let vc:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
                                vc.product = product
                                let navigationController = tabBarController.viewControllers![0] as! UINavigationController
                                navigationController.pushViewController(vc, animated: true)
                            }else if let _ = error{
                                
                            }
                        })
                    }
                }
            }
        }else {
            if let _ = UserDefaults.standard.string(forKey: ConstantsIdentification.defaultsObjectId){
                DBHelper.getCurrentUser { (success) in
                    if success{
                        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let showScreen = storyboard.instantiateViewController(withIdentifier: ConstantsViewController.viewControllerTabBar)
                        self.window = UIWindow(frame:UIScreen.main.bounds)
                        self.window!.rootViewController = showScreen
                        self.window!.makeKeyAndVisible()
                        let when = DispatchTime.now() + 2
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            self.showPages(notification,application: application.applicationState.rawValue)
                        }
                        
                    } else{
                        DBHelper.logoutAndClearDefaults()
                        self.showScreen(ConstantsViewController.viewControllerInitial)
                        
                    }
                }
                
            }else{
                self.showScreen(ConstantsViewController.viewControllerInitial)
            }
            
        }
    }
    
}
