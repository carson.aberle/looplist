//
//  UserProfileEditPasswordViewController.swift
//  alphalooplist
//
//  Created by Looplist Inc on 2/14/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import UIKit

class EditProfilePasswordViewController: UITableViewController {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var changePasswordToEmailText: UITextField!
    
    @IBOutlet weak var enterEmailText: UITextField!
    @IBOutlet weak var resetPasswordButton: UIButton!
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        EditProfilePasswordViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        EditProfilePasswordViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.enterEmailText.placeholder = "ENTER_YOUR_REGISTERED_EMAIL".localized
        self.navigationItem.title = "PASSWORD".localized
        self.resetPasswordButton.setTitle("RESET_PASSWORD".localized, for:.normal)
        addKeyboardHideTap()
    }

    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController{
            EditProfilePasswordViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func changePassword(_ sender: UIButton) {
        self.view.endEditing(true)
        if let userEnteredEmail = changePasswordToEmailText.text,let username = CurrentUser.sharedInstance.currentUser?.userName,let currentUserEmail = CurrentUser.sharedInstance.currentUser?.email {
            if !HelperFunction.validateEmail(userEnteredEmail){
                ErrorHandling.customErrorHandler(message: ErrorHandling.ResetPasswordErrorValidEmail)
            }else if userEnteredEmail != currentUserEmail{
                ErrorHandling.customErrorHandler(message: ErrorHandling.ResetPasswordErrorRegisteredEmail)
            }else{
                
                APIHelperUser.changePassword(username: username)
                
//                DBHelper.callAPIPost("authentication/local/reset-password", params: ["username":username as Optional<AnyObject>], completion: { (result, error) in
//                    if let _ = result{
//                            ErrorHandling.customErrorHandlerWithTitle(title:"Success!", message: ErrorHandling.ResetPasswordSuccess)
//                    }else if let error = error{
//                            ErrorHandling.customErrorHandler(message: error.localizedDescription)
//                    }
//                })
            }
        }else{
            ErrorHandling.unexpectedErrorHandler()
        }
    }
    
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "CHANGE_PASSWORD".localized
    }
}


// MARK: -  //////////////////////////////////////////////////////   EXTENSION   /////////////////////////////////////////////////////////////////////////

extension EditProfilePasswordViewController{
    func addKeyboardHideTap(){
        let hideTap = UITapGestureRecognizer(target: self, action: #selector(EditProfilePasswordViewController.hideKeyboardTap(_:)))
        hideTap.numberOfTapsRequired = 1
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(hideTap)
    }
    
    // Hide keyboard when view is Tapped - 1
    func hideKeyboardTap(_ recognizer:UITapGestureRecognizer){
        self.view.endEditing(true)
    }
}
