//
//  UserProfileEditViewController.swift
//  alphalooplist
//
//  Created by Looplist Inc on 1/26/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import Foundation
import UIKit
import BetterSegmentedControl
import SwiftyJSON
import Alamofire

class EditProfileViewController: UITableViewController, UITextFieldDelegate, UITextViewDelegate{
    
    // MARK: - Variables
    private var genderSelected = "N/A"
    
    
    var valueChangedForUsername = false
    var valueChangedForFirstName = false
    var valueChangedForLastName = false
    var valueChangedForLocation = false
    
    
    var valuesChangedCount = 0
    var saveButton: UIBarButtonItem!
    var backButton: UIBarButtonItem!

    @IBOutlet weak var editGender: UISegmentedControl!
    @IBOutlet weak var editUsernameText: UITextField!
    @IBOutlet weak var editFirstNameText: UITextField!
    @IBOutlet weak var editLastNameText: UITextField!
    @IBOutlet weak var editLocationText: UITextField!
    @IBOutlet weak var editBio: UITextView!
    
    @IBOutlet weak var editEmailText: UITextField!
    @IBOutlet weak var genderSelectorSegmentedControl: BetterSegmentedControl!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.hidden = true
        
        editUsernameText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        editFirstNameText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        editLastNameText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        editLocationText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.editEmailText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.editBio.delegate = self
        self.editUsernameText.delegate = self
        self.editFirstNameText.delegate = self
        self.editLastNameText.delegate = self
        self.editLocationText.delegate = self
        self.editEmailText.delegate = self
        self.editEmailText.tag = 0
        self.editUsernameText.tag = 2
        self.editFirstNameText.tag = 3
        self.editLastNameText.tag = 4
        self.editLocationText.tag = 5
        self.editBio.tag = 7

        
        self.tableView.tableFooterView = UIView()
        self.saveButton = UIBarButtonItem(title: "Save", style: .Plain, target: nil, action: #selector(self.saveUserData(_:)))
        self.backButton = UIBarButtonItem(image: UIImage(named:"Back"), style: .Plain, target: self, action: #selector(self.goback))
        genderSelectorSegmentedControl.titleFont = UIFont(name: "SFUIDisplay-Light", size: 12)!
        genderSelectorSegmentedControl.selectedTitleFont = UIFont(name: "SFUIDisplay-Light", size: 12)!
        genderSelectorSegmentedControl.titles = ["N/A","Male","Female"]
        loadUserData()


    }
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    override func viewDidLayoutSubviews() {
        if let navController = self.navigationController{
            EditProfileViewController.self.setNavigationBar(UIColor.darkGrayColor(), font: Constants.navBarFont, navigationController: navController, statusBar: "Dark", backgroundColor: "White")
            
            self.navigationController!.navigationBar.topItem!.title = "Edit Profile"
            
            backButton.tintColor = UIColor.darkGrayColor()
            self.navigationItem.leftBarButtonItem = backButton
            
            self.saveButton.tintColor = UIColor.darkGrayColor()
            self.navigationItem.rightBarButtonItem = self.saveButton
            self.saveButton.enabled = false
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
    }

    func loadUserGender(userGender:String){
        if userGender == "Male"{
            do {
                try genderSelectorSegmentedControl.setIndex(1, animated: false)
            }
            catch {
                ErrorHandling.unexpectedErrorHandler()
            }
        }else if userGender == "Female"{
            do {
                try genderSelectorSegmentedControl.setIndex(2, animated: false)
            }
            catch {
                ErrorHandling.unexpectedErrorHandler()
            }
        }else if userGender == "N/A"{
            do {
                try genderSelectorSegmentedControl.setIndex(0, animated: false)
            }
            catch {
                ErrorHandling.unexpectedErrorHandler()
            }
        }
    }
    
    // MARK: - Functions
    func loadUserData(){
        
        if let user = CurrentUser.sharedInstance.currentUser, let userName = user.userName, let firstName = user.firstName, let lastName = user.lastName, let gender = user.gender,let location = user.location{
            
            self.editUsernameText.text = userName
            self.editFirstNameText.text = firstName
            self.editLastNameText.text = lastName
            self.loadUserGender(gender)
            self.editLocationText.text = location
        }
        
        if let bio =  CurrentUser.sharedInstance.currentUser?.bio{
            self.editBio.text = bio
        }
        
        if let email =  CurrentUser.sharedInstance.currentUser?.email{
            self.editEmailText.text = email
        }
        
    }
    
    func textViewDidChange(textView: UITextView){
        if let bio = CurrentUser.sharedInstance.currentUser?.bio{
            if bio != self.editBio.text!{
                saveButton.enabled = true
            }
        } else {
            saveButton.enabled = true
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).stringByReplacingCharactersInRange(range, withString: text)
        var textWidth = CGRectGetWidth(UIEdgeInsetsInsetRect(textView.frame, textView.textContainerInset))
        textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
        
        let boundingRect = sizeOfString(newText, constrainedToWidth: Double(textWidth), font: textView.font!)
        let numberOfLines = boundingRect.height / textView.font!.lineHeight;

        
        let newQuoteBio = (textView.text as NSString).stringByReplacingCharactersInRange(range, withString: text)
        let numberOfChars = newQuoteBio.characters.count
        if numberOfChars < 150 && numberOfLines <= 4 {
            return true
        } else {
            return false
        }
    }
    
    func sizeOfString (string: String, constrainedToWidth width: Double, font: UIFont) -> CGSize {
        return (string as NSString).boundingRectWithSize(CGSize(width: width, height: DBL_MAX),
                                                         options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                                                         attributes: [NSFontAttributeName: font],
                                                         context: nil).size
    }
    
    func textFieldDidChange(textField: UITextField) {
        if CurrentUser.sharedInstance.currentUser?.userName == editUsernameText.text! &&
            CurrentUser.sharedInstance.currentUser?.firstName == editFirstNameText.text! &&
            CurrentUser.sharedInstance.currentUser?.lastName == editLastNameText.text! &&
            CurrentUser.sharedInstance.currentUser?.location == editLocationText.text! &&
            CurrentUser.sharedInstance.currentUser?.email == self.editEmailText.text!{
            if CurrentUser.sharedInstance.currentUser?.gender == genderSelected{
                saveButton.enabled = false
            }
        }else{
            saveButton.enabled = true
        }
    }
    
    
    // MARK: - Actions
    
    @IBAction func genderSelectorSegmentedControlValueChanged(sender: BetterSegmentedControl) {
        // assign selected gender
        if sender.index == 0{
            genderSelected = "N/A"
        }else if sender.index == 1{
            genderSelected = "Male"
        }else if sender.index == 2{
            genderSelected = "Female"
        }
        
        // enable disable save button
        if CurrentUser.sharedInstance.currentUser?.gender == genderSelected{
            saveButton.enabled = false
        }else{
            saveButton.enabled = true
        }
    }
    
    @IBAction func saveUserData(sender: UIBarButtonItem) {
        
        saveButton.enabled = false
        self.view.endEditing(true)
        
        if let newUsername = self.editUsernameText.text?.lowercaseString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()),let newFirstName = self.editFirstNameText.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()),let newLastName = self.editLastNameText.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()), let location = self.editLocationText.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()), let bio = self.editBio.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()), let email = self.editEmailText.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()) {
            
            if newUsername.characters.count < 3 {
                
                ErrorHandling.customErrorHandler(nil,message:"Username Should be greater than 3 characters")
                saveButton.enabled = true
            } else if newFirstName.characters.count < 3{
                
                ErrorHandling.customErrorHandler(nil,message:"First Name Should be greater than 2 characters")
                saveButton.enabled = true
            }else if newLastName.characters.count < 2{
                
                ErrorHandling.customErrorHandler(nil,message:"Last Name Should be greater than 2 characters")
                saveButton.enabled = true
                
                
            }else{
                self.view.endEditing(true)

                let parameters:[String:AnyObject] = ["firstName":newFirstName,"lastName":newLastName,"username":newUsername,"gender":self.genderSelected,"location":location,"quote":bio, "email":email]
                
                CurrentUser.sharedInstance.updateCurrentUserDetails(parameters, completion: { (success) in
                    if success{
                        self.navigationController!.popViewControllerAnimated(true)
                    }else{
                        self.saveButton.enabled = true
                        ErrorHandling.customErrorHandler("Oops!", message: "Sorry, we are unable to update your details at this moment")
                    }
                })
            }
        }
    }
    
    func goback() {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    @IBAction func unwindToEditProfile(segue:UIStoryboardSegue) {
        
    }
    // MARK: - Table View
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if indexPath.row == 0{
            self.editEmailText.becomeFirstResponder()
        } else if indexPath.row == 2{
            self.editUsernameText.becomeFirstResponder()
        } else if indexPath.row == 3{
            self.editFirstNameText.becomeFirstResponder()
        } else if indexPath.row == 4{
            self.editLastNameText.becomeFirstResponder()
        }else if indexPath.row == 5{
            self.editLocationText.becomeFirstResponder()
        } else {
            self.editBio.becomeFirstResponder()
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        textField.returnKeyType = .Done
        self.animateTextField(textField, up: true)
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        textView.returnKeyType = .Done
        self.animateTextView(textView, up: true)
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        self.animateTextView(textView, up: false)

    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.animateTextField(textField, up: false)
    }
    
    func animateTextField(textField:UITextField,up: Bool) {
        let indexPath = NSIndexPath(forRow: textField.tag, inSection: 0)
        let cell = self.tableView.cellForRowAtIndexPath(indexPath)

        var navBarHeight:CGFloat = 64
        if let navigationController = self.navigationController{
            if navigationController.navigationBar.hidden == true{
                navBarHeight = 0
            }
        }
        let cellBottomPosition = cell!.frame.origin.y + cell!.frame.size.height + navBarHeight
        let screenHeight = UIScreen.mainScreen().bounds.size.height
        let keyboardHeight:CGFloat = 216
        if cellBottomPosition >= screenHeight - keyboardHeight {
            let movementDistance: CGFloat = cellBottomPosition - (screenHeight - keyboardHeight) + 36
            let movementDuration: NSTimeInterval = 0.3
            let movement: CGFloat = (up ? -movementDistance : movementDistance)
            UIView.beginAnimations("anim", context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            UIView.setAnimationDuration(movementDuration)
            self.view.frame = CGRectOffset(self.view.frame, 0, movement)
            UIView.commitAnimations()
        }
    }
    
    func animateTextView(textView:UITextView,up: Bool) {
        let indexPath = NSIndexPath(forRow: textView.tag, inSection: 0)
        let cell = self.tableView.cellForRowAtIndexPath(indexPath)
        
        var navBarHeight:CGFloat = 64
        if let navigationController = self.navigationController {
            if navigationController.navigationBar.hidden == true{
                navBarHeight = 0
            }
        }
        let cellBottomPosition = cell!.frame.origin.y + cell!.frame.size.height + navBarHeight
        let screenHeight = UIScreen.mainScreen().bounds.size.height
        let keyboardHeight:CGFloat = 216
        if cellBottomPosition >= screenHeight - keyboardHeight{
            let movementDistance: CGFloat = cellBottomPosition - (screenHeight - keyboardHeight) + 36
            let movementDuration: NSTimeInterval = 0.3
            let movement: CGFloat = (up ? -movementDistance : movementDistance)
            UIView.beginAnimations("anim", context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            UIView.setAnimationDuration(movementDuration)
            self.view.frame = CGRectOffset(self.view.frame, 0, movement)
            UIView.commitAnimations()
        }
    }
}
