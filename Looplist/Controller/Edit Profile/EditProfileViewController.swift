//
//  EditProfileViewController.swift
//  Looplist
//
//  Created by Looplist Inc on 1/26/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import Foundation
import UIKit

class EditProfileViewController: UITableViewController, UITextFieldDelegate, UITextViewDelegate{
    
    // MARK: -  ///////////////////////////////////////////////////    VARIABLES    /////////////////////////////////////////////////////////////////////////
    
    fileprivate var valueChangedForUsername = false
    fileprivate var valueChangedForFirstName = false
    fileprivate var valueChangedForLastName = false
    fileprivate var valueChangedForLocation = false
    
    fileprivate var genderSelected = "N/A"
    
    fileprivate var valuesChangedCount = 0
    
    // MARK: -  ////////////////////////////////////////////////////////    OUTLETS   ///////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBOutlet weak var editGender: UISegmentedControl!
    @IBOutlet weak var editUsernameText: UITextField!
    @IBOutlet weak var editFirstNameText: UITextField!
    @IBOutlet weak var editLastNameText: UITextField!
    @IBOutlet weak var editLocationText: UITextField!
    @IBOutlet weak var editBio: UITextView!
    
    @IBOutlet weak var editEmailText: UITextField!
    @IBOutlet weak var genderSelectorSegmentedControl: BetterSegmentedControl!

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    // MARK: -  ////////////////////////////////////////////////    APPLICATION LIFE CYCLE    ///////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        EditProfileViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        EditProfileViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "EDIT_PROFILE".localized
        self.saveButton.title = "SAVE".localized
        self.emailLabel.text = "EMAIL".localized
        self.editEmailText.placeholder = "EMAIL_ADDRESS".localized
        self.passwordLabel.text = "PASSWORD".localized
        self.usernameLabel.text = "USERNAME".localized
        self.firstNameLabel.text = "FIRST_NAME".localized
        self.lastNameLabel.text = "LAST_NAME".localized
        self.locationLabel.text = "LOCATION".localized
        self.genderLabel.text = "GENDER".localized
        self.bioLabel.text = "BIO".localized
        
        // TARGET ACTION
        self.editEmailText.layer.setValue(0, forKey: "indexPath")
        self.editUsernameText.layer.setValue(2, forKey: "indexPath")
        self.editFirstNameText.layer.setValue(3, forKey: "indexPath")
        self.editLastNameText.layer.setValue(4, forKey: "indexPath")
        self.editLocationText.layer.setValue(5, forKey: "indexPath")
        self.editBio.layer.setValue(7, forKey: "indexPath")


        editUsernameText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        editFirstNameText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        editLastNameText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        editLocationText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        self.editEmailText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        // DELEGATES
        self.editBio.delegate = self
        self.editBio.returnKeyType = .default
        self.editUsernameText.delegate = self
        self.editFirstNameText.delegate = self
        self.editLastNameText.delegate = self
        self.editLocationText.delegate = self
        self.editEmailText.delegate = self
        
        // UI
        self.tableView.tableFooterView = UIView()
        genderSelectorSegmentedControl.titleFont = UIFont(name: "SFUIDisplay-Light", size: 12)!
        genderSelectorSegmentedControl.selectedTitleFont = UIFont(name: "SFUIDisplay-Light", size: 12)!
        genderSelectorSegmentedControl.titles = ["N/A".localized,"MALE".localized,"FEMALE".localized]
        
        self.view.backgroundColor = UIColor.white
        
        self.tabBarController?.tabBar.isHidden = true
        
        // LOAD DATA
        loadUserData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController{
            EditProfileViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: -  ///////////////////////////////////////////////////////    LOAD UI    ///////////////////////////////////////////////////////////////////////////
    
    // LOAD SEGMENTED CONTROL
    func loadUserGender(_ userGender:String){
        if userGender == "MALE".localized{
            do {
                try genderSelectorSegmentedControl.setIndex(1, animated: false)
            }
            catch {
                ErrorHandling.unexpectedErrorHandler()
            }
        }else if userGender == "FEMALE".localized{
            do {
                try genderSelectorSegmentedControl.setIndex(2, animated: false)
            }
            catch {
                ErrorHandling.unexpectedErrorHandler()
            }
        }else if userGender == "N/A".localized{
            do {
                try genderSelectorSegmentedControl.setIndex(0, animated: false)
            }
            catch {
                ErrorHandling.unexpectedErrorHandler()
            }
        }
    }
    
    // LOAD TEXT FIELDS
    func loadUserData(){
        
        if let user = CurrentUser.sharedInstance.currentUser, let userName = user.userName, let firstName = user.firstName, let lastName = user.lastName, let gender = user.gender,let location = user.location{
            
            self.editUsernameText.text = userName
            self.editFirstNameText.text = firstName
            self.editLastNameText.text = lastName
            self.loadUserGender(gender)
            self.editLocationText.text = location
        }
        
        if let bio =  CurrentUser.sharedInstance.currentUser?.bio{
            self.editBio.text = bio
        }
        
        if let email =  CurrentUser.sharedInstance.currentUser?.email{
            self.editEmailText.text = email
        }
        
    }
    
    // MARK: -  ///////////////////////////////////////////////////////    ACTIONS    ///////////////////////////////////////////////////////////////////////////
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func genderSelectorSegmentedControlValueChanged(_ sender: BetterSegmentedControl) {
        // assign selected gender
        if sender.index == 0{
            genderSelected = Gender.notApplicable.rawValue
        }else if sender.index == 1{
            genderSelected = Gender.male.rawValue
        }else if sender.index == 2{
            genderSelected = Gender.female.rawValue
        }
        
        // enable disable save button
        if CurrentUser.sharedInstance.currentUser?.gender == genderSelected{
            saveButton.isEnabled = false
        }else{
            saveButton.isEnabled = true
        }
    }
    
    @IBAction func saveUserData(sender: UIBarButtonItem) {
        saveButton.isEnabled = false
        self.view.endEditing(true)
        
        if let newUsername = self.editUsernameText.text?.lowercased().trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),let newFirstName = self.editFirstNameText.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),let newLastName = self.editLastNameText.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), let location = self.editLocationText.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), let bio = self.editBio.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), let email = self.editEmailText.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) {
            
            if newUsername.characters.count < 3 || newUsername.characters.count > 20{
                
                ErrorHandling.customErrorHandlerWithTitle(title:"ERROR".localized,message:"USERNAME_LENGTH_ERROR".localized)
                saveButton.isEnabled = true
            } else if newFirstName.characters.count < 3{
                
                ErrorHandling.customErrorHandlerWithTitle(title:"ERROR".localized,message:"FIRST_NAME_LENGTH".localized)
                saveButton.isEnabled = true
            }else if newLastName.characters.count < 2{
                
                ErrorHandling.customErrorHandlerWithTitle(title:"ERROR".localized,message:"LAST_NAME_LENGTH".localized)
                saveButton.isEnabled = true
                
                
            }else{
                self.view.endEditing(true)
                
                let parameters:[String:AnyObject] = ["firstName":newFirstName as AnyObject,"lastName":newLastName as AnyObject,"username":newUsername as AnyObject,"gender":self.genderSelected as AnyObject,"location":location as AnyObject,"quote":bio as AnyObject, "email":email as AnyObject]
                
                APIHelperUser.updateCurrentUserDetails(parameters, completion: { [weak vc = self](success) in
                    if success{
                        vc?.navigationController!.popViewController(animated: true)
                    }else{
                        vc?.saveButton.isEnabled = true
                        ErrorHandling.customErrorHandlerWithTitle(title:"ERROR".localized, message: "ERROR_UPDATING_USER".localized)
                    }
                })
            }
        }
    }
    
}

// MARK: -  /////////////////////////////////////////////////    EXTENSION   /////////////////////////////////////////////////////////////////////
extension EditProfileViewController{
    
    // MARK: -  /////////////////////////////////////////////////    TABLE VIEW DELEGATE  /////////////////////////////////////////////////////////////////////
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0{
            self.editEmailText.becomeFirstResponder()
        } else if indexPath.row == 2{
            self.editUsernameText.becomeFirstResponder()
        } else if indexPath.row == 3{
            self.editFirstNameText.becomeFirstResponder()
        } else if indexPath.row == 4{
            self.editLastNameText.becomeFirstResponder()
        }else if indexPath.row == 5{
            self.editLocationText.becomeFirstResponder()
        } else if indexPath.row == 6{
            self.editBio.becomeFirstResponder()
        }
    }
    
    // MARK: -  /////////////////////////////////////////////////    TEXT FIELD DELEGATES   /////////////////////////////////////////////////////////////////////
    
    func textFieldDidChange(_ textField: UITextField) {
        if CurrentUser.sharedInstance.currentUser?.userName == editUsernameText.text! &&
            CurrentUser.sharedInstance.currentUser?.firstName == editFirstNameText.text! &&
            CurrentUser.sharedInstance.currentUser?.lastName == editLastNameText.text! &&
            CurrentUser.sharedInstance.currentUser?.location == editLocationText.text! &&
            CurrentUser.sharedInstance.currentUser?.email == self.editEmailText.text!{
            if CurrentUser.sharedInstance.currentUser?.gender == genderSelected{
                saveButton.isEnabled = false
            }
        }else{
            saveButton.isEnabled = true
        }
    }

    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.returnKeyType = .default
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.returnKeyType = .default
        self.tableView.setContentOffset(CGPoint(x:0,y: textView.frame.origin.y + 100), animated: true)
    }
    
    // MARK: -  /////////////////////////////////////////////////    TEXT VIEW DELEGATES   /////////////////////////////////////////////////////////////////////
    
    func textViewDidChange(_ textView: UITextView){
        if let bio = CurrentUser.sharedInstance.currentUser?.bio{
            if bio != self.editBio.text!{
                saveButton.isEnabled = true
            }
        } else {
            saveButton.isEnabled = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let textCount = textField.text?.characters.count,textCount+string.characters.count-range.length > 50{
            return false
        }else{
            return true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        var textWidth = UIEdgeInsetsInsetRect(textView.frame, textView.textContainerInset).width
        textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
        
        let boundingRect = sizeOfString(newText, constrainedToWidth: Double(textWidth), font: textView.font!)
        let numberOfLines = boundingRect.height / textView.font!.lineHeight;
        
        
        let newQuoteBio = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newQuoteBio.characters.count
        if numberOfChars < 100 && numberOfLines <= 4 {
            return true
        } else {
            return false
        }
    }
    
    func sizeOfString (_ string: String, constrainedToWidth width: Double, font: UIFont) -> CGSize {
        return (string as NSString).boundingRect(with: CGSize(width: width, height: DBL_MAX),
                                                         options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                         attributes: [NSFontAttributeName: font],
                                                         context: nil).size
    }
}
