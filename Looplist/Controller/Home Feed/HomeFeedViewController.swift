//
//  HomeFeedTableViewController.swift
//  Looplist
//
//  Created by Arun Sivakumar on 5/30/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.

import UIKit


class HomeFeedViewController: UIViewController, UITabBarControllerDelegate,UIPopoverPresentationControllerDelegate,UIScrollViewDelegate{
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////

//    var animationView:LAAnimationView!


    var isLikingProduct:Bool = false
    var isRepostingProduct:Bool = false
    
    // UI
    private var fakeNavBar = FakeNavBar()
    private var footerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 45))
    private var pagingSpinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
    private var doubleTap:UITapGestureRecognizer = UITapGestureRecognizer()

    // MODEL
    private var currentPage = ConstantsCount.paginationCurrentPage
    private let limitNumber = ConstantsCount.paginationlimitNumber
    private let pollTimeInterval = ConstantsCount.homeFeedPollTimeInterval
    
    fileprivate var activities = [[ActivityHomeFeed]](){
        didSet{
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            self.tableView.reloadData()
        }
    }
    

    private var shouldScrollOnClick = true
    private var screenWidth = UIScreen.main.bounds.size.width
    
    private var timer = Timer()

    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   ///////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var newStoryButtonVisualBlur: UIVisualEffectView!{
        didSet{
            newStoryButtonVisualBlur.layer.cornerRadius = newStoryButtonVisualBlur.frame.height / 2.0
            newStoryButtonVisualBlur.isHidden = true
            newStoryButtonVisualBlur.alpha = 0.0
        }
    }
    @IBOutlet weak var newStoryButton: UIButton!{
        didSet{
            newStoryButton.layer.cornerRadius = newStoryButton.frame.height / 2.0
            newStoryButton.isHidden = true
            newStoryButton.alpha = 0.0

        }
    }
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            if let tableView = tableView{
                tableView.delegate = self
                tableView.dataSource = self
                tableView.estimatedRowHeight = tableView.rowHeight
                tableView.rowHeight = UITableViewAutomaticDimension
                tableView.scrollsToTop = true
                tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 40, right: 0)
                self.setUpFooterView()
                tableView.tableFooterView?.isHidden = true
            }
        }
    }
    
    private func setUpFooterView(){
        pagingSpinner.startAnimating()
        pagingSpinner.color = UIColor.darkGray
        pagingSpinner.hidesWhenStopped = true
        pagingSpinner.center = footerView.center
        footerView.addSubview(pagingSpinner)
        tableView.tableFooterView = footerView
    }
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(HomeFeedViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.backgroundColor = ConstantsColor.pullToRefreshColor
        return refreshControl
    }()
    
    
    // MARK: -  //////////////////////////////////////////////   APPLICATION LIFE CYCLE   ////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureUI()
        self.reload()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController{
            HomeFeedViewController.self.setNavigationBarStatusAndFont(.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar: .default)
        }

        self.fakeNavBar.addFakeNavBarView("Transparent", uView: self.view, offset: 0.0)
        
        self.timer = Timer.scheduledTimer(timeInterval: pollTimeInterval, target: self, selector: #selector(self.pollOnHomeFeed), userInfo: nil, repeats: true)
        
        //self.showHideNewPostButton()
        self.newPostButtonOnIndexPath = nil
        self.newPostAvailable = nil
        
        self.tabBarController?.tabBar.isHidden = false
        
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        HomeFeedViewController.startTimer()
        
        if let navigationController = self.navigationController {
            HomeFeedViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor: NavigationBarBackgroundColor.whiteTransparent)
            HomeFeedViewController.self.popSlideMotionOff(navigationController)
        }
        
        self.fakeNavBar.removeFakeNavBarView("Transparent")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.timer.invalidate()
        
        self.fakeNavBar.removeFakeNavBarView("Transparent")
        HomeFeedViewController.self.popSlideMotionOn(self.navigationController!)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        HomeFeedViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    private func configureUI(){
        self.congigureTabBar()
        self.configureNavBar()
        self.configureTableView()
        self.configureTouch()
        self.configureRemoteNotification()
        self.configureTitle()
    }
    
    private func congigureTabBar(){
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.delegate = self
    }
    
    private func configureNavBar(){
        self.navigationController?.hidesBarsOnSwipe = false
        
        if let navigationController = self.navigationController {
            HomeFeedViewController.self.setNavigationBar(.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor: NavigationBarBackgroundColor.whiteTransparent)
        }
        
    }
    
    private func configureTableView(){
        tableView.addSubview(refreshControl)
    }
    
    private func configureTouch(){
        doubleTap = UITapGestureRecognizer(target: self, action:#selector(HomeFeedViewController.getLikedProductFromImage(_:)))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.delaysTouchesBegan = true
    }
    
    private func configureRemoteNotification(){
        HelperPushNotification.registerForRemoteNotification()
    }
    
    private func configureTitle(){
        self.navigationItem.title = "YOUR_FEED".localized
    }
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////

    @IBAction func showNewPosts(_ sender: UIButton) {
        hideNewPostButtonAndReload()
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
     // HOME FEED  - RELOAD,LOAD,POLL
    
    private func reload(){
        self.newPostButtonOnIndexPath = nil
        self.newPostAvailable = nil
        self.currentPage = ConstantsCount.paginationCurrentPage
        self.refreshControl.beginRefreshing()
        self.loadData()
    }
    
    private func reloadAfterScrollToTop(){
        self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        self.reload()
    }
    
    fileprivate func loadData(){
        APIHelper.homeFeedQuery(skip:self.currentPage, limit: self.limitNumber) {[weak vc = self] (activities, error) in
            if let activities = activities {
                if vc?.currentPage == 0{
                    vc?.activities = [[ActivityHomeFeed]]()
                }
                if activities.count > 0{
                    vc?.tableView.tableFooterView?.isHidden = false
                    vc?.currentPage += activities.count
                    vc?.activities.append(activities)
                }
            }else{
                vc?.refreshControl.endRefreshing()
            }
        }
    }

    
    @objc fileprivate func pollOnHomeFeed(){
        if newPostAvailable == nil{
            DispatchQueue.global(qos: .background).async{
                APIHelper.homeFeedPoll() { [weak vc = self](newActivity, error) in
                    
                    guard let first = vc?.activities.first,first.count > 0,
                        let newActivity = newActivity?.product,
                        let currentActivity = vc?.activities.first?[0].product,
                        newActivity.objectId != currentActivity.objectId,
                        let productImageUrl = newActivity.images?[0],
                        let url = URL(string: productImageUrl)
                        else{return}
                    
                    let imageView = UIImageView()
                    imageView.kf.setImage(with:(url))
                    vc?.newPostAvailable = true
                }
            }
        }
    }
    
    @objc private func refresh(_ sender:AnyObject) {
        reload()
    }

    
    // NEW POST BUTTON
    
    private func showHideNewPostButton(){
        if let newPostAvailable = newPostAvailable,newPostAvailable == true{
            
            guard let indexPath = tableView.indexPathsForVisibleRows?.last else{return}

            if let newPostButtonOnIndexPath = newPostButtonOnIndexPath,indexPath.row <= 2 || indexPath.row < newPostButtonOnIndexPath-3 || indexPath.row > newPostButtonOnIndexPath+3 {
                self.hideNewPostButton()
            }else if newPostButtonOnIndexPath == nil,indexPath.row > 2{
                self.showNewPostButton()
            }
        }
    }
    
    private var newPostAvailable:Bool?{
        didSet{
            if newPostAvailable != nil{
                showHideNewPostButton()
            }
        }
    }
    
    private var newPostButtonOnIndexPath:Int?
    
    private func showNewPostButton(){
        self.view.bringSubview(toFront: (self.newStoryButton))
        
        self.newStoryButton.isHidden = false
        self.newStoryButtonVisualBlur.isHidden = false


        UIView.animate(withDuration: 0.5, animations: {
            self.newStoryButton.alpha = 1.0
            self.newStoryButtonVisualBlur.alpha = 1.0
        }){ (true) in
            guard let indexPath = self.tableView.indexPathsForVisibleRows?.last else{ return }
            self.newPostButtonOnIndexPath = indexPath.row
        }
    }
    
    private func hideNewPostButton(){

        UIView.animate(withDuration: 0.25, animations: {
            self.newStoryButton.alpha = 0.0
            self.newStoryButtonVisualBlur.alpha = 0.0

        }){ (true) in
            self.view.sendSubview(toBack: self.newStoryButton)
            self.newStoryButton.isHidden = true
            self.newStoryButtonVisualBlur.isHidden = true

        }
    }
    
    private func hideNewPostButtonAndReload(){
        UIView.animate(withDuration: 0.25, animations: {
            self.newStoryButton.alpha = 0.0
            self.newStoryButtonVisualBlur.alpha = 0.0

        }) { (true) in
            self.view.sendSubview(toBack: self.newStoryButton)
            self.newStoryButton.isHidden = true
            self.newStoryButtonVisualBlur.isHidden = true
            self.reloadAfterScrollToTop()
        }
    }
    
    
    
    // SELECTION BAR UI - ACTIONS
    
    @objc fileprivate func getLikedProductFromButton(_ sender: UIButton!){
        if !self.isLikingProduct{
            self.isLikingProduct = true
            guard let superview = sender.superview, let cell = superview.superview?.superview as? UITableViewCell,let indexPath = self.tableView.indexPath(for: cell) else{
                return
            }
            if let product = self.activities[indexPath.section][indexPath.row].product{
                SelectionBar.likeProduct(self,product: product, sender:sender)
            }
        }
    }
    
    @objc fileprivate func getAddToCollectionFromButton(_ sender: UIButton!){
        if let superview = sender.superview {
            if let cell = superview.superview?.superview as? UITableViewCell {
                if let indexPath = self.tableView.indexPath(for: cell){
                    if let product = self.activities[indexPath.section][indexPath.row].product{
                        SelectionBar.addProductToCollection(self, product:product, sender:sender)
                    }
                }
            }
        }
    }
    
    @objc fileprivate func getRepostProductFromButton(_ repostButton: UIButton!){
        if !self.isRepostingProduct{
            self.isRepostingProduct = true
            if let superview = repostButton.superview {
                if let cell = superview.superview?.superview as? UITableViewCell {
                    if let indexPath = self.tableView.indexPath(for: cell){
                        if let product = self.activities[indexPath.section][indexPath.row].product{
                            SelectionBar.repostProduct(self, product: product, sender:repostButton)
                        }
                    }
                }
            }
        }
    }
    
    @objc fileprivate func getShareProductFromButton(_ sender: UIButton!){
        if let superview = sender.superview {
            if let cell = superview.superview?.superview as? UITableViewCell {
                if let indexPath = self.tableView.indexPath(for: cell){
                    if let product = self.activities[indexPath.section][indexPath.row].product{
                        SelectionBar.shareProduct(self, product:product, sender:sender)
                    }
                }
            }
        }
    }
    

    
    @objc fileprivate func getLikedProductFromImage(_ sender: UIGestureRecognizer){ //###
        let indexpath = IndexPath(row: (sender.view?.tag)!, section: (sender.view?.tag)!)
        if let currentCell = self.tableView.cellForRow(at: indexpath) as? HomeFeedActivityTableViewCell{
            self.getLikedProductFromButton(currentCell.likesButton)
        }
    }
    

    
    internal func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if tabBarController.selectedIndex == 0{
            if viewController.isKind(of: UINavigationController.self) {
                if(self.shouldScrollOnClick){
                    self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                    self.shouldScrollOnClick = false
                } else {
                    self.shouldScrollOnClick = true
                }
            }
        } else {
            self.shouldScrollOnClick = false
        }
    }
    
    // MARK: - ///////////////////////////////////////////////////   PARALLAX    ///////////////////////////////////////////////////////////////////////
    
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.showHideNewPostButton()
        let offsetY = self.tableView.contentOffset.y //###
        for cell in self.tableView.visibleCells {
            if cell.isKind(of: HomeFeedActivityTableViewCell.self){
                let cellActivity = cell as! HomeFeedActivityTableViewCell
                let x = cellActivity.productImage.frame.origin.x
                let y = ((offsetY - cellActivity.frame.origin.y) / self.screenWidth) * 12
                cellActivity.productImage.frame = CGRect(x: x, y: y, width: self.screenWidth, height: self.screenWidth)
            }
        }
    }
    
    // pass a param to describe the state change, an animated flag and a completion block matching UIView animations completion
    fileprivate func setTabBarVisible(_ visible: Bool, animated: Bool, completion:@escaping (Bool)->Void) {
        
        // bail if the current state matches the desired state
        if (tabBarIsVisible() == visible) {
            return completion(true)
        }
        
        // get a frame calculation ready
        let height = tabBarController!.tabBar.frame.size.height
        let offsetY = (visible ? -height : height)
        
        // zero duration means no animation
        let duration = (animated ? 0.3 : 0.0)
        
        UIView.animate(withDuration: duration, animations: {
            let frame = self.tabBarController!.tabBar.frame
            self.tabBarController!.tabBar.frame = frame.offsetBy(dx: 0, dy: offsetY);
        }, completion:completion)
    }
    
    private func tabBarIsVisible() -> Bool {
        return tabBarController!.tabBar.frame.origin.y < view.frame.maxY
    }
    
    
    // MARK: -  //////////////////////////////////////////////////////////// CUSTOM NAVIGATION   ////////////////////////////////////////////////////////////////////////////
    
    // COLLECTIONS ACTION
    private func showAddToCollectionScreen(view:AddToCollectionPopupView){
        performSegue(withIdentifier: "ShowCollection", sender: view)
    }
    
    internal func expandAddToCollectionPopup(view:AddToCollectionPopupView){
        let animation = CABasicAnimation(keyPath:"cornerRadius")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.fromValue = 10
        animation.toValue = 0
        animation.duration = 0.5
        view.layer.add(animation, forKey: "cornerRadius")
        view.layer.cornerRadius = 0
        
        UIView.animate(withDuration: 0.15, delay: 0.15, options: .curveEaseInOut, animations: {
        }, completion: { finished in
        })
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            view.frame = UIScreen.main.bounds
        }, completion: { finished in
            self.setTabBarVisible(true, animated: false) { (success) in}
            self.showAddToCollectionScreen(view: view)
        })
    }

    
    fileprivate func openProduct(_ product:Product){
        let pvc:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
        pvc.product = product
        self.navigationController?.pushViewController(pvc, animated: true)
    }
    
    @objc fileprivate func openUsersFromActivityButton(_ sender: UIButton!){
        
        if let superview = sender.superview {
            if let cell = superview.superview?.superview as? UITableViewCell {
                if let indexPath = self.tableView.indexPath(for: cell){
                    let activity = self.activities[indexPath.section][indexPath.row]
                    if let product = activity.product{
                        let likingUsersVC:ProductLikingUsersTableViewController = ProductLikingUsersTableViewController.instanceFromStoryboard("Product")
                        likingUsersVC.product = product
                        likingUsersVC.homeFeedActivity = activity
                        self.navigationController?.pushViewController(likingUsersVC, animated: true)
                    }
                }
            }
        }
    }
    

     @objc fileprivate  func openUser(sender: UITapGestureRecognizer){
    
        let tapLocation = sender.location(in: self.tableView)
        
        if let indexPath = self.tableView.indexPathForRow(at: tapLocation){
            if let cell = self.tableView.cellForRow(at: indexPath) as? HomeFeedActivityTableViewCell{
                
                if let user = cell.activity?.getUser(){
                    
                    let userVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
                    userVC.currentUser = user
                    userVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(userVC, animated: false)
                }
            }
        }
    }
    

    
    // MARK: -  ////////////////////////////////////////////////////////////   NAVIGATION   ////////////////////////////////////////////////////////////////////////////

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowCollection"{
            if segue.destination is UINavigationController{
                if let addToCollectionVC = (segue.destination as! UINavigationController).viewControllers.first as? AddToCollectionViewController{
                    if let popupView = sender as? AddToCollectionPopupView{
                        addToCollectionVC.product = popupView.product
                        addToCollectionVC.callingClass = self
                        addToCollectionVC.collections = popupView.collections
                    }
                }
            }
        }
    }

}

// MARK: -  ////////////////////////////////////////////////////////////   EXTENSION   ////////////////////////////////////////////////////////////////////////////

extension HomeFeedViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DATA SOURCE   /////////////////////////////////////////////////////////////////
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return activities.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activities[section].count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return self.view.frame.width + 80 + 53
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //Start Timer

        if let homeFeedCell = cell as? HomeFeedActivityTableViewCell{
            homeFeedCell.startTime = Date()
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.indexPathsForVisibleRows!.index(of: indexPath) == nil {
            //Stop Timer

            if self.activities.count > 0{
                if let homeFeedCell = cell as? HomeFeedActivityTableViewCell{
                    if let startTime = homeFeedCell.startTime{
                        let cellProduct = self.activities[indexPath.section][indexPath.row]
                        let timePassed:Double = abs(startTime.timeIntervalSinceNow)
                        if let productId = cellProduct.product?.objectId{
                            let action = Action(timeOnProduct: timePassed, productId: productId, fromScreen: HomeFeedViewController.getClassName())
                            Analytics.sharedInstance.actionsArray.append(action)
                        }
                        
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if self.activities.count > 0{
            if self.activities.count > indexPath.section{
                if indexPath.section == 0 && indexPath.row == 0{
                    self.setTabBarVisible(true, animated: true, completion: { (finished) in })
                } else if indexPath.section == self.activities.count-1 && indexPath.row == self.activities[indexPath.section].count - 1 {
                    DispatchQueue.global(qos: .userInitiated).async {
                        self.loadData()
                    }
                }
            }
            
            if self.activities.count >= indexPath.section && activities[indexPath.section].count >= indexPath.row{
                
                let activity = activities[indexPath.section][indexPath.row]
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeFeedActivityTableViewCell", for:indexPath) as! HomeFeedActivityTableViewCell
                cell.activity = activity
                cell.likesButton.addTarget(self,action: #selector(HomeFeedViewController.getLikedProductFromButton(_:)),for:UIControlEvents.touchUpInside)
                cell.addToCollectionButton.addTarget(self,action: #selector(HomeFeedViewController.getAddToCollectionFromButton(_:)),for:UIControlEvents.touchUpInside)
                cell.repostButton.addTarget(self,action: #selector(HomeFeedViewController.getRepostProductFromButton(_:)),for:UIControlEvents.touchUpInside)
                cell.activityButton.addTarget(self,action: #selector(HomeFeedViewController.openUsersFromActivityButton(_:)),for:UIControlEvents.touchUpInside)
                cell.shareButton.addTarget(self,action: #selector(HomeFeedViewController.getShareProductFromButton(_:)),for:UIControlEvents.touchUpInside)
                
                let tapGesture = UITapGestureRecognizer(target: self, action:#selector(self.openUser(sender:)))
                cell.activityUserImage.isUserInteractionEnabled = true
                cell.activityUserImage.addGestureRecognizer(tapGesture)
                
                return cell
            }
            
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeFeedActivityTableViewCell", for:indexPath) as! HomeFeedActivityTableViewCell
        return cell
    }
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DELEGATE   /////////////////////////////////////////////////////////////////////////
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let product = self.activities[indexPath.section][indexPath.row].product{
            self.openProduct(product)
        }
    }
}
