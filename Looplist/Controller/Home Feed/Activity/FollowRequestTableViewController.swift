//
//  FollowRequestsTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 9/29/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class FollowRequestTableViewController:UITableViewController {
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES  /////////////////////////////////////////////////////////////////////////
    
    var followRequestsArray:[Activity]?
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = 55
        self.tableView.tableFooterView = UIView()
        
        self.navigationItem.title = "FOLLOW_REQUESTS".localized
    }
    
    override func viewDidAppear(_ animated: Bool) {
        FollowRequestTableViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        FollowRequestTableViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewWillLayoutSubviews() {
        if let navigationController = self.navigationController{
            FollowRequestTableViewController.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func acceptRequest(_ sender: UIButton) {
        let index:Int = sender.tag
        
        if let currentObjectId = CurrentUser.sharedInstance.currentUser?.objectId, let followRequestObjectId = self.followRequestsArray?[index].user?.objectId!{
            sender.isEnabled = false
            DBHelper.callAPIPost("users/\(followRequestObjectId)/following/\(currentObjectId)", completion:{ [weak vc = self](result, error) in
                if let _ = result {
                    Analytics.sharedInstance.actionsArray.append(Action(context:["user": self.followRequestsArray?[index].user?.objectId ?? ""], forScreen:nil, withActivityType:"Accepted Follow Request"))

                    vc?.followRequestsArray?.remove(at: index)
                    if (vc?.followRequestsArray?.count)! > 0{
                        vc?.tableView.reloadData()
                    } else {
                        _ = vc?.navigationController?.popViewController(animated: true)
                    }
                } else if let error = error{
                    print(error)
                    ErrorHandling.customErrorHandler(message:"ERROR_ACCEPTING_FOLLOW_REQUEST".localized)
                }
                sender.isEnabled = true
            })
        }
    }
    
    @IBAction func declineRequest(_ sender: UIButton) {

        let index:Int = sender.tag
        if let currentObjectId = CurrentUser.sharedInstance.currentUser?.objectId, let followRequestObjectId = self.followRequestsArray?[index].user?.objectId{
            sender.isEnabled = false
            
            DBHelper.callAPIDelete("users/\(followRequestObjectId)/following/\(currentObjectId)", completion:{ [weak vc = self](result, error) in
                if error == nil {
                    Analytics.sharedInstance.actionsArray.append(Action(context:["user": vc?.followRequestsArray?[index].user?.objectId ?? "" ], forScreen:nil, withActivityType:"Declined Follow Request"))
                    vc?.followRequestsArray?.remove(at: index)
                    vc?.tableView.reloadData()
                } else {
                    ErrorHandling.customErrorHandler(message:"ERROR_DECLINING_FOLLOW_REQUEST".localized)
                }
                sender.isEnabled = true
            })
        }
    }
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DATA SOURCE   /////////////////////////////////////////////////////////////////////////
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.followRequestsArray?.count)!
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowRequestTableViewCell", for: indexPath) as! FollowRequestTableViewCell
        let cellActivity = self.followRequestsArray?[indexPath.row]
        cell.acceptButton.tag = indexPath.row
        cell.declineButton.tag = indexPath.row
        if let profilePhotoURL = cellActivity?.user?.profileImageUrl{
            cell.userProfileImage.kf.setImage(with:URL(string: profilePhotoURL))
        }
        if let username = cellActivity?.user?.userName{
            cell.requestLabel.text = "@" + username
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cellUser = self.followRequestsArray?[indexPath.row].user{
            let userProfileVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
            userProfileVC.currentUser = cellUser
            self.navigationController?.pushViewController(userProfileVC, animated: true)
        }
    }
}
