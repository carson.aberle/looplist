//
//  ActivityTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 8/25/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class NotificationsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////
    
    private let limitNumber = ConstantsCount.paginationlimitNumber
    fileprivate var isEndOfPage = false
    
    var activityArray:[Activity] = [Activity]()
    var followRequestsArray:[Activity] = [Activity]()
    fileprivate var fakeNavBar = FakeNavBar()
    fileprivate var indicator = CustomActivityIndicator()
    
    var emptySetView: EmptySetView = EmptySetView.instanceFromNib()
    
    @IBOutlet weak var tableView: UITableView!
    fileprivate var firstTime = true
    
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        self.tabBarController?.tabBar.isHidden = false


        self.navigationItem.title = "NOTIFICATIONS".localized
        self.configureTableView()
        self.emptySetView.discriptionLabel.text = "NO_NOTIFICATIONS".localized
        self.tableView.register(UINib(nibName: "ActivityTableViewCell", bundle: nil), forCellReuseIdentifier: "ActivityTableViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getFollowRequestNotifications()
        self.fakeNavBar.addFakeNavBarView("White", uView: self.view, offset: -64.0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.fakeNavBar.removeFakeNavBarView("White")
        NotificationsTableViewController.self.popSlideMotionOn(self.navigationController!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationsTableViewController.startTimer()
        if let navigationController = self.navigationController{
            NotificationsTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
            NotificationsTableViewController.self.popSlideMotionOff(navigationController)
        }
        self.fakeNavBar.removeFakeNavBarView("White")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationsTableViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = 60
//        self.tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 40, right: 0)
        self.tableView?.backgroundView = emptySetView
        self.tableView?.backgroundView?.isHidden = true
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func getFollowRequestNotifications(){
        
        self.tableView?.backgroundView?.isHidden = true
        
        if firstTime {
            self.indicator.showActivityIndicator(uiView: self.view, offset: 64.0)
            firstTime = false
        } else {
            self.indicator.showActivityIndicator(uiView: self.view, offset: 128.0)
        }
//        DBHelper.callAPIGet("activities", queryString: DBHelper.getJSONFromDictionary(["where":["$and":["activityType":ActivityType.followRequest.rawValue, "user":CurrentUser.sharedInstance.currentUser!.objectId!],["activityType":["$ne":"Accepted Follow Request"]]], "populate":[["path":"following"]]]), completion: { [weak vc = self](result, error) in


        DBHelper.callAPIGet("activities", queryString: DBHelper.getJSONFromDictionary(["where":["activityType":ActivityType.followRequest.rawValue, "following":CurrentUser.sharedInstance.currentUser!.objectId!], "populate":[["path":"user"]]]), completion: { [weak vc = self](result, error) in
            vc?.indicator.hideActivityIndicator()

            if let result = result{
                
                print(result)
                if let activities = result["activities"] as? NSArray{
                    vc?.followRequestsArray.removeAll()
                    
                    for activities in activities{
                        if let activityDict = activities as? Dictionary<String, AnyObject>{
                            if let activity = Activity(input: activityDict){
                                vc?.followRequestsArray.append(activity)
                            }
                        }
                    }
//                    vc?.getNotifications(skip:300)

//                    self.tableView.reloadData()
                } else {
//                    vc?.getNotifications(skip:0)
//                    ErrorHandling.customErrorHandler(message:"ERROR_GETTING_NOTIFICATIONS".localized)
                }
            } else {
//                vc?.getNotifications(skip:0)

//                ErrorHandling.customErrorHandler(message:"ERROR_GETTING_NOTIFICATIONS".localized)
            }
            vc?.getNotifications(skip:0)

        })
        
    }
    

    func getNotifications(skip:Int){
        
//        if skip == 0{
//            isEndOfPage = false
//        }
    
        let path = "notifications"
        let queryString = DBHelper.getJSONFromDictionary(["skip":skip,"limit":limitNumber])
        
        DBHelper.callAPIGet(path, queryString: queryString, completion: { [weak vc = self](result, error) in
            vc?.indicator.hideActivityIndicator()
            if let result = result{
                print(result)
                
                
                if let activities = result["activities"] as? NSArray{
                  
                    var activityTemp = [Activity]()
                    for activities in activities{
                        if let activityDict = activities as? Dictionary<String, AnyObject>{
                            if let activities = Activity(input: activityDict){
                                activityTemp.append(activities)
                            }
                        }
                    }
                    
                    if let limit = vc?.limitNumber, activityTemp.count < limit {
                        vc?.isEndOfPage = true
                    }else{
                        vc?.isEndOfPage = false
                    }
                    
                    if activityTemp.count > 0{
                        if skip == 0{
                            vc?.activityArray.removeAll()
                        }
                        vc?.activityArray += activityTemp
                    }else{
                        if skip == 0 && vc?.followRequestsArray.count == 0{
                            vc?.tableView?.backgroundView?.isHidden = false
                        }
                    }
                    
                    vc?.tableView.reloadData()
                }
            }else{
                ErrorHandling.customErrorHandler(message:"ERROR_GETTING_NOTIFICATIONS".localized)
            }
        })
    }
    
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DATA SOURCE   /////////////////////////////////////////////////////////////////////////
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.followRequestsArray.count > 0{
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            if self.followRequestsArray.count > 0{
                return 1
            } else {
                return activityArray.count
            }
        } else {
            return activityArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.followRequestsArray.count > 0 && indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewFollowRequestsTableViewCell", for: indexPath) as! NewFollowRequestsTableViewCell
            if let user = self.followRequestsArray[indexPath.row].user{
                cell.user = user
                cell.count = self.followRequestsArray.count
            }
            return cell
        } else {
            
            if !isEndOfPage && activityArray.count > 0 && indexPath.row == activityArray.count - 1{
                self.getNotifications(skip:self.activityArray.count)
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityTableViewCell", for: indexPath) as! ActivityTableViewCell
            
            if activityArray.count > indexPath.row{
                let activity = activityArray[indexPath.row]
                cell.callingClass = self
                cell.activity = activity
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0000000001
    }
    
//    func getProductAndLoad(_ activities:Activity){
//        if let productDictionary = activities.attributes?["product"] as? NSDictionary{
//            if let productID = productDictionary["_id"] as? String{
//                DBHelper.callAPIGet("products/\(productID)", completion: { (result, error) in
//                    if error == nil{
//                        DispatchQueue.main.async(execute: {
//                            if let productDictionary = result?["product"] as? NSDictionary{
//                                let product = Product(input:productDictionary as! Dictionary<String, AnyObject>)
//                                let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
//                                productVC.product = product
//                                self.navigationController?.pushViewController(productVC, animated: true)
//                            }
//                        })
//                    }
//                })
//            }
//        }
//    }
    
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DELEGATE   /////////////////////////////////////////////////////////////////////////
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.followRequestsArray.count > 0 && indexPath.section == 0{ // .followrequest
            let followRequestVC:FollowRequestTableViewController = FollowRequestTableViewController.instanceFromStoryboard("Main")
            followRequestVC.followRequestsArray = self.followRequestsArray
            self.navigationController?.pushViewController(followRequestVC, animated: true)
        } else {
            if activityArray.count > indexPath.row{
                let cellActivity = activityArray[indexPath.row]
                if let activityType = cellActivity.activityType{
                switch activityType{
                case .follow,.acceptedFollowRequest:
                    let guestUserVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
                    guestUserVC.currentUser = cellActivity.user
                    guestUserVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(guestUserVC, animated: true)
                    break;
                case .likedYourRepostedProduct,.someoneAddedYourProduct:
                    let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
                    productVC.product = cellActivity.product
                    self.navigationController?.pushViewController(productVC, animated: true)
                    break;
                default:
                    break;
                }
                }
            }
        }
    }
}
