//
//  AddCollectionPopupTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 8/12/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class AddToCollectionViewController: UIViewController, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////
    
    var panGestureRecognizer:UIPanGestureRecognizer?
    var initialPosition:CGPoint =  CGPoint(x: 0.0, y: 0.0)
    var currentPosition: CGPoint?
    var collections:[Collection] = [Collection]()
    var callingClass:UIViewController!
    var product:Product?
    fileprivate var indicator = CustomActivityIndicator()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sliderThumb: UIImageView!
    @IBOutlet weak var dismissButton: UIButton!
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor(red:0.956, green:0.956, blue:0.956, alpha:1.0)
        self.tableView.backgroundColor = UIColor(red:0.956, green:0.956, blue:0.956, alpha:1.0)
        self.tableView.separatorStyle = .none
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.2, animations: {
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.ADD_TO_COLLECTION_POPUP_BACKGROUND_TAG)?.alpha = 0
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.ADD_TO_COLLECTION_POPUP_SLIDER_TAG)?.alpha = 0
        }) { (success) in
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.ADD_TO_COLLECTION_POPUP_BACKGROUND_TAG)?.removeFromSuperview()
            UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.ADD_TO_COLLECTION_POPUP_SLIDER_TAG)?.removeFromSuperview()
        }
        AddToCollectionViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        AddToCollectionViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.ADD_TO_COLLECTION_POPUP_BACKGROUND_TAG)?.removeFromSuperview()
        UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.ADD_TO_COLLECTION_POPUP_SLIDER_TAG)?.removeFromSuperview()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 58
        self.tableView.tableFooterView = UIView()
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(), style: .plain, target: self, action: nil)
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
        
        self.addPanGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if let navigationController = self.navigationController{
            AddToCollectionViewController.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.clearNoGradient)
        }
        self.view.bringSubview(toFront: self.sliderThumb)
//        self.indicator.showActivityIndicator(uiView: self.view, offset: 64.0)
        CurrentUser.sharedInstance.currentUser!.getCurrentUserCollectionsWithoutLikes(true,product:self.product, skip: 0, limit: 1000) { [weak vc = self](success, collections) in
            if let collections = collections{
                vc?.collections = collections
                vc?.tableView.reloadData()
//                vc?.indicator.hideActivityIndicator()
//                for collection in collections{
//                    self.collections.append(collection)
//                }
            }
        }
    }
    
    func popViewController(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func animateDismissPopup(){
        UIView.animate(withDuration: 0.2
            , animations: {
                
                self.view.frame.origin = CGPoint(
                    x: 0.0,
                    y: self.view.frame.size.height
                )
        }, completion: { (isfinished) in
            if isfinished {
                if let homeFeed = self.callingClass as? HomeFeedViewController{
                    homeFeed.view.viewWithTag(ConstantsObjects.ADD_TO_COLLECTION_POPUP_BACKGROUND_TAG)?.removeFromSuperview()
                } else if let productVC = self.callingClass as? ProductTableViewController{
                    UIApplication.shared.isStatusBarHidden = false
                    UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
                    productVC.view.viewWithTag(ConstantsObjects.ADD_TO_COLLECTION_POPUP_BACKGROUND_TAG)?.removeFromSuperview()
                }
                self.dismiss(animated: false, completion: nil)
            }
        })
    }
    
    func addPanGesture(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AddToCollectionViewController.animateDismissPopup))
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(AddToCollectionViewController.handlePan(_:)))
        panGestureRecognizer?.cancelsTouchesInView = false
        let panGestureView = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 54))
        panGestureView.backgroundColor = UIColor.clear
        tapGesture.require(toFail: panGestureRecognizer!)
        panGestureView.addGestureRecognizer(panGestureRecognizer!)
        panGestureView.addGestureRecognizer(tapGesture)
        self.view.addSubview(panGestureView)
        self.view.bringSubview(toFront: panGestureView)

    }
    
    func handlePan(_ panGesture: UIPanGestureRecognizer) {
        
        let panTranslation = panGesture.translation(in: view)
        
        if panTranslation.y > 0.0{
            
            if panGesture.state == .began {
                currentPosition = panGesture.location(in: view)
            } else if panGesture.state == .changed {
                view.frame.origin = CGPoint(
                    x: 0.0,
                    y: panTranslation.y
                )
            } else if panGesture.state == .ended {
                let velocity = panGesture.velocity(in: view)
                if velocity.y >= 1000 || self.view.frame.y > view.frame.height / 4.0 {
                    self.animateDismissPopup()
                } else {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.view.frame.y = self.initialPosition.y
                    })
                }
            }
        }
        
    }
    
    
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DATA SOURCE   /////////////////////////////////////////////////////////////////////////
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.collections.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddCollectionTableViewCell", for:indexPath) as! AddCollectionTableViewCell
            cell.callingClass = self
            cell.separatorInset = UIEdgeInsetsMake(0.0, cell.bounds.size.width * 2, 0.0, 0.0)
            cell.addCollectionButton.text = "ADD_TO_COLLECTION".localized
            cell.createNewButton.setTitle("CREATE_NEW".localized, for:.normal)
            cell.backgroundColor = UIColor(red:0.956, green:0.956, blue:0.956, alpha:1.0)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionNameTableViewCell", for:indexPath) as! CollectionNameTableViewCell
            cell.popupVC = self
            cell.backgroundColor = UIColor(red:0.956, green:0.956, blue:0.956, alpha:1.0)
            if self.collections.count > indexPath.row - 1{
                let cellCollection = self.collections[indexPath.row - 1]
                if let containsProduct = cellCollection.containsProduct{
                    if containsProduct{
                        cell.selectionIndicator.image = UIImage(named: "Checkmark Blue")
                    } else {
                        cell.selectionIndicator.image = UIImage(named: "Add")
                    }
                } else {
                    cell.selectionIndicator.image = UIImage(named: "Add")
                }
                if let name = cellCollection.name{
                    cell.collectionName.text! = name
                }
                if let products = cellCollection.products{
                    if let firstProduct = products.first{
                        if let images = firstProduct.images{
                            if images.count > 0{
                                let productImageUrl = images[0]
                                cell.firstProductImage.kf.setImage(with: URL(string:productImageUrl))
                            }
                        }
                    }
                    
                }

            }
            return cell

        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DELEGATE   /////////////////////////////////////////////////////////////////////////
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let addCollectionVC:AddCollectionViewController = AddCollectionViewController.instanceFromStoryboard("Main")
            addCollectionVC.product = self.product
            self.navigationController?.pushViewController(addCollectionVC, animated: true)
        } else if indexPath.row > 0{
            if self.collections.count > indexPath.row - 1{
                let cellCollection = self.collections[indexPath.row - 1]
                if !cellCollection.isAddedProductCollection! {
                    if let containsProduct = cellCollection.containsProduct{
                        if let product = self.product {
                            if !containsProduct{
                                //Add to collection
                                APIHelper.addProductToCollection(collection: cellCollection, product: product, completion: { (success) in
                                    if let _ = success{
                                        self.tableView.reloadData()
                                        if let homeFeed = self.callingClass as? HomeFeedViewController{
                                            homeFeed.tableView.reloadData()
                                        } else if let productVC = self.callingClass as? ProductTableViewController{
                                            productVC.product = self.product
                                            productVC.tableView.reloadData()
                                        }
                                    }
                                })
                            } else {
                                //Remove from collection
                                APIHelper.removeProductFromCollection(collection: cellCollection, product: product, completion: { (success) in
                                    if let _ = success{
                                        self.tableView.reloadData()
                                        if let homeFeed = self.callingClass as? HomeFeedViewController{
                                            homeFeed.tableView.reloadData()
                                        } else if let productVC = self.callingClass as? ProductTableViewController{
                                            productVC.product = self.product
                                            productVC.tableView.reloadData()
                                        }
                                    }
                                    
                                })
                            }
                        }
                    }
                } else {
                    DropDownAlert.customErrorMessage("CANT_ADD_TO_ADDED_PRODUCTS_COLLECTION".localized)
                }
            }
            
        }
    }
}
