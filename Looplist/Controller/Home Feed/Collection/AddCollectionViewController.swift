//
//  AddCollectionViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 8/18/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class AddCollectionViewController:UIViewController, UITextViewDelegate{
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////
    
    var product:Product?
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var collectionDescription: UITextView!
    @IBOutlet weak var collectionName: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var collectionNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE  /////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        self.saveButton.title = "SAVE".localized
        self.navigationItem.title = "NEW_COLLECTION".localized
        self.collectionNameLabel.text = "COLLECTION_NAME".localized
        self.descriptionLabel.text = "COLLECTION_DESCRIPTION".localized
        self.collectionName.placeholder = "COLLECTION_NAME_PLACEHOLDER".localized
        self.collectionDescription.text = "COLLECTION_DESCRIPTION_PLACEHOLDER".localized
        self.collectionDescription.clipsToBounds = true
        self.collectionDescription.layer.borderColor = UIColor(red:0.81, green:0.81, blue:0.81, alpha:1.0).cgColor
        self.collectionDescription.layer.borderWidth = 0.6
        self.collectionDescription.layer.cornerRadius = 6
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardShown(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardShown(_:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        
        self.tabBarController?.tabBar.isHidden = true
        self.collectionDescription.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        AddCollectionViewController.startTimer()
        if let navigationController = self.navigationController{
            AddCollectionViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let objectId = self.product?.objectId{
            AddCollectionViewController.stopTimer(["product":objectId], withActionType:"View Screen")
        } else {
            AddCollectionViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController{
            navigationController.setNavigationBarHidden(false, animated: false)
            AddCollectionViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveCollection(_ sender: UIBarButtonItem) {
        sender.isEnabled = false
        if let currentUserObjectId = CurrentUser.sharedInstance.currentUser?.objectId{
            if(!HelperFunction.checkIfOnlyWhitespace(self.collectionName.text!)){
                var parameters:[String:AnyObject]
                if(!HelperFunction.checkIfOnlyWhitespace(self.collectionDescription.text!) && self.collectionDescription.text! != "COLLECTION_DESCRIPTION_PLACEHOLDER".localized){
                    if self.product == nil{
                        //Create blank collection
                        parameters = ["name": self.collectionName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as AnyObject, "description":self.collectionDescription.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as AnyObject, "owner": currentUserObjectId as AnyObject, "users": [currentUserObjectId] as AnyObject]
                    } else {
                        parameters = ["name": self.collectionName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as AnyObject, "description":self.collectionDescription.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as AnyObject, "owner": currentUserObjectId as AnyObject, "products": [product!.objectId!]as AnyObject, "users": [CurrentUser.sharedInstance.currentUser!.objectId!]as AnyObject]
                    }
                } else {
                    if self.product == nil{
                        //Create blank collection
                        parameters = ["name": self.collectionName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as AnyObject, "owner": currentUserObjectId as AnyObject, "users": [currentUserObjectId]as AnyObject]
                    } else {
                        parameters = ["name": self.collectionName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as AnyObject, "owner": currentUserObjectId as AnyObject, "products": [product!.objectId!] as AnyObject, "users": [currentUserObjectId] as AnyObject]
                    }
                }
                DBHelper.callAPIPost("collections", params: parameters, completion: { (result, error) in
                    sender.isEnabled = true
                    if error == nil{
                        if let collectionObjectId = result?["_id"] as? String{
                            Analytics.sharedInstance.actionsArray.append(Action(context: ["collection":collectionObjectId], forScreen: "Add Product User Selection", withActivityType:"Created Collection"))
                        }
                        ErrorHandling.customSuccessHandler(message: "SUCCESSFULLY_CREATED".localized + " \(self.collectionName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!")
                        _ = self.navigationController?.popViewController(animated: true)
                    } else {
                        ErrorHandling.customErrorHandler(message: "ERROR_CREATING_COLLECTION".localized)
                    }
                })
            } else {
                sender.isEnabled = true
                ErrorHandling.customErrorHandler(message: "COLLECTION_NAME_BLANK".localized)
            }
        } else {
            sender.isEnabled = true
            ErrorHandling.customErrorHandler(message: "ERROR".localized)
        }
    }
    
    
    // MARK: -  //////////////////////////////////////////////////////   TEXT FIELD DELEGATES   /////////////////////////////////////////////////////////////////////////
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardShown(_ n:Foundation.Notification) {
        let d = n.userInfo!
        var r = (d[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        r = self.collectionDescription.convert(r, from:nil)
        self.collectionDescription.contentInset.bottom = r.size.height - 30
        self.collectionDescription.scrollIndicatorInsets.bottom = r.size.height - 30
    }
    
    func keyboardHidden(_ n:Foundation.Notification) {
        self.collectionDescription.contentInset.bottom = 8
        self.collectionDescription.scrollIndicatorInsets.bottom = 8
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text! == "COLLECTION_DESCRIPTION_PLACEHOLDER".localized) {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text! == "") {
            textView.text = "COLLECTION_DESCRIPTION_PLACEHOLDER".localized
            textView.textColor = UIColor(red:0.81, green:0.81, blue:0.81, alpha:1.0)
        }
        textView.resignFirstResponder()
    }
}
