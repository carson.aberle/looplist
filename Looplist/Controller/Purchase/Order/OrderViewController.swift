//
//  ListingPaymentViewController.swift
//  Looplist
//
//  Created by Arun on 5/25/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//
import Foundation
import UIKit

protocol userSelectedPaymentDetailsDelegate:class{
    func userSelectedShippingAddress(_ shipping:Shipping?)
    func userSelectedCC(_ cc:CC?)
}

class OrderViewController: UITableViewController{
    
    // MARK: -  -----------------------------------------------------   PUBLIC API VARIABLES   -----------------------------------------------------
    
    var listing:Listing?
    var shipping:Shipping?{
        didSet{
            loadShippingAddressUI()
        }
    }
    var cc:CC?{
        didSet{
            loadCCUI()
        }
    }
    
    // MARK: -  -----------------------------------------------------   PRIVATE VARIABLES   -----------------------------------------------------

    let titleEdgeInsetLeft:CGFloat = 15
    let titleEdgeInsetRight:CGFloat = 25

    // MARK: -  -----------------------------------------------------   OUTLETS   -----------------------------------------------------
    
    //SHIPPING
    @IBOutlet weak var shippingAddressButton: UIButton!{
        didSet{
            shippingAddressButton.layer.cornerRadius = ConstantsObjects.defaultButtonCornerRadius
            shippingAddressButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            shippingAddressButton.titleEdgeInsets.left = titleEdgeInsetRight
            shippingAddressButton.titleEdgeInsets.right = titleEdgeInsetRight
        }
    }
    
    //CC
    @IBOutlet weak var ccButton: UIButton!{
        didSet{
            ccButton.layer.cornerRadius = ConstantsObjects.defaultButtonCornerRadius
            ccButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            ccButton.titleEdgeInsets.left = titleEdgeInsetRight
            ccButton.titleEdgeInsets.right = titleEdgeInsetRight
        }
    }
    
    //LISTING
    @IBOutlet weak var listingImageView: UIImageView!{
        didSet{
            listingImageView.layer.cornerRadius = ConstantsObjects.defaultImageCornerRadius
            listingImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var listingNameLabel: UILabel!
    @IBOutlet weak var listingAttributesLabel: UILabel!
    
    
    // PAYMENT
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var shippingCostLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var deliverToLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var yourItemLabel: UILabel!
    @IBOutlet weak var orderSummaryLabel: UILabel!
    @IBOutlet weak var subtotalTitle: UILabel!
    @IBOutlet weak var taxTitle: UILabel!
    @IBOutlet weak var serviceTitle: UILabel!
    @IBOutlet weak var shippingCostTitle: UILabel!
    @IBOutlet weak var totalTitle: UILabel!
    @IBOutlet weak var submitOrderButton: UIButton!
    
    fileprivate var indicator = CustomActivityIndicator()

    
    // MARK: -  ////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        OrderViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let objectId = self.listing?.objectId{
            OrderViewController.stopTimer(["listing":objectId], withActionType:"View Screen")
        } else {
            OrderViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
        }
    }
    
    override func viewDidLoad() {
        
        configureUI()
        loadListingPrice()
        
        loadDefaultShippingAddress()
        loadDefaultCC()
        loadListingUI()
        
    }
    


    // MARK: -  //////////////////////////////////////////////////////   CONFIGURE UI   /////////////////////////////////////////////////////////////////////////
    
    func configureUI(){
        congigureTableView()
        configureVC()
        configureTitles()
        
    }
    func congigureTableView(){
        tableView.tableFooterView = UIView()
    }
    
    private func configureTitles(){
        self.deliverToLabel.text = "DELIVER_TO".localized
        self.shippingAddressButton.setTitle("ADD_SHIPPING_ADDRESS".localized, for: .normal)
        self.paymentLabel.text = "PAYMENT".localized
        self.ccButton.setTitle("ADD_CREDIT_CARD".localized, for: .normal)
        self.yourItemLabel.text = "YOUR_ITEM".localized
        self.orderSummaryLabel.text = "ORDER_SUMMARY".localized
        self.subtotalTitle.text = "SUBTOTAL".localized
        self.taxTitle.text = "TAX".localized
        self.serviceTitle.text = "SERVICE".localized
        self.shippingCostTitle.text = "SHIPPING".localized
        self.totalTitle.text = "TOTAL".localized
        self.submitOrderButton.setTitle("SUBMIT_ORDER".localized, for: .normal)
        self.navigationItem.title = "CHECKOUT".localized
        
    }
    func configureVC(){
        
        if let navigationController = self.navigationController {
            OrderViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named: Constants.backImage), style: .plain, target: self, action: #selector(self.goBack))
        }
        
        self.navigationController?.hidesBarsOnSwipe = false
        self.tabBarController?.tabBar.isHidden = true

    }

    
    // MARK: -  //////////////////////////////////////////////////////   GET DATA   /////////////////////////////////////////////////////////////////////////
    
    
    func loadListingPrice(){
        // LAZY LOAD PAYMENT DATA
        if let listing = self.listing{
            if let _ = listing.payment{
                loadPaymentDetailsUI()
            }else{
                
                APIHelperProduct.getPaymentData(listing){ [weak vc = self](success) in
                    if success{
                        vc?.loadPaymentDetailsUI()
                    }
                }
            }
        }
    }
    
    func loadDefaultShippingAddress(){
        if let defaultShippingAddress = CurrentUser.sharedInstance.currentUser?.getDefaultShippingAddress(){
            self.shipping = defaultShippingAddress
        }else{

        }
    }
    
    func loadDefaultCC(){
        if let defaultCC = CurrentUser.sharedInstance.currentUser?.getDefaultPaymentMethod(){
            self.cc = defaultCC
        }else{

        }
    }
    
    
    // MARK: -  //////////////////////////////////////////////////////   LOAD UI   /////////////////////////////////////////////////////////////////////////
    
    
    func loadPaymentDetailsUI(){
        if let listing = self.listing,let payment = listing.payment{
            if let subtotal = payment.subtotal,let tax = payment.tax, let service = payment.service,let shipping = payment.shipping, let total = payment.total{
                
                self.subTotalLabel.text = subtotal
                self.shippingCostLabel.text = shipping
                self.taxLabel.text = tax
                self.serviceLabel.text = service
                self.totalLabel.text = total
            }
        }
    }
    
    func loadShippingAddressUI(){
        
        if let shipping = self.shipping,let fullName = shipping.name, let city = shipping.city,let state = shipping.state{
            let address = fullName + " - " + city + ", " + state
            shippingAddressButton.setTitle(address, for: UIControlState())
            
        } else {
        }
    }
    
    func loadCCUI(){
        if let cc = self.cc, let brand = cc.brand, let last4 = cc.last4{
            let ccData = brand + "  ∙∙∙∙ " + last4
            ccButton.setTitle(ccData, for: UIControlState())
        }else{
        }
    }
    
    
    
    
    func loadListingUI(){
        
        if let listing = listing, let brandName = listing.brandName, let listingImage = listing.images?[0],let listingImageUrl = URL(string:(listingImage)){
            
            self.listingNameLabel.text = brandName
            self.listingImageView.kf.setImage(with:(listingImageUrl))
            
            if let attributes = self.listing?.attributes{
                 self.listingAttributesLabel.text = attributes
            }else{
                listingAttributesLabel.text = ""
            }
        }
    }
    
    
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    
    @IBAction func submitPayment(_ sender: UIButton) {
        sender.isEnabled = false
        
        if self.cc == nil && self.shipping == nil{
            ErrorHandling.customErrorHandler(message:ErrorHandling.transactionsErrorShippingAndPaymentMethod)
            sender.isEnabled = true
            
        }else if self.cc == nil{
            ErrorHandling.customErrorHandler(message: ErrorHandling.transactionsErrorPaymentMethod)
            sender.isEnabled = true
            
        }else if self.shipping == nil{
            ErrorHandling.customErrorHandler(message: ErrorHandling.transactionsErrorShipping)
            sender.isEnabled = true
            
        }else if let listing = self.listing?.objectId,let cc = self.cc?.objectId , let shipping = self.shipping?.objectId, let listingNotes = self.listing?.optionalNotes{
            
            self.indicator.showActivityIndicator(uiView: self.view, offset: 128.0)
            let path =  "paymentMethods/\(cc)/charge"
            
            DBHelper.callAPIPost(path, params:(["listing": listing as AnyObject, "transaction":["shipping":["address": shipping],"customerNotes":listingNotes]] as AnyObject) as! [String : AnyObject?], completion: { [weak vc = self](result, error) in
                vc?.indicator.hideActivityIndicator()
                if let result = result{
                    // show confirmation page
                    Analytics.sharedInstance.actionsArray.append(Action(context:["listing": listing,"paymentMethod" : "Credit Card"], forScreen:nil, withActivityType:"Purchased Product")) // $AS
                    
                    if let transactionId = result["transaction"] as? String{
                        vc?.showOrderConfirmationPage(transactionId)
                    }
                }else if let error = error{
                    print(error)
                    sender.isEnabled = true
                    
                    // show error
                    ErrorHandling.customErrorHandler(message: ErrorHandling.transactionsError)
                    
                }
                
            })
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    
    
    // BACKEND CHARGE
    
    
    
    // MARK: -  //////////////////////////////////////////////////////   NAVIGATION  /////////////////////////////////////////////////////////////////////////
    
    @IBAction func UnwindToOrderVC(_ segue: UIStoryboardSegue) {
        
    }
    
    
    @objc private func goBack(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func showPaymentMethod(_ sender: UIButton){
        let creditCardVC:CCViewController = CCViewController.instanceFromStoryboard("CreditCard")
        creditCardVC.popUp = true
        
        self.navigationController?.pushViewController(creditCardVC, animated: true)
    }
    
    
    @IBAction func showShippingAddress(_ sender: UIButton){
        
        let shippingVC:ShippingAddressesViewController = ShippingAddressesViewController.instanceFromStoryboard("Shipping")
        shippingVC.popUp = true
        
        self.navigationController?.pushViewController(shippingVC, animated: true)

    }
    
    private func showOrderConfirmationPage(_ transationId:String){
        
        let vc:ConfirmationViewController = ConfirmationViewController.instanceFromStoryboard("Purchase")
        vc.listing = self.listing
        vc.shipping = self.shipping
        vc.cc = self.cc
        vc.transactionId = transationId
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 || indexPath.row == 1{
            return (self.view.frame.height)  * 0.157
        }
        else if indexPath.row == 2 || indexPath.row == 3 {
            return (self.view.frame.height) * 0.263
        }
        else if indexPath.row == 4{
            return (self.view.frame.height)  * 0.06
        }
        else if  indexPath.row == 5 {
            return (self.view.frame.height)   * 0.10
        }
        return 0
    }
}
