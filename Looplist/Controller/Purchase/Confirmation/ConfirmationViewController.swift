//
//  ListingPaymentViewController.swift
//  Looplist
//
//  Created by Arun on 5/25/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import UIKit

class ConfirmationViewController: UITableViewController{
    
    // MARK: -  -----------------------------------------------------   PUBLIC API   -----------------------------------------------------
    
    
//    var transaction:Transaction?
    var transactionId:String?
    var listing:Listing?
    var shipping:Shipping?
    var cc:CC?
    
    
    // MARK: -  -----------------------------------------------------   OUTLETS   -----------------------------------------------------
    
    //SHIPPING
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var address1Label: UILabel!
    @IBOutlet weak var address2Label: UILabel!

    //CC
    @IBOutlet weak var paymentMethodLabel: UILabel!

    
    //LISTING
    @IBOutlet weak var listingImageView: UIImageView!{
        didSet{
            if let listingImageView = listingImageView{
                listingImageView.layer.cornerRadius = ConstantsObjects.defaultImageCornerRadius
                listingImageView.clipsToBounds = true
            }
        }
    }
    @IBOutlet weak var listingNameLabel: UILabel!
    @IBOutlet weak var listingAttributesLabel: UILabel!
//    @IBOutlet weak var listingPriceLabel: UILabel!
    
    
    // PAYMENT
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var shippingCostLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    
    @IBOutlet weak var confirmationNumberLabel: UILabel!
    @IBOutlet weak var deliverToLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var yourItemLabel: UILabel!
    @IBOutlet weak var orderSummaryLabel: UILabel!
    @IBOutlet weak var subtotalTitle: UILabel!
    @IBOutlet weak var taxTitle: UILabel!
    @IBOutlet weak var serviceTitle: UILabel!
    @IBOutlet weak var shippingTitle: UILabel!
    @IBOutlet weak var totalTitle: UILabel!
    @IBOutlet weak var confirmationNumberTitle: UILabel!
    // MARK: -  ////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        
        configureUI()
        loadPaymentUI()
        loadShippingAddressUI()
        loadPaymentMethodUI()
        loadListingUI()
        loadConfirmationNumber()
    }
    
    // MARK: -  ////////////////////////////////////////////////   NAVIGATION  /////////////////////////////////////////////////////////////////


    func goBack(){

        if let navController = self.navigationController, let count = navigationController?.viewControllers.count, count >= 2 {
            if let _ = navController.viewControllers[count-2] as? SettingsOrderHistoryTableViewController{
                _ = self.navigationController?.popViewController(animated: true)
                return
            }
        }
        _ = self.navigationController?.popToRootViewController(animated: true)
        
    }
    

    
    // MARK: -  //////////////////////////////////////////////////////   CONFIGURE UI   /////////////////////////////////////////////////////////////////////////
    
    private func configureUI(){
        configureTableView()
        configureVC()
        configureTitles()
    }
    
    private func configureTableView(){
        tableView.contentInset.top = 0
        tableView.tableFooterView = UIView()
    }
    
    private func configureVC(){
        if let navigationController = self.navigationController {
            ConfirmationViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
            
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named: Constants.backImage), style: .plain, target: self, action: #selector(self.goBack))
        }

        self.navigationController?.hidesBarsOnSwipe = false
        self.tabBarController?.tabBar.isHidden = true
    }
    
    private func configureTitles(){
    
        self.deliverToLabel.text = "DELIVER_TO".localized
        self.paymentLabel.text = "PAYMENT".localized
        self.yourItemLabel.text = "YOUR_ITEM".localized
        self.orderSummaryLabel.text = "ORDER_SUMMARY".localized
        self.subtotalTitle.text = "SUBTOTAL".localized
        self.taxTitle.text = "TAX".localized
        self.serviceTitle.text = "SERVICE".localized
        self.shippingTitle.text = "SHIPPING".localized
        self.totalTitle.text = "TOTAL".localized
        self.confirmationNumberTitle.text = "CONFIRMATION_NUMBER".localized
        self.navigationItem.title = "ORDER_CONFIRMATION".localized
    }
    

    
    // MARK: -  //////////////////////////////////////////////////////   LOAD UI   /////////////////////////////////////////////////////////////////////////
    
    
    private func loadPaymentUI(){
        if let listing = self.listing,let payment = listing.payment{
            if let subtotal = payment.subtotal,let tax = payment.tax, let service = payment.service,let shipping = payment.shipping, let total = payment.total{
                
                self.subTotalLabel.text = subtotal
                self.shippingCostLabel.text = shipping
                self.taxLabel.text = tax
                self.serviceLabel.text = service
                self.totalLabel.text = total
            }
        }
    }
    
    private func loadShippingAddressUI(){
        
        if let shipping = self.shipping,let fullName = shipping.name,let city = shipping.city,let state = shipping.state, let zip = shipping.zip{
                self.fullNameLabel.text = fullName
                self.address1Label.text = city + ", " + state
                self.address2Label.text = zip
        } else {
          
        }
    }
    
    private func loadPaymentMethodUI(){
        if let cc = self.cc, let brand = cc.brand, let last4 = cc.last4{ // Credit Card direct transaction
            self.paymentMethodLabel.text  = brand + " " + "ENDING_IN".localized + " " + last4
        }else if let method = listing?.payment?.method{ // From Order History either apple pay or credit vard
            self.paymentMethodLabel.text  = method
        }else if cc == nil{ // Apple Pay
            self.paymentMethodLabel.text = "Apple Pay"
        }else{
             self.paymentMethodLabel.text = " "
        }
    }
    
    private func loadListingUI(){
        if let listing = listing, let brandName = listing.brandName, let listingImage = listing.images?[0],let listingImageUrl = URL(string:(listingImage)){
            
            self.listingNameLabel.text = brandName
            self.listingImageView.kf.setImage(with: (listingImageUrl))
            
            if let attributes = self.listing?.attributes{
                self.listingAttributesLabel.text = attributes
            }else{
                listingAttributesLabel.text = ""
            }
        }
    }
    
    private func loadConfirmationNumber(){
        if let transactionId = self.transactionId{
            self.confirmationNumberLabel.text = transactionId
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return (self.view.frame.height)   * 0.19
        }
        else if indexPath.row == 1 || indexPath.row == 5 {
            return (self.view.frame.height )   * 0.125
        }
        else if indexPath.row == 2 || indexPath.row == 3 {
            return (self.view.frame.height) * 0.25
        }
        else if indexPath.row == 4{
            return (self.view.frame.height)  * 0.06
        }
        
        return 0
    }
}
