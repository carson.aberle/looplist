

//
//  ViewController.swift
//  Looplist
//
//  Created by Arun Sivakumar on 11/4/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

class DiscoverViewController: UIViewController,UITextFieldDelegate,SearchDisplayProtocol,UICollisionBehaviorDelegate, UIViewControllerPreviewingDelegate {
    
    //MARK: - ----------------------------------------------------------------   PRIVATE VARIABLES ----------------------------------------------------------------
    
    var filtersDictionary:NSDictionary?
    private var currentPage = 0
    private let limitNumber = ConstantsCount.paginationlimitNumber
    var isEndOfPage:Bool = true
    private var emptySetView: EmptySetView = EmptySetView.instanceFromNib()
    fileprivate var fakeNavBar = FakeNavBar()
    
    fileprivate enum OverlayState{
        case on
        case off
    }
    
    fileprivate var products = [Product](){
        didSet{
            if self.products.count <= self.limitNumber && self.products.count > 0{
                self.animateCollections()
            } else {
                self.collectionView.reloadData()
            }
        }
    }
    
    fileprivate var userChangedProductOverlayState = OverlayState.off
    
    private var searchTermUserEnteredToSearch:String?{
        didSet{
            if let searchTermUserEnteredToSearch = searchTermUserEnteredToSearch{
                self.dismissSearchOverlay()
                searchProducts(searchTermUserEnteredToSearch, minPrice:nil, maxPrice:nil, colors:nil)
            }
        }
    }
    
    fileprivate var indicator = CustomActivityIndicator()
    fileprivate var firstTime = true
    
    
    //MARK: - ----------------------------------------------------------------  OUTLETS ----------------------------------------------------------------
    
    
    private var navigationBottom: UIImageView?
    
    
    @IBOutlet weak var noLuckLabel: UILabel!
    @IBOutlet weak var searchOverlayButton: UIBarButtonItem!
        {
        didSet{
            self.leftBarButtonInActive()
        }
    }
    @IBOutlet weak var productDetailDisplayButton: UIButton!{
        didSet{
            productDetailDisplayButton.roundCorners([.topLeft , .bottomLeft], radius: productDetailDisplayButton.frame.height / 2.0)
        }
    }
    
    @IBOutlet weak var searchTextField: UITextField!{
        didSet{
            if let searchTextField  = searchTextField{
                searchTextField.delegate = self
                searchTextField.placeholder = "SEARCH_PLACEHOLDER".localized
                searchTextField.addTarget(self, action: #selector(DiscoverViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
            }
            
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            if let collectionView = collectionView{
                collectionView.backgroundView = emptySetView
                collectionView.backgroundView?.isHidden = true
                collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
                
            }
        }
    }
    
    @IBOutlet weak var filterVisualEffectBlur: UIVisualEffectView!{
        didSet{
            if let filterVisualEffectBlur =  filterVisualEffectBlur{
                filterVisualEffectBlur.layer.cornerRadius = filterVisualEffectBlur.frame.height / 2.0
                filterVisualEffectBlur.clipsToBounds = true
                filterVisualEffectBlur.layer.borderColor = UIColor.lightGray.cgColor
                filterVisualEffectBlur.layer.borderWidth = 1.0
            }
        }
    }
    @IBOutlet weak var filterButton: UIButton!{
        didSet{
            if let filterButton = filterButton{
                filterButton.setTitle("FILTERS".localized, for: .normal)
                filterButton.roundCorners([.topRight , .bottomRight], radius: filterButton.frame.height / 2.0)
            }
        }
    }
    @IBOutlet weak var leftNavBarButton: UIBarButtonItem!
    
    
    
    // MARK: -  //////////////////////////////////////////////////////   OVERLAY  ///////////////////////////////////////////////////////////////////
    
    private var searchDisplayVC = SearchDisplayViewController()
    private var searchView:SearchView!
    
    private var panGestureRecognizer:UIPanGestureRecognizer!
    private var animator:UIDynamicAnimator!
    private var gravity:UIGravityBehavior!
    private var container:UICollisionBehavior!
    private var dynamicItem:UIDynamicItemBehavior!
    
    private var showSearchDisplayVC = false
    
    
    // MARK: -  //////////////////////////////////////////////////////   LIFE CYCLE  ///////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        DiscoverViewController.startTimer()
        if let navigationController = self.navigationController{
            DiscoverViewController.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor: NavigationBarBackgroundColor.whiteOpaque)
            DiscoverViewController.self.popSlideMotionOff(navigationController)
        }
        
        self.fakeNavBar.removeFakeNavBarView("White")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        DiscoverViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerFor3DTouch()
        configureUI()
        loadDiscovery()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.automaticallyAdjustsScrollViewInsets = false

        
        self.fakeNavBar.addFakeNavBarView("White", uView: self.view, offset: -64.0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.fakeNavBar.removeFakeNavBarView("White")
        DiscoverViewController.self.popSlideMotionOn(self.navigationController!)
    }
    
    func registerFor3DTouch(){
        if( traitCollection.forceTouchCapability == .available){
            registerForPreviewing(with: self, sourceView: view)
        }
    }
    
    func showFab(){
        if self.filterButton.alpha == 0.0{
            UIView.animate(withDuration: 1.0, animations: {
                self.filterButton.alpha = 0.88
                self.filterVisualEffectBlur.alpha = 0.88
                self.productDetailDisplayButton.alpha = 0.88
            })
        }
    }
    
    func hideFab(){
        if self.filterButton.alpha != 0.0{
            self.filterButton.alpha = 0.0
            self.filterVisualEffectBlur.alpha = 0.0
            self.productDetailDisplayButton.alpha = 0.0
        }
    }
    
    func animateCollections(){
        if !loadedDiscoveryData{
            self.showFab()
        }
        self.collectionView.reloadData()
        self.collectionView.layoutIfNeeded()
        let indexPaths = self.collectionView!.indexPathsForVisibleItems
        let sort = NSSortDescriptor(key: "row", ascending: true)
        let indexPathCells = (indexPaths as NSArray).sortedArray(using: [sort])
        
        var cells = [UICollectionViewCell]()
        for index in indexPathCells{
            cells.append(self.collectionView.cellForItem(at: index as! IndexPath)!)
        }
        
        for cell in cells{
            cell.alpha = 0.0
        }
        
        var delayCounter = 0
        
        for cell in cells{
            UIView.animate(withDuration: 0.7, delay: Double(delayCounter) * Double(0.05), animations: {
                cell.alpha = 1.0
            }, completion: nil)
            delayCounter += 1
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS  ///////////////////////////////////////////////////////////////////
    
    private func findShadowImage(under view: UIView) -> UIImageView? { // ###
        if view is UIImageView && view.bounds.size.height <= 1 {
            return (view as! UIImageView)
        }
        
        for subview in view.subviews {
            if let imageView = findShadowImage(under: subview) {
                return imageView
            }
        }
        return nil
    }
    
    // MARK: -  //////////////////////////////////////////////////////   3D TOUCH  ///////////////////////////////////////////////////////////////////
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        guard let offsetY = self.collectionView?.contentOffset.y else { return nil }
        let newLocation = CGPoint(x: location.x, y: location.y + offsetY)
        guard let indexPath = collectionView?.indexPathForItem(at: newLocation) else { return nil }
        
        guard let cell = collectionView?.cellForItem(at: indexPath) else { return nil }
        
        guard let previewVC = storyboard?.instantiateViewController(withIdentifier: "ProductPreviewViewController") as? ProductPreviewViewController else { return nil }
        
        previewVC.callingClass = self
        let product = self.products[indexPath.row]
        previewVC.product = product
        previewVC.view.layer.setValue(indexPath.row, forKey: "indexPathRow")
        let width = UIScreen.main.bounds.size.width - 100
        previewVC.preferredContentSize = CGSize(width: width, height: width)
        
        previewingContext.sourceRect = CGRect(x: cell.frame.x, y: (cell.frame.y + offsetY), width: cell.frame.size.width, height: cell.frame.size.height)
        
        return previewVC
    }
    
    //Delegate for 3d touch devices
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        if let index = viewControllerToCommit.view.layer.value(forKey: "indexPathRow") as? Int{
            if self.products.count > index{
                let product = self.products[index]
                let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
                productVC.product = product
                self.navigationController?.pushViewController(productVC, animated:false)
            }
        }
    }
    
    // MARK : - LOAD DATA HOME FEED
    
    fileprivate var loadedDiscoveryData = true
    
    var isLoadingDiscovery = false
    fileprivate func loadDiscovery(){
        isLoadingDiscovery = true
        self.hideFab()
        userChangedProductOverlayState = .off
        if firstTime {
            self.indicator.showActivityIndicator(uiView: self.view, offset: 64.0)
            firstTime = false
        } else {
            self.indicator.showActivityIndicator(uiView: self.view, offset: 128.0)
        }
        self.collectionView.backgroundView?.isHidden = true
        APIHelper.discovery(skip: 0, limit: self.limitNumber) {[weak vc = self] (products,productAttributes,productsCount,error) in //### No pagination
            vc?.indicator.hideActivityIndicator()
            if let discoveryProducts = products,vc?.isRaceCondition == false{
                vc?.currentPage = discoveryProducts.count
                vc?.products = discoveryProducts
                vc?.loadedDiscoveryData = true
                vc?.animateCollections()
            }else{
                // emptydata set
            }
            vc?.isRaceCondition = false
            vc?.isLoadingDiscovery = false
            
        }
    }
    
//    func scrollToTop(){
//        self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
//    }
//    
    var isRaceCondition = false
    
//    var isRequestSentInSearch = false
    // MARK : - LOAD DATA SEARCH
    fileprivate func searchProducts(_ searchTerm:String?, minPrice:Int?, maxPrice:Int?, colors:NSArray?){
//        self.isRequestSentInSearch = true
        
        if isLoadingDiscovery{
            isRaceCondition = true
            self.indicator.hideActivityIndicator()
        }
        self.collectionView.backgroundView?.isHidden = true
        self.hideFab()
        self.isEndOfPage = false
        self.filtersDictionary = NSDictionary()
        if let searchTerm = searchTerm, searchTerm != ""{
            
            self.products = [Product]()   // reload to show "Add product from web Header"

            APIHelper.searchGetProducts(searchTerm,skip:0, limit:limitNumber, minPrice:minPrice, maxPrice:maxPrice, colors:colors) { [weak vc = self](products,productAttributes,productsCount,error) in //$CS use product attributes for filter
                
                if let searchedProducts  = products{
                    // Ignore Previous search
                    if searchTerm == vc?.searchTermUserEnteredToSearch{
                        if let productsCount = productsCount{ //### take care of this when there is no json for total results
                            
                            RealmHelper.addToUserRecentSearch(searchTerm,searchTermCount: productsCount)
                        }
                        
                        if searchedProducts.count == 0{
                            //                        vc?.isSearching = false
                            vc?.isEndOfPage = true
                            vc?.initializeEmptySet()
                        }
                        
                        vc?.currentPage = searchedProducts.count
                        vc?.loadedDiscoveryData = false
                        vc?.products = searchedProducts

                    }
                }else {
                    
                }
//                vc?.isRequestSentInSearch = false
            }
        }
    }
    
    //SEARCH PAGINATION
    fileprivate func searchMoreProducts(_ searchTerm:String?, minPrice:Int?, maxPrice:Int?, colors:NSArray?){
        //        self.isSearching = true
        
        if let searchTerm = self.searchTextField.text, searchTerm != ""{
            
            APIHelper.searchGetProducts(searchTerm, skip:currentPage, limit:self.limitNumber, minPrice:minPrice, maxPrice:maxPrice, colors:colors) { [weak vc = self](products,productAttributes,productsCount,error) in
                // Ignore Previous search
                if let searchedProducts  = products{
                    if searchedProducts.count == 0{
//                        vc?.isSearching = false
                        vc?.isEndOfPage = true

                    }
                    if searchTerm == self.searchTermUserEnteredToSearch{
                        vc?.currentPage += searchedProducts.count
                        vc?.loadedDiscoveryData = false
                        vc?.products += searchedProducts
                    }
                }
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS  ///////////////////////////////////////////////////////////////////
    
    @IBAction func refineProducts(_ sender: UIButton) {
        loadFilters()
    }
    
    @IBAction func flipProductLabel(_ sender: UIButton) {
        switch userChangedProductOverlayState{
        case .on:
            Analytics.sharedInstance.actionsArray.append(Action(context:["flipAllTagsOff": true], forScreen:nil, withActivityType:"Flipped All Tags"))
            
            let image = UIImage(named: Constants.imageSearchLabelActive)
            sender.setImage(image, for: UIControlState())
            userChangedProductOverlayState = .off
        case .off:
            Analytics.sharedInstance.actionsArray.append(Action(context:["flipAllTagsOn": true], forScreen:nil, withActivityType:"Flipped All Tags"))
            let image = UIImage(named: Constants.imageSearchLabelInactive)
            sender.setImage(image, for: UIControlState())
            userChangedProductOverlayState = .on
        }
        collectionView.reloadData()
    }
    
    // MARK: -  //////////////////////////////////////////////////////  DELEGATE FROM SEARCH DISPLAY ///////////////////////////////////////////////////////////////////
    
    private func showProductScreen(_ product:Product?){
        if let product = product{
            self.searchTextField.resignFirstResponder()
            let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
            productVC.product = product
            self.navigationController?.pushViewController(productVC, animated:true)
        }
    }
    
    private func showuserProfile(_ user:User?){
        self.searchTextField.resignFirstResponder()
        let guestVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
        if let user = user{
            user.updateUser { (success) in
                guestVC.currentUser = user
                self.navigationController?.pushViewController(guestVC, animated: true)
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////  UI SETUP ///////////////////////////////////////////////////////////////////
    
    private func configureUI(){
        configureNavigationBar()
        configureSearchBar()
        configureOverlay()
        configureTableView()
    }
    
    private func configureNavigationBar(){ //###
        self.tabBarController?.tabBar.isHidden = false
        
        if navigationBottom == nil {
            navigationBottom = findShadowImage(under: navigationController!.navigationBar)
        }
        navigationBottom?.isHidden = true
    }
    private func configureSearchBar(){
        if let width = self.navigationController?.navigationBar.frame.width{
            self.searchTextField.frame.width = width
        }
    }
    
    private func configureTableView(){
        searchDisplayVC.view.frame = CGRect(x: 0, y: 0, width: searchView.frame.size.width, height: searchView.frame.size.height)
        searchDisplayVC = SearchDisplayViewController.instanceFromStoryboard("Main")
        searchDisplayVC.view.alpha = 0.0
        searchDisplayVC.view.clipsToBounds = true
        searchDisplayVC.setSearchTerm(nil)
        self.addChildViewController(searchDisplayVC)
        searchView.addSubview(searchDisplayVC.view)
        searchDisplayVC.delegate = self
    }
    
    private func configureOverlay(){
        if let searchView = Bundle.main.loadNibNamed("SearchView", owner: self, options: nil)!.last as? SearchView{
            searchView.frame = CGRect(x: 0, y: -self.view.frame.size.height + 22, width: self.view.frame.size.width, height: self.view.frame.size.height + 40)
            self.searchView = searchView
            self.view.addSubview(self.searchView)
            setUpOverlayDynamics()
        }
    }
    
    private func  setUpOverlayDynamics(){
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(DiscoverViewController.handlePan(_:)))
        panGestureRecognizer.cancelsTouchesInView = false
        
        animator = UIDynamicAnimator(referenceView: self.view)
        dynamicItem = UIDynamicItemBehavior(items: [searchView])
        
        dynamicItem.allowsRotation = false
        dynamicItem.elasticity = 0
        
        gravity = UIGravityBehavior(items: [searchView])
        gravity.gravityDirection = CGVector(dx: 0, dy: -1)
        
        container = UICollisionBehavior(items: [searchView])
        container.collisionDelegate = self
        
        configureContainer()
        
        animator.addBehavior(gravity)
        animator.addBehavior(dynamicItem)
        animator.addBehavior(container)
    }
    
    private func configureContainer(){
        let boundaryWidth = UIScreen.main.bounds.size.width
        let boundaryHeight = UIScreen.main.bounds.size.height
        
        container.addBoundary(withIdentifier: "upper" as NSCopying, from: CGPoint(x: 0, y: -searchView.frame.size.height + 30.0), to: CGPoint(x: boundaryWidth, y: -searchView.frame.size.height + 30))
        container.addBoundary(withIdentifier: "bottom" as NSCopying, from: CGPoint(x: 0, y: boundaryHeight - 30.0), to: CGPoint(x: boundaryWidth, y: boundaryHeight - 30.0))
        
    }
    
    private func initializeEmptySet(){
        let emptySetView: EmptySetView = EmptySetView.instanceFromNib()
        emptySetView.emojiImageView.image = UIImage()
        emptySetView.shouldShowAnimation = true
        emptySetView.discriptionLabel.text = "NO_DISCOVERY_PRODUCT_FOUND".localized
        emptySetView.arrowTopImage.isHidden = false
        
        self.collectionView.backgroundView = emptySetView
        self.collectionView.backgroundView?.isHidden = false
        self.collectionView.bounces = false
    }

    
    // MARK: -  //////////////////////////////////////////////////////  OVERLAY FUNCTIONS ///////////////////////////////////////////////////////////////////
    
    
    @objc private func handlePan (_ pan:UIPanGestureRecognizer){
        let velocity = pan.velocity(in: searchView)
        
        if pan.state == .ended{
            if velocity.y < 0{
                snapToTop()
            }
        }
    }
    
    @objc internal func collisionBehavior(_ behavior: UICollisionBehavior, endedContactFor item: UIDynamicItem, withBoundaryIdentifier identifier: NSCopying?) {
        
        if let identifier = identifier as? String,identifier == "upper"{
            overLayMovedToTop()
        }else if let identifier = identifier as? String,identifier == "bottom"{
            overLayMovedToBottom()
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   TEXTFIELD  DELEGATES  ///////////////////////////////////////////////////////////////////
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textFieldBeginEditing{
            textFieldBeginEditing = false
            return false
        }else{
            searchTextField.text = ""
            if showSearchDisplayVC == false{
                loadDiscovery()
            }else{
                searchUserEnteredKeyWord()
            }
            return false
        }
    }
    
    @IBAction func closeSearch(_ sender: UIBarButtonItem) {
        
        dismissSearchOverlay()
        if self.searchTextField.text == "" && !loadedDiscoveryData{
            loadDiscovery()
        }
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        searchUserEnteredKeyWord()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool { // search button function
        if let searchTerm = self.searchTextField.text, searchTerm != ""{
            
            Analytics.sharedInstance.actionsArray.append(Action(context: ["searchTerm":searchTerm], forScreen: nil, withActivityType:"Searched User"))

            self.searchTermUserEnteredToSearch = searchTerm
        }
        
        return true
    }
    
    internal func userSelectedSearchTerm(_ searchTerm:String){
        self.searchTextField.text = searchTerm
        self.searchTermUserEnteredToSearch = searchTerm
    }
    
    func getListingsWithFilters(filtersDictionary:NSDictionary){
        self.filtersDictionary = filtersDictionary
        if let searchTerm = self.searchTermUserEnteredToSearch, searchTerm != "" {
            if let minPrice = filtersDictionary["minPrice"] as? Int, let maxPrice = filtersDictionary["maxPrice"] as? Int{
                if let colors = filtersDictionary["colors"] as? NSArray{
                    //Search filters endpoint
                    APIHelper.searchGetProducts(searchTerm,skip:0, limit:limitNumber, minPrice:minPrice, maxPrice:maxPrice, colors:colors) { [weak vc = self](products,productAttributes,productsCount,error) in //$CS use product attributes for filter
                        
                        if let searchedProducts  = products{
                            // Ignore Previous search
                            if searchTerm == self.searchTermUserEnteredToSearch{
                                if let productsCount = productsCount{ //### take care of this when there is no json for total results
                                    RealmHelper.addToUserRecentSearch(searchTerm,searchTermCount: productsCount)
                                }
                                
                                vc?.currentPage = searchedProducts.count
                                vc?.loadedDiscoveryData = false
                                vc?.products = searchedProducts
                            }
                        }else {
                            DropDownAlert.customErrorMessage("ERROR".localized)
                        }
                    }
                } else {
                    DropDownAlert.customErrorMessage("ERROR".localized)
                }
            } else {
                DropDownAlert.customErrorMessage("ERROR".localized)
            }
        } else {
            //Discover filters endpoint
        }
    }
private var textFieldBeginEditing = false

func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
//    if indicator.isLoading{
//        return false
//    }
    textFieldBeginEditing = true
    return true
}

func textFieldDidBeginEditing(_ textField: UITextField) {
    if !showSearchDisplayVC{
        searchUserEnteredKeyWord()
        showSearchOverlay()
    }else{
        
    }
}

fileprivate func loadFilters(){
    DiscoverViewController.showFiltersPopup(callingClass: self, filtersDictionary:self.filtersDictionary)
}

// MARK: -  //////////////////////////////////////////////////////   SEARCH ACTIONS ///////////////////////////////////////////////////////////////////

private func searchUserEnteredKeyWord(){
    searchDisplayVC.setSearchTerm(searchTextField.text!)
}

//    1...

private func showSearchOverlay(){
    if !showSearchDisplayVC{
        showSearchDisplayVC = true
        snapToBottom()
    }
}

private func snapToBottom() {
    self.view.bringSubview(toFront: searchView)
    gravity.gravityDirection = CGVector(dx: 0, dy: 10.5)
}

private func overLayMovedToBottom() {
    UIView.animate(withDuration: 0.4) {
        self.searchDisplayVC.view.alpha = 1.0
    }
    self.leftBarButtonActive()
    self.searchTextField.becomeFirstResponder()
}

//    2...

private func dismissSearchOverlay(){
    snapToTop()
}

private func snapToTop(){
    gravity.gravityDirection = CGVector(dx: 0, dy: -10.5)
}

private func overLayMovedToTop() {
    self.searchDisplayVC.view.alpha = 0.0
    
    if showSearchDisplayVC{
        showSearchDisplayVC = false
        leftBarButtonInActive()
        resignKeyboard()
    }
}

func leftBarButtonActive(){
    self.leftNavBarButton.isEnabled = true
    self.leftNavBarButton.tintColor = UIColor.darkGray
}

func leftBarButtonInActive(){
    self.leftNavBarButton.isEnabled = false
    self.leftNavBarButton.tintColor = UIColor.clear
}

// MARK: -  //////////////////////////////////////////////////////  KEYBOARD  /////////////////////////////////////////////////////////////////////////

func resignKeyboard(){
    self.searchTextField.resignFirstResponder()
}

func showAddProductScreen(){
    performSegue(withIdentifier: "ShowAddProducts", sender: self)
}

override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ShowAddProducts"{
        if let nvc = segue.destination as? UINavigationController{
            if let vc = nvc.childViewControllers[0] as? AddProductWebViewController{
                vc.userEnteredURL = self.searchTextField.text
            }
        }
    }
}
    
}

// MARK: -  //////////////////////////////////////////////////////   COLLECTION VIEW EXTENSION   /////////////////////////////////////////////////////////////////////////

extension DiscoverViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    
    // MARK: -  //////////////////////////////////////////////////////   COLLECTION VIEW DATA SOURCE   /////////////////////////////////////////////////////////////////////////
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if let discoverCell = cell as? DiscoverCollectionViewCell{
            discoverCell.startTime = Date()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView.indexPathsForVisibleItems.index(of: indexPath) == nil {
            //Stop Timer
            
            if self.products.count > 0{
                if let discoverCell = cell as? DiscoverCollectionViewCell{
                    if let startTime = discoverCell.startTime{
                        let cellProduct = self.products[indexPath.row]
                        let timePassed:Double = abs(startTime.timeIntervalSinceNow)
                        if let productId = cellProduct.objectId{
                            let action = Action(timeOnProduct: timePassed, productId: productId, fromScreen: DiscoverViewController.getClassName())
                            Analytics.sharedInstance.actionsArray.append(action)
                        }
                        
                    }
                }
            }
        }
    }

    
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,referenceSizeForHeaderInSection section: Int) -> CGSize{
        
        if loadedDiscoveryData {
            return CGSize.zero //supplementary view will not be displayed if height/width are 0
        } else {
            return CGSize(width:self.view.frame.width,height:65) //size of your UICollectionReusableView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "DiscoverGoToAddProductCollectionViewCell", for: indexPath as IndexPath)
        
        if loadedDiscoveryData {
            headerView.isHidden = true //supplementary view will not be displayed if height/width are 0
        } else {
            headerView.isHidden = false //size of your UICollectionReusableView
        }
        let showTap = UITapGestureRecognizer(target: self, action: #selector(DiscoverViewController.showAddProductScreen))
        showTap.numberOfTapsRequired = 1
        //        self.view.isUserInteractionEnabled = true
        headerView.addGestureRecognizer(showTap)
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(indexPath.row == self.products.count - 1 && !loadedDiscoveryData && self.searchTextField.text != "" ) && !isEndOfPage{
            searchMoreProducts(self.searchTextField.text, minPrice:nil, maxPrice:nil, colors:nil)
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiscoverCollectionViewCell", for: indexPath) as! DiscoverCollectionViewCell
        
        if userChangedProductOverlayState == .on{
            cell.showProductDetails = true
        }else{
            cell.showProductDetails = false
        }
        cell.product = self.products[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: (self.view.frame.size.width / 2.0) - 0.5, height: (self.view.frame.size.width / 2.0) - 1.0)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    // MARK: -  //////////////////////////////////////////////////////   COLLECTION VIEW DELEGATE   /////////////////////////////////////////////////////////////////////////
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
        
        let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
        
        let product = self.products[index]
        productVC.product = product
        self.navigationController?.pushViewController(productVC, animated:true)
        
    }
}

extension UIButton {
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
