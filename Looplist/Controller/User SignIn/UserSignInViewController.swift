//     // ###################commented by Arun change this , fix this issue
//  UserLoginViewController.swift
//  alphalooplist
//
//  Created by Arun on 2/23/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.

import UIKit

import Alamofire
import SwiftyJSON
import LocalAuthentication
import FacebookCore
import FacebookLogin

class UserSignInViewController: UIViewController, UIViewControllerTransitioningDelegate, UITextFieldDelegate, CAAnimationDelegate {
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////
    
    let MyKeychainWrapper = KeychainWrapper()
    var context = LAContext()

    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var facebookLoginButton: TKTransitionSubmitButton!{
        didSet{
            facebookLoginButton.layer.cornerRadius = 6.0
        }
    }
    @IBOutlet weak var signInButton: TKTransitionSubmitButton!{
        didSet{
            if let signInButton = signInButton{
                signInButton.layer.cornerRadius = signInButton.frame.height / 2.0
                signInButton.layer.borderWidth = 1.0
                signInButton.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0).cgColor
            }
        }
    }
    
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var forgotPassword: UIButton!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var touchIdButton: UIButton!
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        UserSignInViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        UserSignInViewController.stopTimer(["preLogin":true, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], withActionType:"View Screen")
    }

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if let count = navigationController?.viewControllers.count, count == 1{
            self.navigationController?.navigationItem.leftBarButtonItem = nil
        }

        self.signInButton.setTitle("LOGIN".localized, for: .normal)
        self.facebookLoginButton.setTitle("LOG_IN_WITH_FACEBOOK".localized, for: .normal)
        self.usernameText.placeholder = "USERNAME_OR_EMAIL".localized
        self.passwordText.placeholder = "PASSWORD".localized
        self.forgotPassword.setTitle("FORGOT_YOUR_PASSWORD".localized, for: .normal)
        self.createAccountButton.setTitle("CREATE_AN_ACCOUNT".localized, for: .normal)

        self.view.isUserInteractionEnabled = true
        self.usernameText.delegate = self
        self.passwordText.delegate = self
        
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error:nil) {
            if let username = self.MyKeychainWrapper.myObject(forKey: kSecAttrAccount) as? String, let password = self.MyKeychainWrapper.myObject(forKey: kSecAttrDescription) as? String{
                if(username != "Account" && password != "Item description" && username != "" && password != ""){
                    self.touchIdButton.isHidden = false
                }
            }
        }
        
        //makes the navigation bar swipe from left to right to pop
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        
    }
    
    @IBAction func UnwindToSignIn(_ segue: UIStoryboardSegue) {
        
    }

    @IBAction func facebookSignIn(_ sender: UIButton) {
        if let _ = AccessToken.current {
            self.signInToLooplistWithFacebook()
        } else {
            self.signInToFacebook()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIApplication.shared.isStatusBarHidden = false;
        if let navigationController = self.navigationController{
            UserSignInViewController.self.setNavigationBar(UIColor.white, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.lightContent, backgroundColor:NavigationBarBackgroundColor.clearNoGradient)
        }
    }

    @IBAction func touchIdLogin(_ sender: UIButton) {
        self.touchIDLoginAction()
    }
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    fileprivate func signInToFacebook(){
        let loginManager = LoginManager()
        loginManager.logIn([.publicProfile, .userFriends, .email], viewController: self, completion: { (loginResult) in
            switch loginResult {
            case .failed( _):
                break;
            case .cancelled:
                break;
            case .success(grantedPermissions: _, declinedPermissions: _, token: let accessToken):
                if let facebookId = accessToken.userId, let objectId = CurrentUser.sharedInstance.currentUser?.objectId{
                    DBHelper.callAPIPut("users/\(objectId)", params: ["facebookId":facebookId as AnyObject], completion: { (result, error) in
                    })
                    self.signInToLooplistWithFacebook()
                }
                break;
            }
            
        })
    }
    
    fileprivate func signInToLooplistWithFacebook(){
        self.facebookLoginButton.startLoadingAnimation()
        if let fbAccessToken = AccessToken.current?.authenticationToken{
            DBHelper.callAPIPost("authentication/facebook/login", params: ["facebookOauthToken":fbAccessToken as Optional<AnyObject>], completion: { (result, error) in
                if let result = result{
                    if let user = result["user"] as? Dictionary<String, AnyObject>, let accessToken = result["accessToken"] as? String{
                        CurrentUser.sharedInstance.assignCurrentUser(User(input:user))
                        //Store access token in keychain
                        self.MyKeychainWrapper.mySetObject(accessToken, forKey:kSecValueData)
                        self.MyKeychainWrapper.writeToKeychain()
                        if let userObjectId = CurrentUser.sharedInstance.currentUser?.objectId {
                            
                            Analytics.sharedInstance.actionsArray.append(Action(context:["userSignIn": userObjectId, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], forScreen:nil, withActivityType:"Sign In")) // $AS
                            
                            self.facebookLoginButton.startFinishAnimation(delay: 2.0, completion: { () -> () in
                                
                                DBHelper.setUserDefaults()
                                
                                self.dismiss(animated: true, completion:nil)
                                if let appDelegate = UIApplication.shared.delegate as? AppDelegate{
                                    appDelegate.showScreen(ConstantsViewController.viewControllerTabBar)
                                    self.signInButton.removeFromSuperview()
                                }
                            })
                            
                        } else {
                            ErrorHandling.customErrorHandler(message: "ERROR_SIGNING_IN".localized)
                        }
                    } else {
                        ErrorHandling.customErrorHandler(message: "ERROR_SIGNING_IN".localized)
                    }
                } else if let _ = error {
                    self.facebookLoginButton.stopLoadingAnimation()
                    ErrorHandling.customErrorHandlerWithTitle(title:"ERROR".localized, message: "FACEBOOK_LOGIN_FAILED".localized)
                }
            })
        }
    }
    
    @IBAction func signIn(_ sender: TKTransitionSubmitButton) {
        self.view.endEditing(true)
        
        if usernameText.text!.isEmpty || passwordText.text!.isEmpty{
            ErrorHandling.customErrorHandlerWithTitle(title:"SORRY".localized, message: "ALL_FIELDS_MANDATORY".localized)
        }
            
        else if (usernameText.text!.characters.count < 3){
            
            ErrorHandling.customErrorHandlerWithTitle(title:"UNABLE_SIGN_IN".localized, message: "ENTER_VALID_USERNAME".localized)
        }
            
        else if (passwordText.text!.characters.count < 8){
            ErrorHandling.customErrorHandlerWithTitle(title:"UNABLE_SIGN_IN".localized, message: "ENTER_VALID_PASSWORD".localized)
        }
            
        else{
            self.signInButton.startLoadingAnimation()
            self.login(self.usernameText.text!, password: self.passwordText.text!)
        }
    }
    
    func login(_ username:String, password:String){
        let path = "authentication/local/login"
        let parameters:[String:AnyObject] = ["username" : username as AnyObject, "password" : password as AnyObject]
        DBHelper.callAPIPost(path, params: parameters, completion: { (result, error) in
            if let result = result{
                if let user = result["user"] as? Dictionary<String, AnyObject>, let accessToken = result["accessToken"] as? String{
                    CurrentUser.sharedInstance.assignCurrentUser(User(input:user))
                    //Store username and password in keychain
                    self.MyKeychainWrapper.mySetObject(accessToken, forKey:kSecValueData)
                    self.MyKeychainWrapper.mySetObject(username, forKey: kSecAttrAccount)
                    self.MyKeychainWrapper.mySetObject(password, forKey: kSecAttrDescription)
                    self.MyKeychainWrapper.writeToKeychain()
                    if let userObjectId = CurrentUser.sharedInstance.currentUser?.objectId {
                        
                        Analytics.sharedInstance.actionsArray.append(Action(context:["userSignIn": userObjectId, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], forScreen:nil, withActivityType:"Sign In")) // $AS
                        
                        self.signInButton.startFinishAnimation(delay: 2.0, completion: { () -> () in
                            
                            DBHelper.setUserDefaults()
                            
                            self.dismiss(animated: true, completion:nil)
                            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.showScreen(ConstantsViewController.viewControllerTabBar)
                            self.signInButton.removeFromSuperview()
                        })
                        
                    } else {
                        ErrorHandling.customErrorHandler(message: "ERROR_SIGNING_IN".localized)
                    }
                } else {
                    ErrorHandling.customErrorHandler(message: "ERROR_SIGNING_IN".localized)
                }
            } else if let _ = error {
                self.signInButton.stopLoadingAnimation()
                self.MyKeychainWrapper.mySetObject("", forKey: kSecAttrAccount)
                self.MyKeychainWrapper.mySetObject("", forKey: kSecAttrDescription)
                self.touchIdButton.isHidden = true
                ErrorHandling.customErrorHandlerWithTitle(title:"INCORRECT_USERNAME_PASSWORD".localized, message: "PLEASE_TRY_AGAIN".localized)
            }
        })
    }
    // MARK: -  //////////////////////////////////////////////////////   TEXT FIELD DELEGATE   /////////////////////////////////////////////////////////////////////////
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view!.endEditing(true)
    }
    
    func touchIDLoginAction() {
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error:nil) {
            
            context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: "TOUCH_ID_PERMISSION_REASON".localized, reply: { (success, error) in
                DispatchQueue.main.async(execute: {
                    if success {
                        if let username = self.MyKeychainWrapper.myObject(forKey: kSecAttrAccount) as? String, let password = self.MyKeychainWrapper.myObject(forKey: kSecAttrDescription) as? String{
                            self.signInButton.startLoadingAnimation()
                            self.login(username, password: password)
                        }else {
                            ErrorHandling.customErrorHandler(message:"TOUCH_ID_ERROR_STORED_CREDENTIALS".localized)
                        }
                    }
                    
                    if error != nil {
                        switch(error!) {
                        case LAError.authenticationFailed:
                            ErrorHandling.customErrorHandler(message:"TOUCH_ID_ERROR_VERIFYING_IDENTITY".localized)
                            break;
                        case LAError.userCancel:
                            break;
                        case LAError.userFallback:
                            ErrorHandling.customErrorHandler(message:"YOU_PASSWORD".localized)
                            break;
                        default:
                            ErrorHandling.customErrorHandler(message:"TOUCH_ID_NOT_CONFIGURED".localized)
                            break;
                        }
                    }
                })
            })

        } else {
            ErrorHandling.customErrorHandler(message:"TOUCH_ID_ERROR_STORED_CREDENTIALS".localized)
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isEqual(self.usernameText){
            self.passwordText.becomeFirstResponder()
        } else if textField.isEqual(self.passwordText){
            self.signIn(self.signInButton)
        }
        textField.resignFirstResponder()
        return true;
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.isEqual(self.usernameText){
            textField.returnKeyType = .next
        } else if textField.isEqual(self.passwordText){
            textField.returnKeyType = .done
        }
    }

    
    // MARK: -  //////////////////////////////////////////////////////   TRANSITIONING DELEGATE   /////////////////////////////////////////////////////////////////////////
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TKFadeInAnimator(transitionDuration: 2.0, startingAlpha: 0.8)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
}
