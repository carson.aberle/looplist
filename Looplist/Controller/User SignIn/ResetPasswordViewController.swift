//
//  File.swift
//  Looplist
//
//  Created by Looplist Inc on 3/6/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import Foundation


class ResetPasswordViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var passwordToEmailText: UITextField!
    @IBOutlet weak var resetPasswordButton: UIButton!{
        didSet{
            if let resetPasswordButton = resetPasswordButton{
                resetPasswordButton.layer.cornerRadius = resetPasswordButton.frame.height / 2.0
                resetPasswordButton.layer.borderWidth = 1.0
                resetPasswordButton.layer.borderColor = UIColor.white.cgColor
            
            }
        }
    }
//    var gradient:CAGradientLayer?
    fileprivate var indicator = CustomActivityIndicator()


    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        self.passwordToEmailText.becomeFirstResponder()
        ResetPasswordViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        ResetPasswordViewController.stopTimer(["preLogin":true, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], withActionType:"View Screen")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view!.endEditing(true)
    }
    
    override func viewDidLoad() {
//        self.view.layer.insertSublayer(self.gradient!, at: 0)
          
        let hideTap = UITapGestureRecognizer(target: self, action: #selector(ResetPasswordViewController.hideKeyboardTap(_:)))
        hideTap.numberOfTapsRequired = 1
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(hideTap)
        
        self.title = "FORGOT_PASSWORD".localized
        self.resetPasswordButton.setTitle("RESET_PASSWORD".localized, for: .normal)
        self.passwordToEmailText.placeholder = "ENTER_YOUR_REGISTERED_EMAIL".localized
        setGradient()
    }
    
    
    func setGradient(){
        self.view.layer.insertSublayer(HelperFunctionAnimation.getLooplistGradient(insideFrame: self.view), at: 0)
    }
    
    
    func goBack(){
//        self.performSegue(withIdentifier: "UnwindToSignIn", sender: self)
        _ = self.navigationController?.popToRootViewController(animated: true)
    }

    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func popViewController(_ sender: UIBarButtonItem) {
        //_ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendToEmail(_ sender: UIButton) {
        sender.isUserInteractionEnabled = false
        self.indicator.showActivityIndicator(uiView: self.view, offset: 0.0)
        let validEmailEnteredByUser =  HelperFunction.validateEmail(passwordToEmailText.text!)
        
        if validEmailEnteredByUser{
            self.view.endEditing(true)
            DBHelper.callAPIPost("authentication/local/reset-password", params: ["username":passwordToEmailText.text! as Optional<AnyObject>], completion: { (result, error) in
                self.indicator.hideActivityIndicator()
                if error == nil {

                     ErrorHandling.customSuccessHandlerWithTitle(title:"SUCCESS".localized, message:"EMAIL_SENT".localized)
                     Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.goBack), userInfo: nil, repeats: false)
                
                } else {
                    sender.isUserInteractionEnabled = true
                    ErrorHandling.customErrorHandlerWithTitle(title:"ERROR".localized, message:"CANT_RESET_PASSWORD".localized)
                }
            })
        }else{
            sender.isUserInteractionEnabled = true
            self.indicator.hideActivityIndicator()
            ErrorHandling.customErrorHandlerWithTitle(title:"ERROR".localized, message:"ENTER_VALID_EMAIL".localized)
        }
    }
    
    
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    func hideKeyboardTap(_ recognizer:UITapGestureRecognizer){
        self.view.endEditing(true)
    }
}
