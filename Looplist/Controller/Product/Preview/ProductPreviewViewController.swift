//
//  HomeFeedPreviewViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 9/1/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class ProductPreviewViewController:UIViewController{
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////
    
    var product:Product?
    var callingClass:UIViewController?
    var likeButton:UIButton!
    var repostButton:UIButton!
    var addToCollectionButton:UIButton!
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var priceRangeLabel: UILabel!
    @IBOutlet weak var attributesLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        ProductPreviewViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        ProductPreviewViewController.stopTimer(NSDictionary(), withActionType: "View Popup")
    }
    
    override func viewDidLoad() {
        if let product = product{
            if let imageURL = product.images?[0]{
                self.productImage.kf.setImage(with:URL(string: imageURL))
            }
            self.priceRangeLabel.text = product.priceRange
            self.productNameLabel.text = product.brandName
            self.attributesLabel.text = product.keywords


        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    override var previewActionItems : [UIPreviewActionItem] {
        var likeTitle:String
        if let product = product{
            if let isLiked = product.isLikedByCurrentUser{
                if isLiked{
                    likeTitle = "UNLIKE".localized
                } else {
                    likeTitle = "LIKE".localized
                }
            } else {
                likeTitle = "LIKE".localized

            }
            
            
            let likeAction = UIPreviewAction(title: likeTitle, style: .default) { (action, viewController) -> Void in
                if let collectionLikesVC = self.callingClass as? CollectionLikesCollectionViewController{
                    SelectionBar.likeProduct(collectionLikesVC, product: product, sender: UIButton())
                    collectionLikesVC.loadCollectionProducts(skip:0)
                } else if let discoverVC = self.callingClass as? DiscoverViewController{
                    SelectionBar.likeProduct(discoverVC, product: product, sender: UIButton())
                }
            }
            
            let addToCollectionAction = UIPreviewAction(title: "ADD_TO_COLLECTION".localized, style: .default) { (action, viewController) -> Void in
                if let collectionLikesVC = self.callingClass as? CollectionLikesCollectionViewController{
                    SelectionBar.addProductToCollection(collectionLikesVC, product: product, sender: UIButton())
                } else if let discoverVC = self.callingClass as? DiscoverViewController{
                    SelectionBar.addProductToCollection(discoverVC, product: product, sender: UIButton())
                }
            }
            
            let repostAction = UIPreviewAction(title: "REPOST".localized, style: .default) { (action, viewController) -> Void in
                if let collectionLikesVC = self.callingClass as? CollectionLikesCollectionViewController{
                    SelectionBar.repostProduct(collectionLikesVC, product: product, sender: UIButton())
                } else if let discoverVC = self.callingClass as? DiscoverViewController{
                    SelectionBar.repostProduct(discoverVC, product: product, sender: UIButton())
                }
            }
            
            return [likeAction, addToCollectionAction, repostAction]
        }
        return []
        
    }
}
