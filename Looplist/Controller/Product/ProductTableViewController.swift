 //
 //  ProductViewController.swift
 //  Looplist
 //
 //  Created by Carson Aberle on 11/5/16.
 //  Copyright © 2016 Looplist. All rights reserved.
 //
 
 import UIKit
 import HTMLAttributedString
 
 class ProductTableViewController: UIViewController {
    
    // MARK: -   -----------------------------------------------------      VARIABLES   -----------------------------------------------------
    
    var product:Product? {
        didSet{
            if let product = self.product{
                APIHelperProduct.updateProduct(product) { (true) in
                    if let tableView = self.tableView{
                        tableView.reloadSections([1,3], with: .none)
//                        tableView.reloadData()
                    }
                }
            }
        }
    }
    
    private var tableHeaderHieght: CGFloat = 414
    private var listFlowLayout = FlowLayout(height: 0.0)
    var isLikingProduct:Bool = false
    var isRepostingProduct:Bool = false
    fileprivate var productVariantsCell:ProductVariantsTableViewCell?
    fileprivate var likingUsersFirstFourURLs:[String?] = [String?]()
    fileprivate var productVariations:[Product?] = [Product?]()
    fileprivate weak var productHeaderView: UIView!
    fileprivate var offsetPoint: CGFloat = 15.0
    fileprivate enum NavigationColor{
        case clear
        case white
    }
    
    fileprivate var navigationColorStatus = NavigationColor.clear
    
    // MARK: -   -----------------------------------------------------      OUTLETS  -----------------------------------------------------
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerCollectionView: UICollectionView!
    @IBOutlet weak var productPageControl: UIPageControl!
    
    // MARK: -   -----------------------------------------------------      LIFE CYCLE  -----------------------------------------------------
    
    override func viewDidAppear(_ animated: Bool) {
        ProductTableViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let objectId = self.product?.objectId{
            ProductTableViewController.stopTimer(["product":objectId], withActionType:"View Screen")
        } else {
            ProductTableViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableHeaderHieght = self.view.frame.width
        self.scrollViewDidScroll(self.tableView)
        self.updateProduct()
        self.getLikingUsers()
    }
    
    // MARK: -   -----------------------------------------------------     ACTIONS  -----------------------------------------------------
    
    @IBAction func openLikingUsers(_ sender: UIButton) {
        let likingUsersVC:ProductLikingUsersTableViewController = ProductLikingUsersTableViewController.instanceFromStoryboard("Product")
        likingUsersVC.product = self.product
        self.navigationController?.pushViewController(likingUsersVC, animated: true)
    }
    
    @IBAction func buyClicked(_ sender: UIButton) {
        sender.isEnabled = false
        if self.product?.status == "pending"{
            if let objectId = self.product?.objectId{
                let params: NSDictionary = ["where": ["product":objectId],"limit": 1]
                APIHelperProduct.getProductListingsWithParameters(DBHelper.getJSONFromDictionary(params), self.product, {[weak vc = self] (listings, success) in
                    if success {
                        if listings.count > 0 {
                            let firstListing = listings[0]
                            let pendingVC:ProductPendingBuyTableViewController = ProductPendingBuyTableViewController.instanceFromStoryboard("Product")
                            pendingVC.product = vc?.product
                            pendingVC.listing = firstListing
                            vc?.navigationController?.pushViewController(pendingVC, animated: true)
                            sender.isEnabled = true

                        } else {
                            ErrorHandling.customErrorHandler(message: "PRODUCT_CANNOT_BE_PURCHASED".localized)
                            sender.isEnabled = true

                        }
                    } else {
                        ErrorHandling.customErrorHandler(message: "PRODUCT_CANNOT_BE_PURCHASED".localized)
                        sender.isEnabled = true
                    }
                })
            }
        } else {
            let productBuyVC:ProductBuyViewController = ProductBuyViewController.instanceFromStoryboard("Product")
            productBuyVC.product = self.product
            self.navigationController?.pushViewController(productBuyVC, animated: true)
            sender.isEnabled = true

        }
    }
    
    @IBAction func addProductToCollection(_ sender: UIButton) {
        if let product = self.product{
            SelectionBar.addProductToCollection(self,product: product, sender: sender)
        } else {
            ErrorHandling.customErrorHandler(message:"ERROR".localized)
        }
    }
    
    @IBAction func repostProduct(_ sender: UIButton) {
        if !self.isRepostingProduct{
            self.isRepostingProduct = true
            if let product = self.product{
                SelectionBar.repostProduct(self, product: product, sender: sender)
            } else {
                ErrorHandling.customErrorHandler(message:"ERROR".localized)
            }
        }
    }
    
    @IBAction func reportProduct(_ sender: UIButton) {
        if let product = self.product{
            SelectionBar.reportProduct(self,product: product, sender: sender)
        } else {
            ErrorHandling.customErrorHandler(message:"ERROR".localized)
        }
    }
    
    @IBAction func shareProduct(_ sender: UIButton) {
        if let product = self.product{
            SelectionBar.shareProduct(self,product: product, sender: sender)
        } else {
            ErrorHandling.customErrorHandler(message:"ERROR".localized)
        }
    }
    
    @IBAction func likeProduct(_ sender: UIButton?) {
        if !self.isLikingProduct{
            self.isLikingProduct = true
            if let product = self.product{
                if let button = sender{
                    SelectionBar.likeProduct(self,product: product, sender: button)
                }
            } else {
                ErrorHandling.customErrorHandler(message:"ERROR".localized)
            }
        }
    }
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    // MARK: -   -----------------------------------------------------     FUNCTIONS   -----------------------------------------------------
    
    fileprivate func configureUI(){
        self.tabBarController?.tabBar.isHidden = true
        
        if let navigationController = self.navigationController {
            ProductTableViewController.self.setNavigationBar(UIColor.white, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.lightContent, backgroundColor:NavigationBarBackgroundColor.clear)
        }
        
        self.productHeaderView = tableView.tableHeaderView
        self.tableView.tableHeaderView = nil
        self.tableView.addSubview(self.productHeaderView)
        
        self.tableView.contentInset = UIEdgeInsets(top: self.view.frame.width - 64, left: 0, bottom: 0, right: 0)
        self.tableView.contentOffset = CGPoint(x: 0, y: -self.view.frame.width + 64)
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 20.0
        self.tableView.tableFooterView = UIView()
        
        if let productVariationImages = self.product?.images {
            self.productPageControl.numberOfPages = productVariationImages.count
            self.productPageControl.currentPage = 0
        }
    }
    
    fileprivate func updateProduct(){
        if let product = self.product{
            DispatchQueue.global(qos: .userInitiated).async {
                self.getProductVariations(product:product)
            }
        }
    }
    
    private func getProductVariations(product:Product){
        APIHelperProduct.getVariations(product,{[weak vc = self] (success) in
            if success{
                if let variations = self.product?.productVariations{
                    vc?.productVariations = variations
                    
                    vc?.tableView.reloadSections([0], with: .none)
                    vc?.productVariantsCell?.variantsCollectionView.reloadData()
                }
            }
        })
    }
    
    fileprivate func getLikingUsers(){
        if let objectId = self.product?.objectId{
            APIHelperProduct.getUsersWhoLikedProduct(objectId, {[weak vc = self] (usersWhoLikedProduct, success) in
                if success {
                    if let usersUrls = usersWhoLikedProduct {
                        if usersUrls.count > 0 {
                            vc?.likingUsersFirstFourURLs = usersUrls
                            vc?.tableView.reloadSections([2], with: .none)
                        }
                    }
                }
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowCollection"{
            if segue.destination is UINavigationController{
                if let addToCollectionVC = (segue.destination as! UINavigationController).viewControllers.first as? AddToCollectionViewController{
                    if let popupView = sender as? AddToCollectionPopupView{
                        addToCollectionVC.product = popupView.product
                        addToCollectionVC.callingClass = self
                        addToCollectionVC.collections = popupView.collections
                    }
                }
            }
        }
    }
    
    fileprivate func showAddToCollectionScreen(view:AddToCollectionPopupView){
        performSegue(withIdentifier: "ShowCollection", sender: view)
    }
    
    func expandAddToCollectionPopup(view:AddToCollectionPopupView){
        let animation = CABasicAnimation(keyPath:"cornerRadius")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.fromValue = 10
        animation.toValue = 0
        animation.duration = 0.5
        view.layer.add(animation, forKey: "cornerRadius")
        view.layer.cornerRadius = 0
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            view.frame = UIScreen.main.bounds
        }, completion: { finished in
            self.showAddToCollectionScreen(view:view)
        })
    }
    
    fileprivate func updateHeaderView(){
        var headerRect = CGRect(x: 0, y: -self.tableHeaderHieght, width: self.tableHeaderHieght, height: self.tableHeaderHieght)
        self.headerCollectionView.collectionViewLayout.invalidateLayout()
        if tableView.contentOffset.y < -self.view.frame.width {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y
            
            self.listFlowLayout.itemHeight = headerRect.size.height
        } else {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y
        }
        
        self.headerCollectionView.setCollectionViewLayout(self.listFlowLayout, animated: true)
        if self.productHeaderView != nil{
            self.productHeaderView.frame = headerRect
        }
    }
    
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isEqual(self.tableView){
            // parallax view update
            self.updateHeaderView()
            
            if scrollView.contentOffset.y  > self.offsetPoint && self.navigationController != nil {
                self.navigationController?.navigationBar.viewWithTag(99)?.removeFromSuperview()
                //WHITE
                if let brand = self.product?.brandName{
                    self.navigationItem.title = brand
                }
                ProductTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController!, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
                let transparentWhiteBackground = self.navigationController?.navigationBar.superview?.viewWithTag(99)
                self.navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGray.withAlphaComponent(0.0)
                self.navigationController?.navigationBar.tintColor = UIColor.darkGray.withAlphaComponent(0.0)
                
                UIView.animate(withDuration: 0.2, animations: {
                    transparentWhiteBackground?.alpha = 1.0
                    self.navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGray.withAlphaComponent(1.0)
                    self.navigationController?.navigationBar.tintColor = UIColor.darkGray.withAlphaComponent(1.0)
                })
                
                self.navigationColorStatus = NavigationColor.white
            } else if self.navigationController != nil{
                self.navigationController?.navigationBar.viewWithTag(99)?.removeFromSuperview()
                // CLEAR
                
                self.navigationItem.title = ""
                ProductTableViewController.self.setNavigationBar(UIColor.white, font: ConstantsFont.navBarFont, navigationController: navigationController!, statusBar:.lightContent, backgroundColor:NavigationBarBackgroundColor.clear)
                self.navigationController?.navigationBar.backgroundColor = UIColor.white.withAlphaComponent(1.0)
                self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white.withAlphaComponent(0.0)
                self.navigationController?.navigationBar.backgroundColor = UIColor.clear
                UIView.animate(withDuration: 0.2, animations: {
                    self.navigationController?.navigationBar.backgroundColor = UIColor.white.withAlphaComponent(0.0)
                    self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white.withAlphaComponent(1.0)
                    self.navigationController?.navigationBar.tintColor = UIColor.white
                })
                
                self.navigationColorStatus = NavigationColor.clear
            }
        }
        
        // header scroll view page update
        if scrollView.isEqual(self.headerCollectionView){
            let pageWidth = scrollView.frame.size.width
            let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
            self.productPageControl?.currentPage = page
        }
    }
 }
 
 // MARK: -   -----------------------------------------------------     TABLEVIEW DELEGATE AND DATASOUCE  -----------------------------------------------------
 
 extension ProductTableViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            if self.productVariations.count > 0{
                self.offsetPoint = 88.0
                return 73
            } else {
                self.offsetPoint = 15.0
                return 0
            }
        case 1:
            return 177
        case 2:
            if let count = self.product?.likesCount, count > 0 {
                return 58
            } else {
                return 0
            }
        case 3:
            if self.product?.descriptionAttributedString?.string != nil {
                return UITableViewAutomaticDimension
            } else {
                return 0
            }
        default:
            return 0
        }
    }
    
    private enum Cell:String {
        case variants = "ProductVariantsTableViewCell"
        case info = "ProductInfoTableViewCell"
        case users = "ProductUserLikingsTableViewCell"
        case description = "ProductDescriptionTableViewCell"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.variants.rawValue, for: indexPath) as! ProductVariantsTableViewCell
            self.productVariantsCell = cell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.info.rawValue, for: indexPath) as! ProductInfoTableViewCell
            if let product = self.product{
                cell.product = product
            }
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.users.rawValue, for: indexPath) as! ProductUserLikingsTableViewCell
            cell.count = self.product?.likesCount
            cell.likingUsersFirstFourURLs = self.likingUsersFirstFourURLs
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.description.rawValue, for: indexPath) as! ProductDescriptionTableViewCell
            cell.pDescription = self.product?.descriptionAttributedString
            return cell
        default:
            let cell = UITableViewCell()
            return cell
        }
    }
 }
 
 // MARK: -   -----------------------------------------------------     COLLECTIONVIEW DELEGETE AND DATASOURCE  -----------------------------------------------------
 
 extension ProductTableViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView.isEqual(self.headerCollectionView)){
            if let productVariationImages = product?.images {
                return productVariationImages.count
            } else {
                return 0
            }
        } else {
            return self.productVariations.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView.isEqual(self.headerCollectionView)){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductHeaderVariationImageCollectionViewCell", for: indexPath) as! ProductHeaderVariationImageCollectionViewCell
            
            if let count = self.product?.images?.count, count > indexPath.row{
                if let imageURL = URL(string:(self.product?.images?[indexPath.row])!){
                    Analytics.sharedInstance.actionsArray.append(Action(context:["changedImageOnProductToURL": imageURL.absoluteString, "productId":self.product!.objectId!], forScreen:nil, withActivityType:"Changed Image on Product"))
                    cell.productHeaderVariationImage.kf.setImage(with:imageURL)
                }
            }
            
            //Load next image as well for instant load
            if let count = self.product?.images?.count, count > indexPath.row + 1{
                if let imageURL = URL(string:(self.product?.images?[indexPath.row + 1])!){
                    let imageView = UIImageView()
                    imageView.kf.setImage(with:imageURL)
                }
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductVariantsCollectionViewCell", for: indexPath) as! ProductVariantsCollectionViewCell
            cell.product = self.productVariations[indexPath.row]
            if(self.productVariations[indexPath.row]?.objectId!.isEqual(self.product?.objectId))!{
                cell.layer.borderColor = ConstantsColor.looplistColor.cgColor
            }
            return cell
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        if(collectionView.isEqual(self.headerCollectionView)){
            return CGSize(width: self.productHeaderView.frame.width, height: self.productHeaderView.frame.height)
        } else {
            return CGSize(width: 56,height: 56)
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   COLLECTION VIEW DELEGATE   /////////////////////////////////////////////////////////////////////////
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !(collectionView.isEqual(self.headerCollectionView)){
            if let product = self.productVariations[indexPath.row]{
                self.product = product
                if let objectId = self.product?.objectId{
                    Analytics.sharedInstance.actionsArray.append(Action(context:["changedVariantOnProductScreen": objectId], forScreen:nil, withActivityType:"Changed Variant On Product"))
                }
                
                self.headerCollectionView.setContentOffset(CGPoint.zero, animated: true)
                self.headerCollectionView.reloadData()
                self.productPageControl.numberOfPages = (product.images?.count)!
                self.productPageControl.currentPage = 0
                self.getLikingUsers()
                self.updateProduct()
            }
        }
    }
 }
