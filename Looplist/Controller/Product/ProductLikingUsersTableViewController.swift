//
//  ProductLikingUsersTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 11/7/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class ProductLikingUsersTableViewController: UITableViewController {
    
    // MARK: -   -----------------------------------------------------      VARIABLES   -----------------------------------------------------
    
    var homeFeedActivity:ActivityHomeFeed?
    var product:Product?
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var usersArray:[User] = [User]()
    
    // MARK: -   -----------------------------------------------------      LIFE CYCLE   -----------------------------------------------------
    
    override func viewDidAppear(_ animated: Bool) {
        ProductLikingUsersTableViewController.startTimer()
        if let navigationController = self.navigationController {
            if navigationController.viewControllers.last!.isEqual(self){
                ProductBuyRetailerTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor: NavigationBarBackgroundColor.whiteOpaque)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let objectId = self.product?.objectId{
            ProductLikingUsersTableViewController.stopTimer(["product":objectId], withActionType:"View Screen")
        } else {
            ProductLikingUsersTableViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        
        
        if let homeFeedActivity = self.homeFeedActivity{
            if let homeFeedActivityType = homeFeedActivity.activityTypeHomeFeed{
                if homeFeedActivityType == .aggLikeProduct{
                    self.navigationItem.title = "WHO_HAS_LIKED_THIS".localized
                    if let users = homeFeedActivity.users{
                        self.usersArray = users
                    }
                    self.tableView.reloadData()
                } else if homeFeedActivityType == .aggRepostProduct{
                    self.navigationItem.title = "WHO_HAS_REPOSTED_THIS".localized
                    if let users = homeFeedActivity.users{
                        self.usersArray = users
                    }
                    self.tableView.reloadData()
                    
                }
            } else {
                self.navigationItem.title = "WHO_HAS_LIKED_THIS".localized
                DropDownAlert.customErrorMessage("ERROR".localized)
            }
        } else {
            self.loadMoreUsers()
        }
        self.tableView.rowHeight = 60
        self.tableView.tableFooterView = UIView()
        self.tableView.register(UINib(nibName: "AddFriendTableViewCell", bundle: nil), forCellReuseIdentifier: "AddFriendTableViewCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController {
            if navigationController.viewControllers.last!.isEqual(self){
                ProductBuyRetailerTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor: NavigationBarBackgroundColor.whiteOpaque)
            }
        }
    }
    
    // MARK: -   -----------------------------------------------------     ACTIONS   -----------------------------------------------------
    
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    // MARK: -   -----------------------------------------------------      FUNCTIONS   -----------------------------------------------------
    
    func loadMoreUsers(){
        if let objectId = self.product?.objectId {
            DBHelper.callAPIGet("products/\(objectId)", queryString: DBHelper.getJSONFromDictionary(["flags":["isFollowedByUser":CurrentUser.sharedInstance.currentUser!.objectId!, "isRequestedByUser": CurrentUser.sharedInstance.currentUser!.objectId!], "populate":[["path":"likes", "sort": "-followersCount"]],"limit":20,"skip":self.usersArray.count])) { [weak vc = self](result, error) in
                if error == nil{
                    if let product = result?["product"] as? NSDictionary{
                        if let users = product["likes"] as? NSArray{
                            for user in users{
                                if let userDic = user as? Dictionary<String, AnyObject>{
                                    let user = User(input: userDic)
                                    if user.userName != "mcurator" && user.userName != "fcurator" && user.userName != "nacurator"{
                                        vc?.usersArray.append(user)
                                    }
                                }
                            }
                            vc?.tableView.reloadData()
                            
                        } else {
                            vc?.tableView.reloadData()
                        }
                        
                    } else {
                        vc?.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    // MARK: - TABLE VIEW DATA SOURCE
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.usersArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddFriendTableViewCell", for: indexPath) as! AddFriendTableViewCell
        let user = self.usersArray[indexPath.row]
        cell.user = user
        if let profileImageURL = user.profileImageUrl{
            cell.userImageView.kf.setImage(with: URL(string:profileImageURL))
        }
        if let fullName = user.fullName, let username = user.userName{
            cell.userFullNameLabel.text = fullName
            cell.userNameLabel.text! = "@" + username
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userProfileVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
        userProfileVC.currentUser = self.usersArray[indexPath.row]
        self.navigationController?.pushViewController(userProfileVC, animated: true)
    }
}
