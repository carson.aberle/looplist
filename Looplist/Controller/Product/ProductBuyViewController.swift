//
//  ProductBuyViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 11/6/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import Stripe
import CoreLocation

class ProductBuyViewController: UIViewController, PKPaymentAuthorizationViewControllerDelegate {
    
    // MARK: -   -----------------------------------------------------      VARIABLES   -----------------------------------------------------
    
    weak var product:Product?{
        didSet{
            if var attributes = self.product?.attributes{
                if attributes["colors"] != nil{
                    attributes.removeValue(forKey: "colors")
                }
                self.noColorAttributes = attributes
            }
            
            self.getListingsWithFilters({ (success) in
                self.collectionView.reloadData()
            })
            self.initizializeArrays(size: self.noColorAttributes.count)
        }
    }
    
    fileprivate var productVariationListings = [Listing?]()
    fileprivate var shipping:Shipping?
    fileprivate var listing:Listing?
    fileprivate var emptySetView: EmptySetView = EmptySetView.instanceFromNib()
    fileprivate var sectionExpander = SectionExpander()
    fileprivate var completedAttributes = [Bool]()
    fileprivate var noColorAttributes = Dictionary<String, AnyObject>()
    fileprivate var appliedFilters = [String]()
    fileprivate var selectedSectionHeader: ProductBuyCollectionSectionHeader?
    fileprivate var lastSectionHeader: ProductBuyCollectionSectionHeader?
    fileprivate var indicator = CustomActivityIndicator()
    
    // MARK: -   -----------------------------------------------------      OUTLETS   -----------------------------------------------------
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: -   -----------------------------------------------------     LIFE CYCLE   -----------------------------------------------------
    
    override func viewDidAppear(_ animated: Bool) {
        ProductBuyViewController.startTimer()
        if let navigationController = self.navigationController {
            ProductBuyViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let objectId = self.product?.objectId{
            ProductBuyViewController.stopTimer(["product":objectId], withActionType:"View Screen")
        } else {
            ProductBuyViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController {
            ProductBuyViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    // MARK: -   -----------------------------------------------------      ACTIONS   -----------------------------------------------------
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    func buyWithApplePay(_ sender: UIButton){
//    @IBAction func buyWithApplePay(_ sender: UIButton) {
        if let index:Int = sender.layer.value(forKey: "index") as? Int{
            if let listing = self.productVariationListings[index]{
                self.listing = listing
                if let _ = listing.payment{
                    loadPaymentForApplePay()
                }else{
                    APIHelperProduct.getPaymentData(listing){ [weak vc = self](success) in
                        if success{
                            vc?.loadPaymentForApplePay()
                        }
                    }
                }
            } else {
                ErrorHandling.customErrorHandler(message:"ERROR".localized)
            }
        }
    }
    
    @IBAction func buyWithCreditCard(_ sender:UIButton){
        let index:Int = sender.layer.value(forKey: "index") as! Int
        if let listing = self.productVariationListings[index]{
            let vc:OrderViewController = OrderViewController.instanceFromStoryboard("Purchase")
            vc.listing = listing
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            ErrorHandling.customErrorHandler(message:"ERROR".localized)
        }
    }
    
    // MARK: -   -----------------------------------------------------      FUNCTIONS   -----------------------------------------------------
    
    fileprivate func configureUI(){
        self.tabBarController?.tabBar.isHidden = true
        
        self.collectionView?.backgroundView = emptySetView
        self.collectionView?.backgroundView?.isHidden = true
        
        self.sectionExpander.expandedSections.insert(0)
    }
    
    func getListingsWithFilters(_ completion: @escaping (_ success: Bool) -> Void) {
        if let productId = self.product?.objectId{
            self.indicator.showActivityIndicator(uiView: self.view, offset: 0)
            self.productVariationListings.removeAll(keepingCapacity: false)
            let whereDictionary:NSMutableDictionary = NSMutableDictionary()
            var params:NSMutableDictionary = NSMutableDictionary()

            if self.appliedFilters.count > 0{
                let key = Array(self.noColorAttributes.keys)
                
                for i in 0 ..< self.appliedFilters.count{
                    if appliedFilters[i] != "" {
                        let keyName = String(key[i].characters.dropLast())
                        whereDictionary.setValue("\(appliedFilters[i])", forKey:("attributes.\(keyName)"))
                    }
                }
                
                whereDictionary.setValue(productId, forKey: "product")
                params.setValue(whereDictionary, forKey: "where")
            } else {
                params = ["where":["product":productId]]
            }
            
            APIHelperProduct.getProductListingsWithParameters(DBHelper.getJSONFromDictionary(params), self.product, { [weak vc = self](results, success) in
                vc?.indicator.hideActivityIndicator()
                if success {
                    if results.count > 0 {
                        vc?.collectionView?.backgroundView?.isHidden = true
                        vc?.productVariationListings = results
                    } else {
                        vc?.emptySetView.discriptionLabel.text = "CANT_FIND_LISTINGS_FILTERS".localized
                        vc?.collectionView?.backgroundView?.isHidden = false
                    }
                    
                    if vc?.lastSectionHeader != nil {
                        vc?.lastSectionHeader?.numberOfOffers = vc?.productVariationListings.count
                    }
                    
                    completion(true)
                } else {
                    ErrorHandling.customErrorHandler(message:"ERROR".localized)
                    completion(true)
                }
            })
        }
    }
    
    // SHOW APPLE PAY SCREEN
    fileprivate func loadPaymentForApplePay(){
        if let payment = self.listing?.payment{
            if let subtotal = payment.subtotal,let tax = payment.tax, let service = payment.service,let shipping = payment.shipping, let total = payment.total{
                let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: ConstantsIdentification.ApplePayMerchantID)
                paymentRequest.requiredShippingAddressFields = PKAddressField.all
                
                var summaryItems = [PKPaymentSummaryItem]()
                summaryItems.append(PKPaymentSummaryItem(label: "Sub Total", amount: NSDecimalNumber(string:"\(subtotal.substringFromIndex(1)))")))
                summaryItems.append(PKPaymentSummaryItem(label: "Tax", amount: NSDecimalNumber(string:"\(tax.substringFromIndex(1))")))
                summaryItems.append(PKPaymentSummaryItem(label: "Services", amount: NSDecimalNumber(string:"\(service.substringFromIndex(1))")))
                summaryItems.append(PKPaymentSummaryItem(label: "Shipping", amount: NSDecimalNumber(string:"\(shipping.substringFromIndex(1))")))
                summaryItems.append(PKPaymentSummaryItem(label: "Total", amount: NSDecimalNumber(string:"\(total.substringFromIndex(1))")))
                summaryItems.append(PKPaymentSummaryItem(label: "LOOPLIST", amount: NSDecimalNumber(string:"\(total.substringFromIndex(1))")))

                
                paymentRequest.paymentSummaryItems = summaryItems
                
                if (Stripe.canSubmitPaymentRequest(paymentRequest)) {
                    let paymentController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
                    paymentController.delegate = self
                    self.present(paymentController, animated: true, completion: nil)
                } else {
                    ErrorHandling.defaultApplePayErrorHandler()
                }
            }
        }
    }
    
    // handle the PKPayment that the payment authorization controller returns by implementing this protocol
    internal func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
        handlePaymentAuthorizationWithPayment(payment, completion: completion)
    }
    
    internal func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // create token
    fileprivate func handlePaymentAuthorizationWithPayment(_ payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> ()) {
        STPAPIClient.shared().createToken(with: payment) { (token, error) -> Void in
            if error != nil {
                completion(PKPaymentAuthorizationStatus.failure)
                return
            }
            
            self.assignAppleShippingInfoToCurrentShipping(payment, completion: { (success) in
                if success{
                    self.createBackendChargeWithToken(token!, completion: completion)
                }
            })
        }
    }
    
    fileprivate func assignAppleShippingInfoToCurrentShipping(_ payment: PKPayment,completion:@escaping (_ success:Bool)->Void){
        if let firstName = payment.shippingContact?.name?.givenName, let lastName =  payment.shippingContact?.name?.familyName, let address1 = payment.shippingContact?.postalAddress?.street, let city = payment.shippingContact?.postalAddress?.city, let state = payment.shippingContact?.postalAddress?.state,let zip = payment.shippingContact?.postalAddress?.postalCode,  let phone =  payment.shippingContact?.phoneNumber {
            
            // Check if shipping address if available in DB ..if available get the object..
            let shipping = Shipping(name: firstName + " " + lastName, address1: address1, address2: "", city: city, state: state, zip: zip, phone: phone.stringValue, instructions: " ")
            
            if let user = CurrentUser.sharedInstance.currentUser{
                user.findAndGetShippingAddress(shipping, completion: { (shipping) in
                    if let shipping = shipping{
                        self.shipping = shipping
                        completion(true)
                    }else{
                        completion(false)
                    }
                })
            }
        }
    }
    
    // MARK: -   -----------------------------------------------------  TRANSACTIONS   -----------------------------------------------------
    
    fileprivate func createBackendChargeWithToken(_ token: STPToken, completion: @escaping (PKPaymentAuthorizationStatus) -> ()) {
        if let listingId = self.listing?.objectId, let shipping = self.shipping?.objectId{
            let parameters = (["token": token.tokenId as AnyObject, "listing": listingId as AnyObject, "transaction":["shipping":["address": shipping],"customerNotes":""]] as AnyObject) as! [String : AnyObject?]
            
            APIHelperProduct.createBackendChargeWithToken(parameters, {[weak vc = self] (transactionId, success) in
                if success {
                    completion(PKPaymentAuthorizationStatus.success)
                    vc?.showOrderConfirmationPage(transactionId)
                    
                } else {
                    completion(PKPaymentAuthorizationStatus.failure)
                }
            })
        }else{
            completion(PKPaymentAuthorizationStatus.failure)
        }
    }
    
    fileprivate func showOrderConfirmationPage(_ transationId:String){
        let vc:ConfirmationViewController = ConfirmationViewController.instanceFromStoryboard("Purchase")
        vc.listing = self.listing
        vc.shipping = self.shipping
        vc.cc = nil
        vc.transactionId = transationId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc fileprivate func headerTapped(_ recognizer: HeaderTapRecognizer) {
        sectionExpander.toggleExpansionForSectionAtIndex(recognizer.section, inCollectionView: self.collectionView, withDuration: 0.5)
    }
    
    fileprivate func initizializeArrays(size: Int){
        for i in 0 ..< size {
            self.completedAttributes.insert(false, at: i)
            self.appliedFilters.insert("", at: i)
        }
    }
    
    fileprivate func checkIfCompleted(attributes: [Bool]) -> Bool{
        var temp = true
        for i in 0 ..< self.noColorAttributes.count{
            if attributes[i] == false {
                temp = false
            }
        }
        return temp
    }
}

// MARK: ----------------------------------------- COLLECTIONVIEW DELEGETE AND DATASOURCE ----------------------------------------------

extension ProductBuyViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        switch self.productVariationListings.count {
        case 0:
            return 0
        default:
            return self.noColorAttributes.count + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case self.noColorAttributes.count:
            return sectionExpander.sectionIsExpandedAtIndex(section) ? self.productVariationListings.count : 0
        default:
            let key = Array(self.noColorAttributes.keys)[section]
            if let count = self.noColorAttributes[key]?.count {
                return sectionExpander.sectionIsExpandedAtIndex(section) ? count + 1 : 0
            } else {
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ProductBuySectionHeaderCell", for: indexPath) as! ProductBuyCollectionSectionHeader
            
            switch indexPath.section {
            case self.noColorAttributes.count:
                cell.numberOfOffers = self.productVariationListings.count
                self.lastSectionHeader = cell
            default:
                cell.optionTitle = Array(noColorAttributes.keys)[indexPath.section]
                cell.subtitleText = appliedFilters[indexPath.section]
            }
            
            cell.addGestureRecognizer(HeaderTapRecognizer(section: indexPath.section, target: self, action: #selector(ProductBuyViewController.headerTapped(_:))))
            
            return  cell
        case UICollectionElementKindSectionFooter:
            let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ProductBuySectionFooterCell", for: indexPath) as! ProductBuyCollectionSectionFooter
            return cell
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case self.noColorAttributes.count:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductBuyCollectionViewCell", for: indexPath) as! ProductBuyCollectionViewCell
            if let listing = self.productVariationListings[indexPath.row]{
                cell.listing = listing
                cell.rowNumber = indexPath.row
                cell.btnApplePay.addTarget(self, action: #selector(ProductBuyViewController.buyWithApplePay(_:)), for: .touchUpInside)
            }
            return cell
        default:
            if indexPath.row != collectionView.numberOfItems(inSection: indexPath.section) - 1 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductBuyAttributeCollectionViewCell", for: indexPath) as! ProductBuyAttributeCollectionViewCell
                let values = Array(noColorAttributes.values)[indexPath.section] as! [AnyObject]
                let title = values[indexPath.row] as? String
                cell.title = title
                if appliedFilters[indexPath.section] == title{
                    cell.isSelected = true
                }
                
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductBuyAttributeCollectionViewCell", for: indexPath) as! ProductBuyAttributeCollectionViewCell
                return cell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case self.noColorAttributes.count:
            return CGSize(width: (UIScreen.main.bounds.width/2) - 1, height: (UIScreen.main.bounds.width/2) + 188)
        default:
            if indexPath.row != collectionView.numberOfItems(inSection: indexPath.section) - 1 {
                let fontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica", size: 18)]
                let values = Array(noColorAttributes.values)[indexPath.section] as! [AnyObject]
                let size = (values[indexPath.row] as! NSString).size(attributes: fontAttributes)
                return CGSize(width: size.width + 35, height: size.height + 16)
            } else {
                return CGSize(width: self.collectionView.frame.width - 60, height: 0)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch section {
        case self.noColorAttributes.count:
            return CGSize(width: self.view.frame.width, height: 100)
        default:
            if appliedFilters[section] == ""{
                return CGSize(width: self.view.frame.width, height: 70)
            } else {
                return CGSize(width: self.view.frame.width, height: 100)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        switch section {
        case self.noColorAttributes.count:
            return 1
        default:
            return 20
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        switch section {
        case self.noColorAttributes.count:
            return 1
        default:
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch section {
        case self.noColorAttributes.count:
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        default:
            return UIEdgeInsetsMake(0, 20, 0, 20)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        switch section {
        case self.noColorAttributes.count:
            return CGSize(width: self.view.frame.width, height: 0.00000001)
        default:
            return CGSize(width: self.view.frame.width, height: 1)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.section {
        case self.noColorAttributes.count:
            if let listing = self.productVariationListings[indexPath.row]{
                let productBuyRetailerVC:ProductBuyRetailerTableViewController = ProductBuyRetailerTableViewController.instanceFromStoryboard("Product")
                productBuyRetailerVC.listing = listing
                self.navigationController?.pushViewController(productBuyRetailerVC, animated: true)
            } else {
                ErrorHandling.customErrorHandler(message:"Error! :/")
            }
        default:
            let cell = collectionView.cellForItem(at: indexPath) as! ProductBuyAttributeCollectionViewCell
            let cellTitle = cell.attributeTitleLabel.text
            
            self.selectedSectionHeader = collectionView.supplementaryView(forElementKind: UICollectionElementKindSectionHeader, at: IndexPath(row: 0, section: indexPath.section)) as? ProductBuyCollectionSectionHeader
            
            if let title = cellTitle {
                self.appliedFilters[indexPath.section] = title
            }
            
            completedAttributes[indexPath.section] = true
            
            self.getListingsWithFilters({ (success) in
                if success {
                    if self.checkIfCompleted(attributes: self.completedAttributes) {
                        self.sectionExpander.toggleExpansionForSectionAtIndex(self.noColorAttributes.count, inCollectionView: self.collectionView, withDuration: 0.5)
                    } else {
                        for i in 0 ..< self.noColorAttributes.count + 1{
                            if self.completedAttributes[i] == false {
                                self.sectionExpander.toggleExpansionForSectionAtIndex(i, inCollectionView: self.collectionView, withDuration: 0.5)
                                break
                            }
                        }
                    }
                    if let title = cellTitle {
                        self.selectedSectionHeader?.subtitleText = title
                    }
                }
            })
        }
    }
}
