//
//  ProductPendingBuyTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 11/18/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import Stripe
import PassKit

class ProductPendingBuyTableViewController: UITableViewController, UITextViewDelegate, PKPaymentAuthorizationViewControllerDelegate {

    var product:Product?
    var listing:Listing?
    var shipping:Shipping?
    
    override func viewDidAppear(_ animated: Bool) {
        ProductPendingBuyTableViewController.startTimer()
        if let navigationController = self.navigationController {
            if navigationController.viewControllers.last!.isEqual(self){
                ProductBuyRetailerTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let listingObjectId = self.listing?.objectId, let productObjectId = self.product?.objectId{
            ProductPendingBuyTableViewController.stopTimer(["listing":listingObjectId, "product":productObjectId], withActionType:"View Screen")
        } else {
            ProductPendingBuyTableViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "PENDING_PRODUCT".localized

        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UIScreen.main.bounds.size.height - 64
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
//        setUpApplePay()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController {
            if navigationController.viewControllers.last!.isEqual(self){
                ProductBuyRetailerTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    func showReturnPolicy(){
        let returnPolicyVC:ReturnPolicyTableViewController = ReturnPolicyTableViewController.instanceFromStoryboard("Product")
        self.navigationController?.pushViewController(returnPolicyVC, animated: true)
    }
    
    //More accurately get keyboard height
    func keyboardWillShow(notification:NSNotification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        UIView.animate(withDuration: 0.3) {
            self.tableView.frame = CGRect(x: 0, y: 64, width: self.tableView.frame.size.width, height: (UIScreen.main.bounds.size.height - keyboardHeight))
        }
    }
    
    //More accurately get keyboard height
    func keyboardWillHide(notification:NSNotification) {
        UIView.animate(withDuration: 0.3) {
            self.tableView.frame = CGRect(x: 0, y: 64, width: self.tableView.frame.size.width, height: (UIScreen.main.bounds.size.height - 64))
        }
    }

    
    func buyWithApplePay(_ sender:UIButton){
        if let listing = self.listing{
        if let _ = listing.payment{
            self.loadPaymentForApplePay()
        }else{
            APIHelperProduct.getPaymentData(listing){ [weak vc = self](success) in
                if success{
                    vc?.loadPaymentForApplePay()
                }
            }
        }
        }
    }
    
    func buyWithApplePaySetUp(_ sender:UIButton){
        PKPassLibrary().openPaymentSetup()
    }
    
    func buyWithCreditCard(_ sender:UIButton){
        let vc:OrderViewController = OrderViewController.instanceFromStoryboard("Purchase")
        vc.listing = self.listing
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
   
    
//    func setUpApplePay(){
//        
//        var btnApplePay: PKPaymentButton
//        if PKPaymentAuthorizationViewController.canMakePayments() {
//            btnApplePay = PKPaymentButton(type: .buy, style: .black)
//        } else {
//            btnApplePay = PKPaymentButton(type: .setUp, style: .black)
//        }
//        
//        btnApplePay.addTarget(self, action: #selector(ProductPendingBuyTableViewController.buyWithApplePay(_:)), for: .touchUpInside)
//        btnApplePay.frame = CGRect(x:40,y: view.frame.size.height - 60, width:view.frame.size.width - 80, height: 40)
////        if let btnApplePay = btnApplePay{
//            view.addSubview(btnApplePay)
////        }
//    }
    


    
    // SHOW APPLE PAY SCREEN
    func loadPaymentForApplePay(){
        if let payment = self.listing?.payment{
            if let subtotal = payment.subtotal,let tax = payment.tax, let service = payment.service,let shipping = payment.shipping, let total = payment.total{
                
                let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: ConstantsIdentification.ApplePayMerchantID)
                
                var summaryItems = [PKPaymentSummaryItem]()
                
                paymentRequest.requiredShippingAddressFields = PKAddressField.all
                
                summaryItems.append(PKPaymentSummaryItem(label: "Sub Total", amount: NSDecimalNumber(string:"\(subtotal.substringFromIndex(1)))")))
                summaryItems.append(PKPaymentSummaryItem(label: "Tax", amount: NSDecimalNumber(string:"\(tax.substringFromIndex(1))")))
                summaryItems.append(PKPaymentSummaryItem(label: "Services", amount: NSDecimalNumber(string:"\(service.substringFromIndex(1))")))
                summaryItems.append(PKPaymentSummaryItem(label: "Shipping", amount: NSDecimalNumber(string:"\(shipping.substringFromIndex(1))")))
                summaryItems.append(PKPaymentSummaryItem(label: "Total", amount: NSDecimalNumber(string:"\(total.substringFromIndex(1))")))
                summaryItems.append(PKPaymentSummaryItem(label: "LOOPLIST", amount: NSDecimalNumber(string:"\(total.substringFromIndex(1))")))
                
                paymentRequest.paymentSummaryItems = summaryItems
                
                if (Stripe.canSubmitPaymentRequest(paymentRequest)) {
                    let paymentController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
                    paymentController.delegate = self
                    self.present(paymentController, animated: true, completion: nil)
                } else {
                    // Show the user your own credit card form (see options 2 or 3)
                    ErrorHandling.defaultApplePayErrorHandler()
                }
            }
        }
    }
    
    // handle the PKPayment that the payment authorization controller returns by implementing this protocol
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
        /*
         We'll implement this method below in 'Creating a single-use token'.
         Note that we've also been given a block that takes a
         PKPaymentAuthorizationStatus. We'll call this function with either
         PKPaymentAuthorizationStatusSuccess or PKPaymentAuthorizationStatusFailure
         after all of our asynchronous code is finished executing. This is how the
         PKPaymentAuthorizationViewController knows when and how to update its UI.
         */
        
        
        handlePaymentAuthorizationWithPayment(payment, completion: completion)
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // create token
    func handlePaymentAuthorizationWithPayment(_ payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> ()) {
        STPAPIClient.shared().createToken(with: payment) { (token, error) -> Void in
            if error != nil {
                completion(PKPaymentAuthorizationStatus.failure)
                return
            }
            /*
             We'll implement this below in "Sending the token to your server".
             Notice that we're passing the completion block through.
             See the above comment in didAuthorizePayment to learn why.
             */
            self.assignAppleShippingInfoToCurrentShipping(payment, completion: { (success) in
                if success{
                    self.createBackendChargeWithToken(token!, completion: completion)
                }
            })
        }
    }
    
    func assignAppleShippingInfoToCurrentShipping(_ payment: PKPayment,completion:@escaping (_ success:Bool)->Void){
        
        if let firstName = payment.shippingContact?.name?.givenName, let lastName =  payment.shippingContact?.name?.familyName, let address1 = payment.shippingContact?.postalAddress?.street, let city = payment.shippingContact?.postalAddress?.city, let state = payment.shippingContact?.postalAddress?.state,let zip = payment.shippingContact?.postalAddress?.postalCode,  let phone =  payment.shippingContact?.phoneNumber {
            
            // Check if shipping address if available in DB ..if available get the object..
            let shipping = Shipping(name: firstName + " " + lastName, address1: address1, address2: "", city: city, state: state, zip: zip, phone: phone.stringValue, instructions: " ")
            
            if let user = CurrentUser.sharedInstance.currentUser{
                user.findAndGetShippingAddress(shipping, completion: { (shipping) in
                    if let shipping = shipping{
                        self.shipping = shipping
                        completion(true)
                    }else{
                        completion(false)
                    }
                })
                
            }
            
        }
    }
    
    // MARK: -  /////////////////////////////////////////////////////    TRANSACTION    /////////////////////////////////////////////////////////////////////////
    
    //## backend charge
    func createBackendChargeWithToken(_ token: STPToken, completion: @escaping (PKPaymentAuthorizationStatus) -> ()) {
        
        if let listing = self.listing?.objectId, let shipping = self.shipping?.objectId, let listingNotes = self.listing?.optionalNotes{
            
            let path =  "stripe/charge"
            
            DBHelper.callAPIPost(path, params:["token": token.tokenId as Optional<AnyObject>, "listing": listing as Optional<AnyObject>, "transaction":["shipping":["address": shipping] as AnyObject, "customerNotes":listingNotes] as Optional<AnyObject>], completion: { [weak vc = self](result, error) in
                
                if let result = result{
                    
                    completion(PKPaymentAuthorizationStatus.success)
                    if let transactionDictionary = result["transaction"] as? NSDictionary{
                        if let transactionId = transactionDictionary["_id"] as? String{
                            completion(PKPaymentAuthorizationStatus.success)
                            vc?.showOrderConfirmationPage(transactionId)
                        }
                    }
                }else {
                    
                    completion(PKPaymentAuthorizationStatus.failure)
                }
            })
            
        }else{
            completion(PKPaymentAuthorizationStatus.failure)
        }
    }
    
    func showOrderConfirmationPage(_ transationId:String){
        
        let vc:ConfirmationViewController = ConfirmationViewController.instanceFromStoryboard("Purchase")
        vc.listing = self.listing
        vc.shipping = self.shipping
        vc.cc = nil
        vc.transactionId = transationId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductPendingTableViewCell", for: indexPath) as! ProductPendingTableViewCell
        cell.pendingBuyVC = self
        if let product = self.product, let listing = self.listing{
            cell.productTitle.text = product.brandName
            if (self.product?.images?.count)! > 0{
                if let productUrl = self.product?.images?[0]{
                    cell.productImage.kf.setImage(with:URL(string:productUrl))
                }
            }
            if let price = listing.price{
                cell.productPrice.text = price
            }
        }
        cell.buyinfoTextView.layer.setValue(indexPath.row, forKey: "indexPath")
        cell.buyinfoTextView.delegate = self
        return cell
    }
    
    // MARK: -  //////////////////////////////////////////////////////   TEXT FIELD DELEGATE   /////////////////////////////////////////////////////////////////////////
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.returnKeyType = .default
        let row = textView.layer.value(forKey: "indexPath") as! Int
        self.tableView.scrollToRow(at: IndexPath(row: row, section: 0), at: .bottom, animated: true)
//        self.tableView.setContentOffset(CGPoint(x: 0, y: textView.frame.origin.y), animated: true)
        if (textView.text! == "PURCHASE_NOTES_TITLE".localized) {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text! == "") {
            textView.text = "PURCHASE_NOTES_TITLE".localized
            textView.textColor = UIColor(red:0.71, green:0.71, blue:0.71, alpha:1.0)
        }
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        textView.resignFirstResponder()
    }

    func textViewDidChange(_ textView: UITextView) {
        if (textView.text! != "PURCHASE_NOTES_TITLE".localized) {
            self.listing?.optionalNotes = textView.text
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
        }
        return true
    }
}
