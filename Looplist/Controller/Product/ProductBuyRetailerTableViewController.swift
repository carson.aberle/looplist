//
//  ProductBuyRetailerTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 11/6/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import HTMLAttributedString
import Stripe

class ProductBuyRetailerTableViewController: UITableViewController, PKPaymentAuthorizationViewControllerDelegate {

    var listing:Listing?
    var listingCell:ProductBuyRetailerHeaderTableViewCell!
    var shipping:Shipping?
    
    
    override func viewDidAppear(_ animated: Bool) {
        ProductBuyRetailerTableViewController.startTimer()
        if let navigationController = self.navigationController {
            if navigationController.viewControllers.last!.isEqual(self){
                ProductBuyRetailerTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let objectId = self.listing?.objectId{
            ProductBuyRetailerTableViewController.stopTimer(["listing":objectId], withActionType:"View Screen")
        } else {
            ProductBuyRetailerTableViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = self.listing?.product?.brandName
        
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tabBarController?.tabBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController {
            if navigationController.viewControllers.last!.isEqual(self){
                ProductBuyRetailerTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
            }
        }
    }
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    func buyWithApplePay(_ sender:UIButton){
        if let listing = listing{
            if self.listing?.product?.status == "pending"{
                let pendingBuyVC:ProductPendingBuyTableViewController = ProductPendingBuyTableViewController.instanceFromStoryboard("Product")
                pendingBuyVC.product = self.listing?.product
                pendingBuyVC.listing = self.listing
                self.navigationController?.pushViewController(pendingBuyVC, animated: true)
            } else {
                
                if let _ = listing.payment{
                    loadPaymentForApplePay()
                }else{
                    APIHelperProduct.getPaymentData(listing){ [weak vc = self](success) in
                        if success{
                            vc?.loadPaymentForApplePay()
                        }
                    }
                }
                
            }
        }
    }
    
    func buyWithCreditCard(_ sender:UIButton){
        if self.listing?.product?.status == "pending"{
            let pendingBuyVC:ProductPendingBuyTableViewController = ProductPendingBuyTableViewController.instanceFromStoryboard("Product")
            pendingBuyVC.product = self.listing?.product
            pendingBuyVC.listing = self.listing
            self.navigationController?.pushViewController(pendingBuyVC, animated: true)
        } else {
            let vc:OrderViewController = OrderViewController.instanceFromStoryboard("Purchase")
            vc.listing = self.listing
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // SHOW APPLE PAY SCREEN
    func loadPaymentForApplePay(){
        
        if let payment = self.listing?.payment{
            if let subtotal = payment.subtotal,let tax = payment.tax, let service = payment.service,let shipping = payment.shipping, let total = payment.total{
                
                let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: ConstantsIdentification.ApplePayMerchantID)
                
                var summaryItems = [PKPaymentSummaryItem]()
                
                paymentRequest.requiredShippingAddressFields = PKAddressField.all
                
                summaryItems.append(PKPaymentSummaryItem(label: "Sub Total", amount: NSDecimalNumber(string:"\(subtotal.substringFromIndex(1)))")))
                summaryItems.append(PKPaymentSummaryItem(label: "Tax", amount: NSDecimalNumber(string:"\(tax.substringFromIndex(1))")))
                summaryItems.append(PKPaymentSummaryItem(label: "Services", amount: NSDecimalNumber(string:"\(service.substringFromIndex(1))")))
                summaryItems.append(PKPaymentSummaryItem(label: "Shipping", amount: NSDecimalNumber(string:"\(shipping.substringFromIndex(1))")))
                summaryItems.append(PKPaymentSummaryItem(label: "Total", amount: NSDecimalNumber(string:"\(total.substringFromIndex(1))")))
                summaryItems.append(PKPaymentSummaryItem(label: "LOOPLIST", amount: NSDecimalNumber(string:"\(total.substringFromIndex(1))")))

                
                paymentRequest.paymentSummaryItems = summaryItems
                
                if (Stripe.canSubmitPaymentRequest(paymentRequest)) {
                    let paymentController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
                    paymentController.delegate = self
                    self.present(paymentController, animated: true, completion: nil)
                } else {
                    // Show the user your own credit card form (see options 2 or 3)
                    ErrorHandling.defaultApplePayErrorHandler()
                }
            }
        }
    }
    
    // handle the PKPayment that the payment authorization controller returns by implementing this protocol
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
        /*
         We'll implement this method below in 'Creating a single-use token'.
         Note that we've also been given a block that takes a
         PKPaymentAuthorizationStatus. We'll call this function with either
         PKPaymentAuthorizationStatusSuccess or PKPaymentAuthorizationStatusFailure
         after all of our asynchronous code is finished executing. This is how the
         PKPaymentAuthorizationViewController knows when and how to update its UI.
         */
        
        
        handlePaymentAuthorizationWithPayment(payment, completion: completion)
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // create token
    func handlePaymentAuthorizationWithPayment(_ payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> ()) {
        STPAPIClient.shared().createToken(with: payment) { (token, error) -> Void in
            if error != nil {
                completion(PKPaymentAuthorizationStatus.failure)
                return
            }
            /*
             We'll implement this below in "Sending the token to your server".
             Notice that we're passing the completion block through.
             See the above comment in didAuthorizePayment to learn why.
             */
            self.assignAppleShippingInfoToCurrentShipping(payment, completion: { (success) in
                if success{
                    self.createBackendChargeWithToken(token!, completion: completion)
                }
            })
            
        }
    }
    
    func assignAppleShippingInfoToCurrentShipping(_ payment: PKPayment,completion:@escaping (_ success:Bool)->Void){
        
        if let firstName = payment.shippingContact?.name?.givenName, let lastName =  payment.shippingContact?.name?.familyName, let address1 = payment.shippingContact?.postalAddress?.street, let city = payment.shippingContact?.postalAddress?.city, let state = payment.shippingContact?.postalAddress?.state,let zip = payment.shippingContact?.postalAddress?.postalCode,  let phone =  payment.shippingContact?.phoneNumber {
            
            // Check if shipping address if available in DB ..if available get the object..
            let shipping = Shipping(name: firstName + " " + lastName, address1: address1, address2: "", city: city, state: state, zip: zip, phone: phone.stringValue, instructions: " ")
            
            if let user = CurrentUser.sharedInstance.currentUser{
                user.findAndGetShippingAddress(shipping, completion: { (shipping) in
                    if let shipping = shipping{
                        self.shipping = shipping
                        completion(true)
                    }else{
                        completion(false)
                    }
                })
                
            }
            
        }
    }
    
    // MARK: -  /////////////////////////////////////////////////////    TRANSACTION    /////////////////////////////////////////////////////////////////////////
    
    //## backend charge
    func createBackendChargeWithToken(_ token: STPToken, completion: @escaping (PKPaymentAuthorizationStatus) -> ()) {
        
        if let listing = self.listing?.objectId, let shipping = self.shipping?.objectId, let listingNotes = self.listing?.optionalNotes{
            
            let path =  "stripe/charge"
            
            DBHelper.callAPIPost(path, params:["token": token.tokenId as Optional<AnyObject>, "listing": listing as Optional<AnyObject>, "transaction":["shipping":["address": shipping],"customerNotes":listingNotes] as AnyObject], completion: {[weak vc = self] (result, error) in
                
                if let result = result{
                    
                    completion(PKPaymentAuthorizationStatus.success)
                    if let transactionDictionary = result["transaction"] as? NSDictionary{
                        if let transactionId = transactionDictionary["_id"] as? String{
                            vc?.showOrderConfirmationPage(transactionId)
                        }
                    }
                }else {
                
                    completion(PKPaymentAuthorizationStatus.failure)
                }
            })
            
        }else{
            completion(PKPaymentAuthorizationStatus.failure)
        }
    }
    
    func showOrderConfirmationPage(_ transationId:String){
        
        let vc:ConfirmationViewController = ConfirmationViewController.instanceFromStoryboard("Purchase")
        vc.listing = self.listing
        vc.shipping = self.shipping
        vc.cc = nil
        vc.transactionId = transationId
        self.navigationController?.pushViewController(vc, animated: true)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1 
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductBuyRetailerHeaderTableViewCell", for: indexPath) as! ProductBuyRetailerHeaderTableViewCell
            self.listingCell = cell
            cell.productBuyRetailerVC = self
            
            cell.listingNameLabel.text = self.listing?.product?.brandName
            if let price = self.listing?.price{
                cell.priceLabel.text = price
            }
            if let count = self.listing?.images?.count{
            cell.imagesPageControl.numberOfPages = count
            }else{
                 cell.imagesPageControl.numberOfPages = 1
            }
            
            if let description = listing?.description{
                let font: UIFont = UIFont(name: ConstantsFont.fontDefault, size: 12)!
            let attributedString = HTMLAttributedString.init(html: description, andBodyFont: font)
                cell.listingDescriptionLabel.attributedText = attributedString?.attributedString
                cell.listingDescriptionLabel.textAlignment = .justified
            }
            
            if self.listing?.product?.keywords != ""{
//                var valueArray = [String]()
                if let attributes = self.listing?.attributes{
//                for value in attributes{
//                    
//                    valueArray.append((value.value as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
//                }
//                }
//                
//                cell.productAttributesText.text = valueArray.joined(separator: ", ")
                    cell.productAttributesText.text = attributes
                }
            }
       
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductBuyRetailerReturnPolicyLabelTableViewCell", for: indexPath) as! ProductBuyRetailerReturnPolicyLabelTableViewCell
            return cell
        }

    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ( indexPath.row == 1){
            let returnPolicyVC:ReturnPolicyTableViewController = ReturnPolicyTableViewController.instanceFromStoryboard("Product")
            self.navigationController?.pushViewController(returnPolicyVC, animated: true)
        }
    }

}

extension ProductBuyRetailerTableViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: -  //////////////////////////////////////////////////////   COLLECTION VIEW DATA SOURCE   /////////////////////////////////////////////////////////////////////////
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = self.listing?.images?.count{
            return count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductHeaderRetailerVariationImageCollectionViewCell", for: indexPath) as! ProductHeaderRetailerVariationImageCollectionViewCell
        
        if((self.listing?.images?.count)! > indexPath.row){
            if let imageURL = URL(string:(self.listing?.images?[indexPath.row])!){
                cell.productHeaderVariationImage.kf.setImage(with:imageURL)
            }
        }
        
        //Load next image as well for instant load
        if((self.listing?.images?.count)! > indexPath.row){
            if let imageURL = URL(string:(self.listing?.images?[indexPath.row])!){
                let imageView = UIImageView()
                imageView.kf.setImage(with:imageURL)
            }
        }
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    // MARK: -  //////////////////////////////////////////////////////   COLLECTION VIEW DELEGATE   /////////////////////////////////////////////////////////////////////////
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.listingCell != nil{
            let pageWidth = scrollView.frame.size.width
            let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
            self.listingCell.imagesPageControl?.currentPage = page
        }
    }
}

