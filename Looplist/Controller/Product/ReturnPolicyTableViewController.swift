//
//  ReturnPolicyTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 11/11/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class ReturnPolicyTableViewController: UITableViewController {
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE  /////////////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        ReturnPolicyTableViewController.startTimer()
        if let navigationController = self.navigationController {
            if navigationController.viewControllers.last!.isEqual(self){
                ProductBuyRetailerTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor: NavigationBarBackgroundColor.whiteOpaque)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        ReturnPolicyTableViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "RETURN_POLICY_TITLE".localized
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100
        self.tableView.tableFooterView = UIView()
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController {
            if navigationController.viewControllers.last!.isEqual(self){
                ProductBuyRetailerTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor: NavigationBarBackgroundColor.whiteOpaque)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: -  //////////////////////////////////////////////////////   NAVIGATION   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: false)
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReturnPolicyTableViewCell", for: indexPath) as! ReturnPolicyTableViewCell
        cell.returnPolicyText.text = "RETURN_POLICY".localized
        return cell
    }
}
