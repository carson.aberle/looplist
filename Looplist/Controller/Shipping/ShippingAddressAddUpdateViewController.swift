//
//  ShippingAddressAddUpdateViewController.swift
//  Looplist
//
//  Created by Arun Sivakumar on 5/24/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import UIKit

class ShippingAddressAddUpdateViewController: UITableViewController,UITextFieldDelegate{
    
      // MARK: -  -----------------------------------------------------   INCOMING DATA   -----------------------------------------------------
    
    var shipping:Shipping?
    var isUpdateAddress = false
    
      // MARK: -  -----------------------------------------------------   CONSTANTS   -----------------------------------------------------
    
    fileprivate let limitPhoneLength = 12
    fileprivate let limitStateLength = 2
    fileprivate let limitZipLength = 5
    
    // MARK: -  -----------------------------------------------------   VARIABLES  -----------------------------------------------------
    
    fileprivate var textFieldsValueChanged = false
    fileprivate var defaultSwitchState = false
    
    
      // MARK: -  -----------------------------------------------------   OUTLETS   -----------------------------------------------------
    
    @IBOutlet weak var deleteButtonCell: UITableViewCell!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    @IBOutlet weak var fullNameText: UITextField!
    @IBOutlet weak var address1Text: UITextField!
    @IBOutlet weak var address2Text: UITextField!
    @IBOutlet weak var cityText: UITextField!
    @IBOutlet weak var stateText: UITextField!
    @IBOutlet weak var zipText: UITextField!
    @IBOutlet weak var phoneText: UITextField!
    @IBOutlet weak var instructionsText: UITextField!
    
    @IBOutlet weak var defaultAddressSwitch: UISwitch!
    
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var aptBuildingLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var zipLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var instructionsLabel: UILabel!
    @IBOutlet weak var defaultLabel: UILabel!
    @IBOutlet weak var deleteAddressButton: UIButton!
    
    // MARK: -  ////////////////////////////////////////////////    APPLICATION LIFE CYCLE    ///////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        ShippingAddressAddUpdateViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        ShippingAddressAddUpdateViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fullNameLabel.text = "FULL_NAME".localized
        self.addressLabel.text = "ADDRESS".localized
        self.aptBuildingLabel.text = "APT_BUILDING".localized
        self.cityLabel.text = "CITY".localized
        self.stateLabel.text = "STATE".localized
        self.zipLabel.text = "ZIP".localized
        self.phoneLabel.text = "PHONE".localized
        self.instructionsLabel.text = "INSTRUCTIONS".localized
        self.defaultLabel.text = "DEFAULT".localized
        
        self.fullNameText.placeholder = "FULL_NAME_PLACEHOLDER".localized
        self.address1Text.placeholder = "ADDRESS_PLACEHOLDER".localized
        self.address2Text.placeholder = "OPTIONAL_PLACEHOLDER".localized
        self.cityText.placeholder = "CITY_PLACEHOLDER".localized
        self.stateText.placeholder = "STATE_PLACEHOLDER".localized
        self.zipText.placeholder = "ZIP_PLACEHOLDER".localized
        self.phoneText.placeholder = "PHONE_PLACEHOLDER".localized
        self.instructionsText.placeholder = "OPTIONAL_PLACEHOLDER".localized
        self.deleteAddressButton.setTitle("DELETE_ADDRESS".localized, for: .normal)
        self.navigationItem.title = "DELIVERY_ADDRESS".localized
        self.addButton.title = "SAVE".localized
        // SETUP VC UI
        
        self.tableView.tableFooterView = UIView()
        self.addKeyboardHideTap()
        
        // TARGET ACTION
        fullNameText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        address1Text.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        address2Text.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        cityText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        stateText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        zipText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        phoneText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        instructionsText.addTarget(self, action: #selector(EditProfileViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    
        
        // DELEGATE TO CHECK IF CHANGED
        //        fullNameText.delegate = self
        //        address1Text.delegate = self
        //        address2Text.delegate = self
        //        cityText.delegate = self
        
        // DELEGATE TO CHECK IF CHANGED & DELEGATE FOR CHARACTER COUNT
        stateText.delegate = self
        zipText.delegate = self
        phoneText.delegate = self
        
        // LOAD DATA
        loadShippingAddressUI()
    }
    
    override func viewWillLayoutSubviews() {
        if let navigationController = self.navigationController{
            ShippingAddressAddUpdateViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    // MARK: -  ///////////////////////////////////////////////////////    LOAD UI    ///////////////////////////////////////////////////////////////////////////
    
    func loadShippingAddressUI(){
        
//        if !isUpdateAddress{ //### hiding this button
            deleteButtonCell.isHidden = true
//        }
        
        addButton.isEnabled = false
        
        if let name = shipping?.name, let address1 =  shipping?.address1, let address2 = shipping?.address2, let city = shipping?.city, let state = shipping?.state, let zip = shipping?.zip, let phone = shipping?.phone{
            
            fullNameText.text = name
            address1Text.text = address1
            address2Text.text = address2
            cityText.text = city
            stateText.text = state
            zipText.text = zip
            phoneText.text = phone
            
            if let instructions = shipping?.instructions{
                instructionsText.text = instructions
            }
            
            if let defaultShippingAddressObjectId = CurrentUser.sharedInstance.currentUser?.defaultShippingAddressObjectId, let objectId = self.shipping?.objectId, defaultShippingAddressObjectId == objectId{
                defaultAddressSwitch.setOn(true, animated: false)
                self.defaultSwitchState = true
            }else{
                defaultAddressSwitch.setOn(false, animated: false)
                self.defaultSwitchState = false
            }
        }
    }
    
    // MARK: -  ///////////////////////////////////////////////////////    ACTIONS    ///////////////////////////////////////////////////////////////////////////
    
    @IBAction func cancelAddShippingAddress(_ sender: AnyObject) {
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func saveUserData(_ sender: UIBarButtonItem) {
                addButton.isEnabled = false
        self.view.endEditing(true)
        
        if fullNameText.text == "" || address1Text.text == "" || cityText.text == "" || stateText.text == "" || zipText.text == "" || phoneText.text == ""{
            
            ErrorHandling.customErrorHandlerWithTitle(title:"ERROR".localized, message: "ALL_FIELDS_MANDATORY".localized)
        
            addButton.isEnabled = true
        }else if stateText.text?.characters.count != 2 {
            ErrorHandling.customErrorHandlerWithTitle(title:"ERROR".localized, message: "ERROR_INVALID_STATE".localized)
            addButton.isEnabled = true
        }else if zipText.text?.characters.count != 5 {
            ErrorHandling.customErrorHandlerWithTitle(title:"ERROR".localized, message: "ERROR_INVALID_ZIP".localized)
            addButton.isEnabled = true

        }else if phoneText.text?.characters.count != 12{
            ErrorHandling.customErrorHandlerWithTitle(title:"ERROR".localized, message: "ERROR_INVALID_PHONE".localized)
            addButton.isEnabled = true

        }else {
            if isUpdateAddress {
                saveAddress()
        

            } else {
                addAddress()
                

            }
        }
    }
    @IBAction func valueChanged(_ sender: UISwitch) {
        if sender.isOn == defaultSwitchState && !textFieldsValueChanged{
            addButton.isEnabled = false
        }else{
            addButton.isEnabled = true
        }
    }
    
    @IBAction func deleteAddress(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: nil, message: "CONFIRM_DELETE_ADDRESS".localized, preferredStyle: .alert)
        
        let deleteAddress = UIAlertAction(title: "DELETE".localized, style: .default) { (action) in
            if let shipping = self.shipping{
                if let objectId = shipping.objectId{
                    CurrentUser.sharedInstance.currentUser?.deleteShippingAddress(objectId, completion: { (success) in
                        if success{
                            _ = self.navigationController?.popViewController(animated: true)
                        }else{
                            ErrorHandling.unexpectedErrorHandler()
                        }
                    })
                }
            }
        }
        let cancelAction = UIAlertAction(title: "CANCEL".localized, style: .destructive, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.addAction(deleteAddress)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    // MARK: -  ///////////////////////////////////////////////////////    FUNCTIONS    ///////////////////////////////////////////////////////////////////////////
    
    //ADD NEW ADDRESS
    func addAddress(){
        if let name = fullNameText.text, let address1 = address1Text.text,let address2 = address2Text.text,let city = cityText.text,let state = stateText.text, let zip = zipText.text, let phone = phoneText.text, let instructions = instructionsText.text{
            
            let userEnteredShippingAddress = Shipping(name: name, address1: address1, address2: address2, city: city, state: state, zip: zip, phone: phone, instructions:instructions)
            
            if let user = CurrentUser.sharedInstance.currentUser{
                user.postShipping(defaultAddressSwitch.isOn, shippingAddress: userEnteredShippingAddress, completion: { (shipping) in
                    if shipping != nil{
                        self.goBack()
//                        self.navigationController!.popViewControllerAnimated(true)
                    }else{
                        ErrorHandling.unexpectedErrorHandler()
                        self.addButton.isEnabled = true

                    }
                })
            }
        }
    }
    
    func goBack(){
        performSegue(withIdentifier: "UnwindToShipping", sender: self)
    }
    
    //UPDATE EXISTING ADDRESS
    func saveAddress(){
        
        if let name = fullNameText.text, let address1 = address1Text.text,let address2 = address2Text.text,let city = cityText.text,let state = stateText.text, let zip = zipText.text, let phone = phoneText.text, let instructions = instructionsText.text{
            if let shipping = self.shipping{
                shipping.updateShippingAddress(name, address1: address1, address2: address2, city: city, state: state, zip: zip, phone: phone, instructions:instructions)
                if let user =  CurrentUser.sharedInstance.currentUser{
                    if let objectId = shipping.objectId{
                        
                        //  CASE 1: SWITCH AND TEXT FIELDS CHANGED
                        //  CASE 2: SWITCH CHANGED
                        //  CASE 3: TEXT FIELDS CHANGED
                        
                        if textFieldsValueChanged && (defaultAddressSwitch.isOn != defaultSwitchState){
                            user.updateShippingAddress(objectId, shipping: shipping, completion: { (success) in
                                if success{
                                    user.setUserDefaultShippingAddress(objectId, completion: { (success) in
                                        if success{
                                             self.goBack()
//                                            self.navigationController!.popViewControllerAnimated(true)
                                        }else{
                                            ErrorHandling.unexpectedErrorHandler()
                                            self.addButton.isEnabled = true

                                        }
                                    })
                                    
                                }else{
                                    //ErrorHandling.unexpectedErrorHandler()
                                    self.addButton.isEnabled = true

                                }
                            })
                        }else if defaultAddressSwitch.isOn != defaultSwitchState{
                            
                            user.toggleUserDefaultShippingAddress(objectId, completion: { (success) in
                                if success{
                                     self.goBack()
//                                    self.navigationController!.popViewControllerAnimated(true)
                                }else{
                                    //ErrorHandling.unexpectedErrorHandler()
                                    self.addButton.isEnabled = true

                                }
                                
                            })
                            
                        }else if textFieldsValueChanged{
                            user.updateShippingAddress(objectId, shipping: shipping, completion: { (success) in
                                if success{
                                     self.goBack()
//                                    self.navigationController!.popViewControllerAnimated(true)
                                }else{
                                    //ErrorHandling.unexpectedErrorHandler()
                                    self.addButton.isEnabled = true

                                }
                            })
                        }
                    }
                }
            }
        }
    }
}

// MARK: -  ////////////////////////////////////////////    TABLE VIEW DELEGATE   //////////////////////////////////////////////////////////////

extension  ShippingAddressAddUpdateViewController{
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
//        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath == IndexPath(row: 5, section: 0) {
//            let alertController = UIAlertController(title: nil, message: "CONFIRM_DELETE_ADDRESS".localized, preferredStyle: .alert)
//            
//            let deleteAddress = UIAlertAction(title: "DELETE".localized, style: .default) { (action) in
//                if let shipping = self.shipping{
//                    if let objectId = shipping.objectId{
//                        CurrentUser.sharedInstance.currentUser?.deleteShippingAddress(objectId, completion: { (success) in
//                            if success{
//                                _ = self.navigationController?.popViewController(animated: true)
//                            }else{
//                                ErrorHandling.unexpectedErrorHandler()
//                            }
//                        })
//                    }
//                }
//            }
//            let cancelAction = UIAlertAction(title: "CANCEL".localized, style: .destructive, handler: nil)
//            
//            alertController.addAction(cancelAction)
//            alertController.addAction(deleteAddress)
//            
//            self.present(alertController, animated: true, completion: nil)
        }
    }
}

    // MARK: -  /////////////////////////////////////////////////////////    EXTENSION   ////////////////////////////////////////////////////////////////////////

extension ShippingAddressAddUpdateViewController{
    
    // MARK: -  /////////////////////////////////////////////////    TEXT FIELD EXTENSION   /////////////////////////////////////////////////////////////////////
    
    func textFieldDidChange(_ textField: UITextField) {
        if  shipping?.name == fullNameText.text! &&
            shipping?.address1 == address1Text.text! &&
            shipping?.address2 == address2Text.text! &&
            shipping?.city == cityText.text! &&
            shipping?.state == stateText.text! &&
            shipping?.zip == zipText.text &&
            shipping?.phone ==  phoneText.text &&
            shipping?.instructions == instructionsText.text {
            textFieldsValueChanged = false
            if defaultAddressSwitch.isOn == defaultSwitchState{
                addButton.isEnabled = false
                
            }
        }else{
            addButton.isEnabled =  true
            textFieldsValueChanged = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            let newLength = text.characters.count + string.characters.count - range.length
            
            switch textField{
            case phoneText:
                if newLength == range.location && newLength % 4 == 0 && newLength > 0{
                    phoneText.text = String(text.characters.prefix(newLength-1))
                }
                else  if  newLength < limitPhoneLength - 2 && newLength % 4 == 0 && newLength > 0{
                    phoneText.text = text + "-"
                }
                
                return newLength <= limitPhoneLength
                
            case stateText:
                return newLength <= limitStateLength
            case zipText:
                return newLength <= limitZipLength
                
            default:
                return true
            }
        }
        return true
    }
}


// MARK: -  /////////////////////////////////////////////////////    KEYBOARD EXTENSION   /////////////////////////////////////////////////////////////////////

extension ShippingAddressAddUpdateViewController{
    func addKeyboardHideTap(){
        let hideTap = UITapGestureRecognizer(target: self, action: #selector(ShippingAddressAddUpdateViewController.hideKeyboardTap(_:)))
        hideTap.numberOfTapsRequired = 1
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(hideTap)
    }
    
    // Hide keyboard when view is Tapped - 1
    func hideKeyboardTap(_ recognizer:UITapGestureRecognizer){
        self.view.endEditing(true)
    }
}
