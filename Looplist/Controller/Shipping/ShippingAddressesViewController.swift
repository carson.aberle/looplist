//
//  ShippingAddressesViewController.swift
//  Looplist
//
//  Created by Roman Wendelboe on 9/13/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class ShippingAddressesViewController: UIViewController{
    
      // MARK: -  -----------------------------------------------------   VARIABLES  -----------------------------------------------------
    
    var popUp = false
//    weak var submitOrderVC:SubmitOrderViewController!
    
//    var selectedShipping:Shipping?
    var emptySetView: EmptySetView?{
        didSet{
            self.emptySetView?.discriptionLabel.text = "HAVENT_ADDED_ADDRESS_YET".localized
            self.tableView.backgroundView = emptySetView
            self.tableView.backgroundView?.isHidden = true
        }
    }
    
     // MARK: -  -----------------------------------------------------   OUTLETS   -----------------------------------------------------
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            if let tableView = tableView{
                tableView.tableFooterView = UIView()
                tableView.contentInset = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
            }
        }
    }
    
    // MARK: -  ////////////////////////////////////////////////    APPLICATION LIFE CYCLE    ///////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        ShippingAddressesViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        ShippingAddressesViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emptySetView = EmptySetView.instanceFromNib()
        self.navigationItem.title = "SHIPPING_ADDRESSES".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.setNeedsLayout()
        
        loadData()
    }
    
    override func viewWillLayoutSubviews() {
        if let navigationController = self.navigationController{
            ShippingAddressesViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    func loadData(){
        
        if let user = CurrentUser.sharedInstance.currentUser{
            let count = user.getShippingAddresesCount()
            if  count == 0{
                fetchData()
            }else if count > 0{
                self.tableView.reloadData()
            }
        }
        
    }
    
    func fetchData(){
        
        if let user = CurrentUser.sharedInstance.currentUser{
            user.getUserShippingAddresses(true, skip: 0, limit: 1000, completion: { (success) in
                if success{
                    self.tableView.reloadData()
                }else{
                    // Error
                    self.tableView.backgroundView?.isHidden = false
                }
            })
        }

        
    }
    
    @IBAction func UnwindToShipping(_ segue: UIStoryboardSegue) {
        fetchData()
    }
    
    // MARK: -  ///////////////////////////////////////////////////////    ACTIONS    ///////////////////////////////////////////////////////////////////////////
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addShippingAddress(_ sender: AnyObject) {
        let shippingAddressVC:ShippingAddressAddUpdateViewController = ShippingAddressAddUpdateViewController.instanceFromStoryboard("Shipping")
//        shippingAddressVC.isSaveButtonVisible = false
        self.navigationController?.pushViewController(shippingAddressVC, animated: true)
    }
    
      // MARK: -  ///////////////////////////////////////////////////////    FUNCTIONS    ///////////////////////////////////////////////////////////////////////////
    
    func makeAddressDefault(_ sender: UIButton!){
        sender.isEnabled = false
        if let index = (sender.layer.value(forKey: "index")) as? Int{
            if let user = CurrentUser.sharedInstance.currentUser{
                if let objectId = user.shipping?[index].objectId{
                    
                
                    user.toggleUserDefaultShippingAddress(objectId, completion: { (success) in
                        if success{
                            sender.isEnabled = true
                            self.tableView.reloadData()
                        }else{
                            sender.isEnabled = true
                            //ErrorHandling.unexpectedErrorHandler()
                        }
                    })
                }
            }
        }
    }
    
    func editAddress(_ sender: UIButton!){

        if let index = (sender.layer.value(forKey: "edit")) as? Int{
            if let user = CurrentUser.sharedInstance.currentUser{
                let shippingAddressVC:ShippingAddressAddUpdateViewController = ShippingAddressAddUpdateViewController.instanceFromStoryboard("Shipping")
                shippingAddressVC.shipping = user.shipping![index]
                shippingAddressVC.isUpdateAddress = true
                shippingAddressVC.addButton.title = "SAVE".localized
                
                self.navigationController?.pushViewController(shippingAddressVC, animated: true)
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////////    NAVIGATION  //////////////////////////////////////////////////////////////////////////

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "unwindToOrderVC"{
            if let ovc = segue.destination as? OrderViewController{
                if let user = CurrentUser.sharedInstance.currentUser{
                    if let selectedRow = tableView.indexPathForSelectedRow?.row{
                    if user.getShippingAddresesCount() >= selectedRow{
                        if let shipping = user.shipping?[selectedRow]{
                            ovc.shipping = shipping
                        }
                    }
                    }
                }
            }
        }
    }

}

// MARK: -  ////////////////////////////////////////////    TABLE VIEW DATA SOURCE & DELEGATE   //////////////////////////////////////////////////////////////

extension ShippingAddressesViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: -  ////////////////////////////////////////////////    TABLE VIEW DATA SOURCE   ////////////////////////////////////////////////////////////////////
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let user = CurrentUser.sharedInstance.currentUser{
                    if user.getShippingAddresesCount() == 0{
                       self.tableView.backgroundView?.isHidden = false
                        return 0
                    } else {
                        self.tableView.backgroundView?.isHidden = true
                        return user.getShippingAddresesCount()
                    }
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShippingAddressesCell", for: indexPath) as! ShippingAddressesTableViewCell
        if let user = CurrentUser.sharedInstance.currentUser{
            cell.shipping = user.shipping![indexPath.row]
            
            cell.primaryButton.layer.setValue(indexPath.row, forKeyPath: "index")
            cell.primaryButton.addTarget(self, action: #selector(ShippingAddressesViewController.makeAddressDefault(_:)), for: .touchUpInside)
            
            cell.editButton.layer.setValue(indexPath.row, forKeyPath: "edit")
            cell.editButton.addTarget(self, action: #selector(ShippingAddressesViewController.editAddress(_:)), for: .touchUpInside)
        }
        return cell
    }
    
    // MARK: -  //////////////////////////////////////////////////    TABLE VIEW DELEGATE   /////////////////////////////////////////////////////////////////////
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if popUp{
            if let user = CurrentUser.sharedInstance.currentUser{
                if indexPath.row == user.getShippingAddresesCount() {
                    self.addShippingAddress(self)
                }else{
                    performSegue(withIdentifier: "unwindToOrderVC", sender: tableView)
//                    let shipping = user.shipping![indexPath.row]
//                    if let orderVC = self.orderVC{
//                        self.submitOrderVC.shipping = shipping
////                        orderVC.loadShippingAddressUI(shipping)
////                        self.callingClass.dismissPopupViewControllerAnimated(true, completion: nil)
//                        self.navigationController?.popViewControllerAnimated(true)
//                    }
                }
            }
        }
    }

    // MARK: -  ///////////////////////////////////////////////    TABLE VIEW CONFIGURATION   ///////////////////////////////////////////////////////////////////
    
    
    // DELETE SHIPPING ADDRESS
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            tableView.isUserInteractionEnabled = false
            
            guard let user = CurrentUser.sharedInstance.currentUser,let shipping = user.shipping?[indexPath.row], let objectId = shipping.objectId else { tableView.isUserInteractionEnabled = true; return;}
            
                CurrentUser.sharedInstance.currentUser?.deleteShippingAddress(objectId, completion: { (success) in
                    tableView.isUserInteractionEnabled = true
                    if success{
                        tableView.deleteRows(at: [indexPath], with: .fade)
                    }else{
                        ErrorHandling.unexpectedErrorHandler()
                    }
                })
        
        }
    }
}
