//
//  FindFriendsSocialTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 12/13/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import TwitterKit
import FacebookShare

class FindFriendsSocialTableViewController: UITableViewController {
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////
    var accountName:String?
    var facebookFriendsOnApp:[User] = [User]()
    var isGeneratingInviteCode:Bool = false
    var remainingInviteCount:Int?
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendMessagesOnFacebook(_ sender: UIBarButtonItem) {
        self.sendMessagesOnFacebook()
    }

    // MARK: -  //////////////////////////////////////////////////////   LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "FIND_FRIENDS_SOCIAL".localized
        self.configureTableView()
        
        if let accountName = self.accountName{
            self.navigationItem.title = accountName
            
            //Build arrays
            if accountName == ExternalAccount.facebook.rawValue{
                self.getFacebookFriendsOnApp()
            } else {
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let navigationController = self.navigationController{
            FindFriendsSocialTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
        
        
        self.tableView.reloadData()
    }
    
    override func viewWillLayoutSubviews() {
  
    }
    
    private func configureTableView(){
        
        self.tableView.tableFooterView = UIView()
        self.tableView.dataSource = self
        self.tableView.delegate = self

        self.tableView.separatorStyle = .none
        
        self.tableView.register(UINib(nibName: "AddFriendTableViewCell", bundle: nil), forCellReuseIdentifier: "AddFriendTableViewCell")
        self.tableView.register(UINib(nibName: "InviteFriendTableViewCell", bundle: nil), forCellReuseIdentifier: "InviteFriendTableViewCell")
        self.tableView.register(UINib(nibName: "FindFriendsTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "FindFriendsTitleTableViewCell")
    }
    
    struct FriendsOnAppRequest: GraphRequestProtocol {
        struct Response: GraphResponseProtocol {
            var responseArray:NSArray?
            
            init(rawResponse: Any?) {
                // Decode JSON from rawResponse into other properties here.
                if let json = rawResponse as? NSDictionary{
                    if let notOnApp = json["data"] as? NSArray{
                        self.responseArray = notOnApp
                    }
                }
            }
        }
        
        var graphPath = "me/friends?limit=2000"
        var parameters: [String : Any]? = ["fields": "id, name, picture"]
        var accessToken = AccessToken.current
        var httpMethod: GraphRequestHTTPMethod = .GET
        var apiVersion: GraphAPIVersion = .defaultVersion
    }
    
    func getFacebookFriendsOnApp(){
        let connection = GraphRequestConnection()
        
        connection.add(FriendsOnAppRequest()) { response, result in
            switch result {
            case .success(let response):
                if let responseArray = response.responseArray{
                    var facebookIds:[String] = [String]()
                    for response in responseArray{
                        if let fbResponse = response as? NSDictionary{
                            if let id = fbResponse["id"] as? String{
                                facebookIds.append(id)
                            }
                        }
                    }
                    
                    if facebookIds.count > 0{
                        DBHelper.callAPIGet("users", queryString: "{\"flags\":{\"isFollowedByUser\":\"\(CurrentUser.sharedInstance.currentUser!.objectId!)\",\"isRequestedByUser\":\"\(CurrentUser.sharedInstance.currentUser!.objectId!)\"}, \"where\":{\"facebookId\":{\"$in\":\(facebookIds)}}}", completion: { (result, error) in
                            if error == nil{
                                if let users = result?["users"] as? NSArray{
                                    for userElement in users{
                                        if let userDictionary = userElement as? Dictionary<String, AnyObject>{
                                            let user:User = User(input: userDictionary)
                                            self.facebookFriendsOnApp.append(user)
                                        }
                                    }
                                    self.tableView.reloadData()
                                }
                            }
                        })
                    }
                    self.tableView.reloadData()
                }
        
            case .failed(_): break
            }
        }
        connection.start()

    }
    
    func postOnTwitter(){
        self.getInviteCode { (inviteCode) in
            if let inviteCode = inviteCode{
                if let title = CurrentUser.sharedInstance.currentUser?.fullName, let objectId = CurrentUser.sharedInstance.currentUser?.objectId, let imageUrl = CurrentUser.sharedInstance.currentUser?.profileImageUrl{
                    HelperFunction.getBranchLink(title: title, forUser: objectId, forProduct: nil,  withInviteCode:nil,withUrl: imageUrl, andContentDescription: "INVITE_MESSAGE".localized + "\n\n" + title, onChannel: ExternalAccount.facebook.rawValue, completion:{ (url, error) in
                        
                        let imageView = UIImageView()
                        
                        imageView.kf.setImage(with: URL(string: imageUrl)!, placeholder: nil,
                                              completionHandler: { image, error, cacheType, imageURL in
                                                if error == nil{
                                                    let composer = TWTRComposer()
                                                    
                                                    composer.setText("INVITE_MESSAGE".localized + "\n\n" + title + "\n\nInvite code: " + inviteCode)
                                                    composer.setURL(URL(string:url!))
                                                    composer.setImage(imageView.image)
                                                    // Called from a UIViewController
                                                    composer.show(from: self) { result in
                                                        if (result == TWTRComposerResult.cancelled) {
                                                        }
                                                        else {
                                                        }
                                                    }
                                                }
                        })
                        
                    })
                }
            }
        }
    }
    
    func postOnFacebook(){
        self.getInviteCode { (inviteCode) in
            if let inviteCode = inviteCode{
                if let title = CurrentUser.sharedInstance.currentUser?.fullName, let objectId = CurrentUser.sharedInstance.currentUser?.objectId, let imageUrl = CurrentUser.sharedInstance.currentUser?.profileImageUrl{
                    HelperFunction.getBranchLink(title: title, forUser: objectId, forProduct: nil,  withInviteCode:nil, withUrl: imageUrl, andContentDescription: "\(title) on Looplist", onChannel: ExternalAccount.facebook.rawValue, completion:{ (url, error) in
                        
                        let content = LinkShareContent(url: URL(string:url!)!, title: title, description: "INVITE_MESSAGE".localized + "\n\n" + title + "\n\nInvite code: " + inviteCode, imageURL: URL(string:imageUrl))
                        do{
                            try ShareDialog.show(from:self, content:content)
                        }
                        catch{}
                    })
                }
            }
        }
    }
    
    func sendMessagesOnFacebook(){
        self.getInviteCode { (inviteCode) in
            if let inviteCode = inviteCode{
                if let title = CurrentUser.sharedInstance.currentUser?.fullName, let objectId = CurrentUser.sharedInstance.currentUser?.objectId, let imageUrl = CurrentUser.sharedInstance.currentUser?.profileImageUrl{
                    HelperFunction.getBranchLink(title: title, forUser: objectId, forProduct: nil,  withInviteCode:nil,withUrl: imageUrl, andContentDescription: "INVITE_MESSAGE".localized + "\n\n" + title.localized, onChannel: ExternalAccount.facebook.rawValue, completion:{ (url, error) in
                        
                        let content = LinkShareContent(url: URL(string:url!)!, title: title, description: "INVITE_MESSAGE".localized + "\n\n" + title + "\n\nInvite code: " + inviteCode, imageURL: URL(string:imageUrl))
                        do{
                            try MessageDialog.show(content)
                        }
                        catch{}
                    })
                }
            }
        }
    }
    
    func getInviteCode(completion:@escaping (_ inviteCode:String?)->Void){
        if let remainingInviteCount = self.remainingInviteCount{
            if remainingInviteCount > 0 {
                if !self.isGeneratingInviteCode{
                    let alert = UIAlertController(title: "USE_INVITE_TITLE".localized, message: "USE_INVITE_DESCRIPTION".localized, preferredStyle: UIAlertControllerStyle.alert)
                    let yes = UIAlertAction(title: "YES".localized, style: UIAlertActionStyle.default, handler: {(action: UIAlertAction) -> Void in
                        self.isGeneratingInviteCode = true
                        DBHelper.callAPIPost("invites", completion: { (result, error) in
                            self.isGeneratingInviteCode = false
                            if error == nil{
                                if let invite = result?["invite"] as? NSDictionary{
                                    if let passcode = invite["passcode"] as? String{
                                        UIPasteboard.general.string = passcode
                                        DropDownAlert.customSuccessMessage("INVITE_CODE_COPIED".localized)
                                        completion(passcode)
                                    } else {
                                        completion(nil)
                                        DropDownAlert.customErrorMessage("ERROR_GETTING_INVITE_CODE".localized)
                                    }
                                } else {
                                    completion(nil)
                                    DropDownAlert.customErrorMessage("ERROR_GETTING_INVITE_CODE".localized)
                                }
                            } else {
                                completion(nil)
                                DropDownAlert.customErrorMessage("ERROR_GETTING_INVITE_CODE".localized)
                            }
                        })
                    })
                    
                    alert.addAction(yes)
                    let no = UIAlertAction(title: "NO".localized, style: UIAlertActionStyle.cancel, handler: nil)
                    alert.addAction(no)
                    self.present(alert, animated: true, completion: nil)
                }

            } else {
                _ = self.navigationController?.popViewController(animated: true)
                DropDownAlert.customErrorMessage("OUT_OF_INVITES".localized)
            }
        } else {
            DropDownAlert.customErrorMessage("ERROR".localized)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if self.accountName == ExternalAccount.twitter.rawValue{
            return 1
        } else {
            if self.facebookFriendsOnApp.count > 0{
                return 2
            } else {
                return 1
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 && self.facebookFriendsOnApp.count > 0{
            return self.facebookFriendsOnApp.count
        } else {
            if self.accountName == ExternalAccount.twitter.rawValue{
                return 1
            } else {
                return 2
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 58
        } else {
            return 40
        }
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            if self.accountName == ExternalAccount.twitter.rawValue{
                return 50
            }else {
                return 50
            }
        } else {
            return 50
        }
    }
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            if self.accountName == ExternalAccount.twitter.rawValue{
                return "INVITE".localized
            }else {
                if self.facebookFriendsOnApp.count > 0{
                    return "ADD_FRIENDS".localized
                } else {
                    return "INVITE".localized
                }
            }
        } else {
            return "INVITE".localized
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        let title = UILabel()
        title.font = UIFont(name: ConstantsFont.fontDefaultBlack, size: 16)!
        title.textColor = ConstantsColor.looplistGreyColor
        
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font=title.font
        header.textLabel?.textColor=title.textColor
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 && self.facebookFriendsOnApp.count > 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddFriendTableViewCell", for: indexPath) as! AddFriendTableViewCell
            if self.accountName == ExternalAccount.facebook.rawValue{
                if self.facebookFriendsOnApp.count > indexPath.row{
                    let user = self.facebookFriendsOnApp[indexPath.row]
                    cell.user = user
                }
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FindFriendsTitleTableViewCell", for: indexPath) as! FindFriendsTitleTableViewCell
            if indexPath.row == 0{
                if self.accountName == ExternalAccount.twitter.rawValue{
                    cell.titleLabel.text! = "SEND_TWEET".localized
                } else {
                    cell.titleLabel.text! = "WRITE_POST".localized
                }
            } else {
                cell.titleLabel.text! = "SEND_MESSAGE".localized
            }

            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && self.facebookFriendsOnApp.count > 0{
            if self.accountName == ExternalAccount.facebook.rawValue{
                if self.facebookFriendsOnApp.count > indexPath.row{
                    let userVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
                    userVC.currentUser = self.facebookFriendsOnApp[indexPath.row]
                    self.navigationController?.pushViewController(userVC, animated: true)
                }
            }

        } else {
            if let accountName = self.accountName{
                if indexPath.row == 0{
                    //Post
                    if accountName == ExternalAccount.facebook.rawValue{
                        self.postOnFacebook()
                    } else if accountName == ExternalAccount.twitter.rawValue{
                        self.postOnTwitter()
                    }
                } else {
                    //Send in message
                    if accountName == ExternalAccount.facebook.rawValue{
                        self.sendMessagesOnFacebook()
                    }
                }
            }
        }
    }

}
