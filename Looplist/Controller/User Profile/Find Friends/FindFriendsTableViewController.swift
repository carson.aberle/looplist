//
//  FindFriendsTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 12/13/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import TwitterKit
import FacebookShare
import MessageUI

class FindFriendsTableViewController: UIViewController, MFMessageComposeViewControllerDelegate,UITextFieldDelegate,SearchDisplayUserProtocol{
    
    // MARK: -  //////////////////////////////////////////////////////   PRIVATE VARIABLES   /////////////////////////////////////////////////////////////////////////
    
    //    private enum searchState{
    //        case On
    //        case Off
    //    }
    //
    //    private var curentSearchState = searchState.Off
    fileprivate var indicator = CustomActivityIndicator()
    
    fileprivate var users:[User] = [User](){
        didSet{
            tableView.reloadData()
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var leftBarButtonItem: UIBarButtonItem!
    
    
    @IBOutlet weak var containerView: UIView!{
        didSet{
            containerView.isHidden = true
        }
    }
    
    @IBOutlet weak var searchTextField: UITextField!{
        didSet{
            if let searchTextField = searchTextField{
                searchTextField.delegate = self
                searchTextField.addTarget(self, action: #selector(FindFriendsTableViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
            }
        }
    }
    
    private weak var searchDisplayUserViewController:SearchDisplayUserViewController?
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func goBack(_ sender: UIBarButtonItem) {
        if containerView.isHidden{
            _ = self.navigationController?.popViewController(animated: true)
        }else{
            dismissSearchOverlay()
        }
    }
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            if let tableView = tableView{
                tableView.register(UINib(nibName: "AddFriendTableViewCell", bundle: nil), forCellReuseIdentifier: "AddFriendTableViewCell")
                tableView.register(UINib(nibName: "FindFriendsTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "FindFriendsTitleTableViewCell")
            }
        }
    }
    
    var remainingInviteCount:Int!
    
    
    // MARK: -  //////////////////////////////////////////////////////   LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchTextField.placeholder = "FIND_FRIENDS_PLACEHOLDER".localized
        self.navigationController?.tabBarController?.tabBar.isHidden = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.getRecommendedUsers()

        self.getInviteCount()
        
        if let navigationController = self.navigationController{
            FindFriendsTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    
    private func getRecommendedUsers(){
        self.indicator.showActivityIndicator(uiView: self.view, offset: 64.0)
        APIHelper.getRecommenderUsers { [weak vc = self](users, error) in
            vc?.indicator.hideActivityIndicator()
            if let users = users{
                vc?.users = users
            }else{
                // empty data set
            }
        }
    }
    
    private func showSearchOverlay(){
        containerView.isHidden = false
        self.leftBarButtonItem.image = UIImage(named:"Close Large")
    }
    
    
    private func dismissSearchOverlay(){
        containerView.isHidden = true
        self.leftBarButtonItem.image = UIImage(named:"Back")
        searchTextField.resignFirstResponder()
        searchDisplayUserViewController?.userEnteredSearchTerm =  nil
        searchTextField.text = nil
    }
    
    func getInviteCount(){
        DBHelper.callAPIGet("invites/count") { [weak vc = self](result, error) in
            if error == nil{
                if let result = result{
                    if let count = result["count"] as? Int{
                        vc?.remainingInviteCount = count
                        vc?.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    fileprivate func signInToFacebook(){
        let loginManager = LoginManager()
        loginManager.logIn([.publicProfile, .userFriends, .email], viewController: self, completion: { (loginResult) in
            switch loginResult {
            case .failed( _):
                break;
            case .cancelled:
                break;
            case .success(grantedPermissions: _, declinedPermissions: _, token: let accessToken):
                if let facebookId = accessToken.userId, let objectId = CurrentUser.sharedInstance.currentUser?.objectId{
                    DBHelper.callAPIPut("users/\(objectId)", params: ["facebookId":facebookId as AnyObject], completion: { [weak vc = self](result, error) in
                        //                        CurrentUser.sharedInstance.currentUser?.facebookId = facebookId
                    })
                }
                break;
            }
            
        })
    }
    
    fileprivate func signInToTwitter(){
        Twitter.sharedInstance().logIn {
            (session, error) -> Void in
            if (session != nil) {
                if let twitterId = session?.userID, let objectId = CurrentUser.sharedInstance.currentUser?.objectId{
                    DBHelper.callAPIPut("users/\(objectId)", params: ["twitterId":twitterId as AnyObject], completion: { (result, error) in
                    })
                }
            } else {
            }
        }
    }
    
    internal func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: -  //////////////////////////////////////////////////////   TEXTFIELD  DELEGATES  ///////////////////////////////////////////////////////////////////
    //
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        showSearchOverlay()
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        if let searchTerm = textField.text{
            searchDisplayUserViewController?.userEnteredSearchTerm = searchTerm
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissSearchOverlay()
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        if let searchTerm = textField.text{
            searchDisplayUserViewController?.userEnteredSearchTerm = searchTerm
        }
        return false
    }
    
    
    func resignKeyboard(){
        self.searchTextField.resignFirstResponder()
    }
    
    // MARK: -  //////////////////////////////////////////////////////   NAVIGATION  ///////////////////////////////////////////////////////////////////
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "ShowUserSearch"){
            if let searchDisplayUserVC = segue.destination as? SearchDisplayUserViewController{
                self.searchDisplayUserViewController = searchDisplayUserVC
                self.searchDisplayUserViewController?.delegate = self
            }
        }
    }
    
    //    fileprivate enum SectionHeader:String{
    //            case invite = "Invite"
    //            case recommended = "Recommended"
    //    }
}

// MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DATA SOURCE EXTENSION   /////////////////////////////////////////////////////////////////////////

extension FindFriendsTableViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            if let count = self.remainingInviteCount {
                if count > 0 {
                    if (MFMessageComposeViewController.canSendText()) {
                        return 3
                    } else {
                        return 2
                    }
                } else {
                    return 1
                }
            } else {
                return 0
            }
        } else {
            return self.users.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 40
        } else {
            return 58
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            if let count = self.remainingInviteCount {
                return "INVITES".localized + " (\(count) " + "REMAINING".localized + ")"
            } else {
                return "INVITES".localized + " " + "REMAINING".localized + ")"
            }
        } else {
            return "RECOMMENDED".localized
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0000000001
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        let title = UILabel()
        title.font = UIFont(name: ConstantsFont.fontDefaultBlack, size: 16)!
        title.textColor = ConstantsColor.looplistGreyColor
        
        if let header = view as? UITableViewHeaderFooterView{
            header.textLabel?.font = title.font
            header.textLabel?.textColor = title.textColor
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FindFriendsTitleTableViewCell", for: indexPath) as! FindFriendsTitleTableViewCell
            if self.remainingInviteCount > 0 {
                if indexPath.row == 0{
                    cell.titleLabel.text = ExternalAccount.facebook.rawValue
                } else if indexPath.row == 1 {
                    cell.titleLabel.text = ExternalAccount.twitter.rawValue
                } else {
                    cell.titleLabel.text = ExternalAccount.sms.rawValue
                }
            } else {
                cell.titleLabel.text = "REQUEST_MORE_INVITES".localized
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddFriendTableViewCell", for: indexPath) as! AddFriendTableViewCell
            if self.users.count > indexPath.row{
                let user = self.users[indexPath.row]
                cell.user = user
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            let findFriendsSocialVC:FindFriendsSocialTableViewController = FindFriendsSocialTableViewController.instanceFromStoryboard("Main")
            
            if self.remainingInviteCount > 0 {
                findFriendsSocialVC.remainingInviteCount = self.remainingInviteCount
                if indexPath.row == 0{
                    if let _ = AccessToken.current {
                        findFriendsSocialVC.accountName = ExternalAccount.facebook.rawValue
                        self.navigationController?.pushViewController(findFriendsSocialVC, animated: true)
                    } else {
                        self.signInToFacebook()
                    }
                } else if indexPath.row == 1{
                    let store = Twitter.sharedInstance().sessionStore
                    if let _ = store.session() {
                        findFriendsSocialVC.accountName = ExternalAccount.twitter.rawValue
                        findFriendsSocialVC.remainingInviteCount = self.remainingInviteCount
                        self.navigationController?.pushViewController(findFriendsSocialVC, animated: true)
                    } else {
                        self.signInToTwitter()
                    }
                } else {
                    let alert = UIAlertController(title: "USE_INVITE_TITLE".localized, message: "USE_INVITE_DESCRIPTION".localized, preferredStyle: UIAlertControllerStyle.alert)
                    let yes = UIAlertAction(title: "YES".localized, style: UIAlertActionStyle.default, handler: {(action: UIAlertAction) -> Void in
                        DBHelper.callAPIPost("invites", completion: { (result, error) in
                            if error == nil{
                                if let invite = result?["invite"] as? NSDictionary{
                                    if let passcode = invite["passcode"] as? String{
                                        if var count = self.remainingInviteCount {
                                            count -= 1
                                            self.remainingInviteCount = count
                                        }
                                        self.tableView.reloadData()
                                        if let title = CurrentUser.sharedInstance.currentUser?.fullName, let objectId = CurrentUser.sharedInstance.currentUser?.objectId, let imageUrl = CurrentUser.sharedInstance.currentUser?.profileImageUrl{
                                            HelperFunction.getBranchLink(title: title, forUser: objectId, forProduct: nil, withInviteCode:passcode, withUrl: imageUrl, andContentDescription: "\(title) on Looplist", onChannel: ExternalAccount.facebook.rawValue, completion:{ (url, error) in
                                                if (MFMessageComposeViewController.canSendText()) {
                                                    let controller = MFMessageComposeViewController()
                                                    controller.body = ("INVITE_MESSAGE".localized + "\n\n" + url! + "\n\n\nInvite Code:  " + passcode)
                                                    controller.messageComposeDelegate = self
                                                    self.present(controller, animated: true, completion: nil)
                                                } else {
                                                    DropDownAlert.customErrorMessage("ERROR_CANT_SEND_TEXT".localized)
                                                }
                                            })
                                        }
                                    } else {
                                        DropDownAlert.customErrorMessage("ERROR_GETTING_INVITE_CODE".localized)
                                    }
                                } else {
                                    DropDownAlert.customErrorMessage("ERROR_GETTING_INVITE_CODE".localized)
                                }
                            } else {
                                DropDownAlert.customErrorMessage("ERROR_GETTING_INVITE_CODE".localized)
                            }
                        })
                        
                    })
                    
                    alert.addAction(yes)
                    let no = UIAlertAction(title: "NO".localized, style: UIAlertActionStyle.cancel, handler: nil)
                    alert.addAction(no)
                    self.present(alert, animated: true, completion: nil)
                }
                
            } else {
                self.indicator.showActivityIndicator(uiView: self.view, offset: 64.0)
                MessagingManager.getConversations(true, chatId: nil, skip: 0, limit: 1, completion: { (conversation) in
                    self.indicator.hideActivityIndicator()

                    if let converstionArray = conversation{
                   
                        if converstionArray.count > 0 {
                            let messagingVC:MessagingViewController = MessagingViewController.instanceFromStoryboard("Messaging")
                            let messageConversation = MessageConversation(user: nil, chatId: nil, userLastMessage: nil, date: nil, read: nil)
                            let messageConverstion = converstionArray[0]
                            messagingVC.messageConversation = messageConverstion
                            messagingVC.preFilledMessage = "MAVEN_REQUEST_MORE_INVITES".localized
                            self.navigationController?.pushViewController(messagingVC, animated: true)
                        }
                    }
                })
            }
        } else if indexPath.section == 1{
            if self.users.count > indexPath.row{
                let user = self.users[indexPath.row]
                let userProfileVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
                userProfileVC.currentUser = user
                self.navigationController?.pushViewController(userProfileVC, animated: true)
            }
        }
    }
}
