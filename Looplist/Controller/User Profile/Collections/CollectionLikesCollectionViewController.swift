//
//  CollectionLikesViewController.swift
//  Looplist
//

//  Created by Carson Aberle on 8/17/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class CollectionLikesCollectionViewController:UICollectionViewController,  UICollectionViewDelegateFlowLayout, UIViewControllerPreviewingDelegate{
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////
    
    var headerView:CollectionLikesHeaderView!
    var collection:Collection!
    var currentUser: User?
    var shouldShowEditButton = true
    fileprivate var limitNumberCollections = ConstantsCount.paginationlimitNumber
    fileprivate var isEndOfCollection = false
    fileprivate var textHeight: CGFloat = 0.0
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editCollection(_ sender: UIBarButtonItem) {
        let collectionEditVC:CollectionEditTableViewController = CollectionEditTableViewController.instanceFromStoryboard("Main")
        collectionEditVC.collection = self.collection
        collectionEditVC.currentUser = self.currentUser
        self.navigationController?.pushViewController(collectionEditVC, animated: true)
    }
    
    @IBAction func privacyChanged(_ sender: UISwitch) {
        if let collectionId = self.collection.objectId{
            APIHelperUser.changePrivacyStatus(collectionId: collectionId, status: sender.isOn, { (success) in
                if success{
                    self.collection?.isPrivate = sender.isOn
                }
            })
        }
    }
    
    @IBAction func loadUser(_ sender: UIButton) {
        if let user = self.collection.owner{
            let guestUserProfileVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
            guestUserProfileVC.currentUser = user
            self.navigationController?.pushViewController(guestUserProfileVC, animated: true)
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Remove template products with images only
        self.collection.products?.removeAll()
        
//        self.navigationItem.title = collection.name!
        
        self.loadCollectionProducts(skip:0)
        
        self.configureEditButton()
        
        self.configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        
        if let navigationController = self.navigationController{
            CollectionLikesCollectionViewController.self.setNavigationBar(UIColor.white, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.lightContent, backgroundColor:NavigationBarBackgroundColor.clearNoGradient)
        }
        self.navigationItem.title = collection.name ?? ""
        self.collectionView?.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CollectionLikesCollectionViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let objectId = self.collection.objectId{
            CollectionLikesCollectionViewController.stopTimer(["collection":objectId], withActionType:"View Screen")
        } else {
            CollectionLikesCollectionViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    func configureUI(){
        let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.sectionHeadersPinToVisibleBounds = true
        
        self.tabBarController?.tabBar.isHidden = true
        
        if( traitCollection.forceTouchCapability == .available){
            registerForPreviewing(with: self, sourceView: view)
        }
    }
    
    func configureEditButton(){
        if self.collection.isLikesCollection == false && self.collection.owner?.objectId == CurrentUser.sharedInstance.currentUser?.objectId {
            self.editButton.isEnabled = true
            self.editButton.tintColor = UIColor.white
        } else {
            self.editButton.isEnabled = false
            self.editButton.tintColor = UIColor.clear
        }
    }
    
    func loadCollectionProducts(skip:Int){

        if skip != 0{
            self.hideEmptySetView()
        }
        
        if let collectionId = self.collection.objectId, let userId = self.currentUser?.objectId{
            APIHelperAnyUser.getUserCollectionsFull(userId: userId, collectionId: collectionId, skip: skip, limit: self.limitNumberCollections, completion: { [weak vc = self](collection) in
                if let resultCollection = collection{
                    if let products = resultCollection.products{
                        
                        if let limit = vc?.limitNumberCollections, products.count != limit{
                            vc?.isEndOfCollection = true
                        }else{
                            vc?.isEndOfCollection = false
                        }
                        
                        if products.count > 0{
                            if skip == 0{
                                vc?.collection.products = [Product]()
                            }
//                            vc?.collection.products = products
                            
                            for product in products{
                                self.collection.products?.append(product)
                            }
                            
                            vc?.collectionView?.reloadData()
                        } else {
                            if skip == 0{
                                vc?.showEmptySetView()
                            }
                        }
                    }
                }
            })
        }
    }
    
    func showEmptySetView(){ // lazy init
        let emptySetView: EmptySetView = EmptySetView.instanceFromNib()
        emptySetView.discriptionLabel.text = "EMPTY_COLLECTION_PLACEHOLDER".localized
        self.collectionView?.backgroundView = emptySetView
        self.collectionView?.backgroundView?.isHidden = false
    }
    
    func hideEmptySetView(){ // lazy init
        self.collectionView?.backgroundView?.isHidden = true
    }
    
    func requiredHeight(_ labelPassed:UILabel) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 110, width: self.view.frame.width - 32.0, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = labelPassed.font
        label.text = labelPassed.text
        label.sizeToFit()
        
        return label.frame.height + 18.0
//        return label.frame.height
    }
    
    //MARK : - 3D TOUCH
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        guard let offsetY = self.collectionView?.contentOffset.y else { return nil }
        let newLocation = CGPoint(x: location.x, y: location.y + offsetY)
        guard let indexPath = collectionView?.indexPathForItem(at: newLocation) else { return nil }
        
        guard let cell = collectionView?.cellForItem(at: indexPath) else { return nil }
        
        guard let previewVC = storyboard?.instantiateViewController(withIdentifier: "ProductPreviewViewController") as? ProductPreviewViewController else { return nil }
        
        guard let product = self.collection.products?[indexPath.row] else { return nil }
        
        previewVC.callingClass = self
        previewVC.product = product
        previewVC.view.layer.setValue(indexPath.row, forKey: "indexPathRow")
        let width = UIScreen.main.bounds.size.width - 100
        previewVC.preferredContentSize = CGSize(width: width, height: width)
        
        let collectionViewOffsetY = self.collectionView!.contentOffset.y
        previewingContext.sourceRect = CGRect(x: cell.frame.x, y: (cell.frame.y - collectionViewOffsetY), width: cell.frame.size.width, height: cell.frame.size.height)
        return previewVC
    }
    
    //Delegate for 3d touch devices
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        if let index = viewControllerToCommit.view.layer.value(forKey: "indexPathRow") as? Int{
            if let count = self.collection.products?.count, count > index{
                if let product = self.collection.products?[index]{
                    let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
                    productVC.product = product
                    self.navigationController?.pushViewController(productVC, animated:false)
                }
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   COLLECTION VIEW DATA SOURCE   /////////////////////////////////////////////////////////////////////////
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = self.collection.products?.count{
            return count
        } else {
            return 0
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let cell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CollectionLikesHeaderView", for: indexPath) as! CollectionLikesHeaderView
        
        cell.currentCollection = self.collection
        self.headerView = cell
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        if let headerView = self.headerView{
            if collection.description != "" && collection.description != nil{
                let label = UILabel()
                label.text = collection.description
                textHeight = self.requiredHeight(label)
            }else{
                textHeight = 0.0
            }
//        }
        return CGSize(width: collectionView.frame.width, height: 110 + textHeight)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let count = self.collection.products?.count{
            if count > 0 && indexPath.row == count - 1 && !isEndOfCollection{
                self.loadCollectionProducts(skip: count)
            }
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionLikesCollectionViewCell", for: indexPath) as! CollectionLikesCollectionViewCell
        
        cell.indexPath = indexPath
        cell.currentCollection = self.collection
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.size.width / 2) - 1 , height: (self.view.frame.size.width / 2) - 1.5)
        
    }
    
    // MARK: -  //////////////////////////////////////////////////////   COLLECTION VIEW DELEGATE   /////////////////////////////////////////////////////////////////////////
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
        productVC.product = self.collection.products?[indexPath.row]
        self.navigationController?.pushViewController(productVC, animated: true)
    }
}
