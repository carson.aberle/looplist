//
//  CollectionEditTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 8/19/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class CollectionEditTableViewController:UITableViewController, UITextViewDelegate, UITextFieldDelegate {
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////
    
    var collection:Collection!{
        didSet{
            if let collection = collection{
                
                 self.collectionName = collection.name ?? ""
                 self.collectionDescription = collection.description ?? ""
                 self.collectionPrivate = collection.isPrivate ?? false
            }
        }
    }
    var currentUser: User!
//    fileprivate var collectionItemsToRemoveIndexes:NSMutableArray = NSMutableArray()
    fileprivate var limitNumberCollections = ConstantsCount.paginationlimitNumber
    fileprivate var isEndOfCollection = false
    
    var headerReference:CollectionEditHeaderView?
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE /////////////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        CollectionEditTableViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let objectId = self.collection.objectId{
            CollectionEditTableViewController.stopTimer(["collection":objectId], withActionType:"View Screen")
        } else {
            CollectionEditTableViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
        }
    }
    
    override func viewDidLoad() {
        
        //Remove template products with images only
        self.collection.products?.removeAll()
        
        self.configureUI()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.saveButton.isEnabled = false
        self.navigationItem.title = collection.name ?? ""
        self.tableView.resetScrollPositionToTop()
        
        self.loadCollectionProducts(skip:0)
    }
    
     // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func loadUser(_ sender: UIButton) {
        if let user = self.collection.owner{
            let guestUserProfileVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
            guestUserProfileVC.currentUser = user
            self.navigationController?.pushViewController(guestUserProfileVC, animated: true)
        }
    }
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    fileprivate var collectionName = ""
    fileprivate var collectionDescription = ""
    fileprivate var collectionPrivate = false

    
    func setUserChangedValuesToCollectionItems(){
        if let collection = collection{
            
            collection.name = self.collectionName
            collection.description = self.collectionDescription
            collection.isPrivate = self.collectionPrivate
        }
    }
    
    @IBAction func saveCollection(_ sender: UIBarButtonItem) {
        
         sender.isEnabled = false
         self.view.endEditing(true)
        
        
        if collectionName.isEmpty{
            DropDownAlert.customErrorMessage("COLLECTION_NAME_BLANK".localized)
            sender.isEnabled = true
            return
        }
//       
//        if (description == "COLLECTION_DESCRIPTION_PLACEHOLDER".localized){
//            description = ""
//        }
        
        
        guard let collectionObjectId = self.collection.objectId else {
            DropDownAlert.customErrorMessage("Error saving collection".localized)
              sender.isEnabled = true
            return
        }
        
        APIHelperUser.saveCollection(collectionId: collectionObjectId, isCollectionPrivate: self.collectionPrivate, collectionName: collectionName, collectionDescription: collectionDescription) { [weak vc = self](success) in
            vc?.setUserChangedValuesToCollectionItems()
            vc?.checkStatusOfSaveButton()
            if success{
//                let indexPath = IndexPath(item: 0, section: 0)
//                vc?.tableView.reloadRows(at: [indexPath], with: .none)
                vc?.navigationItem.title = vc?.collection.name ?? ""
//                 _ = vc?.navigationController?.popToRootViewController(animated: true)
            }else{
                
                sender.isEnabled = true
            }
        }
        

    }
    
    @IBAction func changePrivacy(_ sender: UISwitch) {
        self.collectionPrivate = sender.isOn
        checkStatusOfSaveButton()
    }
    
    @IBAction func deleteCollection(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: "COLLECTION_CONFIRM_DELETE".localized, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "CANCEL".localized, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let deleteCollectionAction = UIAlertAction(title: "DELETE".localized, style: .destructive) { (action) in
            if let objectId = self.collection.objectId, let collectionName = self.collection.name{
                APIHelperUser.deleteCollecton(collectionId: objectId, collectionName: collectionName, { (result) in
                    if result{
                        _ = self.navigationController?.popToRootViewController(animated: true)
                    }
                })
            }
        }
        
        alertController.addAction(deleteCollectionAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    func checkStatusOfSaveButton(){
        if self.collectionName == self.collection.name && self.collectionDescription == self.collection.description && self.collectionPrivate == self.collection.isPrivate{
            self.saveButton.isEnabled = false
            self.tableView.allowsSelectionDuringEditing = true
            

        }else{
            self.saveButton.isEnabled = true
            self.tableView.allowsSelectionDuringEditing = false
        }
        
    }
    
    
    func configureUI(){
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 274
        
        if let navigationController = self.navigationController{
            CollectionEditTableViewController.self.setNavigationBar(UIColor.white, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.lightContent, backgroundColor:NavigationBarBackgroundColor.clearNoGradient)
        }
    }
    
    func loadCollectionProducts(skip:Int){
        if let collectionId = self.collection.objectId, let userId = self.currentUser?.objectId{
            APIHelperAnyUser.getUserCollectionsFull(userId: userId, collectionId: collectionId, skip: skip, limit: self.limitNumberCollections, completion: { [weak vc = self](collection) in
                if let resultCollection = collection{
                    if let products = resultCollection.products{
                        
                        if let limit = vc?.limitNumberCollections, products.count < limit{
                            vc?.isEndOfCollection = true
                        }else{
                            vc?.isEndOfCollection = false
                        }
                        
                        if products.count > 0{
                            if skip == 0{
                                vc?.collection.products = [Product]()
                            }
                            
                            for product in products{
                                self.collection.products?.append(product)
                            }
                            
                            vc?.tableView?.reloadData()
                        }
                    }
                }
            })
        }
    }

    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DATA SOURCE   /////////////////////////////////////////////////////////////////////////
    
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.collection.products?.count{
            return count + 1
        } else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerSection = tableView.dequeueReusableCell(withIdentifier:"CollectionEditTableViewHeaderCell" ) as! CollectionEditHeaderView
        headerSection.currentCollection = self.collection
        self.headerReference = headerSection
        
        return headerSection.contentView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 110
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 274
        } else {
            return 100
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionEditDescriptionTableViewCell") as! CollectionEditDescriptionTableViewCell
            cell.currentCollection = self.collection
            cell.collectionTitle.delegate = self
            cell.collectionTitle.addTarget(self, action: #selector(CollectionEditTableViewController.textFieldDidChange(textField:)), for: .editingChanged)
            cell.collectionDescription.delegate = self
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionEditTableViewCell") as! CollectionEditTableViewCell
            
            if let count = self.collection.products?.count{
                if count > 0 && indexPath.row == count - 2 && !isEndOfCollection{
                    self.loadCollectionProducts(skip: count)
                }
                
                if count >= indexPath.row-1{
                    cell.currentProduct = self.collection.products?[indexPath.row - 1]
                     cell.deleteButton.tag = indexPath.row-1
                }
                
            }
            
            return cell
        }
    }
    
    func deleteProduct(product:Product){
        APIHelperUser.deleteProductFromCollecton(collection: self.collection, product: product) { (success) in }
        
    }
    
    
    @IBAction func deleteProductPressed(_ sender: UIButton) {
        
        let index = sender.tag
        
        guard let count = self.collection.products?.count, count >= index, let product = self.collection.products?[index] else{
            DropDownAlert.customErrorMessage("Error deleting the product from this collection")
            return
        }
        
        self.collection.products?.remove(at: index)
        self.collection.collectionItemsCountInt -= 1
        let newIndexPath = IndexPath(row: index+1, section: 0)
        self.tableView?.deleteRows(at: [newIndexPath], with: .fade)
        self.headerReference?.collectionItemsCount.text = self.collection.collectionItemsCount
        
        APIHelperUser.deleteProductFromCollecton(collection: self.collection, product: product) { (success) in }
        
    }
    
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DELEGATE   /////////////////////////////////////////////////////////////////////////

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    // MARK: -  //////////////////////////////////////////////////////   TEXT FIELD DELEGATE   /////////////////////////////////////////////////////////////////////////
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view!.endEditing(true)
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view!.endEditing(true)
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text! == "COLLECTION_DESCRIPTION_PLACEHOLDER".localized) {
            textView.text = ""
            textView.textColor = ConstantsColor.looplistGreyColor
        }
        textView.becomeFirstResponder()
    }
    
    func textFieldDidChange(textField: UITextField) {
        if let text = textField.text{
            print(text)
            self.collectionName = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        checkStatusOfSaveButton()
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        if let text = textView.text{
            print(text)
            self.collectionDescription = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
            //            if text == ""{
            //                textView.text = "COLLECTION_DESCRIPTION_PLACEHOLDER".localized
            //            }
        }
        checkStatusOfSaveButton()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"{
            textView.resignFirstResponder()
            return false
        }
        if let textCount = textView.text?.characters.count,textCount + text.characters.count - range.length > 100{
            return false
        }else{
            return true
        }
    }
    
//    func textViewDidEndEditing(_ textView: UITextView) {
//        if (textView.text! == "") {
//            textView.text = "COLLECTION_DESCRIPTION_PLACEHOLDER".localized
//            textView.textColor = UIColor(red:0.81, green:0.81, blue:0.81, alpha:1.0)
//        }
//        textView.resignFirstResponder()
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        if let textCount = textField.text?.characters.count,textCount + string.characters.count - range.length > 50{
            return false
        }else{
            return true
        }
    }
}
