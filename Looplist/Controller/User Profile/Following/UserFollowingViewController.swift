//
//  UsersFollowingViewController.swift
//  Looplist
//
//  Created by Roman Wendelboe on 7/19/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class UserFollowingViewController: UIViewController, UITextFieldDelegate{
    
    // MARK: -  ------------------------------------------------------------------  API VARIABLES   ------------------------------------------------------------------
    
    var currentUser:User?
    
    // MARK: -  ------------------------------------------------------------------   PRIVATE VARIABLES   ------------------------------------------------------------------
    
    fileprivate var currentPage = 0
    private let limitNumber = ConstantsCount.paginationlimitNumber
    fileprivate var isEndOfPage = false
    
    fileprivate var users:[User] = [User](){
        didSet{
            if self.indicator.isLoading{
                self.indicator.hideActivityIndicator()
            }
            tableView.reloadData()
            //works
        }
    }
    
    fileprivate var indicator = CustomActivityIndicator()

    
    // MARK: -  ------------------------------------------------------------------  OUTLETS   ------------------------------------------------------------------
    
//    lazy var refreshControl: UIRefreshControl = {
//        let refreshControl = UIRefreshControl()
//        refreshControl.addTarget(self, action: #selector(UserFollowingViewController.refresh(_:)), for: UIControlEvents.valueChanged)
//        refreshControl.backgroundColor = UIColor.white
//        return refreshControl
//    }()
    
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            //            self.tableView.backgroundView = emptySetView
            //            self.tableView.backgroundView?.isHidden = true
            self.tableView.register(UINib(nibName: "AddFriendTableViewCell", bundle: nil), forCellReuseIdentifier: "AddFriendTableViewCell")
        }
    }
    
    var emptySetView: EmptySetView? = EmptySetView.instanceFromNib()
    var emptySetViewSearch: EmptySetViewSearch? = EmptySetViewSearch.instanceFromNib()
    
    @IBOutlet weak var searchTextField: UITextField!{
        didSet{
            if let searchTextField = searchTextField{
                searchTextField.delegate = self
                searchTextField.addTarget(self, action: #selector(UserFollowingViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.reload()
        
        if let navigationController = self.navigationController{
            UserFollowingViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
        
        //        initializeEmptySet()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UserFollowingViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let objectId = self.currentUser?.objectId{
            UserFollowingViewController.stopTimer(["user":objectId], withActionType:"View Screen")
        } else {
            UserFollowingViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.searchTextField.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureUI()
        
    }
    
    
    private func configureUI(){
        
        self.congigureTabBar()
        self.configureNavBar()
//        self.configureTableView()
        self.configureTitle()
        
    }
    
    private func congigureTabBar(){
        self.tabBarController?.tabBar.isHidden = true
    }
    
    private func configureNavBar(){
    }
    
//    private func configureTableView(){
//        self.tableView.addSubview(refreshControl)
//    }
    
    private func configureTitle(){
        self.navigationItem.title = "FOLLOWING".localized
        self.searchTextField.placeholder = "SEARCH_FOLLOWING_PLACEHOLDER".localized
    }
    
    private func reload(){
        //        self.refreshControl.beginRefreshing()
        self.currentPage = 0
        self.indicator.showActivityIndicator(uiView: self.view, offset: 0.0)
        Timer.scheduledTimer(timeInterval: ConstantsTime.indicatorMinimumTime, target: self, selector: #selector(self.loadInitialData), userInfo: nil, repeats: false)
    }
    
    @objc func loadInitialData(){
        self.loadData(searchTerm: self.searchTextField.text)
    }
    
//    @objc func refresh(_ sender:UIRefreshControl) {
//        self.loadData(searchTerm: self.searchTextField.text)
//    }
//    



    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    

    
    fileprivate func loadData(searchTerm:String?){
        if let user = currentUser{
            //            self.users = [User]()
            
            
            if self.searchTextField.text == ""{
                currentPage = self.users.count
            }else{
                currentPage = 0
                self.showEmptyDataSet(data: .Loading)
            }
            
            APIHelperAnyUser.getUserFollowings(user,searchFollower:searchTerm, skip: currentPage, limit: limitNumber) {[weak vc = self] (users) in
                
                if let users = users,searchTerm == self.searchTextField.text{
                    
                    if let limitNumber = vc?.limitNumber, users.count < limitNumber{
                        vc?.isEndOfPage = true // stop pagination
                    }else{
                        vc?.isEndOfPage = false
                    }
                    
                    if vc?.currentPage == 0{
                        vc?.users = users // initial load
                        vc?.currentPage = users.count

                        if users.count == 0 { // configure empty data set
                            searchTerm == "" ? vc?.showEmptyDataSet(data: .Empty) : vc?.showEmptyDataSet(data: .Search)
                        }else{
                            vc?.hideEmptyDataSet()
                        }
                    }else{
                        vc?.users += users // pagination
                        vc?.currentPage += users.count

                        
                    }
                }
            }
        }
    }
    
//     SHOW EMPTY SET
//    
//    
//     SHOW EMPTY SET
    
    private func showEmptyDataSet(data:EmptyDataSet){ // Lazy Init
        
        switch data{
        case .Loading:
            emptySetViewSearch?.discriptionLabel.text = "LOADING_PLACEHOLDER".localized
            self.tableView.backgroundView = emptySetViewSearch
        case .Search:
            emptySetViewSearch?.discriptionLabel.text = "NO_SEARCH_RESULTS".localized
            self.tableView.backgroundView = emptySetViewSearch
        case .Empty:
            if self.currentUser!.isCurrentUser(){
                emptySetView?.discriptionLabel.text = "NO_FOLLOWING_YET".localized
            } else {
                if let name = self.currentUser?.fullName{
                    emptySetView?.discriptionLabel.text = name + " " + "HASNT_FOLLOWED_ANYONE_YET".localized
                }
            }
            self.tableView.backgroundView = emptySetView
        }
        self.tableView.backgroundView?.isHidden = false
    }
    
    private func hideEmptyDataSet(){
        self.tableView.backgroundView?.isHidden = true
        
    }
    
    
    // MARK: -  //////////////////////////////////////////////////////   TEXTFIELD  DELEGATES  ///////////////////////////////////////////////////////////////////
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return (self.users.count == 0  && searchTextField.text == "") ? false : true
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if let searchTerm = textField.text{
            self.loadData(searchTerm: searchTerm)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        if let searchTerm = textField.text{
            loadData(searchTerm: searchTerm)
        }
        return false
    }
    
}

// MARK: -  //////////////////////////////////////////////////////   TABLE VIEW EXTENSION   /////////////////////////////////////////////////////////////////////////

extension UserFollowingViewController: UITableViewDataSource, UITableViewDelegate{
    
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DATA SOURCE   /////////////////////////////////////////////////////////////////////////
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == currentPage-1 && !isEndOfPage{
            loadData(searchTerm: self.searchTextField.text)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddFriendTableViewCell", for: indexPath) as! AddFriendTableViewCell
        let user = users[indexPath.row]
        cell.user = user
        //        cell.followingButton.layer.setValue(indexPath.row, forKey: "row")
        //        cell.followingButton.layer.setValue(indexPath.section, forKey: "section")
        //        cell.followingButton.addTarget(self, action:  #selector(UserFollowingViewController.followUnfollowAction(sender:)), for: .touchUpInside)
        return cell
    }
    
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DELEGATE   /////////////////////////////////////////////////////////////////////////
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let guestVC: UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
        let user = self.users[indexPath.row]
        guestVC.currentUser = user
        if self.searchTextField.isEditing{
            searchTextField.resignFirstResponder()
        }
        self.navigationController?.pushViewController(guestVC, animated: true)
    }
}
