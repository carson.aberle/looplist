//
//  UserProfileViewController.swift
//  Looplist
//
//  Created by Roman Wendelboe on 12/18/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate, UINavigationControllerDelegate, SectionHeaderSegmentedControlDelegate, UIViewControllerPreviewingDelegate{
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////
    
    var currentUser:User?
    var isUpdatingCollections:Bool = false
    var isUpdatingActivities:Bool = false
    var isUpdatingLikes:Bool = false
    
    var isEndOfPageCollections:Bool = false
    var isEndOfPageActivities:Bool = false
    var isEndOfPageLikes:Bool = false
    
    
    var refreshView: BreakOutToRefreshView!
    fileprivate var limitNumberLikes = 500 + 1
    fileprivate var limitNumberCollections = 100
    fileprivate var fakeNavBarView: UIView!
    fileprivate var fakeNavBar = FakeNavBar()
    fileprivate var collectionsTutorial: UserCollectionTutorial?
    
    fileprivate var collections = [Collection]()
//    {
//        didSet{
//            self.tableView.reloadData()
//        }
//    }
    
    fileprivate var activities = [Activity]()
    fileprivate var likes = [Product]()

    
    fileprivate enum ContentType {
        case collections
        case activity
        case likes
    }
    
    fileprivate var contentToDisplay = ContentType.collections
    @IBOutlet weak var addNewCollectionButton: UIButton!{
        didSet{
            self.addNewCollectionButton.layer.cornerRadius =  addNewCollectionButton.frame.height / 2.0
            //            addNewCollectionButton.clipsToBounds = true
            
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            self.tableView.tableFooterView = UIView()
            self.tableView.backgroundColor = UIColor.white
        }
    }
    
    @IBOutlet weak var header: UserProfileHeaderView!
    @IBOutlet weak var showSettings: UIBarButtonItem!
    @IBOutlet weak var showFindUsers: UIBarButtonItem!
    
    var shadowLayer: CAShapeLayer!
    
    @IBOutlet weak var addNewCollectionBackgroundView: UIImageView!
    @IBOutlet weak var addNewCollectionVisualEffectView: UIVisualEffectView!{
        didSet{
            
            addNewCollectionVisualEffectView.layer.masksToBounds = true
            self.addNewCollectionVisualEffectView.layer.cornerRadius =  addNewCollectionButton.frame.height / 2.0
        }
    }
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.currentUser == nil {
            if let user = CurrentUser.sharedInstance.currentUser{
                self.currentUser = user
            }
        }
        self.tableView.register(UINib(nibName: "ActivityTableViewCell", bundle: nil), forCellReuseIdentifier: "ActivityTableViewCell")
        
        header.user = currentUser
        
        self.setUpUserProfileImage()
        
        if( traitCollection.forceTouchCapability == .available){
            registerForPreviewing(with: self, sourceView: view)
        }
        
        if self.currentUser!.isProfilePrivate && !self.currentUser!.isCurrentUser() && (self.currentUser!.followRequestStatus == .request(true) || self.currentUser!.followRequestStatus == .request(false)){
            self.checkForPrivacy()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.configureNavigationButtonsAndTitle()
        self.updateAllInProfile()
        
        if let indexPath = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: indexPath, animated: false)
        }
        
        self.setUpPullToRefresh()
        
        if let navigationController = self.navigationController{
            UserProfileViewController.self.setNavigationBarStatusAndFont(.darkGray, font: ConstantsFont.navBarFontBold, navigationController: navigationController, statusBar: .default)
        }
        self.fakeNavBar.addFakeNavBarView("White", uView: self.view, offset: 0.0)
        
        if self.contentToDisplay != .collections{
            self.hideFab()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UserProfileViewController.startTimer()
        
        if let navigationController = self.navigationController{
            UserProfileViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFontBold, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
            if navigationController.viewControllers.count == 1 {
                UserProfileViewController.self.popSlideMotionOff(navigationController)
                self.tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 40, right: 0)
            } else {
                self.tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 49, right: 0)
            }
        }
        
        self.fakeNavBar.removeFakeNavBarView("White")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let objectId = self.currentUser?.objectId{
            UserProfileViewController.stopTimer(["user":objectId], withActionType:"View Screen")
        } else {
            UserProfileViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if refreshView != nil{
            refreshView.removeFromSuperview()
            refreshView = nil
        }
        
        // nav bar functions
        self.fakeNavBar.removeFakeNavBarView("White")
        UserProfileViewController.self.popSlideMotionOn(self.navigationController!)
    }
    
    override func viewDidLayoutSubviews() {
        // Dynamic sizing for the header view
        if let headerView = self.tableView.tableHeaderView {
            let height = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
            var headerFrame = headerView.frame
            
            if height != headerFrame.size.height {
                headerFrame.size.height = height
                headerView.frame = headerFrame
                self.tableView.tableHeaderView = headerView
            }
        }
    }
    
    func showFab(){
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.addNewCollectionButton.transform = CGAffineTransform.identity
            self.addNewCollectionVisualEffectView.transform = CGAffineTransform.identity
            self.addNewCollectionBackgroundView.transform = CGAffineTransform.identity
        })
    }
    
    func hideFab(){
        let when = DispatchTime.now() + 0.08
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.tableView.reloadData()
        }
        UIView.animate(withDuration: 0.25, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.addNewCollectionButton.transform = CGAffineTransform(translationX: 0, y: 100)
            self.addNewCollectionVisualEffectView.transform = CGAffineTransform(translationX: 0, y:100)
            self.addNewCollectionBackgroundView.transform = CGAffineTransform(translationX: 0, y:100)
            self.hideCollectionsTutorial()
        })
    }
    
    private func setUpPullToRefresh(){
        refreshView = BreakOutToRefreshView(scrollView: tableView)
        refreshView.refreshDelegate = self
        
        tableView.addSubview(refreshView)
    }
    
    private func updateAllInProfile(){
        if let user = self.currentUser{
            user.updateUser({ (success) in
                self.header.user = user
            })
        }
        
        switch contentToDisplay {
        case .collections:
            self.loadUserCollections(skip: 0)
        case .activity:
            self.loadUserActivities(skip: 0)
        case .likes:
            self.loadUserLikes(skip:0)
        }
    }
    
    private func configureNavigationButtonsAndTitle(){
        if let userName = self.currentUser?.userName{
            self.navigationItem.title = "@" + userName.lowercased()
        }
        
        if  self.navigationController?.viewControllers.count == 1{
            self.tabBarController?.tabBar.isHidden = false
            
            self.showFindUsers.isEnabled = true
            self.showFindUsers.tintColor = UIColor.darkGray
            
            self.showSettings.isEnabled = true
            self.showSettings.tintColor = UIColor.darkGray
            
            if self.currentUser!.isProfilePrivate && !self.currentUser!.isCurrentUser() && (self.currentUser!.followRequestStatus == .request(true) || self.currentUser!.followRequestStatus == .request(false)) {
                self.hideFab()
            } else {
                if self.currentUser!.objectId! != CurrentUser.sharedInstance.currentUser!.objectId!{
                    self.hideFab()
                } else {
                    self.showFab()
                }
            }
            
        } else {
            self.tabBarController?.tabBar.isHidden = true
            
            if let navigationController = self.navigationController{
                UserProfileViewController.self.popSlideMotionOn(navigationController)
            }
            self.hideFab()
            
            let backButton:UIBarButtonItem = UIBarButtonItem(image: UIImage(named: Constants.backImage), style: .plain, target: self, action: #selector(self.popViewController))
            self.navigationItem.leftBarButtonItem = backButton
            self.navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGray
            
            self.showSettings.isEnabled = false
            self.showSettings.tintColor = UIColor.clear
        }
        
    }
    
    fileprivate func showCollectionsTutorial(){
        if self.currentUser!.objectId! == CurrentUser.sharedInstance.currentUser!.objectId!{
            self.hideCollectionsTutorial()
            self.collectionsTutorial = UserCollectionTutorial.instanceFromNib()
            let position = CGPoint(x: 0, y: self.view.frame.height - (self.view.frame.width/1.73))
            let viewSize = CGSize(width: self.view.frame.width, height: (self.view.frame.width/4))
            self.collectionsTutorial?.frame = CGRect(origin: position, size: viewSize)
            self.view.addSubview(self.collectionsTutorial!)
        }
    }
    
    fileprivate func hideCollectionsTutorial(){
        if self.collectionsTutorial != nil {
            self.collectionsTutorial?.removeFromSuperview()
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func addNewCollection(_ sender: UIButton) {
        let addCollectionVC:AddCollectionViewController = AddCollectionViewController.instanceFromStoryboard("Main")
        self.navigationController?.pushViewController(addCollectionVC, animated: true)
    }
    
    @IBAction func showSettins(_ sender: UIBarButtonItem){
        let settingsVC:SettingsViewController = SettingsViewController.instanceFromStoryboard("Settings")
        self.navigationController?.pushViewController(settingsVC, animated: true)
    }
    
    @IBAction func showFindUsers(_ sender: UIBarButtonItem) {
        let findFriendsVC:FindFriendsTableViewController = FindFriendsTableViewController.instanceFromStoryboard("Main")
        self.navigationController?.pushViewController(findFriendsVC, animated: true)
    }
    
    @IBAction func openEditProfile(_ sender: UIButton) {
        if self.currentUser!.isCurrentUser(){
            let editProfileVC: EditProfileViewController = EditProfileViewController.instanceFromStoryboard("EditProfile")
            self.navigationController?.pushViewController(editProfileVC, animated: true)
        } else if let user = self.currentUser{
            ObjectStateHelper.userActionForFollowerFollowingButton(user: user, button: sender, grey: true)
        }
    }
    
    @IBAction func openFollowersVC(_ sender: UIButton) {
        let followersVC: UserFollowersViewController = UserFollowersViewController.instanceFromStoryboard("Main")
        followersVC.currentUser = self.currentUser
        self.navigationController?.pushViewController(followersVC, animated: true)
    }
    
    @IBAction func openFollowingVC(_ sender: UIButton) {
        let followingVC: UserFollowingViewController = UserFollowingViewController.instanceFromStoryboard("Main")
        followingVC.currentUser = self.currentUser
        self.navigationController?.pushViewController(followingVC, animated: true)
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        guard let offsetY = self.tableView?.contentOffset.y else { return nil }
        let newLocation = CGPoint(x: location.x, y: location.y + offsetY)
        guard let indexPath = self.tableView?.indexPathForRow(at: newLocation) else {return nil}
        
        guard let cell = tableView?.cellForRow(at: indexPath) as? UserProfileLikesTableViewCell else { return nil }
        
        guard let previewVC = storyboard?.instantiateViewController(withIdentifier: "ProductPreviewViewController") as? ProductPreviewViewController else { return nil }
        
        var product:Product
        if location.x < UIScreen.main.bounds.size.width/2 {
            if let productOne = cell.productOne {
                product = productOne
            } else {
                return nil
            }
        } else {
            if let productTwo = cell.productTwo {
                product = productTwo
            } else {
                return nil
            }
        }
        
        previewVC.callingClass = self
        previewVC.product = product
        
        let width = UIScreen.main.bounds.size.width - 100
        previewVC.preferredContentSize = CGSize(width: width, height: width)
        
        let collectionViewOffsetY = self.tableView!.contentOffset.y
        
        var sourceRect:CGRect
        if location.x < UIScreen.main.bounds.size.width/2 {
            sourceRect = CGRect(x: cell.frame.x, y: (cell.frame.y - collectionViewOffsetY), width: cell.frame.size.width/2, height: cell.frame.size.height)
        } else {
            sourceRect = CGRect(x: cell.frame.size.width/2, y: (cell.frame.y - collectionViewOffsetY), width: cell.frame.size.width/2, height: cell.frame.size.height)
        }
        
        if location.x < UIScreen.main.bounds.size.width/2 {
            previewVC.view.layer.setValue((indexPath.row * 2), forKey: "indexPathRow")
        } else {
            previewVC.view.layer.setValue(((indexPath.row * 2) + 1), forKey: "indexPathRow")
            
        }
        
        previewingContext.sourceRect = sourceRect
        return previewVC
    }
    
    //Delegate for 3d touch devices
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        if let index = viewControllerToCommit.view.layer.value(forKey: "indexPathRow") as? Int{
            if self.likes.count > index{
                let product = self.likes[index]
                let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
                productVC.product = product
                self.navigationController?.pushViewController(productVC, animated:false)
            }
        }
    }
    
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    @objc private func popViewController(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // SEGMENTED CONTROL FUNCTION
    
    func segmentedControl(selectedSegment: Int) {
        if selectedSegment == 0 {
            contentToDisplay = .collections
            
            if self.currentUser!.isProfilePrivate && !self.currentUser!.isCurrentUser() {
                self.hideFab()
                self.initializeEmptySet()
            } else {
                if self.currentUser!.objectId! != CurrentUser.sharedInstance.currentUser!.objectId!{
                    self.hideFab()
                } else {
                    self.showFab()
                }
                
                self.loadUserCollections(skip: 0)
            }
        } else if selectedSegment == 1{
            contentToDisplay = .likes
            self.hideFab()
            if self.currentUser!.isProfilePrivate && !self.currentUser!.isCurrentUser() {
                self.initializeEmptySet()
            } else {
                self.loadUserLikes(skip: 0)
            }
        }else {
            contentToDisplay = .activity
            self.hideFab()
            if self.currentUser!.isProfilePrivate && !self.currentUser!.isCurrentUser() {
                self.initializeEmptySet()
            } else {
                self.loadUserActivities(skip: 0)
            }
        }
        
        self.tableView.reloadData()
    }
    
    private func setUpUserProfileImage(){
        // IMAGE BUTTON
        
        if self.currentUser!.isCurrentUser(){
            let profilePictureTap = UITapGestureRecognizer(target:self,action: #selector(self.updateUserProfileImage(_:)))
            profilePictureTap.numberOfTapsRequired = 1
            self.header.userProfileImageView?.isUserInteractionEnabled = true
            self.header.userProfileImageView?.addGestureRecognizer(profilePictureTap)
        }
    }
    
    // IMAGE PICKER FUNCTION
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        let image = info[UIImagePickerControllerEditedImage] as? UIImage
        let imageData:Data = UIImageJPEGRepresentation(image!, 0.75)!
        let strBase64:String = imageData.base64EncodedString(options: .lineLength64Characters)
        
        APIHelperUser.updateCurrentUserProfilePicture(strBase64) { [weak vc = self](success) in
            if(success){
                vc?.header.userProfileImageView?.image = image
                
                // force refresh Kingfisher
                var sampleImageView = UIImageView()
                if let profileImageUrl = CurrentUser.sharedInstance.currentUser?.profileImageUrl{
                    if let url = URL(string: profileImageUrl){
                        sampleImageView.kf.setImage(with: url, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                        sampleImageView = UIImageView()
                    }
                }
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func updateUserProfileImage(_ recognizer:UITapGestureRecognizer){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    // COLECTIONS FUNCTIONS
    
    public func loadUserActivities(skip:Int){
        if !isUpdatingActivities{
            self.isUpdatingActivities = true
            
          
            
            APIHelperAnyUser.getUserActivities(user: self.currentUser!, skip: skip, limit: self.limitNumberCollections) { [weak vc = self](activities) in
                vc?.isUpdatingActivities = false
                if let activities = activities {
                    
                    if skip == 0{
                        vc?.activities = [Activity]()
                    }
                    
                    if let limit = vc?.limitNumberCollections , activities.count < limit{
                        vc?.isEndOfPageActivities = true
                    }else{
                        vc?.isEndOfPageActivities = false
                    }
                    
                    
                    if activities.count > 0{
                        vc?.activities += activities
                        vc?.tableView.backgroundView?.isHidden = true
                        vc?.tableView.reloadData()
                    } else if  skip == 0 && activities.count == 0{
                     
//                            vc?.activities.removeAll(keepingCapacity: false)
                        vc?.initializeEmptySet()
                    }
                }
            }
        }
    }
    
    public func loadUserCollections(skip:Int){
        
        if !isUpdatingCollections{
            self.isUpdatingCollections = true
         
            
            APIHelperAnyUser.getUserCollections(user: self.currentUser!, skip: skip, limit: self.limitNumberCollections) { [weak vc = self](collections) in
                vc?.isUpdatingCollections = false
                if let collections = collections {
                    
                    if skip == 0{
//                        self.collections.removeAll(keepingCapacity: false)
                        vc?.collections = [Collection]()
                    }
                    if let limit = vc?.limitNumberCollections,collections.count < limit{
                        vc?.isEndOfPageCollections = true
                    }else{
                        vc?.isEndOfPageCollections = false

                    }
                    if collections.count > 0{
                        vc?.collections += collections
                        vc?.tableView.backgroundView?.isHidden = true
                        vc?.hideCollectionsTutorial()
                        vc?.tableView.reloadData()
                    } else if skip == 0 && collections.count == 0{
                       // self.collections.removeAll(keepingCapacity: false)
         
                        vc?.initializeEmptySet()
                            
                        if vc?.currentUser!.objectId! == CurrentUser.sharedInstance.currentUser!.objectId!{
                            vc?.showCollectionsTutorial()
                        }
                   
                    }
                }
            }
        }
    }
    
    public func loadUserLikes(skip:Int){
        if !isUpdatingLikes{
            self.isUpdatingLikes = true
            
           
            APIHelperAnyUser.getUserLikes(user: self.currentUser!, skip: skip, limit: self.limitNumberLikes) { [weak vc = self](products) in
                vc?.isUpdatingLikes = false
                if let products = products {
                    
                    if skip == 0{
//                        self.likes.removeAll(keepingCapacity: false)
                        vc?.likes = [Product]()
                    }
                    
                    if let limit = vc?.limitNumberLikes, products.count < limit{
                        vc?.isEndOfPageLikes = true
                    }else{
                        vc?.isEndOfPageLikes = false

                    }
                    
                    if products.count > 0{
                        vc?.likes += products
                        vc?.tableView.backgroundView?.isHidden = true
                        vc?.tableView.reloadData()
                    } else if skip == 0 && products.count == 0{
//                        self.likes.removeAll(keepingCapacity: false)
                  
                            vc?.initializeEmptySet()
                  

                    }
                }
            }
        }
    }
    
    // CHECK FOR PRIVACY
    
    private func checkForPrivacy(){
        let emptySetView: EmptySetView = EmptySetView.instanceFromNib()
        emptySetView.emojiImageView.image = UIImage(named: Constants.lockedProfileImage)
        emptySetView.discriptionLabel.text = "USER_PRIVATE_INFO".localized
        self.tableView.backgroundView = emptySetView
        self.tableView.backgroundView?.isHidden = false
        self.tableView.bounces = false
        self.tableView.backgroundView?.bounds.top = -150
    }
    
    // SHOW EMPTY SET
    
    private func initializeEmptySet(){
        if self.currentUser!.isProfilePrivate && !self.currentUser!.isCurrentUser() && (self.currentUser!.followRequestStatus == .request(true) || self.currentUser!.followRequestStatus == .request(false)){
            self.checkForPrivacy()
        }else {
            let emptySetView: EmptySetView = EmptySetView.instanceFromNib()
            emptySetView.emojiImageView.image = UIImage(named: Constants.sadEmojiImage)
            switch contentToDisplay {
            case .collections:
                if self.currentUser!.isCurrentUser(){
                    emptySetView.discriptionLabel.text = "NO_COLLECTIONS_YET".localized
                } else {
                    if let name = self.currentUser?.fullName{
                        emptySetView.discriptionLabel.text = name + " " + "DOESNT_HAVE_COLLECTIONS_YET".localized
                    }
                }
            case .activity:
                if self.currentUser!.isCurrentUser(){
                    emptySetView.discriptionLabel.text = "NO_ACTIVITY_YET".localized
                } else {
                    if let name = self.currentUser?.fullName{
                        emptySetView.discriptionLabel.text = name + " " + "DOESNT_HAVE_ACTIVITY_YET".localized
                    }
                }
            case .likes:
                if self.currentUser!.isCurrentUser(){
                    emptySetView.discriptionLabel.text = "NO_LIKES_YET".localized
                } else {
                    if let name = self.currentUser?.fullName{
                        emptySetView.discriptionLabel.text = name + " " + "DOESNT_HAVE_LIKES_YET".localized
                    }
                }
            }
            
            self.tableView.backgroundView = emptySetView
            self.tableView.backgroundView?.isHidden = false
            self.tableView.bounces = false
            self.tableView.backgroundView?.bounds.top = -110
        }
    }
}

extension UserProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: -  ////////////////////////////////////////////////////// TABLE VIEW EXTENSION   /////////////////////////////////////////////////////////////////////////
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.currentUser != nil{
            switch contentToDisplay {
            case .collections:
                if self.currentUser!.isProfilePrivate && !self.currentUser!.isCurrentUser() && (self.currentUser!.followRequestStatus == .request(true) || self.currentUser!.followRequestStatus == .request(false)) {
                    return 0
                }else {
                    let count = collections.count
                    if count > 0 {
                        self.tableView.bounces = true
                        return count
                    } else {
                        self.tableView.bounces = false
                        return 0
                    }
                }
            case .activity:
                if self.currentUser!.isProfilePrivate && !self.currentUser!.isCurrentUser() && (self.currentUser!.followRequestStatus == .request(true) || self.currentUser!.followRequestStatus == .request(false)) {
                    return 0
                } else {
                    return self.activities.count
                }
            case .likes:
                if self.currentUser!.isProfilePrivate && !self.currentUser!.isCurrentUser() && (self.currentUser!.followRequestStatus == .request(true) || self.currentUser!.followRequestStatus == .request(false)) {
                    return 0
                } else {
                    print(self.likes.count)
                    if self.likes.count % 2 == 0{
                        return Int(ceil(Double(self.likes.count)/2))
                    }else{
                        print(Int(ceil(Double(self.likes.count+1)/2)))
                        return Int(ceil(Double(self.likes.count+1)/2))

                    }
                }
            }
        }
        return 0
    }
    
    private enum Cell:String {
        case single = "singleImageCell"
        case double = "doubleImageCell"
        case multiple = "multipleImagesCell"
        case activity = "activityCell"
        case header = "sectionHeader"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch contentToDisplay {
            
        case .collections:
            
            if collections.count > indexPath.row{
                //If count isnt zero, if it isn't the last page of collections (mod by the limit), if its within 5 of the end, and collections are not updating
                if self.collections.count > 0 && indexPath.row == self.collections.count - 1 && !isEndOfPageCollections{
                    self.loadUserCollections(skip: self.collections.count)
                }
                
                
                let collection = collections[indexPath.row]
                
                if let count = collection.products?.count{
                    switch count{
                        
                    case 1:
                        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.single.rawValue, for: indexPath)
                        if let collectionCell = cell as? UserProfileCollectionWithOneItemTableViewCell{
                            if  collections.count > indexPath.row{
                                collectionCell.collection = collections[indexPath.row]
                            }
                            return cell
                        }
                    case 2..<8:
                        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.double.rawValue, for: indexPath)
                        if let collectionCell = cell as? UserProfileCollectionWithTwoItemTableViewCell{
                           
                            if collections.count > indexPath.row{
                                
                                collectionCell.collection = collections[indexPath.row]
                            }
                            return cell
                        }
                    default:
                        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.multiple.rawValue, for: indexPath)
                        if let collectionCell = cell as? UserProfileCollectionWithMultipleItemTableViewCell{
                            if  collections.count > indexPath.row{
                                
                                collectionCell.collection = collections[indexPath.row]
                            }
                            return cell
                        }
                    }
                }
            }
            
            
            
        case .activity:
            
            if self.activities.count > 0  && indexPath.row == self.activities.count - 1 && !isEndOfPageActivities{
                self.loadUserActivities(skip: self.activities.count)
            }
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityTableViewCell", for: indexPath) as! ActivityTableViewCell
            if self.activities.count > indexPath.row{
                cell.callingClass = self
                cell.activity = self.activities[indexPath.row]
            }
            return cell
            
        case .likes:
            
            //If count isnt zero, if it isn't the last page of collections (mod by the limit), if its within 5 of the end, and collections are not updating
//            print("likes count-\(self.likes.count)")
//            print("index path row-\(indexPath.row)")

            if self.likes.count > 0 && indexPath.row == (self.likes.count / 2) - 1 && !isEndOfPageLikes{
                self.loadUserLikes(skip: self.likes.count)
            }
            
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserProfileLikesTableViewCell", for: indexPath) as! UserProfileLikesTableViewCell
            cell.userProfileVC = self
            cell.productOne = nil
            cell.productTwo = nil
            let indexOfOne = (indexPath.row * 2)
            let indexOfTwo = (indexPath.row * 2) + 1
            if self.likes.count > indexOfOne{
                let productOne = self.likes[indexOfOne]
                cell.productOne = productOne
                if let images = productOne.images{
                    if images.count > 0{
                        let imageUrl = images[0]
                        cell.imageOne.kf.setImage(with: URL(string:imageUrl))
                    }
                }
            }else{
               cell.imageOne.image = nil
            }
            if self.likes.count > indexOfTwo{
                print(indexPath.row)
                let productTwo = self.likes[indexOfTwo]
                cell.productTwo = productTwo
                if let images = productTwo.images{
                    if images.count > 0{
                        let imageUrl = images[0]
                        cell.imageTwo.kf.setImage(with: URL(string:imageUrl))
                    }
                }
            }else{
                 cell.imageTwo.image = nil
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserProfileActivityTableViewCell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerSection = tableView.dequeueReusableCell(withIdentifier:Cell.header.rawValue ) as! UserSegmentedControlSectionHeaderView
        
        switch (section) {
        case 0:
            headerSection.user = self.currentUser
            headerSection.delegate = self
            
            switch contentToDisplay {
            case .collections:
                headerSection.selectLeftSegment()
            case .likes:
                headerSection.selectMiddleSegment()
            case .activity:
                headerSection.selectRightSegment()
            }
            return headerSection
        default:
            let headerView = UIView()
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch contentToDisplay {
        case .collections:
            return 188
        case .activity:
            return 60
        case .likes:
            return UIScreen.main.bounds.size.width/2
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DELEGATE   /////////////////////////////////////////////////////////////////////////
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch contentToDisplay {
        case .activity:
            if self.activities.count > indexPath.row{
                let activity = self.activities[indexPath.row]
                if let product = activity.product{
                    let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
                    productVC.product = product
                    self.navigationController?.pushViewController(productVC, animated: true)
                }
            }
            break
        case .collections:
            if self.collections.count > indexPath.row{
                let collection = self.collections[indexPath.row]
                let collectionLikesCollectionViewController:CollectionLikesCollectionViewController = CollectionLikesCollectionViewController.instanceFromStoryboard("Main")
                collectionLikesCollectionViewController.collection = collection
                collectionLikesCollectionViewController.currentUser = self.currentUser
                if self.currentUser?.objectId != CurrentUser.sharedInstance.currentUser?.objectId {
                    collectionLikesCollectionViewController.shouldShowEditButton = false
                }
                self.navigationController?.pushViewController(collectionLikesCollectionViewController, animated: true)
            }
        case .likes:
            break
        }
    }
}

extension UserProfileViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.contentOffset.y > 0 && refreshView != nil){
            refreshView.endRefreshing()
        }
        if self.refreshView != nil{
            refreshView.scrollViewDidScroll(scrollView)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if self.refreshView != nil{
            refreshView.scrollViewWillEndDragging(scrollView, withVelocity: velocity, targetContentOffset: targetContentOffset)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if self.refreshView != nil{
            refreshView.scrollViewWillBeginDragging(scrollView)
        }
    }
}

extension UserProfileViewController: BreakOutToRefreshDelegate {
    
    func refreshViewDidRefresh(_ refreshView: BreakOutToRefreshView) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(NSEC_PER_SEC * 2)) / Double(NSEC_PER_SEC), execute: { () -> Void in
            refreshView.endRefreshing()
        })
    }
}
