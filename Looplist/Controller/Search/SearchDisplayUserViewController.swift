//
//  SearchDisplayUserViewController.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/6/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

protocol SearchDisplayUserProtocol:class {
//    func userSelected(_ user:User?)
    func resignKeyboard()
}

class SearchDisplayUserViewController: UIViewController {
    
    //MARK: - ------------------------------------------------------- PUBLIC FUNCTIONS/ VARIABLES -------------------------------------------------------
    
//    internal func setSearchTerm(_ searchTerm:String?){
//        self.userEnteredSearchTerm = searchTerm
//        
//    }
    
    var userEnteredSearchTerm:String?{
        didSet{
            if let userEnteredSearchTerm = userEnteredSearchTerm,userEnteredSearchTerm != "" {
                searchForUsers()
            }else{
                self.users = nil
            }
        }
    }

    
    weak var delegate:SearchDisplayUserProtocol?
    
    //MARK: - ------------------------------------------------------------------ OUTLETS -------------------------------------------------------------------
    
    @IBOutlet weak fileprivate var tableView: UITableView!{
    didSet{
        self.tableView.register(UINib(nibName: "AddFriendTableViewCell", bundle: nil), forCellReuseIdentifier: "AddFriendTableViewCell")
        }
    }
    
    
    //MARK: - ------------------------------------------------------------------ PRIVATE VARIABLES --------------------------------------------------------
    

    
    fileprivate var users:[User]?{
        didSet{
            self.tableView.reloadData()
        }
    }
    

    
    //MARK: - ------------------------------------------------------------------ LIFECYCLE -------------------------------------------------------------------
    
    override func viewDidAppear(_ animated: Bool) {
        SearchDisplayUserViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SearchDisplayUserViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - ------------------------------------------------------------------ FUNCTIONS -------------------------------------------------------------------
    
    
    fileprivate func searchForUsers(){
        if let localSearchTerm = self.userEnteredSearchTerm{
            
            APIHelper.searchGetUsers(localSearchTerm,skip:0, limit: ConstantsCount.searchResultCount) { [weak vc = self](users, error) in
                
                if error == nil{
                    Analytics.sharedInstance.actionsArray.append(Action(context: ["searchTerm":localSearchTerm], forScreen: nil, withActivityType:"Live Search User"))
                }
                if localSearchTerm == vc?.userEnteredSearchTerm{
                    if let users = users{
                        vc?.users = users
                    }else{
                        // no users
                    }
                }
            }
        }
    }
}

//MARK: -------------------------------------------------------------------- EXTENSION -------------------------------------------------------------------

extension SearchDisplayUserViewController: UITableViewDataSource,UITableViewDelegate{
    
    //MARK: - TABLE VIEW DATASOURCE
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let tableWidth = tableView.frame.size.width
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableWidth, height: 30))
        //        view.backgroundColor = Constants.looplistColor
        //        view.opaque = true
        let label = UILabel(frame: CGRect(x: 30, y: 5, width: tableWidth, height: 18))
        label.font = UIFont(name: ConstantsFont.fontDefaultBlack,size: 16)
        label.textColor = ConstantsColor.looplistGreyColor
        view.addSubview(label)
        if let _ = self.users{
            label.text =  "PEOPLE_SUGGESTIONS".localized
        }else{
            label.text =  ""
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let tableWidth = tableView.frame.size.width
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableWidth, height: 30))
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        let val = self.users?.count ?? 1
//       return self.users?.count ?? 1
//        
//        if self.users?.count > 0{
//            return
//        }
        
//        return (self.users?.count)! > 0 ? (self.users?.count)! : 1
        
        if let count = self.users?.count, count > 0{
            return count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let users = self.users, users.count > 0{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for:indexPath) as! SearchDisplayUserTableViewCell
//            let user = users[indexPath.row]
//            cell.user = user
//            return cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddFriendTableViewCell", for: indexPath) as! AddFriendTableViewCell
            
            let user = users[indexPath.row]
            cell.user = user
            return cell
            
            
        }else if let users = self.users, users.count == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptySearchCell", for:indexPath) // in Search Display VC
            cell.textLabel!.text = "WE_CANT_FIND".localized + " \(userEnteredSearchTerm ?? "") 😥"
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptySearchCell", for:indexPath) // in Search Display VC
            cell.textLabel!.text = ""
            return cell
        }
    }
    //MARK: - TABLE VIEW DELEGATE
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let users = users, users.count >= indexPath.row{
        if let user = self.users?[indexPath.row]{
            delegate?.resignKeyboard()
            let guestVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
            user.updateUser { (success) in
                guestVC.currentUser = user
                self.navigationController?.pushViewController(guestVC, animated: false)
            }
        }
        }
    }
}


