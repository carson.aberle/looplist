//
//  FiltersViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 11/7/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import CoreLocation

class FiltersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var panGestureRecognizer:UIPanGestureRecognizer?
    var initialPosition:CGPoint =  CGPoint(x: 0.0, y: 0.0)
    var currentPosition: CGPoint?
    
    
    var expandedSections:NSMutableArray = NSMutableArray()
    var filtersHeaderCell:FiltersHeaderTableViewCell!
    let overlayView:UIView = UIView()
    var filterOptionsDictionary:NSMutableDictionary = NSMutableDictionary()
    var filtersDictionary:NSMutableDictionary! = NSMutableDictionary()
    var productBuyVC:ProductBuyViewController?
    var discoverVC:DiscoverViewController?
    
    var products:[Product] = [Product]()
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var applyFiltersButton: UIButton!
    
    override func viewDidAppear(_ animated: Bool) {
        FiltersViewController.startTimer()
        UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.FILTER_POPUP_BACKGROUND_TAG)?.removeFromSuperview()
        UIApplication.shared.keyWindow?.viewWithTag(1235)?.removeFromSuperview()
        UIApplication.shared.keyWindow?.viewWithTag(ConstantsObjects.EXPAND_WIDTH_VIEW_TAG)?.removeFromSuperview()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        FiltersViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.applyFiltersButton.setTitle("APPLY_FILTERS".localized, for: .normal)
        
        self.setNavigationBar()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 80
        
        self.getFilterOptionsDictionary()
//        self.navigationItem.title = "Search Filters"
//        self.navigationController?.hidesBarsOnSwipe = false
//        self.tabBarController?.tabBar.isHidden = true
//        self.navigationController?.navigationBar.isTranslucent = true
//        let resetButton: UIBarButtonItem = UIBarButtonItem(title: "Reset", style: .plain, target: self, action: #selector(self.resetFilters))
//        self.navigationItem.rightBarButtonItem = resetButton
        
        self.addPanGesture()

    }
    
    func addPanGesture(){
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(FiltersViewController.handlePan(_:)))
        panGestureRecognizer?.cancelsTouchesInView = false
        let panGestureView = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 64))
        panGestureView.backgroundColor = UIColor.clear
        panGestureView.addGestureRecognizer(panGestureRecognizer!)
        self.view.addSubview(panGestureView)
        self.view.bringSubview(toFront: panGestureView)

    }
    
    func handlePan(_ panGesture: UIPanGestureRecognizer) {
        
        let panTranslation = panGesture.translation(in: view)
        
        if panTranslation.y > 0.0{
        
        if panGesture.state == .began {
            currentPosition = panGesture.location(in: view)
        } else if panGesture.state == .changed {
            view.frame.origin = CGPoint(
                x: 0.0,
                y: panTranslation.y
            )
        } else if panGesture.state == .ended {
            let velocity = panGesture.velocity(in: view)
            if velocity.y >= 1000 || self.view.frame.y > view.frame.height / 4.0 {
                if let discoverVC = self.discoverVC{
                    discoverVC.navigationController?.setNavigationBarHidden(false, animated: true)
                }
                UIView.animate(withDuration: 0.1
                    , animations: {
                        
                        self.view.frame.origin = CGPoint(
                            x: 0.0,
                            y: self.view.frame.size.height
                        )
                }, completion: { (isfinished) in
                    if isfinished {
                        if let discoverVC = self.discoverVC{
                            discoverVC.view.viewWithTag(ConstantsObjects.FILTER_POPUP_BACKGROUND_TAG)?.removeFromSuperview()
                        }
                        self.dismiss(animated: false, completion: nil)
                    }
                })
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.frame.y = self.initialPosition.y
                })
            }
        }
        }
    
    }
    
    @IBAction func saveFilters(_ sender: UIButton) {
        //Check for coordinates
        self.filtersDictionary.setValue(["minPrice":Int(self.filtersHeaderCell.rangeSlider.leftValue), "maxPrice":Int(self.filtersHeaderCell.rangeSlider.rightValue)], forKey: "price")

        self.closeViewController()
    }
    
     @IBAction func resetFilters(_ sender: UIButton) {
//    func resetFilters(){
        self.filtersDictionary.setValue(["minPrice":0, "maxPrice":1000], forKey: "price")
        self.filtersDictionary.removeAllObjects()
        self.closeViewController()
    }
    
    func closeViewController(){
//        if let buyVC = self.productBuyVC{
//            Analytics.sharedInstance.actionsArray.append(Action(context: ["filters":self.filtersDictionary], forScreen: "Product Buy", withActivityType:"Filter"))
////            buyVC.appliedFiltersDictionary = self.filtersDictionary
//            //buyVC.getListingsWithFilters()
//            self.dismiss(animated: true, completion: nil)
//        }
        if let discoverVC = self.discoverVC{
            Analytics.sharedInstance.actionsArray.append(Action(context: ["filters":self.filtersDictionary], forScreen: "Discovery", withActivityType:"Filter"))
//            discoverVC.appliedFiltersDictionary = self.filtersDictionary
//            discoverVC.getListingsWithFilters()
            discoverVC.view.viewWithTag(ConstantsObjects.FILTER_POPUP_BACKGROUND_TAG)?.removeFromSuperview()
            discoverVC.navigationController?.setNavigationBarHidden(false, animated: true)
            self.dismiss(animated: true, completion: nil)
        }
    }
    func getFilterOptionsDictionary(){

        for product in self.products{
            if let attributes = product.attributes{
                for productAttribute in attributes{
                    if self.filterOptionsDictionary.object(forKey: productAttribute.key) != nil{
                        if var alreadyInAttributes = self.filterOptionsDictionary.object(forKey: productAttribute.key) as? [String]{
                            if let productsToAdd = productAttribute.value as? [String]{
                                for productIndividualAttribute in productsToAdd{
                                    if !alreadyInAttributes.contains(productIndividualAttribute){
                                        alreadyInAttributes.append(productIndividualAttribute)
                                    }
                                }
                                self.filterOptionsDictionary.setValue(alreadyInAttributes, forKey: productAttribute.key)
                            }
                        }
                    } else {
                        self.filterOptionsDictionary.addEntries(from: attributes)
                    }
                }

            }
        }
        self.sortFilterOptions()
    }
    
    func sortFilterOptions(){
        //Sort in ascending order if number and alphabetical otherwise
        for attributes in self.filterOptionsDictionary{
            if let valuesArray = attributes.value as? [String]{
                let sortedArray = valuesArray.sorted{ $0.localizedStandardCompare($1) == ComparisonResult.orderedAscending }
                self.filterOptionsDictionary.setValue(sortedArray, forKey: attributes.key as! String)
            }
        }
        self.tableView.reloadData()
    }
    
    @IBAction func closeViewController(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNavigationBar(){
        if let navigationController = self.navigationController {
            ProductTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.clearNoGradient)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    override func viewWillLayoutSubviews() {
        self.setNavigationBar()
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1 + self.filterOptionsDictionary.count
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //KEEP THIS -Carson
        //        if(section == 0){
        //            if(self.expandedSections.containsObject(section)){
        //                return 2
        //            } else {
        //              return 1
        //            }
        //        } else {
        //            if(self.expandedSections.containsObject(section)){
        //                let key = self.filterOptionsDictionary.allKeys[section - 1] as! String
        //                return self.filterOptionsDictionary[key]!.count + 1
        //            } else {
        //                return 1
        //            }
        //        }
        if(self.expandedSections.contains(section)){
            let key = self.filterOptionsDictionary.allKeys[section - 1] as! String// -1 for locations
            return (self.filterOptionsDictionary[key]! as AnyObject).count + 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //KEEP THIS -Carson
        //        if(indexPath.section == 0 && indexPath.row == 1){
        //            if(self.selectedRadio == "Online"){
        //                return 90
        //            } else {
        //                if(self.currentLocation == nil && self.googleMapsPlace == nil){
        //                    return 138
        //                } else {
        //                    return 220
        //                }
        //            }
        //        } else {
        //            return UITableViewAutomaticDimension
        //        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FiltersHeaderTableViewCell", for: indexPath) as! FiltersHeaderTableViewCell
            self.filtersHeaderCell = cell
            if let priceFilter = self.filtersDictionary.object(forKey: "price") as? NSDictionary{
                if let maxPrice = priceFilter.object(forKey: "maxPrice") as? Int, let minPrice = priceFilter.object(forKey: "minPrice") as? Int{
                    cell.rangeSlider.setLeftValue(CGFloat(minPrice), rightValue: CGFloat(maxPrice))
                    cell.valueChanged(cell.rangeSlider)
                } else {
                    cell.rangeSlider.setLeftValue(0, rightValue: 1000)
                    cell.valueChanged(cell.rangeSlider)
                }
            } else {
                cell.rangeSlider.setLeftValue(0, rightValue: 1000)
                cell.valueChanged(cell.rangeSlider)
            }
            cell.rangeSlider.setMinValue(0, maxValue: 1000)
            cell.rangeSlider.backgroundColor = UIColor.clear
            cell.rangeSlider.tintColor = ConstantsColor.looplistColor
            return cell
            
        }else {
            if(indexPath.row == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "FiltersTextTableViewCell", for: indexPath) as! FiltersTextTableViewCell
                cell.filterTitleText.text = (self.filterOptionsDictionary.allKeys[indexPath.section - 1] as! String).capitalized
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FiltersTextCheckTableViewCell", for: indexPath) as! FiltersTextCheckTableViewCell
                let key = self.filterOptionsDictionary.allKeys[indexPath.section - 1] as! String // -1 for locations
                
                if let attributeMain = self.filterOptionsDictionary[key] as? NSArray{
                    cell.filterAttributeText.text = (attributeMain[indexPath.row - 1] as! String).capitalized.trimmingCharacters(in: .whitespacesAndNewlines)
                }
                if let attributeValueArray = self.filterOptionsDictionary.object(forKey: key) as? NSArray{
                    let selectedValue = attributeValueArray.object(at: indexPath.row - 1)
                    if let selectedFiltersValueArray = self.filtersDictionary.value(forKey: key) as? NSArray{
                        if(selectedFiltersValueArray.contains(selectedValue)){
                            cell.filterAttributeText.textColor = ConstantsColor.looplistColor
                        } else {
                            cell.filterAttributeText.textColor = UIColor.black
                        }
                    } else {
                        cell.filterAttributeText.textColor = UIColor.black
                    }
                } else {
                    cell.filterAttributeText.textColor = UIColor.black
                }
                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //KEEP THIS -Carson
        /**
         if indexPath.section == 0{
         if indexPath.row == 0{
         if self.expandedSections.containsObject(indexPath.section){
         self.expandedSections.removeObject(indexPath.section)
         self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow:1, inSection: 0)], withRowAnimation: .Top)
         } else {
         self.expandedSections.addObject(indexPath.section)
         self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow:1, inSection: 0)], withRowAnimation: .Top)
         }
         }
         } else {
         //Copy below here for locations added back
         }
         **/
        if indexPath.section == 0{
            
        } else {
            if indexPath.row == 0{
                if self.expandedSections.contains(indexPath.section){
                    self.expandedSections.remove(indexPath.section)
                    var indexPaths:[IndexPath] = [IndexPath]()
                    let key = self.filterOptionsDictionary.allKeys[indexPath.section - 1] as! String// -1 for locations
                    for i in 0..<(self.filterOptionsDictionary[key]! as AnyObject).count{
                        indexPaths.append(IndexPath(row:i+1, section:indexPath.section))
                    }
                    self.tableView.deleteRows(at: indexPaths, with: .top)
                    
                } else {
                    self.expandedSections.add(indexPath.section)
                    var indexPaths:[IndexPath] = [IndexPath]()
                    let key = self.filterOptionsDictionary.allKeys[indexPath.section - 1] as! String // -1 for locations
                    for i in 0..<(self.filterOptionsDictionary[key]! as AnyObject).count{
                        indexPaths.append(IndexPath(row:i+1, section:indexPath.section))
                    }
                    self.tableView.insertRows(at: indexPaths, with: .top)
                }
            } else {
                //Check if this is in the filter
                let key = self.filterOptionsDictionary.allKeys[indexPath.section - 1] as! String // -1 for locations
                if let attributeValueArray = self.filterOptionsDictionary.object(forKey: key) as? NSArray{
                    let selectedValue = attributeValueArray.object(at: indexPath.row - 1)  as! String // -1 for locations
                    if var selectedFiltersValueArray = self.filtersDictionary.value(forKey: key)as? [String]{
                        if let index = selectedFiltersValueArray.index(of: selectedValue){
                            selectedFiltersValueArray.remove(at: index)
                        } else {
                            selectedFiltersValueArray.append(selectedValue)
                        }
                        self.filtersDictionary.setValue(selectedFiltersValueArray, forKey: key)
                    } else {
                        var array:[String] = [String]()
                        array.append(selectedValue)
                        self.filtersDictionary.setValue(array, forKey: key)
                    }
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                    
                } else {
                    ErrorHandling.customErrorHandler(message: "ERROR_ADDING_FILTER".localized)
                }
                
            }
        }
        
    }
}

extension FiltersViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        overlayView.backgroundColor = UIColor.darkGray
        var x = UIScreen.main.bounds
        x.origin.y = -64
        overlayView.frame = x
        overlayView.alpha = 0.0
        self.view.addSubview(overlayView)
        
        //        self.controller = GooglePlacesSearchController(apiKey: "AIzaSyDU5uCtiZinOPUS8gnk0yzamYdFGTXzZEI")
        //        self.controller.delegate = self
        //        self.controller.searchBar.delegate = self
        //        self.controller.didSelectGooglePlace { (place) -> Void in
        //            UIView.animateWithDuration(0.4, delay: 0.1,options: UIViewAnimationOptions.CurveEaseOut, animations: {self.overlayView.alpha = 0.0 }, completion: {(success) in
        //                self.overlayView.removeFromSuperview()
        //            })
        //            self.currentLocation = nil
        //            self.currentLocationPlacemark = nil
        //            self.googleMapsPlace = place
        //            self.controller.dismissViewControllerAnimated(true, completion: nil)
        //            self.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow:1, inSection: 0)], withRowAnimation: .None)
        //        }
        //        self.presentViewController(controller, animated: true, completion: nil)
        UIView.animate(withDuration: 0.5, delay: 0.0,options: UIViewAnimationOptions.curveEaseOut, animations: {self.overlayView.alpha = 1.0 }, completion: {(success) in})
        
    }
}
extension FiltersViewController:UISearchBarDelegate{
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.overlayView.removeFromSuperview()
        self.tableView.reloadRows(at: [IndexPath(row:1, section: 0)], with: .none)
    }
}

extension FiltersViewController:UISearchControllerDelegate{
    func willDismissSearchController(_ searchController: UISearchController) {
        UIView.animate(withDuration: 0.4, delay: 0.1,options: UIViewAnimationOptions.curveEaseOut, animations: {self.overlayView.alpha = 0.0 }, completion: {(success) in
            self.overlayView.removeFromSuperview()
        })
        
    }
    
}
