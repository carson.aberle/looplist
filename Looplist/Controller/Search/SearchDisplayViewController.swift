//
//  SearchDisplayViewController.swift
//  Looplist
//
//  Created by Arun Sivakumar on 11/8/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import RealmSwift


protocol SearchDisplayProtocol:class {
    
    func userSelectedSearchTerm(_ searchTerm:String)
    func resignKeyboard()
    
}

class SearchDisplayViewController: UIViewController {
    
    //MARK: - ------------------------------------------------------- PUBLIC FUNCTIONS/ VARIABLES -------------------------------------------------------
    
    internal func setSearchTerm(_ searchTerm:String?){
        self.userEnteredSearchTerm = searchTerm
        
    }
    
    weak var delegate:SearchDisplayProtocol?
    
    
    //MARK: - ------------------------------------------------------------------ PRIVATE VARIABLES --------------------------------------------------------
    
    fileprivate var userEnteredSearchTerm:String?{
        didSet{
            // call API functions
            if let userEnteredSearchTerm = userEnteredSearchTerm, userEnteredSearchTerm == ""{
                showRecentSearch = true
            }else{
                showRecentSearch = false
            }
        }
    }
    
    fileprivate var showRecentSearch = true{
        didSet{
            showRecentSearch ? showRecentSearchResults() : searchForUsersAndProducts()
        }
    }
        
    fileprivate var productsCount:Int?{
        didSet{
            self.tableView.reloadData()
        }
    }
    
    fileprivate var users:[User]?{
        didSet{
            self.tableView.reloadData()
        }
    }
    
    
    fileprivate var userRecentSearchResults:Results<UserRecentSearch>!{
        didSet{
//            if userRecentSearchResults.count > 0{
                self.tableView.reloadData()
//            }
        }
    }
    
    //MARK: - ------------------------------------------------------------------ OUTLETS -------------------------------------------------------------------
    
    @IBOutlet weak fileprivate var tableView: UITableView!{
        didSet{
            self.tableView.register(UINib(nibName: "GoToAddProductTableViewCell", bundle: nil), forCellReuseIdentifier: "GoToAddProductTableViewCell")
            self.tableView.register(UINib(nibName: "AddFriendTableViewCell", bundle: nil), forCellReuseIdentifier: "AddFriendTableViewCell")

        }
    }
    
    
    //MARK: - ------------------------------------------------------------------ LIFECYCLE -------------------------------------------------------------------
    
    override func viewDidAppear(_ animated: Bool) {
        SearchDisplayViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SearchDisplayViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - ------------------------------------------------------------------ FUNCTIONS -------------------------------------------------------------------
    
    fileprivate func showRecentSearchResults(){
        
        if let userRecentSearchResults = RealmHelper.getUserRecentSearch(){
            self.userRecentSearchResults = userRecentSearchResults
        }
    }
    
//    if users.count > 0 && users[0].userName == "ryan"{
//    usersm += users
//    usersm += users
//    
//    usersm += users
//    
//    usersm += users
//    
//    usersm += users
//    
//    usersm += users
//    
//    usersm += users
//    
//    usersm += users
//    usersm += users
//    usersm += users
//    usersm += users
//    usersm += users
//    usersm += users
//    usersm += users
//    usersm += users
//    vc?.users = usersm
//    
//    
//    }else{
//    vc?.users = users
//    }

    weak var analytics = Analytics.sharedInstance
    
    fileprivate func searchForUsersAndProducts(){
        if let localSearchTerm = self.userEnteredSearchTerm{
            //self.productsCount = nil
           // self.users = [User]()
            
            APIHelper.searchGetProductsCount(localSearchTerm,skip:0, limit:1) { [weak vc = self](productsCount,error) in //$CS use product attributes for filter
                if let productsCount = productsCount{
                    vc?.analytics?.actionsArray.append(Action(context: ["searchTerm":localSearchTerm], forScreen: nil, withActivityType:"Live Search Product"))
                    vc?.productsCount = productsCount
                }else{
                    // emptydata set
                }
            }
            
            APIHelper.searchGetUsers(localSearchTerm,skip:0, limit: ConstantsCount.searchResultCount) { [weak vc = self](users, error) in
                
                if error == nil{
                   vc?.analytics?.actionsArray.append(Action(context: ["searchTerm":localSearchTerm], forScreen: nil, withActivityType:"Live Search User"))
                }
                if localSearchTerm == vc?.userEnteredSearchTerm{
                    if let users = users{
                         vc?.users = users
                    }else{
                        // no users
                    }
                }
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowAddProducts"{
            if let nvc = segue.destination as? UINavigationController{
                if let vc = nvc.childViewControllers[0] as? AddProductWebViewController{
                    vc.userEnteredURL = self.userEnteredSearchTerm
                }
            }
        }
    }
}

//MARK: -------------------------------------------------------------------- EXTENSION -------------------------------------------------------------------

extension SearchDisplayViewController: UITableViewDataSource,UITableViewDelegate{
    
    //MARK: - TABLE VIEW DATASOURCE
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let tableWidth = tableView.frame.size.width
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableWidth, height: 30))
        let label = UILabel(frame: CGRect(x: 30, y: 5, width: tableWidth, height: 18))
        label.font = UIFont(name: ConstantsFont.fontDefaultBlack,size: 16)
        label.textColor = ConstantsColor.looplistGreyColor
        view.addSubview(label)
        if showRecentSearch == false{
            if section == 0{
                label.text = "PRODUCT_SUGGESTIONS".localized
            }
            else if section == 1{
                label.text = "PEOPLE_SUGGESTIONS".localized
            }
            view.backgroundColor = UIColor.white
        }else if showRecentSearch == true{
            if userRecentSearchResults.count > 0{
                label.text = "RECENT_PRODUCT_SEARCHES".localized
                label.font = UIFont(name: ConstantsFont.fontDefaultBlack,size: 16)
                label.textColor = ConstantsColor.looplistGreyColor
            }else{
                label.text = ""
                label.textColor = UIColor.clear
            }
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let tableWidth = tableView.frame.size.width
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableWidth, height: 30))
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if showRecentSearch == false{
            
            switch indexPath.section{
            case 0:
                return 50
                
            case 1:
                return 58
            default:
                return 0
            }
        }else if showRecentSearch == true{
            if userRecentSearchResults.count > 0{
                return 40
            }else{
                return 100
            }
        }
        return 30
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if showRecentSearch == false{
            return 2
        }else if showRecentSearch == true{
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if showRecentSearch == false{
            switch section{
            case 0:
                return 1
            case 1:
                if let count = self.users?.count, count > 0{
                    return count
                }else{
                    return 1
                }
            default:
                break
            }
        }else if showRecentSearch == true{
                return userRecentSearchResults.count > 0 ? (userRecentSearchResults.count > 6 ? 6 : userRecentSearchResults.count)  : 1
        }
        // Default Fallback case - empty data set
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if showRecentSearch == false{
            switch indexPath.section{
                
            case 0:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for:indexPath) as! SearchDisplayProductTableViewCell
                cell.productBrandName = "\"" + (self.userEnteredSearchTerm ?? "") + "\""
                cell.productCount = self.productsCount ?? 0
                return cell
                
            case 1:
                // user
                if let users = self.users, users.count > 0{
//                    let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for:indexPath) as! SearchDisplayUserTableViewCell
//
//                    let user = users[indexPath.row]
//                    cell.user = user
//                    return cell
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AddFriendTableViewCell", for: indexPath) as! AddFriendTableViewCell
                    
                    let user = users[indexPath.row]
                    cell.user = user
                    return cell

                }else{
                    if let userEnteredSearchTerm = self.userEnteredSearchTerm{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "EmptySearchCell", for:indexPath)
                        cell.textLabel!.text = "WE_CANT_FIND".localized + " \(userEnteredSearchTerm) 😥"
                        return cell
                    }
                }
            default:
                break
            }
        }else if showRecentSearch == true && userRecentSearchResults.count > 0{
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RecentSearchCell", for:indexPath) as? SearchDisplayRecentTableViewCell{
                let row = indexPath.row
                let userRecentSearch = userRecentSearchResults[row] as UserRecentSearch
                cell.productNameLabel.text = userRecentSearch.searchTerm
                cell.productCountLabel.text = HelperFunction.getResultString(count: userRecentSearch.searchTermCount)
                return cell
            }else{
                
            }
            
        }
        
        // Default Fallback case - empty data set
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MavenCell", for:indexPath)
        
        cell.textLabel!.text = "NO_RECENT_SEARCH_RESULTS".localized
        cell.textLabel!.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.textLabel!.numberOfLines = 3
        return cell
        
    }
    //MARK: - TABLE VIEW DELEGATE
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

            if showRecentSearch == true{ // Showing recent searches

                let userRecentSearch = userRecentSearchResults[indexPath.row] as UserRecentSearch
                delegate?.userSelectedSearchTerm(userRecentSearch.searchTerm)
                
            }else if showRecentSearch == false{
                
                switch indexPath.section{
                case 0:
                    if let userEnteredSearchTerm = userEnteredSearchTerm{
                        delegate?.userSelectedSearchTerm(userEnteredSearchTerm)
                    }


                case 1:
                    if let users = users, users.count > indexPath.row{
                        if let user = self.users?[indexPath.row]{
                            delegate?.resignKeyboard()
                            let guestVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
                            user.updateUser { (success) in
                                guestVC.currentUser = user
                                self.navigationController?.pushViewController(guestVC, animated: false)
                            }
                        }
                    }

                default:
                    break
                }
         }
        
      
    }
}

