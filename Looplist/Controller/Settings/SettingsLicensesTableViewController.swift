//
//  SettingsLicensesTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 8/12/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

open class SettingsLicensesTableViewController:UITableViewController {
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////
    
    private var podTitles:[String] = ["Lottie","TKSubmitTransition","RealmSwift", "Branch", "BreakOutToRefresh","Kingfisher", "Stripe", "Alamofire", "SwiftyJSON", "CreditCardValidator", "KTCenterFlowLayout", "HTMLAttributedString", "CWPopup", "SwiftyDrop", "MARKRangeSlider"]
    
    private var licenseTextHeaders:[String]! = ["Apache License\nVersion 2.0, January 2004/nhttp://www.apache.org/licenses/","The MIT License (MIT)\r\n\nCopyright (c) 2015 Takuya Okamoto","","The MIT License (MIT)\r\n\nCopyright (c) 2015 Branch Metrics, Inc.", "Copyright (c) 2015 Dominik Hauser <dominik.hauser@dasdom.de>", "The MIT License (MIT)\r\n\r\nCopyright (c) 2015 Wei Wang", "The MIT License\r\n\r\nCopyright (c) 2011- Stripe, Inc. (https://stripe.com)", "Copyright (c) 2014-2016 Alamofire Software Foundation (http://alamofire.org/)", "The MIT License (MIT)\r\n\r\nCopyright (c) 2014 Ruoyu Fu", "The MIT License (MIT)\r\nCopyright (c) 2014 Hearst", "Copyright (c) keighl (http://github.com/keighl)", "Copyright (c) 2015 Maxime Epain <maxime.epain@gmail.com>","","The MIT License (MIT)\r\n\r\nCopyright (c) 2013 Cezary Wojcik <http://www.cezarywojcik.com>", "The MIT License (MIT)\r\n\r\nCopyright (c) 2015 Morita Naoki", "The MIT License (MIT)\r\n\r\nCopyright (c) 2015 Vadym Markov"]
    
    private var licenseType:[String]! = ["APACHE 2.0","MIT","APACHE 2.0", "MIT","MIT","MIT", "MIT", "MIT", "MIT", "MIT","MIT", "MIT", "UNLICENSE", "MIT", "MIT", "MIT"]
    
    private var unlicense:String = "This is free and unencumbered software released into the public domain.\r\n\r\nAnyone is free to copy, modify, publish, use, compile, sell, or\r\ndistribute this software, either in source code form or as a compiled\r\nbinary, for any purpose, commercial or non-commercial, and by any\r\nmeans.\r\n\r\nIn jurisdictions that recognize copyright laws, the author or authors\r\nof this software dedicate any and all copyright interest in the\r\nsoftware to the public domain. We make this dedication for the benefit\r\nof the public at large and to the detriment of our heirs and\r\nsuccessors. We intend this dedication to be an overt act of\r\nrelinquishment in perpetuity of all present and future rights to this\r\nsoftware under copyright law.\r\n\r\nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,\r\nEXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\r\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.\r\nIN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR\r\nOTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,\r\nARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR\r\nOTHER DEALINGS IN THE SOFTWARE.\r\n\r\nFor more information, please refer to <http://unlicense.org>"
    
    private var mitLicense:String = "\n\nPermission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\r\n\r\nThe above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\r\n\r\nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
    
    private var apache2License:String = ""
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE  /////////////////////////////////////////////////////////////////////////
    
    override open func viewDidAppear(_ animated: Bool) {
        SettingsLicensesTableViewController.startTimer()
    }
    
    override open func viewDidDisappear(_ animated: Bool) {
        SettingsLicensesTableViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override open func viewDidLoad() {
        self.navigationItem.title = "LICENSES".localized
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 150
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController{
            SettingsLicensesTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   NAVIGATION   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func backButton(_ sender: UIBarButtonItem){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DATA SOURCE   /////////////////////////////////////////////////////////////////////////
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return podTitles.count
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LicenseTableViewCell") as! LicenseTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.projectTitle.text = podTitles[indexPath.row]
        var licenseBody:String = self.licenseTextHeaders[indexPath.row]
        switch(licenseType[indexPath.row])
        {
        case "MIT":
            licenseBody += self.mitLicense
            break
        case "Apache 2.0":
            licenseBody += self.apache2License
            break
        case "UNLICENSE":
            licenseBody += self.unlicense
            break
        default:
            licenseBody += self.mitLicense
            break
        }
        cell.licenseBody.text = licenseBody
        cell.licenseBody.sizeToFit()
        return cell
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
