//
//  UserProfileSettingsHelpViewController.swift
//  Looplist
//
//  Created by Arun on 4/22/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import UIKit

class SettingsHelpViewController: UITableViewController{
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        self.navigationItem.title = "HELP".localized
    }
    override func viewDidAppear(_ animated: Bool) {
        SettingsHelpViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SettingsHelpViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController{
            SettingsHelpViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////

    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "HELP".localized
    }
    @IBAction func openMailApp(_ sender: UIButton) {
        let recipients = "feedback@looplist.com"
        if let url = URL(string: "mailto:\(recipients)"){
            UIApplication.shared.openURL(url)
        }else{
            ErrorHandling.customErrorHandlerWithTitle(title: "SORRY".localized, message: "CANT_PROCESS_REQUEST".localized)
        }
    }
}
