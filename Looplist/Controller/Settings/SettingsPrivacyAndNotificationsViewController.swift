//
//  SettingsPushNotificationsViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 8/24/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class SettingsPrivacyAndNotificationsViewController:UIViewController{
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var privateProfileSwitch: UISwitch!
    @IBOutlet weak var messagesSwitch: UISwitch!
    @IBOutlet weak var followsSwitch: UISwitch!
    @IBOutlet weak var followRequestsSwitch: UISwitch!
    @IBOutlet weak var acceptedfollowRequestsSwitch: UISwitch!
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var privacyLabel: UILabel!
    @IBOutlet weak var privateProfileLabel: UILabel!
    @IBOutlet weak var pushNotificationsLabel: UILabel!
    @IBOutlet weak var followsLabel: UILabel!
    @IBOutlet weak var messagesLabel: UILabel!
    @IBOutlet weak var followRequestsLabel: UILabel!
    @IBOutlet weak var acceptedFollowRequestsLabel: UILabel!
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveSettings(_ sender: UIBarButtonItem) {
        sender.isEnabled = false

        if let currentObjectId = CurrentUser.sharedInstance.currentUser?.objectId{
            DBHelper.callAPIPut("users/\(currentObjectId)", params: ["notifications":["follows":self.followsSwitch.isOn, "messages":self.messagesSwitch.isOn,"followRequests":self.followRequestsSwitch.isOn,"acceptedFollowRequests":self.acceptedfollowRequestsSwitch.isOn] as AnyObject, "isPrivate":self.privateProfileSwitch.isOn as AnyObject]) { (result, error) in
                if error == nil{
                    Analytics.sharedInstance.actionsArray.append(Action(context:["pushPrivacy": true], forScreen:nil, withActivityType:"Updated Settings"))
                    CurrentUser.sharedInstance.currentUser?.updateUser({ (success) in
                        ErrorHandling.customSuccessHandler(message: "SUCCESSFULLY_UPDATED_PUSH_SETTINGS".localized)
                        _ = self.navigationController?.popViewController(animated: true)
                    })
                } else {
                    ErrorHandling.customErrorHandler(message: "ERROR_UPDATING_PUSH_SETTINGS".localized)
                    sender.isEnabled = true
                }
            }
        } else {
            ErrorHandling.customErrorHandler(message: "ERROR_UPDATING_PUSH_SETTINGS".localized)
            sender.isEnabled = true
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    
    override func viewDidAppear(_ animated: Bool) {
        SettingsPrivacyAndNotificationsViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SettingsPrivacyAndNotificationsViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewDidLoad(){
        loadUI()
        
        self.navigationItem.title = "PUSH_NOTIFICATIONS".localized
        self.saveButton.title = "SAVE".localized
        self.privacyLabel.text = "PRIVACY".localized
        self.privateProfileLabel.text = "PRIVATE_PROFILE".localized
        self.pushNotificationsLabel.text = "PUSH_NOTIFICATIONS".localized
        self.followsLabel.text = "FOLLOWS".localized
        self.messagesLabel.text = "MESSAGES".localized
        self.followRequestsLabel.text = "FOLLOW_REQUESTS".localized
        self.acceptedFollowRequestsLabel.text = "ACCEPTED_FOLLOW_REQUESTS".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController{
            SettingsPrivacyAndNotificationsViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    func loadUI(){
        if CurrentUser.sharedInstance.currentUser?.followNotifications == true{
            self.followsSwitch.isOn = true
        } else {
            self.followsSwitch.isOn = false
        }
        
        if CurrentUser.sharedInstance.currentUser?.messageNotifications == true{
            self.messagesSwitch.isOn = true
        } else {
            self.messagesSwitch.isOn = false
        }
        
        if CurrentUser.sharedInstance.currentUser?.followRequestsNotifications == true{
            self.followRequestsSwitch.isOn = true
        } else {
            self.followRequestsSwitch.isOn = false
        }
        
        if CurrentUser.sharedInstance.currentUser?.acceptedfollowRequestsNotifications == true{
            self.acceptedfollowRequestsSwitch.isOn = true
        } else {
            self.acceptedfollowRequestsSwitch.isOn = false
        }
        if CurrentUser.sharedInstance.currentUser?.isProfilePrivate == true{
            self.privateProfileSwitch.isOn = true
        } else {
            self.privateProfileSwitch.isOn = false
        }
    }
}
