//
//  UserProfileSettingsViewController.swift
//  alphalooplist
//
//  Created by Looplist Inc on 1/24/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//
//

import Foundation
import UIKit

class SettingsViewController: UITableViewController {
    
    let MyKeychainWrapper = KeychainWrapper()

    @IBOutlet weak var editProfileLabel: UILabel!
    @IBOutlet weak var privacyAndNotificationsLabel: UILabel!
    @IBOutlet weak var orderHistoryLabel: UILabel!
    @IBOutlet weak var shippingAddressesLabel: UILabel!
    @IBOutlet weak var manageCreditCardsLabel: UILabel!
    @IBOutlet weak var externalAccountsLabel: UILabel!
    @IBOutlet weak var termsOfServiceLabel: UILabel!
    @IBOutlet weak var privacyPolicyLabel: UILabel!
    @IBOutlet weak var helpLabel: UILabel!
    @IBOutlet weak var licensesLabel: UILabel!
    @IBOutlet weak var signOutLabel: UILabel!
    @IBOutlet weak var editInterestsLabel: UILabel!
    @IBOutlet weak var deactivateAccountLabel: UILabel!
    // MARK: -  ////////////////////////////////////////////////    ACTIONS    ///////////////////////////////////////////////////////////////////
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
   
    // MARK: -  ////////////////////////////////////////////////    APPLICATION LIFE CYCLE    ///////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        SettingsViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SettingsViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "SETTINGS".localized
        self.editProfileLabel.text = "EDIT_PROFILE".localized
        self.privacyAndNotificationsLabel.text = "PRIVACY_AND_NOTIFICATIONS".localized
        self.orderHistoryLabel.text = "ORDER_HISTORY".localized
        self.shippingAddressesLabel.text = "SHIPPING_ADDRESSES".localized
        self.manageCreditCardsLabel.text = "MANAGE_CREDIT_CARDS".localized
        self.externalAccountsLabel.text = "EXTERNAL_ACCOUNTS".localized
        self.termsOfServiceLabel.text = "TERMS_AND_CONDITIONS".localized
        self.privacyPolicyLabel.text = "PRIVACY_POLICY".localized
        self.helpLabel.text = "HELP".localized
        self.licensesLabel.text = "LICENSES".localized
        self.signOutLabel.text = "SIGN_OUT".localized
        self.deactivateAccountLabel.text = "DEACTIVATE_ACCOUNT".localized
        self.editInterestsLabel.text = "EDIT_INTERESTS".localized
        
        self.tabBarController?.tabBar.isHidden = true
        
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
        
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 20.0;
        self.tableView.contentInset = UIEdgeInsetsMake(20,0,0,0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let navigationController = self.navigationController{
            SettingsViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    func logout(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.sendAnalytics()
        // signout
        let appDomain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        //Clear Keychain of only access token
        let MyKeychainWrapper = KeychainWrapper()
        MyKeychainWrapper.mySetObject("", forKey:kSecValueData)
        MyKeychainWrapper.writeToKeychain()
        
        // reset Singletons
        CurrentUser.sharedInstance.resetCurrentUser()
        
        appDelegate.showScreen(ConstantsViewController.viewControllerSignIn)

    }
}

// MARK: -  ////////////////////////////////////////////    TABLE VIEW DELEGATE   //////////////////////////////////////////////////////////////

extension SettingsViewController{
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
   
        let title = UILabel()
        title.font = UIFont(name: ConstantsFont.fontDefaultBlack, size: 16)!
        title.textColor = ConstantsColor.looplistGreyColor
        
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font=title.font
        header.textLabel?.textColor=title.textColor
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "ACCOUNT".localized
        } else if section == 1{
            return "MORE_CAPITALIZED".localized
        } else {
            return ""
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath == IndexPath(row: 0, section: 0) {
            let editProfileVC:EditProfileViewController = EditProfileViewController.instanceFromStoryboard("EditProfile")
            self.navigationController?.pushViewController(editProfileVC, animated: true)
        } else if indexPath == IndexPath(row: 1, section: 0){
            let settingsPushNotificationsVC:SettingsPrivacyAndNotificationsViewController = SettingsPrivacyAndNotificationsViewController.instanceFromStoryboard("Settings")
            self.navigationController?.pushViewController(settingsPushNotificationsVC, animated: true)
        } else if indexPath == IndexPath(row: 2, section: 0){
            let settingsOrderHistoryVC:SettingsOrderHistoryTableViewController = SettingsOrderHistoryTableViewController.instanceFromStoryboard("Settings")
            self.navigationController?.pushViewController(settingsOrderHistoryVC, animated: true)
        }else if indexPath == IndexPath(row: 3, section: 0){
            let settingsShippingVC:ShippingAddressesViewController = ShippingAddressesViewController.instanceFromStoryboard("Shipping")
            self.navigationController?.pushViewController(settingsShippingVC, animated: true)
        } else if indexPath == IndexPath(row: 4, section: 0){
            let creditCardVC:CCViewController = CCViewController.instanceFromStoryboard("CreditCard")
            self.navigationController?.pushViewController(creditCardVC, animated: true)
        } else if indexPath == IndexPath(row: 5, section: 0){
            let externalAccountsVC:SettingsExternalAccountsTableViewController = SettingsExternalAccountsTableViewController.instanceFromStoryboard("Settings")
            self.navigationController?.pushViewController(externalAccountsVC, animated: true)
        }else if indexPath == IndexPath(row: 6, section: 0){
            let onBoardEditVC:OnBoardViewController = OnBoardViewController.instanceFromStoryboard("LoginAndRegistration")
            onBoardEditVC.isEditingInterests = true
            self.navigationController?.pushViewController(onBoardEditVC, animated: true)
        } else if indexPath == IndexPath(row: 0, section: 1){
            let termsOfServiceVC:SettingsTermsAndConditionsViewController = SettingsTermsAndConditionsViewController.instanceFromStoryboard("Settings")
            self.navigationController?.pushViewController(termsOfServiceVC, animated: true)
        } else if indexPath == IndexPath(row: 1, section: 1){
            let privacyPolicyVC:SettingsPrivacyPolicyViewController = SettingsPrivacyPolicyViewController.instanceFromStoryboard("Settings")
            self.navigationController?.pushViewController(privacyPolicyVC, animated: true)
        } else if indexPath == IndexPath(row: 2, section: 1){
            let helpVC:SettingsHelpViewController = SettingsHelpViewController.instanceFromStoryboard("Settings")
            self.navigationController?.pushViewController(helpVC, animated: true)
        } else if indexPath == IndexPath(row: 3, section: 1){
            let licensesVC:SettingsLicensesTableViewController = SettingsLicensesTableViewController.instanceFromStoryboard("Settings")
            self.navigationController?.pushViewController(licensesVC, animated: true)
        } else if indexPath == IndexPath(row: 0, section: 2) {
            let alertController = UIAlertController(title: nil, message: "SIGN_OUT_CONFIRMATION".localized, preferredStyle: .alert)
            
            let signoutAction = UIAlertAction(title: "SIGN_OUT".localized, style: .default) { (action) in
                
                DBHelper.callAPIDelete("authentication/logout", params: ["uuid":UIDevice.current.identifierForVendor!.uuidString as AnyObject], completion: { [weak vc = self](result, error) in
                    vc?.MyKeychainWrapper.mySetObject("", forKey: kSecAttrAccount)
                    vc?.MyKeychainWrapper.mySetObject("", forKey: kSecAttrDescription)
                    vc?.MyKeychainWrapper.writeToKeychain()
                    vc?.logout()
                })
            }
            let cancelAction = UIAlertAction(title: "CANCEL".localized, style: .destructive, handler: nil)
            
            alertController.addAction(cancelAction)
            alertController.addAction(signoutAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else if indexPath == IndexPath(row: 0, section: 3) {
            
            let alert = UIAlertController(title: "DEACTIVATE_ACCOUNT".localized, message: "DEACTIVATE_CONFIRMATION".localized, preferredStyle:
                UIAlertControllerStyle.alert)
            
            let signoutAction = UIAlertAction(title: "DEACTIVATE_ACCOUNT".localized, style: .default) { (action) in
                DBHelper.getCurrentUser({ (success) in
                    if(success){
                        //Got current user, now delete
                        if let objectId = CurrentUser.sharedInstance.currentUser?.objectId{
                            DBHelper.callAPIDelete("users/\(objectId)", completion: { (result, error) in
                                if(error == nil){
                                    
                                    if let userObjectId = CurrentUser.sharedInstance.currentUser?.objectId{
                                        Analytics.sharedInstance.actionsArray.append(Action(context:NSDictionary(), forScreen:nil, withActivityType:"Deactivate User")) // $AS
                                    }
                                    //Successfully deleted account
                                    // signout
                                    //Clear Keychain
                                    let MyKeychainWrapper = KeychainWrapper()
                                    MyKeychainWrapper.mySetObject(nil, forKey:kSecValueData)
                                    MyKeychainWrapper.writeToKeychain()
                                    let appDomain = Bundle.main.bundleIdentifier!
                                    UserDefaults.standard.removePersistentDomain(forName: appDomain)
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.showScreen(ConstantsViewController.viewControllerInitial)
                                } else {
                                    //Error deleting account
                                }
                            })
                        }
                    } else {
                        //Error getting current user
                    }
                })
            }
            let cancelAction = UIAlertAction(title: "CANCEL".localized, style: .destructive, handler: nil)
            
            alert.addAction(cancelAction)
            alert.addAction(signoutAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
}

// MARK: -  //////////////////////////////////////////////////////////    EXTENSIONS   //////////////////////////////////////////////////////////////////////////

extension SettingsViewController{
    
    func addKeyboardHideTap(){
        
        let hideTap = UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.hideKeyboardTap(_:)))
        hideTap.numberOfTapsRequired = 1
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(hideTap)
    }
    
    // Hide keyboard when view is Tapped - 1
    func hideKeyboardTap(_ recognizer:UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    
    
    // Alert Message
    func presentAlertMessage(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: {
            (action) -> Void in
        }))
        //present alert message
        self.present(alert, animated: true, completion: nil)
    }
}
