//
//  UserProfileSettingsPrivacyPolicyViewController.swift
//  Looplist
//
//  Created by Arun Sivakumar on 7/12/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class SettingsPrivacyPolicyViewController: UITableViewController {

    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        SettingsPrivacyPolicyViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SettingsPrivacyPolicyViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewDidLoad() {
        self.navigationItem.title = "PRIVACY_POLICY".localized
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 600

    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController{
            SettingsPrivacyPolicyViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        // to make header visiible change to 40.0
        return 0.0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
