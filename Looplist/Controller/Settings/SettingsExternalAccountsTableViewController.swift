//
//  SettingsExternalAccountsTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 11/28/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import TwitterKit

class SettingsExternalAccountsTableViewController: UITableViewController {

    // MARK: -  //////////////////////////////////////////////////////   VARIABLS   /////////////////////////////////////////////////////////////////////////

    private let accountNames:[String] = [ExternalAccount.facebook.rawValue, ExternalAccount.twitter.rawValue]
    private let accountImageNames:[String] = [ExternalAccount.facebook.rawValue, ExternalAccount.twitter.rawValue]
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////

    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    

    // MARK: -  //////////////////////////////////////////////////////   LIFE CYCLE   /////////////////////////////////////////////////////////////////////////

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "LINK_EXTERNAL_ACCOUNTS".localized
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 60
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
        
        if let navigationController = self.navigationController{
            SettingsExternalAccountsTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        SettingsPrivacyAndNotificationsViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SettingsPrivacyAndNotificationsViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.accountNames.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsExternalAccountsTableViewCell", for: indexPath) as! SettingsExternalAccountsTableViewCell
        if indexPath.row == 0{
            if let _ = AccessToken.current {
                // User is logged in, use 'accessToken' here.
                cell.accountNameLabel.text = "UNLINK".localized + " " + self.accountNames[indexPath.row]
            }else {
                //User is not logged in
                cell.accountNameLabel.text = "LINK".localized + " " + self.accountNames[indexPath.row]
            }
        } else if indexPath.row == 1{
            let store = Twitter.sharedInstance().sessionStore
            if let _ = store.session() {
                cell.accountNameLabel.text = "UNLINK".localized + " " + self.accountNames[indexPath.row]
            } else {
                //User is not logged in
                cell.accountNameLabel.text = "LINK".localized + " " + self.accountNames[indexPath.row]
            }
        }
        cell.accountImage.image = UIImage(named: self.accountImageNames[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            //Facebook
            let loginManager = LoginManager()
            if let _ = AccessToken.current {
                loginManager.logOut()
                if let objectId = CurrentUser.sharedInstance.currentUser?.objectId{
                    DBHelper.callAPIPut("users/\(objectId)", params: ["facebookId":"" as AnyObject], completion: { (result, error) in
//                        CurrentUser.sharedInstance.currentUser?.facebookId = ""
                    })
                }
                self.tableView.reloadData()
            }else {
                loginManager.logIn([.publicProfile, .userFriends, .email], viewController: self, completion: { (loginResult) in
                    self.tableView.reloadData()
                    switch loginResult {
                    case .failed(_):
                        break;
                    case .cancelled:
                        break;
                    case .success(grantedPermissions: _, declinedPermissions: _, token: let accessToken):
                        if let facebookId = accessToken.userId, let objectId = CurrentUser.sharedInstance.currentUser?.objectId{
                            DBHelper.callAPIPut("users/\(objectId)", params: ["facebookId":facebookId as AnyObject], completion: { (result, error) in
//                                CurrentUser.sharedInstance.currentUser?.facebookId = facebookId
                            })
                        }

                        break;
                    }
                    
                })
            }
            
        } else if indexPath.row == 1{
            let store = Twitter.sharedInstance().sessionStore
            if let storeSession = store.session() {
                let userID = storeSession.userID
                store.logOutUserID(userID)
                if let objectId = CurrentUser.sharedInstance.currentUser?.objectId{
                    DBHelper.callAPIPut("users/\(objectId)", params: ["twitterId":"" as AnyObject], completion: { (result, error) in
//                        CurrentUser.sharedInstance.currentUser?.twitterId = ""
                    })
                }
                self.tableView.reloadData()

            }else {
                Twitter.sharedInstance().logIn {
                    (session, error) -> Void in
                    self.tableView.reloadData()
                    if (session != nil) {
                        if let twitterId = session?.userID, let objectId = CurrentUser.sharedInstance.currentUser?.objectId{
                            DBHelper.callAPIPut("users/\(objectId)", params: ["twitterId":twitterId as AnyObject], completion: { (result, error) in
//                                CurrentUser.sharedInstance.currentUser?.twitterId = twitterId
                            })
                        }
                    }
                }
            }
        }else if indexPath.row == 2{
            //Instagram
        }
    }
}
