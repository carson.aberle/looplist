//
//  SettingsOrderHistoryTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 9/6/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class SettingsOrderHistoryTableViewController:UIViewController{
    
    // MARK: - ------------------------------------------------------------------ API VARIABLES  ------------------------------------------------------------------

    var currentUser: User?
    
    // MARK: -  ------------------------------------------------------------------   VARIABLES   ------------------------------------------------------------------
    
    private var currentPage = 0
    private let limitNumber = ConstantsCount.paginationlimitNumber
    private var indicator = CustomActivityIndicator()
    
//    var isUpdatingTransactions:Bool = false
    fileprivate var transactions = [[Transaction]](){
        didSet{
             self.tableView?.reloadData()
        }
    }
    
    // MARK: -  ------------------------------------------------------------------ OUTLETS   ------------------------------------------------------------------
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            if let tableView = tableView{
                
                tableView.tableFooterView = UIView()
                tableView.estimatedRowHeight = 133
                tableView.rowHeight = UITableViewAutomaticDimension
                tableView.backgroundView?.isHidden = true
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        SettingsOrderHistoryTableViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SettingsOrderHistoryTableViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUserTransactions()
        self.navigationItem.title = "ORDER_HISTORY".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController{
            SettingsOrderHistoryTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
        
        self.indicator.showActivityIndicator(uiView: self.view, offset: 64.0)
    }
    
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    // INITIAL LOAD USER TRANSACTIONS
    
    
    fileprivate func loadUserTransactions(){
//        self.isUpdatingTransactions = true
        
        APIHelperUser.getCurrentUserTransactions(skip: self.currentPage, limit: self.limitNumber) { [weak vc = self](transactions) in
            vc?.indicator.hideActivityIndicator()
            if let transactions = transactions{
                
                if transactions.count > 0 {
                    vc?.currentPage += transactions.count
                    vc?.transactions.append(transactions)
                }else if transactions.count == 0 && self.currentPage == 0{
                    vc?.initializeEmptySet()
                }
            }
        }
    }
    
    // SHOW EMPTY SET
    
    private func initializeEmptySet(){ // Lazy Init
        let emptySetView: EmptySetView = EmptySetView.instanceFromNib()
        emptySetView.discriptionLabel.text = "HAVENT_COMPLETED_ORDERS_YET".localized
        self.tableView.backgroundView = emptySetView
        self.tableView.backgroundView?.isHidden = false
    }
}

extension SettingsOrderHistoryTableViewController: UITableViewDelegate, UITableViewDataSource{
    
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DATA SOURCE   /////////////////////////////////////////////////////////////////////////
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return transactions.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return transactions[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == self.transactions.count-1 && indexPath.row == self.transactions[indexPath.section].count - 1 {
            loadUserTransactions()
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsOrderHistoryTableViewCell", for: indexPath) as! SettingsOrderHistoryTableViewCell
        
            cell.transaction = transactions[indexPath.section][indexPath.row]
            return cell
    }

    
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DELEGATE   /////////////////////////////////////////////////////////////////////////
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let listing = transactions[indexPath.section][indexPath.row].listing, let shipping = transactions[indexPath.section][indexPath.row].shipping, let payment = transactions[indexPath.section][indexPath.row].payment {
            let orderConfirmationVC:ConfirmationViewController = ConfirmationViewController.instanceFromStoryboard("Purchase")
            
            listing.payment = payment
            orderConfirmationVC.listing = listing
            orderConfirmationVC.shipping = shipping
            orderConfirmationVC.transactionId = transactions[indexPath.section][indexPath.row].objectId
            self.navigationController?.pushViewController(orderConfirmationVC, animated: true)
            
        }
    }
}
