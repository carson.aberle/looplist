//
//  UserRegistrationEmailInitialViewController.swift
//  alphalooplist
//
//  Created by Arun on 2/23/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import UIKit


class UserRegistrationEmailInitialViewController: UIViewController,UITextFieldDelegate {
    
    // MARK: -  //////////////////////////////////////////////////////    VARIABLES    /////////////////////////////////////////////////////////////////////////
    
    fileprivate var user:User?
    var inviteCode:String?

//    var yValue:CGFloat?
//    var keyboard = CGRect()
//    var emailTaken: Bool = false
    var email:NSString = ""
    var firstName:NSString = ""
    var lastName:NSString = ""
//    var shouldLinkFacebookEI:Bool = false
//    var frame1:CGRect!
//    var frame2:CGRect!
//    var gradient:CAGradientLayer?
    fileprivate var indicator = CustomActivityIndicator()

    // MARK: -  //////////////////////////////////////////////////////    OUTLETS    /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var firstNameText: UITextField!
    @IBOutlet weak var lastNameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var nextButton: UIButton!{
        didSet{
            if let nextButton = nextButton{
                nextButton.layer.cornerRadius = nextButton.frame.height / 2.0
                nextButton.layer.borderWidth = 1.0
                nextButton.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
    @IBOutlet weak var stackView: UIStackView!
    
    // MARK: -  //////////////////////////////////////////////////////    APPLICATION LIFE CYCLE    /////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "BASIC_INFO".localized
        self.firstNameText.placeholder = "FIRST_NAME".localized
        self.lastNameText.placeholder = "LAST_NAME".localized
        self.emailText.placeholder = "EMAIL".localized
        self.nextButton.setTitle("NEXT".localized, for: .normal)
        
        self.firstNameText.delegate = self;
        self.lastNameText.delegate = self;
        self.emailText.delegate = self;
        
        if(email != "") {
            emailText.text = email as String
        }
        if(firstName != "") {
            firstNameText.text = firstName as String
        }
        if(lastName != "") {
            lastNameText.text = lastName as String
        }
        
        setGradient()
    }
    
    
    func setGradient(){
        self.view.layer.insertSublayer(HelperFunctionAnimation.getLooplistGradient(insideFrame: self.view), at: 0)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        firstNameText.becomeFirstResponder()
        UserRegistrationEmailInitialViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        UserRegistrationEmailInitialViewController.stopTimer(["preLogin":true, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], withActionType:"View Screen")
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.view.layer.insertSublayer(self.gradient!, at: 0)
        
        
        
        if let navigationController = self.navigationController{
            UserRegistrationEmailInitialViewController.self.setNavigationBar(UIColor.white, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.lightContent, backgroundColor:NavigationBarBackgroundColor.clearNoGradient)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view!.endEditing(true)
//        self.animateTextField(UITextField(), up: false)
    }
    
    @IBAction func popToInitialScreen(_ sender:UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -  //////////////////////////////////////////////////////    ACTION    /////////////////////////////////////////////////////////////////////////
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowNext"{
            if let vc = segue.destination as? UserRegistrationEmailFinalViewController{
                vc.emailSent = self.emailText.text!.lowercased()
                vc.firstNameSent = self.firstNameText.text!
                vc.lastNameSent = self.lastNameText.text!
                vc.inviteCode = self.inviteCode
            }
        }else if segue.identifier == "GoBack"{
            self.view.endEditing(true)
            
        }
    }
    
    @IBAction func Register(_ sender: UIButton) {
        let emailUserEntered: String = self.emailText.text!.lowercased()
        
        //Check if email is valid
        if firstNameText.text!.characters.count < 2 {
            DropDownAlert.customErrorMessage("FIRST_NAME_LENGTH".localized)
        } else if (HelperFunction.checkIfOnlyWhitespace(firstNameText.text!)) {
            DropDownAlert.customErrorMessage("FIRST_NAME_BLANK".localized)
        } else if lastNameText.text!.characters.count < 2 {
            DropDownAlert.customErrorMessage("LAST_NAME_LENGTH".localized)
        } else if (HelperFunction.checkIfOnlyWhitespace(lastNameText.text!)) {
            DropDownAlert.customErrorMessage("LAST_NAME_BLANK".localized)
        }else if HelperFunction.validateEmail(emailUserEntered) == false {
            DropDownAlert.customErrorMessage("ENTER_VALID_EMAIL".localized)
        } else{
            self.indicator.showActivityIndicator(uiView: self.view, offset: 0.0)
            //Check if username is already taken
            let params:[String: AnyObject] = ["email": emailUserEntered as AnyObject]
            DBHelper.callAPIGet("authentication/local/availability", queryString: DBHelper.getJSONFromDictionary(params as NSDictionary), completion: { (result, error) in
                self.indicator.hideActivityIndicator()
                if let result = result{
                    if let _ = result["email"] as? Bool {
                        self.performSegue(withIdentifier: "ShowNext", sender: self)

                    } else {
                        ErrorHandling.customErrorHandlerWithTitle(title:"SORRY".localized, message: "EMAIL_TAKEN".localized)
                    }
                } else {
                    ErrorHandling.customErrorHandlerWithTitle(title:"SORRY".localized, message: "AN_ERROR_OCCURED".localized)
                }
            })
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view!.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isEqual(self.firstNameText){
            self.lastNameText.becomeFirstResponder()
        } else if textField.isEqual(self.lastNameText){
            self.emailText.becomeFirstResponder()
        } else if textField.isEqual(self.emailText){
            self.Register(UIButton())
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.isEqual(self.firstNameText){
            textField.returnKeyType = .next
        } else if textField.isEqual(self.lastNameText){
            textField.returnKeyType = .next
        } else if textField.isEqual(self.emailText){
            textField.returnKeyType = .done
        }
    }
}
