//
//  OnBoardViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 6/16/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import Foundation
import KTCenterFlowLayout

class OnBoardViewController : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    // MARK: -  //////////////////////////////////////////////////////   VERIABLES   /////////////////////////////////////////////////////////////////////////
    
    var isEditingInterests:Bool = false
    var interests:[Interest] = [Interest]()
    var selectAmountCopy:[String] = ["FOUR_MORE_INTEREST".localized, "THREE_MORE_INTEREST".localized, "TWO_MORE_INTEREST".localized, "ONE_MORE_INTEREST".localized, "NO_MORE_INTERESTS".localized]
    var selectedItems: NSMutableArray = NSMutableArray()
    var headerView:OnBoardTitleTextTableViewCell?
    let layout = KTCenterFlowLayout()
    var doneButton:UIBarButtonItem!
    fileprivate var indicator = CustomActivityIndicator()

    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var onBoardCollectionView: UICollectionView!
    
    @IBOutlet weak var smallHeader: UIView!
    @IBOutlet weak var amountToPickText: UILabel!
    @IBOutlet weak var gradientImage: UIImageView!
    @IBOutlet weak var selectInterestsTitle: UILabel!
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    override func viewDidAppear(_ animated: Bool) {
        OnBoardViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if(isEditingInterests){
            OnBoardViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
        } else {
            OnBoardViewController.stopTimer(["preLogin":true, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], withActionType:"View Screen")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        OnBoardViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: self.navigationController!, statusBar:.default, backgroundColor: NavigationBarBackgroundColor.whiteOpaque)
        self.navigationItem.title = "PERSONALIZE".localized
        
        self.onBoardCollectionView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 40, right: 0)

        layout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.size.width, height: 160)
        layout.footerReferenceSize = CGSize(width: UIScreen.main.bounds.size.width, height: 40)
        layout.minimumInteritemSpacing = 10.0
        layout.minimumLineSpacing = 10.0
        layout.sectionHeadersPinToVisibleBounds = false
        self.onBoardCollectionView.collectionViewLayout = layout
        self.onBoardCollectionView.dataSource = self
        self.onBoardCollectionView.delegate = self
        self.onBoardCollectionView.reloadData()
        
        self.doneButton = UIBarButtonItem(title:"DONE".localized, style: .plain, target: self, action: #selector(self.doneSelecting))
        self.navigationItem.rightBarButtonItem = self.doneButton
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.darkGray
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        self.getParentInterests(skip: 0)
        
        if let navigationController = self.navigationController{
            OnBoardViewController.self.popSlideMotionOff(navigationController)
        }
        
    }
    
    func queryForSelectedInterests(){
        self.indicator.showActivityIndicator(uiView: self.view, offset: 64.0)
        var selectedItemsIds:[String] = [String]()
        for selectedItem in self.interests{
            if selectedItem.isInUsersInterests == true{
                if let objectId = selectedItem.objectId{
                    selectedItemsIds.append(objectId)
                }
            }
        }
        DBHelper.callAPIGet("interests", queryString: DBHelper.getJSONFromDictionary(["flags":["isInUsersInterests":CurrentUser.sharedInstance.currentUser!.objectId!],"where":["parent":["$in":selectedItemsIds]]]), completion: { (result, error) in
            if error == nil{
                if let childResultDictionary = result, let childInterestsArray = childResultDictionary["interests"] as? NSArray{
                    for childInterest in childInterestsArray {
                        if let childInterestDicionary = childInterest as? Dictionary<String, AnyObject>{
                            if let childInterestObject = Interest(input:childInterestDicionary){
                                self.interests.append(childInterestObject)
                                if childInterestObject.isInUsersInterests == true{
                                    self.selectedItems.add(childInterestObject)
                                }
                            }
                        }
                    }
                    if self.selectedItems.count >= 4 {
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
                    } else {
                        self.navigationItem.rightBarButtonItem?.isEnabled = false
                    }
                    self.onBoardCollectionView.reloadData()
                }
            }
        })
        self.onBoardCollectionView.reloadData()
        self.indicator.hideActivityIndicator()
    }
    func getParentInterests(skip:Int){
        self.indicator.showActivityIndicator(uiView: self.view, offset: 64.0)

        DBHelper.callAPIGet("interests", queryString: DBHelper.getJSONFromDictionary(["skip":skip,"flags":["isInUsersInterests":CurrentUser.sharedInstance.currentUser!.objectId!],"where":["parent":["$exists":false]]])) { (result, error) in
            if error == nil{
                if let resultDictionary = result, let interestsArray = resultDictionary["interests"] as? NSArray{
                    for interest in interestsArray {
                        if let interestDicionary = interest as? Dictionary<String, AnyObject>{
                            if let interestObject = Interest(input:interestDicionary){
                                self.interests.append(interestObject)
                                if interestObject.isInUsersInterests{
                                    self.selectedItems.add(interestObject)
                                }
                            }
                        }
                    }
                    if self.interests.count % 50 == 0 && self.interests.count != 0{
                        self.getParentInterests(skip: self.interests.count)
                    }else {
                        if self.isEditingInterests{
                            self.queryForSelectedInterests()
                        } else {
                            self.indicator.hideActivityIndicator()
                            self.onBoardCollectionView.reloadData()
                        }
                    }
                    
                } else {
                    self.indicator.hideActivityIndicator()
                    DropDownAlert.customErrorMessage("ERROR".localized)
                }
            } else {
                self.indicator.hideActivityIndicator()
                DropDownAlert.customErrorMessage("ERROR".localized)
            }
        }
    }
    
    func doneSelecting(){
        var selectedInterestIds:[String] = [String]()
        for interestElement in self.selectedItems{
            if let interest = interestElement as? Interest{
                if let interestId = interest.objectId{
                    selectedInterestIds.append(interestId)
                }
            }
        }
        DBHelper.callAPIPut("users/\(CurrentUser.sharedInstance.currentUser!.objectId!)", params: ["interests":selectedInterestIds as AnyObject]) { (result, error) in
            if error == nil{
                if self.isEditingInterests{
                    _ = self.navigationController?.popViewController(animated: true)
                } else {
                    if self.selectedItems.count >= 4 {
                        let curatorsVC:AddCuratorsTableViewController = AddCuratorsTableViewController.instanceFromStoryboard("LoginAndRegistration")
                        var selectedItemsArray:[Interest] = [Interest]()
                        for item in self.selectedItems {
                            if let itemInterest = item as? Interest{
                                selectedItemsArray.append(itemInterest)
                            }
                        }
                        curatorsVC.selectedInterests = selectedItemsArray
                        self.navigationController?.pushViewController(curatorsVC, animated: true)
                    } else {
                        //            DropDownAlert.customErrorMessage("Please choose more interests")
                        ErrorHandling.customErrorHandler(message:"CHOOSE_MORE_INTERESTS".localized)
                    }

                }
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////    COLLECTION VIEW DATA SOURCE    /////////////////////////////////////////////////////////////////////////
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let scrollViewContentHeight = scrollView.contentSize.height
        let scrollViewFrameHeight = scrollView.frame.size.height
        if self.onBoardCollectionView.contentOffset.y < 52{
            self.smallHeader.isHidden = true
        } else {
            self.smallHeader.isHidden = false
            
        }
        if offsetY < 0{
            UIView.animate(withDuration: 0.6, animations: {
                self.gradientImage.alpha = 1.0
            }, completion: { (success) in
                self.gradientImage.isHidden = false
            })
        } else if (offsetY >= (scrollViewContentHeight - scrollViewFrameHeight - 50) && !self.gradientImage.isHidden) {
            UIView.animate(withDuration: 0.6, animations: {
                self.gradientImage.alpha = 0.0
            }, completion: { (success) in
                self.gradientImage.isHidden = true
            })
        } else if (offsetY < (scrollViewContentHeight - scrollViewFrameHeight - 50) && gradientImage.isHidden){
            UIView.animate(withDuration: 0.6, animations: {
                self.gradientImage.alpha = 1.0
            }, completion: { (success) in
                self.gradientImage.isHidden = false
            })
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.interests.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "onBoardCollectionViewCell", for: indexPath) as! OnBoardCollectionViewCell
        
        if self.interests.count > indexPath.row {
            let interest = self.interests[indexPath.row]
            if self.selectedItems.count < 5{
                if let headerView = self.headerView{
                    headerView.amountToPickText.text = self.selectAmountCopy[self.selectedItems.count]
                }
                self.amountToPickText.text = self.selectAmountCopy[self.selectedItems.count]
            } else {
                //Set default text
                if let headerView = self.headerView{
                    headerView.amountToPickText.text = self.selectAmountCopy[4]
                }
                self.amountToPickText.text = self.selectAmountCopy[4]
            }
            
            
            if interest.isInUsersInterests {
                cell.layer.borderColor = ConstantsColor.looplistColor.cgColor
                cell.onBoardLabel.textColor = ConstantsColor.looplistColor
                cell.backgroundColor = UIColor.clear
            } else {
                cell.onBoardLabel.textColor = UIColor.darkGray
                cell.backgroundColor = UIColor.clear
                cell.layer.borderColor = UIColor.darkGray.cgColor
            }
            cell.onBoardLabel.text = interest.name
            cell.backgroundColor = UIColor.clear
            cell.layer.cornerRadius = 17
            cell.layer.borderWidth = 1
        }
        return cell
    }
    
    // MARK: -  //////////////////////////////////////////////////////    COLLECTION VIEW DELEGATE    /////////////////////////////////////////////////////////////////////////
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)! as! OnBoardCollectionViewCell
        
        if self.interests.count > indexPath.row{
            let interest = self.interests[indexPath.row]
            
            if self.selectedItems.contains(interest){
                //Remove interests children
                var amountRemoved:Int = 0
                for i in 0...(self.interests.count - 1){
                    if (i - amountRemoved) < self.interests.count{
                        let itertatedInterest = self.interests[(i - amountRemoved)]
                        if let itertatedInterestId = itertatedInterest.objectId{
                            if interest.childrenIds.contains(itertatedInterestId){
                                self.interests.remove(at: (i - amountRemoved))
                                amountRemoved += 1
                            }
                        }
                    }
                }
                self.onBoardCollectionView.reloadData()
            } else {
                //Get interests children
                if let objectId = interest.objectId{
                    if interest.children.count > 0 || interest.childrenIds.count > 0{
                        DBHelper.callAPIGet("interests", queryString: DBHelper.getJSONFromDictionary(["flags":["isInUsersInterests":CurrentUser.sharedInstance.currentUser!.objectId!],"where":["parent":objectId]]), completion: { (result, error) in
                            if error == nil{
                                if let resultDictionary = result, let interestsArray = resultDictionary["interests"] as? NSArray{
                                    for interest in interestsArray {
                                        if let interestDicionary = interest as? Dictionary<String, AnyObject>{
                                            if let interestObject = Interest(input:interestDicionary){
                                                self.interests.append(interestObject)
                                            }
                                        }
                                    }
                                    self.onBoardCollectionView.reloadData()
                                }
                            }
                        })
                    }
                }
            }
        }
        
        UIView.animate(withDuration: 0.15, animations: {() -> Void in
            cell.transform = CGAffineTransform(scaleX: 1.10, y: 1.10)
        }, completion: {(finished: Bool) -> Void in
            if self.interests.count > indexPath.row{
                let interest = self.interests[indexPath.row]
                if(!self.selectedItems.contains(interest)) {
                    interest.isInUsersInterests = true
                    self.selectedItems.add(interest)
                } else {
                    interest.isInUsersInterests = false
                    self.selectedItems.remove(interest)
                }
                if self.selectedItems.count >= 4 {
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                } else {
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                }
                collectionView.reloadData()
            }
            
            
            UIView.animate(withDuration: 0.15, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform(scaleX: 1, y: 1)
            }, completion: { (success) in
                collectionView.reloadData()
                
            })
            
        })
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "OnBoardTitleTextTableViewCell",
                                                                             for: indexPath) as! OnBoardTitleTextTableViewCell
            self.headerView = headerView
            headerView.selectInterestsTitleText.text = "INTERESTS_TITLE".localized
            if self.selectedItems.count < 5 {
                headerView.amountToPickText.text = self.selectAmountCopy[self.selectedItems.count]
            } else {
                headerView.amountToPickText.text = self.selectAmountCopy[4]
            }
            return headerView
        default:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "OnBoardingFooter",
                                                                             for: indexPath)
            return footerView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        if let font = UIFont(name: "Helvetica", size: 18) {
            let fontAttributes = [NSFontAttributeName: font]
            if self.interests.count > indexPath.row{
                if let title = self.interests[indexPath.row].name {
                    let size = title.size(attributes: fontAttributes)
                    return CGSize(width: size.width + 16, height: size.height + 16)
                } else {
                    return CGSize(width: 0, height: 100)
                }
            } else {
                return CGSize(width: 0, height: 100)
            }
            
        } else {
            return CGSize(width: 0, height: 100)
        }
    }
}
