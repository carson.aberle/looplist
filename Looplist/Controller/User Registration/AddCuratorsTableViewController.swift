//
//  AddCuratorsTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 12/29/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

public class AddCuratorsTableViewController: UITableViewController{
    
    var selectedInterests:[Interest]?
    var recommendedCurators:[User] = [User]()
    @IBOutlet weak var addCuratorsTitleText: UILabel!
    
    
    override public func viewDidAppear(_ animated: Bool) {
        AddCuratorsTableViewController.startTimer()
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        AddCuratorsTableViewController.stopTimer(["preLogin":true, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], withActionType:"View Screen")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "ADD_CURATORS".localized
        self.addCuratorsTitleText.text = "ADD_CURATORS_TITLE".localized
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 200
        self.tableView.tableFooterView = UIView()
        
        let doneButton = UIBarButtonItem(title:"DONE".localized, style: .plain, target: self, action: #selector(self.doneSelecting))
        self.navigationItem.rightBarButtonItem = doneButton
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.darkGray
        
        let backButton = UIBarButtonItem(image: UIImage(named: Constants.backImage), style: .plain, target: self, action: #selector(self.popViewController))
        self.navigationItem.leftBarButtonItem = backButton
        self.getRecommendedCurators()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController {
            AddCuratorsTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    func popViewController(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func doneSelecting(){
        if let appDelegate:AppDelegate = UIApplication.shared.delegate as? AppDelegate{
            appDelegate.showScreen(ConstantsViewController.viewControllerTabBar)
        }
    }
    
    func getRecommendedCurators(){
        if let selectedInterests = self.selectedInterests{
            var selectedInterestIds = [String]()
            for interest in selectedInterests{
                if let objectId = interest.objectId{
                    selectedInterestIds.append(objectId)
                }
            }
            DBHelper.callAPIGet("users", queryString: DBHelper.getJSONFromDictionary(["flags":["isRequestedByUser":CurrentUser.sharedInstance.currentUser!.objectId, "isFollowedByUser":CurrentUser.sharedInstance.currentUser!.objectId!], "where":["interests":["$in":selectedInterestIds],"_id":["$ne":CurrentUser.sharedInstance.currentUser!.objectId!]],"populate":[["path":"products"]],"sort":"-followersCount"])) { (result, error) in
                if let users = result?["users"] as? NSArray{
                    for userElement in users{
                        if let userDictionary = userElement as? Dictionary<String,AnyObject>{
                            let user = User(input: userDictionary)
                            self.recommendedCurators.append(user)
                        }
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recommendedCurators.count
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddCuratorTableViewCell", for: indexPath) as! AddCuratorTableViewCell
        if self.recommendedCurators.count > indexPath.row{
            let user = self.recommendedCurators[indexPath.row]
            if let profileImageUrl = user.profileImageUrl{
                cell.userImage.kf.setImage(with: URL(string:profileImageUrl))
            }
            if let userFullName = user.fullName{
                cell.userName.text = userFullName
            }
            cell.user = user
            ObjectStateHelper.getStateForFollowerFollowingButton(user: user, button: cell.followButton)
            cell.imageOne.image = nil
            cell.imageTwo.image = nil
            cell.imageThree.image = nil
            cell.imageFour.image = nil
            cell.imageFive.image = nil
            cell.imageSix.image = nil
            cell.imageSeven.image = nil
            cell.imageEight.image = nil

            for i in 0..<user.addedProducts.count{
                switch i{
                case 0:
                    if let count = user.addedProducts[i].images?.count, count > 0{
                        if let imageOneUrl = user.addedProducts[i].images?[0] {
                            cell.imageOne.kf.setImage(with: URL(string:imageOneUrl))
                        }
                    }
                    break
                case 1:
                    if let count = user.addedProducts[i].images?.count, count > 0{
                        if let imageTwoUrl = user.addedProducts[i].images?[0] {
                            cell.imageTwo.kf.setImage(with: URL(string:imageTwoUrl))
                        }
                    }
                    break
                case 2:
                    if let count = user.addedProducts[i].images?.count, count > 0{
                        if let imageThreeUrl = user.addedProducts[i].images?[0] {
                            cell.imageThree.kf.setImage(with: URL(string:imageThreeUrl))
                        }
                    }
                    break
                case 3:
                    if let count = user.addedProducts[i].images?.count, count > 0{
                        if let imageFourUrl = user.addedProducts[i].images?[0] {
                            cell.imageFour.kf.setImage(with: URL(string:imageFourUrl))
                        }
                    }
                    break
                case 4:
                    if let count = user.addedProducts[i].images?.count, count > 0{
                        if let imageFiveUrl = user.addedProducts[i].images?[0] {
                            cell.imageFive.kf.setImage(with: URL(string:imageFiveUrl))
                        }
                    }
                    break
                case 5:
                    if let count = user.addedProducts[i].images?.count, count > 0{
                        if let imageSixUrl = user.addedProducts[i].images?[0] {
                            cell.imageSix.kf.setImage(with: URL(string:imageSixUrl))
                        }
                    }
                    break
                case 6:
                    if let count = user.addedProducts[i].images?.count, count > 0{
                        if let imageSevenUrl = user.addedProducts[i].images?[0] {
                            cell.imageSeven.kf.setImage(with: URL(string:imageSevenUrl))
                        }
                    }
                    break
                case 7:
                    if let count = user.addedProducts[i].images?.count, count > 0{
                        if let imageEightUrl = user.addedProducts[i].images?[0] {
                            cell.imageEight.kf.setImage(with: URL(string:imageEightUrl))
                        }
                    }
                    break
                default:
                    break
                }
            }

        }
        
        
        return cell
    }

}
