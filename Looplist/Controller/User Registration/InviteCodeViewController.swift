//
//  InviteCodeViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 1/23/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class InviteCodeViewController: UIViewController {

    @IBOutlet weak var noCodeText: UILabel!
    @IBOutlet weak var inviteCodeText: InputTextField!
    @IBOutlet weak var submitInviteButton: UIButton!{
        didSet{
            if let submitInviteButton = submitInviteButton{
                submitInviteButton.layer.cornerRadius = submitInviteButton.frame.height / 2.0
                submitInviteButton.layer.borderWidth = 1.0
                submitInviteButton.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
    @IBOutlet weak var requestCodeButton: UIButton!{
        didSet{
            if let requestCodeButton = requestCodeButton{
                requestCodeButton.layer.cornerRadius = requestCodeButton.frame.height / 2.0
                requestCodeButton.layer.borderWidth = 1.0
                requestCodeButton.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
    @IBOutlet weak var requestInfoText: UILabel!
    @IBOutlet weak var inviteTitleText: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        InviteCodeViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        InviteCodeViewController.stopTimer(["preLogin":true, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], withActionType:"View Screen")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.submitInviteButton.setTitle("SUBMIT".localized, for: .normal)
        self.inviteCodeText.placeholder = "INVITE_CODE_PLACEHOLDER".localized
        self.requestCodeButton.setTitle("REQUEST_CODE".localized, for: .normal)
        self.noCodeText.text = "NO_CODE".localized
        self.requestInfoText.text = "REQUEST_INFO".localized
        self.inviteTitleText.text = "SHOW_INVITE_CODE".localized

        let emptyBackButton = UIBarButtonItem(image: UIImage(named: Constants.backImage), style: .plain, target: self, action: #selector(self.popViewController))
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = emptyBackButton
        setGradient()
    }
    
    
    func setGradient(){
        self.view.layer.insertSublayer(HelperFunctionAnimation.getLooplistGradient(insideFrame: self.view), at: 0)
    }
    

    func popViewController(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitInviteCode(_ sender: UIButton) {
        if let inviteCode = self.inviteCodeText.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
            DBHelper.callAPIGet("invites", queryString: DBHelper.getJSONFromDictionary(["passcode": inviteCode]), completion: { [weak vc = self](result, error) in
                if error == nil{
                    if let invite = result?["invite"] as? NSDictionary, let objectId = invite["_id"] as? String {
                        let createAccountVC:CreateAccountChoiceViewController = CreateAccountChoiceViewController.instanceFromStoryboard("LoginAndRegistration")
                        createAccountVC.inviteCode = objectId
                        vc?.navigationController?.pushViewController(createAccountVC, animated: true)
                    }
                } else {
                    DropDownAlert.customErrorMessage("INVALID_ERROR_CODE".localized)
                }
            })
        }
    }
    @IBAction func requestInviteCode(_ sender: UIButton) {
//        
//        let onboardingVC:OnBoardViewController = OnBoardViewController.instanceFromStoryboard("LoginAndRegistration")
//        self.navigationController?.pushViewController(onboardingVC, animated: true)
        if let url = URL(string: "http://www.looplist.com/curators-info.html") {
            UIApplication.shared.openURL(url)
        }
    }
}
