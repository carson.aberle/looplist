//
//  TutorialCollectionViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 7/21/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class ITutorialViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: -  //////////////////////////////////////////////////////    VARIABLES    /////////////////////////////////////////////////////////////////////////
    
    var pageTitles = ["Welcome to your perfect store.","A personal algorithm, just for you.", "Your new product discovery resource.", "Discover with (and for) your friends.", "Maven is always by your side."]
    var pageDescriptions = ["For you alone it is the perfect store.  It is catered to your tastes by your individuality and your social influence."
        ,"By beta testing, you are training our alorithm to make the perfect store. (Thanks for that!)"
        , "Browse, add, share and like products. You’ll soon see a personalized experience. "
        , "Your social interactions play a crucial role in the development of your identity. Your friends help evolve your ever evolving taste."
        ,"Maven is Looplist’s evolving Artficial Intelligence. Send it a message to find products, ask questions or get help."]
    var pageImages:[UIImage] = [UIImage(named: Constants.tutorialUserProfileImage)!,UIImage(named: Constants.tutorialMavenPageImage)!,UIImage(named: Constants.tutorialListingPageImage)!]
    var tutorialCell:TutorialCollectionViewCell?
    
    var signInVC = UINavigationController()//UserSignInViewController()
    var locationOfGetStartedButton = 0
    
    // MARK: -  //////////////////////////////////////////////////////    OUTLETS    /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var tutorialCollectionView: UICollectionView!
    @IBOutlet weak var tutorialPageControl: UIPageControl!
    
    @IBOutlet weak var getStartedButton: UIButton!{
        didSet{
            if let getStartedButton = getStartedButton{
                getStartedButton.layer.cornerRadius = getStartedButton.frame.height / 2.0
                getStartedButton.layer.borderWidth = 0.5
                getStartedButton.layer.borderColor = UIColor(red: 95/255, green: 95/255, blue: 95/255, alpha: 1.0).cgColor
            }
        }
    }
    // MARK: -  //////////////////////////////////////////////////////    APPLICATION LIFE CYCLE    /////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tutorialCollectionView.delegate = self
        self.tutorialCollectionView.dataSource = self
        self.tutorialCollectionView.backgroundColor = Constants.looplistColor
        self.tutorialCollectionView.backgroundView = UIView(frame: CGRect.zero)
        self.tutorialCollectionView.bounces = false
        
        tutorialPageControl.numberOfPages = self.pageTitles.count + 1
        
        UIApplication.shared.isStatusBarHidden = false;
        if let navigationController = self.navigationController{
            TutorialViewController.self.setNavigationBar(UIColor.white, font: Constants.navBarFont, navigationController: navigationController, statusBar:"Light", backgroundColor:"Clear No Gradient")
        }
        //        self.navigationItem.title = "Sign In"
        self.navigationItem.backBarButtonItem?.title = ""
        
//        signInVC = storyboard?.instantiateViewController(withIdentifier: "UserSignInViewController") as! UserSignInViewController
        signInVC = storyboard?.instantiateViewController(withIdentifier: "SignInNavigationController") as! UINavigationController
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
    }
    
    @IBAction func UnwindToTutorialScreen(_ segue: UIStoryboardSegue) {
        
    }
    
    // MARK: -  //////////////////////////////////////////////////////    FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func skipTutorial(_ sender: UIButton) {
        let lastRowIndexPath = IndexPath(row: self.pageTitles.count, section: 0)
        self.tutorialCollectionView.scrollToItem(at: lastRowIndexPath, at: UICollectionViewScrollPosition.left, animated: false)
    }
    
//    func loadSignInScreen(){
//        let storyboard: UIStoryboard = UIStoryboard(name: "LoginAndRegistration", bundle: nil)
//        let signInVC = storyboard.instantiateViewController(withIdentifier: Constants.viewControllerSignIn) as! UINavigationController
////        self.navigationController?.pushViewController(signInVC, animated: true)
//        let v: UIView = signInVC.view!
//        var f: CGRect = v.frame
//        f.origin.x += self.view.frame.size.width
//        //move to right
//        v.frame = f
//        UIView.animate(withDuration: 1.5, animations: {() -> Void in
//            v.frame = self.view.frame
//            }, completion: {(finished: Bool) -> Void in
//                self.dismiss(animated: false, completion: nil)
//                self.present(signInVC, animated: false, completion: { _ in })
//        })
//    }
    
    // MARK: -  //////////////////////////////////////////////////////    COLLECTION VIEW DATA SOURCE    /////////////////////////////////////////////////////////////////////////
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.pageTitles.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (indexPath.row < pageTitles.count){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TutorialCollectionViewCell", for: indexPath) as! TutorialCollectionViewCell
            self.tutorialCell = cell
//            getStartedButton.alpha = 1.0
            
            cell.pageTitle.text = pageTitles[indexPath.row]
            cell.pageDescription.text = pageDescriptions[indexPath.row]
//            cell.pageControl.numberOfPages = self.pageTitles.count
//            if(indexPath.row != 0){
////                cell.phoneScreen.image = pageImages[(indexPath.row - 1)]
//            }
            
//            if (indexPath.row == pageTitles.count && self.getStartedButton.alpha == 0.0){
//                self.getStartedButton.alpha = 1.0
//            }
            return cell
        }else {
//            self.loadSignInScreen()
//            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TutorialCollectionViewSignInCell", for: indexPath) as! TutorialCollectionViewSignInCell
           
//            signInVC.view.frame = CGRect(x:0,y:0,width:cell.frame.width,height:cell.frame.height)
////
            cell.view.addSubview(signInVC.view)
            
////            getStartedButton.alpha = 0.0
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if !(indexPath.row < pageTitles.count){
            self.getStartedButton.alpha = 0.0
        }else{
//             self.getStartedButton.alpha = 1.0
        }
    }
    
     @objc(collectionView:didEndDisplayingCell:forItemAtIndexPath:) func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if !(indexPath.row < pageTitles.count){
            self.getStartedButton.alpha = 1.0
        }
        
//        else{
//            self.getStartedButton.alpha = 1.0
//        }
    }

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: self.view.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(-64, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        locationOfGetStartedButton = page
        tutorialPageControl?.currentPage = page
    }
}
