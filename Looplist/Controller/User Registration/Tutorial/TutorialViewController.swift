//
//  TutorialViewController.swift
//  Looplist
//
//  Created by Roman Wendelboe on 12/6/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {
    
    // Mark: Variables
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
//    @IBOutlet weak var getStartedButton: UIButton!{
//        didSet{
//            if let getStartedButton = getStartedButton{
//                getStartedButton.layer.cornerRadius = getStartedButton.frame.height / 2.0
//                getStartedButton.layer.borderWidth = 0.5
//                getStartedButton.layer.borderColor = UIColor(red: 95/255, green: 95/255, blue: 95/255, alpha: 1.0).cgColor
//            }
//        }
//    }
    @IBOutlet weak var swipeToBeginStackView: UIStackView!
    
//    @IBOutlet weak var getBackButton: UIBarButtonItem!
    var lastIndex = 0
    
    var tutorialPageViewController: TutorialPageViewController? {
        didSet {
            tutorialPageViewController?.tutorialDelegate = self
        }
    }
    
    // Mark: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.getStartedButton.setTitle("GET_STARTED".localized, for: .normal)
        pageControl.addTarget(self, action: #selector(TutorialViewController.didChangePageControlValue), for: .valueChanged)
        
        UIApplication.shared.isStatusBarHidden = false;
        if let navigationController = self.navigationController{
            TutorialViewController.self.setNavigationBar(UIColor.white, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.lightContent, backgroundColor:NavigationBarBackgroundColor.clearNoGradient)
        }
        setGradient()
    }
    
    
    func setGradient(){
        self.containerView.layer.insertSublayer(HelperFunctionAnimation.getLooplistGradient(insideFrame: self.view), at: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        self.hideLeftBarItem()
        if let tutorialPageViewController = segue.destination as? TutorialPageViewController {
            self.tutorialPageViewController = tutorialPageViewController
        }
    }
    
    // Mark : Functions
    
//    @IBAction func skipToSignIn(_ sender: UIButton) {
//        UIView.animate(withDuration: 0.3) { 
//            self.getStartedButton.isHidden = true
//            self.pageControl.isHidden = true
//
//        }
//        self.lastIndex = pageControl.currentPage
//        tutorialPageViewController?.scrollToViewController(index: 3)
//        self.showLeftBarItem()
//    }
    
//    @IBAction func getBack(_ sender: UIBarButtonItem) {
//        self.hideLeftBarItem()
//        tutorialPageViewController?.scrollToViewController(index: self.lastIndex)
//    }
    
//    func hideLeftBarItem(){
//        self.getBackButton.isEnabled = false
//        self.getBackButton.tintColor = UIColor.clear
//    }
    
//    func showLeftBarItem(){
//        UIView.animate(withDuration: 0.3, delay: 0.0, options: [], animations: {
//            self.getBackButton.isEnabled = true
//            self.getBackButton.tintColor = UIColor.white
//        }, completion: nil)
//    }
    
    // Fired when the user taps on the pageControl to change its current page.
    func didChangePageControlValue() {
        tutorialPageViewController?.scrollToViewController(index: pageControl.currentPage)
    }

}

// Mark: TutorialPageViewControllerDelegate
extension TutorialViewController: TutorialPageViewControllerDelegate {
    
    func tutorialPageViewController(_ tutorialPageViewController: TutorialPageViewController, didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    func tutorialPageViewController(_ tutorialPageViewController: TutorialPageViewController, didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
        if index == 3{
//            getStartedButton.isHidden = true
            swipeToBeginStackView.isHidden = true
            pageControl.isHidden = true
//            self.showLeftBarItem()
        }
        else if index == 1 || index == 2{
            swipeToBeginStackView.isHidden = true
            
        }
        
        else {
//            self.hideLeftBarItem()
//            getStartedButton.isHidden = false
            swipeToBeginStackView.isHidden = false

            pageControl.isHidden = false
        }
    }
    
    func willApear(_ tutorialPageViewController: TutorialPageViewController, willMoveToIndex index: Int) {
        if index == 3{
//            getStartedButton.isHidden = true
            swipeToBeginStackView.isHidden = true
            pageControl.isHidden = true
        }else if index == 1 || index == 2{
            swipeToBeginStackView.isHidden = true
            
        }
        
        else{
//            self.hideLeftBarItem()
        }
    }
}
