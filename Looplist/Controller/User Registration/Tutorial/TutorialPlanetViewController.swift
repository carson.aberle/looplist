//
//  TutorialPlanetViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 1/4/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class TutorialPlanetViewController: UIViewController {
    @IBOutlet weak var planet: UIImageView!
    @IBOutlet weak var telescope: UIImageView!
    @IBOutlet weak var stars: UIImageView!
    @IBOutlet weak var titleText: UILabel!

    
    override func viewDidDisappear(_ animated: Bool) {
        TutorialPlanetViewController.stopTimer(["preLogin":true, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], withActionType:"View Screen")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.telescope.isHidden = true
        self.stars.isHidden = true
        self.planet.isHidden = true
        self.titleText.text = "YOUR_NEW_PRODUCT_DISCOVERY_RESOURCE".localized
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.telescope.isHidden = true
        self.stars.isHidden = true
        self.planet.isHidden = true
        
        self.telescope.frame.origin.x -= UIScreen.main.bounds.size.width
        self.stars.alpha = 0.0
        self.planet.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        TutorialPlanetViewController.startTimer()

        self.telescope.isHidden = false
        self.stars.isHidden = false
        self.planet.isHidden = false
        UIView.animate(withDuration: 0.4, delay:0.4, animations: {
            self.stars.alpha = 1.0
            self.telescope.frame.origin.x += UIScreen.main.bounds.size.width
            self.view.bringSubview(toFront: self.telescope)
        })

        UIView.animate(withDuration: 0.5, delay: 0.1, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.planet.transform = CGAffineTransform.identity

        }) { (success) in
            
        }
    }

}
