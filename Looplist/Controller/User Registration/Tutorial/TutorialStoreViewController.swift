//
//  TutorialStoreViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 1/4/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class TutorialStoreViewController: UIViewController {

    @IBOutlet weak var ground: UIImageView!
    @IBOutlet weak var store: UIImageView!
    @IBOutlet weak var sign: UIImageView!
    @IBOutlet weak var titleText: UILabel!
    

    override func viewDidDisappear(_ animated: Bool) {
        TutorialStoreViewController.stopTimer(["preLogin":true, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], withActionType:"View Screen")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleText.text = "WELCOME_TO_YOUR_PERFECT_STORE".localized

        self.ground.isHidden = true
        self.store.isHidden = true
        self.sign.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.ground.isHidden = true
        self.store.isHidden = true
        self.sign.isHidden = true
        
        self.ground.alpha = 0.0
        self.store.frame.origin.x -= UIScreen.main.bounds.size.width
        self.sign.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        TutorialStoreViewController.startTimer()

        self.ground.isHidden = false
        self.store.isHidden = false
        self.sign.isHidden = false
        
        UIView.animate(withDuration: 0.3, delay:0.4, animations: {
            self.sign.transform = CGAffineTransform.identity
        })
        UIView.animate(withDuration: 0.6, delay: 0.15, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.ground.alpha = 1.0
            self.store.frame.origin.x += UIScreen.main.bounds.size.width
        }) { (success) in
            
        }
    }

}
