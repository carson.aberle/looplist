//
//  TutorialMusicViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 1/4/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class TutorialAlgorithmViewController: UIViewController {

    @IBOutlet weak var spires: UIImageView!
    @IBOutlet weak var mainBubbles: UIImageView!
    @IBOutlet weak var backgroundBubbles: UIImageView!

    @IBOutlet weak var titleText: UILabel!

    override func viewDidDisappear(_ animated: Bool) {
        TutorialAlgorithmViewController.stopTimer(["preLogin":true, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], withActionType:"View Screen")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleText.text = "A_PERSONAL_ALGORITHM_JUST_FOR_YOU".localized
        self.spires.isHidden = true
        self.mainBubbles.isHidden = true
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        self.spires.isHidden = true
        self.mainBubbles.isHidden = true
        
        self.spires.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        self.mainBubbles.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        TutorialAlgorithmViewController.startTimer()

        self.spires.isHidden = false
        self.mainBubbles.isHidden = false
        
        UIView.animate(withDuration: 0.5, delay: 0.1, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.spires.transform = CGAffineTransform.identity
            self.mainBubbles.transform = CGAffineTransform.identity
            
        }) { (success) in
            
        }
    }

}
