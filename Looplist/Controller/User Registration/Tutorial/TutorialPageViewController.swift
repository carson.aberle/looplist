//
//  TutorialPageViewController.swift
//  Looplist
//
//  Created by Roman Wendelboe on 12/6/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class TutorialPageViewController: UIPageViewController {
    
    // Mark: Variabels
    
    weak var tutorialDelegate: TutorialPageViewControllerDelegate?
    
    fileprivate(set) lazy var orderedViewControllers: [UIViewController] = {
        // The view controllers will be shown in this order
        return [self.newTutorialViewController("TutorialStoreViewController"),
                self.newTutorialViewController("TutorialMusicViewController"),
                self.newTutorialViewController("TutorialPlanetViewController"),
                self.newTutorialViewController("UserSignInViewController")]
    }()
    
    var nextIndex = 0
    var currentIndex = 0
    
    // Mark: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        delegate = self
        
        if let initialViewController = orderedViewControllers.first {
            scrollToViewController(initialViewController)
        }
        
        tutorialDelegate?.tutorialPageViewController(self, didUpdatePageCount: orderedViewControllers.count)
    }
    
    // Scrolls to the next view controller.
    
    func scrollToNextViewController() {
        if let visibleViewController = viewControllers?.first,
            let nextViewController = pageViewController(self, viewControllerAfter: visibleViewController) {
            scrollToViewController(nextViewController)
        }
    }
    
    // Scrolls to the view controller at the given index. Automatically calculates the direction.
    func scrollToViewController(index newIndex: Int) {
        if let firstViewController = viewControllers?.first,
            let currentIndex = orderedViewControllers.index(of: firstViewController) {
            let direction: UIPageViewControllerNavigationDirection = newIndex >= currentIndex ? .forward : .reverse
            let nextViewController = orderedViewControllers[newIndex]
            scrollToViewController(nextViewController, direction: direction)
        }
    }
    
    // Call view controller from code
    fileprivate func newTutorialViewController(_ identifier: String) -> UIViewController {
        return UIStoryboard(name: "LoginAndRegistration", bundle: nil) .instantiateViewController(withIdentifier: "\(identifier)")
    }
    
    // Scrolls to the given 'viewController' page.
    
    fileprivate func scrollToViewController(_ viewController: UIViewController, direction: UIPageViewControllerNavigationDirection = .forward) {
        setViewControllers([viewController], direction: direction, animated: true, completion: { (finished) -> Void in self.notifyTutorialDelegateOfNewIndex()
        })
    }
    
    // Notifies '_tutorialDelegate' that the current page index was updated.
    
    fileprivate func notifyTutorialDelegateOfNewIndex() {
        if let firstViewController = viewControllers?.first,
            let index = orderedViewControllers.index(of: firstViewController) {
            tutorialDelegate?.tutorialPageViewController(self, didUpdatePageIndex: index)
        }
    }
    
}

// MARK: UIPageViewControllerDataSource

extension TutorialPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        if let controller = pendingViewControllers.first{
            self.nextIndex = self.orderedViewControllers.index(of: controller)!
            
            tutorialDelegate?.willApear(self, willMoveToIndex: self.nextIndex)
        }
    }
}

extension TutorialPageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool,previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        notifyTutorialDelegateOfNewIndex()
    }
}

protocol TutorialPageViewControllerDelegate: class {
    
    func tutorialPageViewController(_ tutorialPageViewController: TutorialPageViewController, didUpdatePageCount count: Int)
    
    func tutorialPageViewController(_ tutorialPageViewController: TutorialPageViewController, didUpdatePageIndex index: Int)
    
    func willApear(_ tutorialPageViewController: TutorialPageViewController, willMoveToIndex index: Int)
}
