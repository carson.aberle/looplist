//
//  UserRegistrationEmailFinalViewController.swift
//  alphalooplist
//
//  Created by Looplist Inc on 3/4/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import Foundation
import UIKit

class UserRegistrationEmailFinalViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate {
    
    // MARK: -  //////////////////////////////////////////////////////    VARIABLES    /////////////////////////////////////////////////////////////////////////
    
    var firstNameSent: String = ""
    var lastNameSent: String = ""
    var emailSent: String = ""
    var inviteCode:String?

    var usernameTaken = false
    
    fileprivate var indicator = CustomActivityIndicator()

    

    // MARK: -  //////////////////////////////////////////////////////    OUTLETS    /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var acceptTermsAndConditionsSwitch: UISwitch!

    @IBOutlet weak var nextButton: UIButton!{
        didSet{
            if let nextButton = nextButton{
                nextButton.layer.cornerRadius = nextButton.frame.height / 2.0
                nextButton.layer.borderWidth = 1.0
                nextButton.layer.borderColor = UIColor.white.cgColor
            }
        }
    }

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var acceptTermsAndConditionsLabel: UIButton!
    
    // MARK: -  //////////////////////////////////////////////////////    APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "ACCOUNT_INFO".localized
        self.usernameText.placeholder = "USERNAME".localized
        self.passwordText.placeholder = "PASSWORD".localized
        self.nextButton.setTitle("NEXT".localized, for: .normal)
        self.acceptTermsAndConditionsLabel.setTitle("I_ACCEPT_THE_TERMS_AND_CONDITIONS".localized, for: .normal)
        
        self.usernameText.delegate = self
        self.passwordText.delegate = self
        
         setGradient()
    }
    
    
    func setGradient(){
        self.view.layer.insertSublayer(HelperFunctionAnimation.getLooplistGradient(insideFrame: self.view), at: 0)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        usernameText.becomeFirstResponder()
        UserRegistrationEmailFinalViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        UserRegistrationEmailFinalViewController.stopTimer(["preLogin":true, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], withActionType:"View Screen")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController{
            UserRegistrationEmailInitialViewController.self.setNavigationBar(UIColor.white, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.lightContent, backgroundColor:NavigationBarBackgroundColor.clearNoGradient)
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////    FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func termsAndConditionToggled(_ sender: UISwitch) {
    }
    
    @IBAction func showTermsAndConditions(_ sender: UIButton) {
        let termsAndConditionsVC:SettingsTermsAndConditionsViewController = SettingsTermsAndConditionsViewController.instanceFromStoryboard("Settings")
        self.navigationController?.pushViewController(termsAndConditionsVC, animated:true)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view!.endEditing(true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowNext"{
            if let vc = segue.destination as? UserRegistrationProfilePhotoViewController{
                vc.firstNameSent = self.firstNameSent
                vc.lastNameSent = self.lastNameSent
                vc.emailSent = self.emailSent
                vc.usernameSent = (self.usernameText.text?.lowercased())!
                vc.passwordSent = self.passwordText?.text
                vc.inviteCode = self.inviteCode
            }
        }
    }
    
    @IBAction func FinalizeRegistration(_ sender: UIButton!) {
        self.view.endEditing(true)
        
        let usernameEntered:String = (self.usernameText.text?.lowercased())!
        let passwordEntered:String = self.passwordText.text!
        
        self.view.endEditing(true)
        
        if usernameEntered.characters.count < 3 || usernameEntered.characters.count > 20  {
            DropDownAlert.customErrorMessage("USERNAME_LENGTH_ERROR".localized)
        } else if passwordEntered.characters.count < 8 || passwordEntered.characters.count > 20 {
            DropDownAlert.customErrorMessage("PASSWORD_LENGTH_ERROR".localized)
        } else if !self.acceptTermsAndConditionsSwitch.isOn {
            DropDownAlert.customErrorMessage("ACCEPT_TERMS_AND_CONDITIONS_ERROR".localized)
        } else if passwordEntered.range(of: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") != nil {
            DropDownAlert.customErrorMessage("PASSWORD_REQUIREMENTS_ERROR".localized)
        } else if (HelperFunction.checkIfOnlyWhitespace(usernameEntered)) {
            DropDownAlert.customErrorMessage("USERNAME_BLANK_ERROR".localized)
        } else if (HelperFunction.checkIfAlphanumericDotUnderscore(usernameEntered)) {
            DropDownAlert.customErrorMessage("USERNAME_REQUIREMENTS_ERROR".localized)
        }else {
            self.indicator.showActivityIndicator(uiView: self.view, offset: 0.0)
            //Check if username is already taken
            let params:NSDictionary = ["username": usernameEntered]
            DBHelper.callAPIGet("authentication/local/availability", queryString: DBHelper.getJSONFromDictionary(params), completion: { (result, error) in
                self.indicator.hideActivityIndicator()
                if(error == nil){
                    if(result?["username"] as! Bool) {
                        self.performSegue(withIdentifier: "ShowNext", sender: self)

                    } else {
                        ErrorHandling.customErrorHandlerWithTitle(title:"SORRY".localized, message: "USERNAME_TAKEN".localized)
                    }
                } else {
                    ErrorHandling.customErrorHandlerWithTitle(title:"SORRY".localized, message: "AN_ERROR_OCCURED".localized)
                }
            })
        }
    }
    
    @IBAction func popViewController(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isEqual(self.usernameText){
            self.passwordText.becomeFirstResponder()
        }
        textField.resignFirstResponder()
        return true;
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.isEqual(self.usernameText){
            textField.returnKeyType = .next
        } else if textField.isEqual(self.passwordText){
            textField.returnKeyType = .done
        }
//        self.animateTextField(textField, up: true)
    }
    
}
