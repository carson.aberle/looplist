//
//  CreateAccountChoiceViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 1/22/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin

class CreateAccountChoiceViewController: UIViewController {
    
    let MyKeychainWrapper = KeychainWrapper()
    var inviteCode:String?
    
    @IBOutlet weak var orText: UILabel!
    @IBOutlet weak var emailButton: UIButton!{
        didSet{
            if let emailButton = emailButton{
                emailButton.layer.cornerRadius = emailButton.frame.height / 2.0
                emailButton.layer.borderWidth = 1.0
                emailButton.setTitleColor(UIColor.white, for: .normal)
                emailButton.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
    @IBOutlet weak var facebookButton: TKTransitionSubmitButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "CREATE_ACCOUNT_TITLE".localized
        self.emailButton.setTitle("SIGN_UP_WITH_EMAIL".localized, for: .normal)
        self.facebookButton.setTitle("SIGN_UP_WITH_FACEBOOK".localized, for: .normal)
        
        self.orText.text! = "OR".localized
        
        let emptyBackButton = UIBarButtonItem(image: UIImage(named: Constants.backImage), style: .plain, target: self, action: #selector(self.popViewController))
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = emptyBackButton
        
        self.setGradient()
    }
    
    func setGradient(){
        self.view.layer.insertSublayer(HelperFunctionAnimation.getLooplistGradient(insideFrame: self.view), at: 0)
    }
    
    @IBAction func signUpWithFacebook(_ sender: UIButton) {
        if let _ = AccessToken.current?.authenticationToken{
            self.getFacebookInfo()
        } else {
            let loginManager = LoginManager()
            loginManager.logIn([.publicProfile, .userFriends, .email], viewController: self, completion: { (loginResult) in
                switch loginResult {
                case .failed( _):
                    break;
                case .cancelled:
                    break;
                case .success(grantedPermissions: _, declinedPermissions: _, token: _):
                    self.getFacebookInfo()
                    break;
                }
                
            })
        }
    }
    
    func getFacebookInfo(){
        let connection = GraphRequestConnection()
        connection.add(MyProfileRequest()) { response, result in
            switch result {
            case .success(let response):
                self.checkIfFacebookIdIsUsed(response: response)
                break

            case .failed(_):
                DropDownAlert.customErrorMessage("ERROR".localized)
            }
        }
        connection.start()
    }
    
    @IBAction func signupWithEmail(_ sender: UIButton) {
        let userRegistrationInitial:UserRegistrationEmailInitialViewController = UserRegistrationEmailInitialViewController.instanceFromStoryboard("LoginAndRegistration")
        userRegistrationInitial.inviteCode = self.inviteCode
        self.navigationController?.pushViewController(userRegistrationInitial, animated: true)
    }
    func startSignupProcess(response:MyProfileRequest.Response){
        let facebookFirstVC:FacebookSignupFirstViewController = FacebookSignupFirstViewController.instanceFromStoryboard("LoginAndRegistration")
        facebookFirstVC.response = response
        facebookFirstVC.inviteCode = self.inviteCode
        self.navigationController?.pushViewController(facebookFirstVC, animated: true)
    }
    
    func popViewController(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func checkIfFacebookIdIsUsed(response:MyProfileRequest.Response){
        if let id = response.id{
            //Query to see if a user already exists with given Facebook ID
            DBHelper.callAPIGet("authentication/facebook/availability", queryString: DBHelper.getJSONFromDictionary(["facebookId": id]), completion: { (result, error) in
                if let result = result{
                    if let isFBIdAvailable = result["facebookId"] as? Bool{
                        if isFBIdAvailable {
                            DBHelper.callAPIGet("authentication/local/availability", queryString: DBHelper.getJSONFromDictionary(["email":response.email!]), completion: { (result, error) in
                                if error == nil{
                                    if let isEmailAvailable = result?["email"] as? Bool{
                                        if isEmailAvailable{
                                            self.startSignupProcess(response: response)
                                        } else {
                                            DropDownAlert.customErrorMessage("EMAIL_TAKEN".localized)
                                        }
                                    }
                                } else {
                                    DropDownAlert.customErrorMessage("ERROR".localized)
                                }
                            })
                        } else {
                            self.signInToLooplistWithFacebook()
                        }
                    } else {
                        DropDownAlert.customErrorMessage("ERROR".localized)
                    }
                } else {
                    DropDownAlert.customErrorMessage("ERROR".localized)
                }
            })
        } else {
            DropDownAlert.customErrorMessage("ERROR".localized)
        }
    }
    
    fileprivate func signInToLooplistWithFacebook(){
        self.facebookButton.startLoadingAnimation()
        if let fbAccessToken = AccessToken.current?.authenticationToken{
            DBHelper.callAPIPost("authentication/facebook/login", params: ["facebookOauthToken":fbAccessToken as Optional<AnyObject>], completion: { (result, error) in
                if let result = result{
                    if let user = result["user"] as? Dictionary<String, AnyObject>, let accessToken = result["accessToken"] as? String{
                        CurrentUser.sharedInstance.assignCurrentUser(User(input:user))
                        //Store access token in keychain
                        self.MyKeychainWrapper.mySetObject(accessToken, forKey:kSecValueData)
                        self.MyKeychainWrapper.writeToKeychain()
                        if let userObjectId = CurrentUser.sharedInstance.currentUser?.objectId {
                            
                            Analytics.sharedInstance.actionsArray.append(Action(context:["userSignIn": userObjectId, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], forScreen:nil, withActivityType:"Signed In")) // $AS
                            
                            self.facebookButton.startFinishAnimation(delay: 2.0, completion: { () -> () in
                                
                                DBHelper.setUserDefaults()
                                
                                self.dismiss(animated: true, completion:nil)
                                if let appDelegate = UIApplication.shared.delegate as? AppDelegate{
                                    appDelegate.showScreen(ConstantsViewController.viewControllerTabBar)
                                    self.facebookButton.removeFromSuperview()
                                }
                            })
                            
                        } else {
                            ErrorHandling.customErrorHandler(message: "ERROR_SIGNING_IN".localized)
                        }
                    } else {
                        ErrorHandling.customErrorHandler(message: "ERROR_SIGNING_IN".localized)
                    }
                } else if let _ = error {
                    self.facebookButton.stopLoadingAnimation()
                    ErrorHandling.customErrorHandlerWithTitle(title:"ERROR".localized, message: "FACEBOOK_LOGIN_FAILED".localized)
                }
            })
        }
    }
    
    
}

public struct MyProfileRequest: GraphRequestProtocol {
    
    public struct Response: GraphResponseProtocol {
        var id:String?
        var firstName:String?
        var lastName:String?
        var email:String?
        var gender:String?
        var photoUrl:String?
        
        public init(rawResponse: Any?) {
            if let response = rawResponse as? NSDictionary{
                if let id = response["id"] as? String{
                    self.id = id
                }
                if let email = response["email"] as? String{
                    self.email = email
                }
                if let firstName = response["first_name"] as? String{
                    self.firstName = firstName
                }
                if let lastName = response["last_name"] as? String{
                    self.lastName = lastName
                }
                if let gender = response["gender"] as? String{
                    self.gender = gender
                }
                if let picture = response["picture"] as? NSDictionary, let data = picture["data"] as? NSDictionary, let url = data["url"] as? String{
                    self.photoUrl = url
                }
            }
        }
    }
    
    public var graphPath = "/me"
    public var parameters: [String : Any]? = ["fields": "id, email, first_name, last_name, gender, picture.type(large)"]
    public var accessToken = AccessToken.current
    public var httpMethod: GraphRequestHTTPMethod = .GET
    public var apiVersion: GraphAPIVersion = .defaultVersion
}

