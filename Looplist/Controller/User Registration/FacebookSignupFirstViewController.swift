//
//  FacebookSignupFirstViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 1/23/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class FacebookSignupFirstViewController: UIViewController, UITextFieldDelegate {

    var response:MyProfileRequest.Response?
    var inviteCode:String?

    @IBOutlet weak var locationText: InputTextField!
    @IBOutlet weak var termsAndConditionsSwitch: UISwitch!
    @IBOutlet weak var termsAndConditionsText: UIButton!
    @IBOutlet weak var nextButton: UIButton!{
        didSet{
            if let nextButton = nextButton{
                nextButton.layer.cornerRadius = nextButton.frame.height / 2.0
                nextButton.layer.borderWidth = 1.0
                nextButton.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "BASIC_INFO".localized
        let emptyBackButton = UIBarButtonItem(image: UIImage(named: Constants.backImage), style: .plain, target: self, action: #selector(self.popViewController))
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = emptyBackButton
        
        self.nextButton.setTitle("NEXT".localized, for: .normal)
        self.locationText.placeholder = "LOCATION_EXAMPLE".localized
        self.termsAndConditionsText.setTitle("I_ACCEPT_THE_TERMS_AND_CONDITIONS".localized, for: .normal)
        
        self.locationText.delegate = self
        setGradient()
    }
    
    
    func setGradient(){
        self.view.layer.insertSublayer(HelperFunctionAnimation.getLooplistGradient(insideFrame: self.view), at: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = self.navigationController{
            FacebookSignupFirstViewController.setNavigationBar(UIColor.white, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.lightContent, backgroundColor:NavigationBarBackgroundColor.clearNoGradient)
        }
    }

    func popViewController(){
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func next(_ sender: UIButton) {
        if self.termsAndConditionsSwitch.isOn{
            let facebookSignupLastVC:FacebookSignupLastViewController = FacebookSignupLastViewController.instanceFromStoryboard("LoginAndRegistration")
            facebookSignupLastVC.response = self.response
            facebookSignupLastVC.inviteCode = self.inviteCode
            facebookSignupLastVC.location = self.locationText.text
            self.navigationController?.pushViewController(facebookSignupLastVC, animated: true)
        } else {
            DropDownAlert.customErrorMessage("ACCEPT_TERMS_AND_CONDITIONS_ERROR".localized)
        }
        
    }
    @IBAction func showTermsAndConditions(_ sender: UIButton) {
        let termsAndConditionsVC:SettingsTermsAndConditionsViewController = SettingsTermsAndConditionsViewController.instanceFromStoryboard("Settings")
        self.navigationController?.pushViewController(termsAndConditionsVC, animated:true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view!.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }

}
