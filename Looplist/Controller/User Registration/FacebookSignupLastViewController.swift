//
//  FacebookSignupLastViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 1/23/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit
import FacebookCore

class FacebookSignupLastViewController: UIViewController {
    
    let MyKeychainWrapper = KeychainWrapper()
    var strBase64:String = ""
    var inviteCode:String?
    var response:MyProfileRequest.Response?
    var location:String?
    var selectedUserProfileImage:UIImage?
    @IBOutlet weak var profilePhoto: UIImageView!{
        didSet{
            profilePhoto.clipsToBounds = true
            profilePhoto.layer.cornerRadius = profilePhoto.frame.height / 2.0
        }
    }
    @IBOutlet weak var usernameText: InputTextField!
    @IBOutlet weak var signUpButton: TKTransitionSubmitButton!{
        didSet{
            if let signUpButton = signUpButton{
                signUpButton.layer.cornerRadius = signUpButton.frame.height / 2.0
                signUpButton.layer.borderWidth = 1.0
                signUpButton.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "SIGN_UP".localized
        self.usernameText.placeholder = "USERNAME".localized
        self.signUpButton.setTitle("SIGN_UP".localized, for: .normal)
        
        if let profilePhotoUrl = self.response?.photoUrl{
            self.profilePhoto.kf.setImage(with: URL(string:profilePhotoUrl), completionHandler: { (image, error, cache, url) in
                if let image = image{
                    self.selectedUserProfileImage = image
                    let imageData:Data = UIImageJPEGRepresentation(image, 0.75)!
                    self.strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
                    
                }
            })
            
        }
        
        let emptyBackButton = UIBarButtonItem(image: UIImage(named: Constants.backImage), style: .plain, target: self, action: #selector(self.popViewController))
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = emptyBackButton
        setGradient()
    }
    
    
    func setGradient(){
        self.view.layer.insertSublayer(HelperFunctionAnimation.getLooplistGradient(insideFrame: self.view), at: 0)
    }
    
    
    func popViewController(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func checkUsernameInDB(){
        if let usernameEntered = usernameText.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            //Check if username is already taken
            let params:NSDictionary = ["username": usernameEntered]
            DBHelper.callAPIGet("authentication/local/availability", queryString: DBHelper.getJSONFromDictionary(params), completion: { (result, error) in
                if(error == nil){
                    if(result?["username"] as! Bool) {
                        self.finalizeSignup()
                    } else {
                        ErrorHandling.customErrorHandlerWithTitle(title:"SORRY".localized, message: "USERNAME_TAKEN".localized)
                    }
                } else {
                    ErrorHandling.customErrorHandlerWithTitle(title:"SORRY".localized, message: "AN_ERROR_OCCURED".localized)
                }
            })
        }
    }
    
    func finalizeSignup(){
        signUpButton.startLoadingAnimation()
        
        if let username = usernameText.text?.trimmingCharacters(in: .whitespacesAndNewlines), let firstName = self.response?.firstName, let lastName = self.response?.lastName, let email = self.response?.email, let location = self.location, let facebookId = self.response?.id, var gender = self.response?.gender, let accessToken = AccessToken.current?.authenticationToken {
            
            var parameters:[String:AnyObject]
            let path = "authentication/facebook/signup"
            if gender == "male"{
                gender = "Male"
            } else {
                gender = "Female"
            }
            if self.strBase64 != "" {
                
                parameters = ["username": username as AnyObject, "facebookOauthToken": accessToken as AnyObject, "facebookId": facebookId as AnyObject,"firstName":firstName as AnyObject,"lastName":lastName as AnyObject,"email":email as AnyObject,"gender":gender as AnyObject,"location":location as AnyObject,"profile64":self.strBase64 as AnyObject]
            } else {
                parameters = ["username": username as AnyObject,"facebookOauthToken": accessToken as AnyObject, "facebookId": facebookId as AnyObject,"firstName":firstName as AnyObject,"lastName":lastName as AnyObject,"email":email as AnyObject,"gender":gender as AnyObject,"location":location as AnyObject]
            }
            DBHelper.callAPIPost(path, params: parameters, completion: { (result, error) in
                if error == nil {
                    if let userId = result?["_id"] as? String{
                        Analytics.sharedInstance.actionsArray.append(Action(context:["createUser": userId, "signup":true, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], forScreen: "Registration Photo", withActivityType:"Created Account"))
                    }
                    self.signUpButton.startFinishAnimation(delay: 2.0, completion: { () -> () in })
                    let delayTime = DispatchTime.now() + Double(Int64(2 * (Double(NSEC_PER_SEC) * 0.98))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        
                        if let user = result?["user"] as? NSDictionary, let accessToken = result?["accessToken"] as? String{
                            CurrentUser.sharedInstance.assignCurrentUser(User(input:user as! Dictionary<String, AnyObject>))
                            self.MyKeychainWrapper.mySetObject(accessToken, forKey:kSecValueData)
                            self.MyKeychainWrapper.mySetObject(username, forKey: kSecAttrAccount)
                            self.MyKeychainWrapper.writeToKeychain()
                            
                            if let inviteCode = self.inviteCode{
                                DBHelper.callAPIPut("invites/\(inviteCode)/accept", completion: { (result, error) in
                                    if error == nil {
                                        if let _ = CurrentUser.sharedInstance.currentUser?.objectId {
                                            DBHelper.setUserDefaults()
                                            
                                            let onBoardVC:OnBoardViewController = OnBoardViewController.instanceFromStoryboard("LoginAndRegistration")
                                            self.navigationController?.pushViewController(onBoardVC, animated:true)
                                            self.signUpButton.removeFromSuperview()
                                        }
                                    }
                                })
                            }
                            
                        } else {
                            DropDownAlert.customErrorMessage("ERROR".localized)
                            self.signUpButton.stopLoadingAnimation()
                        }
                    }
                } else {
                    DropDownAlert.customErrorMessage("ERROR".localized)
                    self.signUpButton.stopLoadingAnimation()
                }
            })
        } else {
            DropDownAlert.customErrorMessage("ERROR".localized)
            self.signUpButton.stopLoadingAnimation()
        }
    }
    
    @IBAction func signUp(_ sender: TKTransitionSubmitButton) {
        if let usernameEntered = usernameText.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            if usernameEntered.characters.count < 3 || usernameEntered.characters.count > 20  {
                DropDownAlert.customErrorMessage("USERNAME_LENGTH_ERROR".localized)
            } else if (HelperFunction.checkIfOnlyWhitespace(usernameEntered)) {
                DropDownAlert.customErrorMessage("USERNAME_BLANK_ERROR".localized)
            } else if (HelperFunction.checkIfAlphanumericDotUnderscore(usernameEntered)) {
                DropDownAlert.customErrorMessage("USERNAME_REQUIREMENTS_ERROR".localized)
            }else {
                if let _ = self.selectedUserProfileImage {
                    self.checkUsernameInDB()
                } else {
                    let alert = UIAlertController(title: "NO_IMAGE_ERROR_TITLE".localized, message: "NO_IMAGE_ERROR".localized, preferredStyle: UIAlertControllerStyle.alert)
                    let yes = UIAlertAction(title: "YES".localized, style: UIAlertActionStyle.default, handler: {(action: UIAlertAction) -> Void in
                        self.checkUsernameInDB()
                    })
                    
                    alert.addAction(yes)
                    let no = UIAlertAction(title: "NO".localized, style: UIAlertActionStyle.cancel, handler: nil)
                    alert.addAction(no)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        } else {
            DropDownAlert.customErrorMessage("USERNAME_BLANK_ERROR".localized)
        }
        
    }
    
}
