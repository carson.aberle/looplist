//
//  UserProfilePhotoViewController.swift
//  alphalooplist
//
//  Created by Looplist Inc on 3/22/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

class UserRegistrationProfilePhotoViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate {
    
    // MARK: -  //////////////////////////////////////////////////////    VARIABLES    //////////////////////////////////////////////////////////////////////////
    
    var selectedUserProfileImage : UIImage?
    
    var firstNameSent: String?
    var lastNameSent: String?
    var emailSent: String?
    var genderSelected = "N/A"
    var inviteCode:String?
    var usernameSent:String?
    var passwordSent:String?
    
    let MyKeychainWrapper = KeychainWrapper()
    
    var isProfilePhotoSelected = false
    var strBase64:String = ""

    // MARK: -  //////////////////////////////////////////////////////    OUTLETS    //////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var genderBar: UISegmentedControl!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var registerButton: TKTransitionSubmitButton!{
        didSet{
            if let registerButton = registerButton{
                registerButton.layer.cornerRadius = registerButton.frame.height / 2.0
                registerButton.layer.borderWidth = 1.0
                registerButton.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0).cgColor
            }
        }
    }
    @IBOutlet weak var locationAddress: UITextField!
    @IBOutlet weak var profilePhoto: UIImageView!{
        didSet{
            if let profilePhoto = profilePhoto{
            profilePhoto.layer.cornerRadius = profilePhoto.frame.height / 2.0
            
            let profilePictureTap = UITapGestureRecognizer(target:self,action: #selector(UserRegistrationProfilePhotoViewController.updateUserProfileImage(_:)))
            profilePictureTap.numberOfTapsRequired = 1
            profilePhoto.addGestureRecognizer(profilePictureTap)
            profilePhoto.clipsToBounds = true
            profilePhoto.isUserInteractionEnabled = true
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////    APPLICATION LIFE CYCLE    //////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "SIGN_UP".localized
        self.registerButton.setTitle("SIGN_UP".localized, for: .normal)
        self.locationAddress.placeholder = "LOCATION_EXAMPLE".localized
        self.genderBar.setTitle("N/A".localized, forSegmentAt: 0)
        self.genderBar.setTitle("MALE".localized, forSegmentAt: 1)
        self.genderBar.setTitle("FEMALE".localized, forSegmentAt: 2)

        
        self.locationAddress.delegate = self
        setGradient()
    }
    
    
    func setGradient(){
        self.view.layer.insertSublayer(HelperFunctionAnimation.getLooplistGradient(insideFrame: self.view), at: 0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UserRegistrationProfilePhotoViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        UserRegistrationProfilePhotoViewController.stopTimer(["preLogin":true, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], withActionType:"View Screen")
    }
    
    @IBAction func popViewController(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func register (_ sender: UIButton!) {
        self.view.endEditing(true)
        
        if(self.selectedUserProfileImage == nil) {
            let alert = UIAlertController(title: "NO_IMAGE_ERROR_TITLE".localized, message: "NO_IMAGE_ERROR".localized, preferredStyle: UIAlertControllerStyle.alert)
            let yes = UIAlertAction(title: "YES".localized, style: UIAlertActionStyle.default, handler: {(action: UIAlertAction) -> Void in
                self.signUpUser()
            })
            
            alert.addAction(yes)
            let no = UIAlertAction(title: "NO".localized, style: UIAlertActionStyle.cancel, handler: nil)
            alert.addAction(no)
            self.present(alert, animated: true, completion: nil)
        } else {
            self.signUpUser()
        }
    }
    
    func signUpUser(){
        registerButton.startLoadingAnimation()
        
        if let username = usernameSent, let password = passwordSent, let firstName = firstNameSent, let lastName = lastNameSent, let email = emailSent{
            
            var location = ""
            if let locationUserEntered = locationAddress.text{
                location = locationUserEntered
            }
            var parameters:[String:AnyObject]
            let path = "authentication/local/signup"
            if self.strBase64 != "" {
               
                parameters = ["username": username as AnyObject, "password": password as AnyObject,"firstName":firstName as AnyObject,"lastName":lastName as AnyObject,"email":email as AnyObject,"gender":self.genderSelected as AnyObject,"location":location as AnyObject,"profile64":self.strBase64 as AnyObject]
            } else {
                parameters = ["username": username as AnyObject, "password": password as AnyObject,"firstName":firstName as AnyObject,"lastName":lastName as AnyObject,"email":email as AnyObject,"gender":self.genderSelected as AnyObject,"location":location as AnyObject]
            }
            
            DBHelper.callAPIPost(path, params: parameters, completion: { (result, error) in
                if error == nil {
                    if let userId = result?["_id"] as? String{
                        Analytics.sharedInstance.actionsArray.append(Action(context:["createUser": userId, "signup":true, "deviceUUID":UIDevice.current.identifierForVendor!.uuidString], forScreen: "Registration Photo", withActivityType:"Create Account"))
                    }
                    self.registerButton.startFinishAnimation(delay: 2.0, completion: { () -> () in
                        
                    })
                    let delayTime = DispatchTime.now() + Double(Int64(2 * (Double(NSEC_PER_SEC) * 0.98))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {

                        if let user = result?["user"] as? Dictionary<String, AnyObject>, let accessToken = result?["accessToken"] as? String{
                            CurrentUser.sharedInstance.assignCurrentUser(User(input:user))
                            self.MyKeychainWrapper.mySetObject(accessToken, forKey:kSecValueData)
                            self.MyKeychainWrapper.mySetObject(username, forKey: kSecAttrAccount)
                            self.MyKeychainWrapper.mySetObject(password, forKey: kSecAttrDescription)
                            self.MyKeychainWrapper.writeToKeychain()
                            
                            if let inviteCode = self.inviteCode{
                                DBHelper.callAPIPut("invites/\(inviteCode)/accept", completion: { (result, error) in
                                    if error == nil{
                                        if let _ = CurrentUser.sharedInstance.currentUser?.objectId {
                                            DBHelper.setUserDefaults()

                                            let onBoardVC:OnBoardViewController = OnBoardViewController.instanceFromStoryboard("LoginAndRegistration")
                                            self.navigationController?.pushViewController(onBoardVC, animated:true)
                                            self.registerButton.removeFromSuperview()
                                        } else {
                                            // user info, object id not set
                                        }
                                    }
                                })
                            }
                            
                        } else {
                            
                            self.registerButton.stopLoadingAnimation()
                        }
                    }
                } else {
                    self.registerButton.stopLoadingAnimation()
                }
            })
        }
    }
    
    func endEditingAndAnimation() {
        self.view.endEditing(true)

        self.registerButton.startFinishAnimation(delay: 2.0, completion: { () -> () in
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    @IBAction func genderChanged(_ sender: UISegmentedControl) {
        if(genderBar.selectedSegmentIndex == 0) {
            genderSelected = Gender.notApplicable.rawValue
        }
        else if(genderBar.selectedSegmentIndex == 1) {
            genderSelected = Gender.male.rawValue
        }
        else if(genderBar.selectedSegmentIndex == 2) {
            genderSelected = Gender.female.rawValue
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        let changedUserProfileImage = info[UIImagePickerControllerEditedImage] as? UIImage
        let imageData:Data = UIImageJPEGRepresentation(changedUserProfileImage!, 0.75)!
        self.strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        self.selectedUserProfileImage = changedUserProfileImage;
        self.profilePhoto.image = changedUserProfileImage
        self.profilePhoto.clipsToBounds = true
        self.isProfilePhotoSelected = true
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateUserProfileImage(_ recognizer:UITapGestureRecognizer){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view!.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}
