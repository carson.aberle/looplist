//
//  UserProfileEditAddressViewController.swift
//  alphalooplist
//
//  Created by Arun Sivakumar on 5/24/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import UIKit
import CreditCardValidator
import Stripe
//import Alamofire
//import SwiftyJSON

    // MARK: -   -----------------------------------------------------   PROTOCOL    -----------------------------------------------------

//protocol UserDidEnterCardInformationDelegate: class {
//    func userDidEnterCardInformation(token:STPToken,last4:String,brand:String)
//    weak var delegate: UserDidEnterCardInformationDelegate? = nil
//}

class CCAddViewController: UITableViewController, UITextFieldDelegate {
    
    // MARK: -   -----------------------------------------------------   CONSTANTS    -----------------------------------------------------
    
    fileprivate let limitSLength = 4
    fileprivate let limitELength = 7
    fileprivate let limitZLength = 5
    fileprivate let limitCLength = 19
    fileprivate let limitNLength = 50
    
    // MARK: -  -----------------------------------------------------   VARIABLES    -----------------------------------------------------
    
    fileprivate var vaildCCData = false
    fileprivate var validCard = false
    fileprivate var cartType = ""

    fileprivate var creditCardValidator =  CreditCardValidator()
    fileprivate var creditCard:CC?
    
    // MARK: -   -----------------------------------------------------   OUTLETS    -----------------------------------------------------
    
    @IBOutlet weak var cText: UITextField!
    @IBOutlet weak var eText: UITextField!
    @IBOutlet weak var sText: UITextField!
    @IBOutlet weak var zText: UITextField!
    @IBOutlet weak var nText: UITextField!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var cardNicknameLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var expLabel: UILabel!
    @IBOutlet weak var cvvLabel: UILabel!
    @IBOutlet weak var zipLabel: UILabel!
    @IBOutlet weak var defaultLabel: UILabel!
    
    
        @IBOutlet weak var defaultAddressSwitch: UISwitch!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        CCAddViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        CCAddViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "ADD_CARD".localized
        self.cardNicknameLabel.text = "CARD_NICKNAME".localized
        self.nameText.placeholder = "CARD_NICKNAME_PLACEHOLDER".localized
        self.cardNumberLabel.text = "CARD_NUMBER".localized
        self.cText.placeholder = "CARD_NUMBER_PLACEHOLDER".localized
        self.fullNameLabel.text = "FULL_NAME".localized
        self.nText.placeholder = "FULL_NAME_PLACEHOLDER".localized
        self.expLabel.text = "EXP".localized
        self.eText.placeholder = "EXP_PLACEHOLDER".localized
        self.cvvLabel.text = "CVV".localized
        self.sText.placeholder = "CVV_PLACEHOLDER".localized
        self.zipLabel.text = "ZIP".localized
        self.zText.placeholder = "ZIP_PLACEHOLDER".localized
        self.defaultLabel.text = "DEFAULT".localized
        
        self.saveButton.title = "SAVE".localized
        
        self.tableView.tableFooterView = UIView()
        
        cText.delegate = self
        eText.delegate = self
        sText.delegate = self
        zText.delegate = self
        nText.delegate = self
        
        cText.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        eText.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        sText.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        zText.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        nText.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
//        self.navigationItem.rightBarButtonItem!.enabled = false ###
        self.addKeyboardHideTap()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillLayoutSubviews() {
        if let navigationController = self.navigationController{
            CCAddViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func addData(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        
        if cText.text != "" &&  nText.text != "" && eText.text != "" && sText.text != "" && zText.text != ""{
            saveCCData()
        }else{
            ErrorHandling.mandatoryFieldsrrorHandler()
        }
    }
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    func validator(){
        if cText.text != "" &&  nText.text != "" && eText.text != "" && sText.text != "" && zText.text != ""{
            self.navigationItem.rightBarButtonItem!.isEnabled = true
            self.navigationItem.rightBarButtonItem!.tintColor = ConstantsColor.addButtonColor
        } else {
            self.navigationItem.rightBarButtonItem!.isEnabled = false
        }
    }
    
    func saveCCData(){
        self.saveButton.isEnabled = false
        let expirationDate = self.eText.text!.components(separatedBy: "/")
        let expMonth = UInt(expirationDate[0].trimmingCharacters(in: CharacterSet.whitespaces))
        let expYear = UInt(expirationDate[1].trimmingCharacters(in: CharacterSet.whitespaces))
        
        let stripCard = STPCardParams()
        stripCard.number = self.cText.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        stripCard.cvc = self.sText.text
        stripCard.expMonth = expMonth!
        stripCard.expYear = expYear!
        
        STPAPIClient.shared().createToken(withCard: stripCard) { (token, error) -> Void in
            if let error = error  {
                ErrorHandling.tokenErrorHandler(error as NSError)
                self.saveButton.isEnabled = true
            }else if let token = token {
                
                if let user = CurrentUser.sharedInstance.currentUser{
                    var cardName:String? = nil
                    if let name = self.nameText.text, name != "" {
                        cardName =  name
                    }
                    user.postUserCC(self.defaultAddressSwitch.isOn,token: token,name:cardName, completion: { (cc) in
                        
                        if cc != nil{
                            self.goBack()
                        }else{
                            ErrorHandling.unexpectedErrorHandler()
                            self.saveButton.isEnabled = true
                        }
                        
                    })
                    
                }
            }
        }
    }
    
    func goBack(){
        performSegue(withIdentifier: "UnwindToCC", sender: self)
    }
    
    func validateCardNumber(_ number: String) -> String{
        if creditCardValidator.validate(string:number) {
            
            if let type = creditCardValidator.type(from: number) {
                
                self.cartType = type.name
                self.vaildCCData = true
                
                if type.name == "Amex" || type.name == "Discover" || type.name == "Visa" || type.name == "MasterCard"{
                    return type.name
                }
                else {
                    return ""
                }
                
            } else {
                return ""
            }
        } else {
            return ""
        }
    }
    
    // DETECT CREDIT CARD NUMBER
    func detectCardNumberType(_ number: String) {
        if let type = creditCardValidator.type(from: number) {
            
            self.cartType = type.name
            self.vaildCCData = true
        } else {
            self.cartType = ""
            
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   TEXT FIELD DELEGATES   /////////////////////////////////////////////////////////////////////////
    
    func textFieldDidChange(_ textField: UITextField) {
//        if textField.text == "" {
//            self.validator()
//        }
//        if textField.isEqual(cText){
//            //Validate card number
//            
//            if textField.text?.characters.count == 19 {
////                self.cartType = self.validateCardNumber(textField.text!)###Arun
//                self.validator()
//                if self.cartType == "" {
//                    textField.text = ""
//                    self.validator()
//                    let alertController = UIAlertController(title: "Error", message: "We do not support this card at the moment. Sorry for the inconvinience", preferredStyle: .Alert)
//                    let okAction = UIAlertAction(title: "Ok", style: .Default) { (action) in }
//                    alertController.addAction(okAction)
//                    
//                    self.presentViewController(alertController, animated: true, completion: nil)
//                }
//            }
//            
//            
//        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField.isEqual(cText)
//        {
//            if(textField.text?.characters.count == 18)
//            {
//                self.validator()
//                //They used Amex
//                self.cartType = self.validateCardNumber(textField.text!)
//                if self.cartType == "" {
//                    textField.text = ""
//                    self.validator()
//                    let alertController = UIAlertController(title: "Error", message: "We do not support this card at the moment. Sorry for the inconvinience", preferredStyle: .Alert)
//                    let okAction = UIAlertAction(title: "Ok", style: .Default) { (action) in }
//                    alertController.addAction(okAction)
//                    
//                    self.presentViewController(alertController, animated: true, completion: nil)
//                }
//                
//            }
//        }
//        else
//        {
//            self.validator()
//        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text {
            let newLength = text.characters.count + string.characters.count - range.length
            
            switch textField{
                
            case cText:
                if newLength == range.location && newLength % 5 == 0{
                    cText.text = String(text.characters.prefix(newLength))
                }
                else  if newLength < limitCLength && newLength % 5 == 0{
                    cText.text = text + " "
                }
                
                return newLength <= limitCLength
            case eText:
                
                if newLength == 3{
                    eText.text = text + " / "
                }
                else if range.location == 5{
                    eText.text = String(text.characters.prefix(3))
                }
                return newLength <= limitELength
            case sText:
                return newLength <= limitSLength
            case zText:
                return newLength <= limitZLength
            case nText:
                return newLength <= limitNLength
            default:
                return true
            }
        }
        return true
    }
}

// MARK: -  //////////////////////////////////////////////////////   EXTENSIONS   /////////////////////////////////////////////////////////////////////////

extension CCAddViewController{
    func addKeyboardHideTap(){
        let hideTap = UITapGestureRecognizer(target: self, action: #selector(CCAddViewController.hideKeyboardTap(_:)))
        hideTap.numberOfTapsRequired = 1
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(hideTap)
    }
    
    func hideKeyboardTap(_ recognizer:UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    
    // ALERT MESSAGE
    
    func presentAlertMessage(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {
            (action) -> Void in
        }))
        //present alert message
        self.present(alert, animated: true, completion: nil)
    }
}
