//
//  CreditCardTableViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 10/3/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit


class CCViewController:UIViewController{
    
    // MARK: -  ///////////////////////////////////////////////////////////   VARIABLES  /////////////////////////////////////////////////////////////////////////
    

    var popUp = false
    var emptySetView: EmptySetView?{
        didSet{
            self.emptySetView?.discriptionLabel.text = "HAVENT_ADDED_CREDIT_CARD_YET".localized
            self.tableView.backgroundView = emptySetView
            self.tableView.backgroundView?.isHidden = true
        }
    }
    
    @IBOutlet var tableView: UITableView!{
        didSet{
            if let tableView = tableView{
                tableView.delegate = self
                tableView.dataSource = self
                //        self.tableView.rowHeight = 50
                tableView.tableFooterView = UIView()
                tableView.contentInset = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        CCViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        CCViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    
    override func viewDidLoad() {
        self.navigationItem.title = "CREDIT_CARD".localized
        self.emptySetView = EmptySetView.instanceFromNib()
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.view.setNeedsLayout()

        loadData()
        
    }
    
    override func viewWillLayoutSubviews() {
        if let navigationController = self.navigationController{
            CCViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    func loadData(){
        if let user = CurrentUser.sharedInstance.currentUser{
            let count = user.getPaymentMethodCount()
            if  count == 0{
                fetchData()
            }else if count > 0{
                self.tableView.reloadData()
            }
        }
    }
    
    func fetchData(){
        if let user = CurrentUser.sharedInstance.currentUser{
            user.getUserCC(true, skip: 0, limit: 1000, completion: { (success) in
                if success{
                    self.tableView.reloadData()
                }else{
                    // Error
                    self.tableView.backgroundView?.isHidden = false
                }
            })
        }
    }
    
    @IBAction func UnwindToCC(_ segue: UIStoryboardSegue) {
       fetchData()
    }
    
    // MARK: -  ///////////////////////////////////////////////////////    ACTIONS    ///////////////////////////////////////////////////////////////////////////
    
    @IBAction func addPaymentMethod(_ sender: AnyObject) {
        let ccVC:CCAddViewController = CCAddViewController.instanceFromStoryboard("CreditCard")
        //        shippingAddressVC.isSaveButtonVisible = false
        self.navigationController?.pushViewController(ccVC, animated: true)
    }
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: -  ///////////////////////////////////////////////////////    FUNCTIONS    ///////////////////////////////////////////////////////////////////////////
    
    func makePaymentMethodDefault(_ sender: UIButton!){
        sender.isEnabled = false
        if let index = (sender.layer.value(forKey: "index")) as? Int{
            if let user = CurrentUser.sharedInstance.currentUser{
                if let count = user.cc?.count{
                    if count > index{
                        if let objectId = user.cc?[index].objectId{
                            user.toggleUserDefaultPaymentMethod(objectId, completion: { (success) in
                                if success{
                                    sender.isEnabled = true
                                    self.tableView.reloadData()
                                }else{
                                    sender.isEnabled = true
                                    //ErrorHandling.unexpectedErrorHandler()
                                }
                            })
                        }
                    }
                }
            }
        }
    }
}
extension CCViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: -  ////////////////////////////////////////////////    TABLE VIEW DATA SOURCE   ////////////////////////////////////////////////////////////////////
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let user = CurrentUser.sharedInstance.currentUser{
                if user.getPaymentMethodCount() == 0{
                    self.tableView.backgroundView?.isHidden = false
                    return 0
                }else {
                    self.tableView.backgroundView?.isHidden = true
                    return user.getPaymentMethodCount()
                }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CCTableViewCell", for: indexPath) as! CCTableViewCell
        if let user = CurrentUser.sharedInstance.currentUser{
          

           
            cell.cc = user.cc![indexPath.row]
            if user.cc![indexPath.row].objectId == user.defaultPaymentMethodObjectId{
                cell.primaryButton.isUserInteractionEnabled = false
            }else{
                cell.primaryButton.layer.setValue(indexPath.row, forKeyPath: "index")
                cell.primaryButton.addTarget(self, action: #selector(CCViewController.makePaymentMethodDefault(_:)), for: .touchUpInside)
            }
     
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "unwindToOrderVC"{
            if let ovc = segue.destination as? OrderViewController{
                if let user = CurrentUser.sharedInstance.currentUser{
//                    let selectedRow = tableView.indexPathForSelectedRow?.row
                    if let count = user.cc?.count, let selectedRow = tableView.indexPathForSelectedRow?.row, count >= selectedRow{
                        if let cc = user.cc?[selectedRow]{
                            ovc.cc = cc
                        }
                    }
                }
            }
        }
    }

    

    // MARK: -  //////////////////////////////////////////////////    TABLE VIEW DELEGATE   /////////////////////////////////////////////////////////////////////
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if popUp{
            if let user = CurrentUser.sharedInstance.currentUser{
                if indexPath.row == user.getPaymentMethodCount() {
                    self.addPaymentMethod(self)
                }else{
                    performSegue(withIdentifier: "unwindToOrderVC", sender: tableView)

                }
            }
        }
    }
    
    // DELETE CC
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            tableView.isUserInteractionEnabled = false

            guard let user = CurrentUser.sharedInstance.currentUser,let cc = user.cc?[indexPath.row], let objectId = cc.objectId else{
                tableView.isUserInteractionEnabled = true
                return
            }
                user.deletePaymentMethod(objectId, completion: { (success) in
                    tableView.isUserInteractionEnabled = true

                    if success{
                        tableView.deleteRows(at: [indexPath], with: .fade)
                    }else{
                        ErrorHandling.unexpectedErrorHandler()
                    }
                })
            
        }
    }
}
