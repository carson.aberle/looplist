//
//  MessagingInboxViewController.swift
//  Looplist
//
//  Created by Arun Sivakumar on 8/12/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import RealmSwift

class MessagingInboxTableViewController: UIViewController,UITextFieldDelegate {
    
    // MARK: -  //////////////////////////////////////////////////////   PUBLIC API   /////////////////////////////////////////////////////////////////////////

    
    internal func reload(){ // App delegate
        self.messages = [MessageConversation]()
//        self.refreshControl.beginRefreshing()

        if firstTimeLoad {
            self.indicator.showActivityIndicator(uiView: self.view, offset: 64.0)
            self.firstTimeLoad = false
        } else {
            self.indicator.showActivityIndicator(uiView: self.view, offset: 128.0)
        }
        self.currentPage = 0
        Timer.scheduledTimer(timeInterval: ConstantsTime.indicatorMinimumTime, target: self, selector: #selector(self.loadInitialData), userInfo: nil, repeats: false)
        
    }
    
    @objc func loadInitialData(){
        self.loadData()
    }
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////
    
    fileprivate var currentPage = 0
    private let limitNumber = ConstantsCount.paginationlimitNumber
    fileprivate var isEndOFPage = false
    fileprivate var cellHeight:CGFloat = 93.5
    private let cellRatio:CGFloat = 7.40
    fileprivate var firstTimeLoad = true
    
    fileprivate var messages = [MessageConversation](){
        didSet{
            if self.indicator.isLoading{
                self.indicator.hideActivityIndicator()
            }
             self.tableView.reloadData()
        }
    }
    
    fileprivate var userSearchedConversation:[MessageConversation]?{
        didSet{
            self.tableView.reloadData()
        }
    }
    
    
    fileprivate var messagesMaven = [MessageConversation](){
        didSet{
            if self.indicator.isLoading{
                self.indicator.hideActivityIndicator()
            }
            self.tableView.reloadData()
           
        }
    }
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
             self.tableView.tableFooterView = UIView()
        }
    }

    var refreshControl = UIRefreshControl(){
        didSet{
            refreshControl.backgroundColor = ConstantsColor.pullToRefreshColor
        }
    }
    @IBOutlet weak var searchTextField: SearchTextField!{
        didSet{
            searchTextField.delegate = self
            searchTextField.addTarget(self, action: #selector(MessagingInboxTableViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        }
    }
    
    fileprivate var indicator = CustomActivityIndicator()


    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        MessagingInboxTableViewController.startTimer()
        MessagingInboxTableViewController.self.popSlideMotionOff(self.navigationController!)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        MessagingInboxTableViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cellHeight = self.view.frame.height / cellRatio
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
        reload()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.searchTextField.endEditing(true)
        MessagingInboxTableViewController.self.popSlideMotionOn(self.navigationController!)
    }
    
    private func configureUI(){
        self.congigureTabBar()
        self.configureNavBar()
//        self.configureRefreshControl()
    
    }
    
    private func congigureTabBar(){
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    private func configureNavBar(){
        if let navigationController = self.navigationController {
            MessagingInboxTableViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor: NavigationBarBackgroundColor.whiteOpaque)
            self.navigationItem.title = "INBOX".localized
        }
    }
    
//    private func configureRefreshControl(){
//        tableView.addSubview(refreshControl)
//    }
    
    @objc private func refresh(_ sender:AnyObject) {
        reload()
    }
    
    @objc fileprivate func openMavenMessage(){
        let messagingVC:MessagingViewController = MessagingViewController.instanceFromStoryboard("Messaging")
        let messageConversation = self.messagesMaven[0]
        messagingVC.messageConversation = messageConversation
        self.navigationController?.pushViewController( messagingVC, animated: true)
    }
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func showFollowers(sender: UIBarButtonItem) {
        let messagingNewVC:MessagingNewMessageViewController = MessagingNewMessageViewController.instanceFromStoryboard("Messaging")
        self.navigationController?.pushViewController(messagingNewVC, animated: true)
    }

    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////

    
    private func loadData(){
        DispatchQueue.global(qos: .userInteractive).async {
            self.getMavenConversation { (success) in }
        }
        DispatchQueue.global(qos: .userInteractive).async {
            
            self.getConversations()
        }
        DispatchQueue.global(qos: .userInitiated).async {
            self.findMessageUnreadCount()
        }
    }

    private func getMavenConversation(_ completion:@escaping (_ success: Bool) -> Void){
        
        MessagingManager.getConversations(true,chatId:nil,skip: 0, limit: 1) { [weak vc = self](conversation) in
            if let conversation = conversation{
                    vc?.messagesMaven = conversation
                completion(true)
            }else{
                completion(false)
            }
        }
    }
    
    fileprivate func getConversations(){
        MessagingManager.getConversations(false,chatId: nil,skip: self.currentPage, limit: self.limitNumber) { [weak vc = self](conversation) in
            if let conversation = conversation {
                    if conversation.count < self.limitNumber{
                        vc?.isEndOFPage = true
                    }else{
                        vc?.isEndOFPage = false
                }
                    vc?.currentPage += conversation.count
                    vc?.messages += conversation
               
            }
        }
    }
    
    private func findMessageUnreadCount(){
        MessagingManager.findMessageUnreadCount({ (count) in
            if let count = count, count > 0{
                //                self.tabBarController?.tabBar.items?[1].badgeValue = "\(count)"
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "showMessagingNotificationBadge"), object:nil)
                
            }else{
                //                self.tabBarController?.tabBar.items?[1].badgeValue = nil
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "hideMessagingNotificationBadge"), object:nil)
                
            }
        })
    }
    
    
    private var userEnteredSearchTerm:String?{
        didSet{
            if let userEnteredSearchTerm = userEnteredSearchTerm?.lowercased(){
                if userEnteredSearchTerm == ""{
                    self.tableView.backgroundView?.isHidden = true
                    userSearchedConversation = nil
                }else{
                    userSearchedConversation = messages.filter{($0.user?.userName?.lowercased().contains(userEnteredSearchTerm))! || ($0.user?.firstName?.lowercased().contains(userEnteredSearchTerm))! ||  ($0.user?.lastName?.lowercased().contains(userEnteredSearchTerm))!}
                    if userSearchedConversation?.count == 0{
                        self.initializeEmptySearch()
                    } else {
                        self.tableView.backgroundView?.isHidden = true
                    }
                }
            }
        }
    }
    
    
    private func initializeEmptySearch(){ // Lazy Init
        let emptySetView: EmptySetView = EmptySetView.instanceFromNib()
        emptySetView.discriptionLabel.text = "NO_SEARCH_RESULTS".localized
        emptySetView.frame = self.tableView.frame
        self.tableView.backgroundView?.isHidden = true
        self.tableView.backgroundView = emptySetView
    }
    

    // MARK: -  //////////////////////////////////////////////////////   TEXTFIELD  DELEGATES  ///////////////////////////////////////////////////////////////////

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if self.indicator.isLoading{
            return false
        }
        return true
    }
    func textFieldDidChange(_ textField: UITextField) {
        if let searchTerm = textField.text{
            userEnteredSearchTerm = searchTerm
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DATA SOURCE   /////////////////////////////////////////////////////////////////////////
    extension MessagingInboxTableViewController:UITableViewDataSource,UITableViewDelegate{
        
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            //return cellHeight + 20
            return 116.0
        }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessagingInboxMavenCell") as! MessagingInboxMavenTableViewCell
            if messagesMaven.count > 0 && messagesMaven[0].chatId != nil {
                cell.message =  self.messagesMaven[0]
                let singleTap = UITapGestureRecognizer(target: self, action:#selector(MessagingInboxTableViewController.openMavenMessage))
                singleTap.numberOfTapsRequired = 1
                singleTap.delaysTouchesBegan = true
                cell.addGestureRecognizer(singleTap)
            }else{
                cell.fallBackMavenMessage = true
            }
            cell.layer.shadowColor = UIColor.red.cgColor
            cell.layer.shadowRadius = 10.0
            return cell
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if indexPath.row == 0{
             return  cellHeight  - 10.0
            }
            return cellHeight
        }
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if let userSearchedConversation = self.userSearchedConversation{
                return userSearchedConversation.count
                
            }else{
                return self.messages.count
                
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessagingInboxCell", for:indexPath) as! MessagingInboxTableViewCell
            
            if let userSearchedConversation = self.userSearchedConversation{
                cell.message = userSearchedConversation[indexPath.row]
            }else{
                if indexPath.row == self.messages.count - 1 && !isEndOFPage{
                    getConversations()
                }
                cell.message = self.messages[indexPath.row]
            }
            
            cell.selectionStyle = .none
            return cell
        }
    
    // MARK: -  //////////////////////////////////////////////////////   TABLE VIEW DELEGATE   /////////////////////////////////////////////////////////////////////////
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if let cell = tableView.cellForRow(at: indexPath) as? MessagingInboxTableViewCell{
        
            let messagingVC:MessagingViewController = MessagingViewController.instanceFromStoryboard("Messaging")
            if messages.count > 0{
                let messageConversation = self.messages[indexPath.row]
                messagingVC.messageConversation = messageConversation
            //  messagingVC.receiverProfilePicture = cell.userProfilePictureImage.image
            //  messagingVC.receiverName = cell.message?.user?.fullName ?? ""
                self.navigationController?.pushViewController( messagingVC, animated: true)
            }
//        }
    }
}
