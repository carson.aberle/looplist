//
//  MessagingInboxViewController.swift
//  Looplist
//
//  Created by Arun Sivakumar on 8/12/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class MessagingNewMessageViewController: UIViewController,UITextFieldDelegate{
    
    // MARK: -  //////////////////////////////////////////////////////////////////////// VARIABLES ////////////////////////////////////////////////////////////////////////
    
    fileprivate var currentPage = 0
    private let limitNumber = ConstantsCount.paginationlimitNumber
    fileprivate var isEndOfPage = false
    
    fileprivate var users:[User] = [User](){
        didSet{
            if self.indicator.isLoading{
                self.indicator.hideActivityIndicator()
            }
            self.tableView.reloadData()
        }
    }
    fileprivate var indicator = CustomActivityIndicator()

//    var product:Product?
    
    
    // MARK: -  //////////////////////////////////////////////////////////////////////// OUTLETS ////////////////////////////////////////////////////////////////////////
    
//    lazy var refreshControl: UIRefreshControl = {
//        let refreshControl = UIRefreshControl()
//        refreshControl.addTarget(self, action: #selector(MessagingNewMessageViewController.refresh(_:)), for: UIControlEvents.valueChanged)
//        refreshControl.backgroundColor = UIColor.white
//        return refreshControl
//    }()
    
    
    @IBOutlet weak var searchTextField: UITextField!{
        didSet{
            if let searchTextField = searchTextField{
                searchTextField.delegate = self
                searchTextField.addTarget(self, action: #selector(MessagingNewMessageViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
            }
        }
    }
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            self.tableView.register(UINib(nibName: "UserTableViewCell", bundle: nil), forCellReuseIdentifier: "UserTableViewCell")
        }
    }
    
    
    var emptySetView: EmptySetView? = EmptySetView.instanceFromNib()
    var emptySetViewSearch: EmptySetViewSearch? = EmptySetViewSearch.instanceFromNib()
    
//    @IBOutlet weak var search: UISearchBar!
    
    // MARK: -  //////////////////////////////////////////////////////////////////////// ACTIONS ////////////////////////////////////////////////////////////////////////
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        self.searchTextField.resignFirstResponder()
        _ = self.navigationController?.popViewController(animated: true)

    }
    
    // MARK: -  //////////////////////////////////////////////////////////////////////// APPLICATION LIFE CYCLE ////////////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        MessagingNewMessageViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        MessagingNewMessageViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        if let navigationController = self.navigationController {
            MessagingNewMessageViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.searchTextField.endEditing(true)
    }
    
    override func viewDidLoad() {
        configureUI()
        self.reload()
    }
    
    func configureUI(){
        
        self.congigureTabBar()
        self.configureNavBar()
//        self.configureTableView()
        self.configureTitle()
    }
    
    private func congigureTabBar(){
        self.tabBarController?.tabBar.isHidden = true
    }
    
    private func configureNavBar(){
    }
    
//    private func configureTableView(){
//        self.tableView.addSubview(refreshControl)
//    }
    
    private func configureTitle(){
        self.navigationItem.title = "NEW_MESSAGE".localized
        self.searchTextField.placeholder = "TO_PLACEHOLDER".localized
    }
    
    private func reload(){
        //        self.refreshControl.beginRefreshing()
        self.currentPage = 0
        self.indicator.showActivityIndicator(uiView: self.view, offset: 0.0)
        Timer.scheduledTimer(timeInterval: ConstantsTime.indicatorMinimumTime, target: self, selector: #selector(self.loadInitialData), userInfo: nil, repeats: false)
    }
    
    @objc func loadInitialData(){
        self.loadData(searchTerm: self.searchTextField.text)
    }


    // MARK: -  //////////////////////////////////////////////////////////////////////// FUNCTIONS ////////////////////////////////////////////////////////////////////////

    
    fileprivate func loadData(searchTerm:String?){
        if let user = CurrentUser.sharedInstance.currentUser{
            

            if self.searchTextField.text == ""{
                currentPage = self.users.count
            }else{
                currentPage = 0
                self.showEmptyDataSet(data: .Loading)
            }
            APIHelperAnyUser.getUserFollowers(false,user:user,searchFollower:searchTerm, skip: currentPage, limit: limitNumber) {[weak vc = self] (users) in
                if let users = users,searchTerm == self.searchTextField.text{
                    
                    if let limitNumber = vc?.limitNumber, users.count < limitNumber{
                        vc?.isEndOfPage = true // stop pagination
                    }else{
                         vc?.isEndOfPage = false
                    }
                    
                    if vc?.currentPage == 0{
                        
                        vc?.users = users // initial load
                        vc?.currentPage = users.count

                        
                        if users.count == 0 { // configure empty data set
                            searchTerm == "" ? vc?.showEmptyDataSet(data: .Empty) : vc?.showEmptyDataSet(data: .Search)
                        }else{
                            vc?.hideEmptyDataSet()
                        }
                    }else{
                        vc?.users += users // pagination
                        vc?.currentPage += users.count

                        
                    }
                }
            }
        }
    }
   /*
    private func initializeEmptySet(){ // Lazy Init
        
        self.tableView?.backgroundView = emptySetView
        self.tableView?.backgroundView?.isHidden = true
    }
    
 */
    
    private func showEmptyDataSet(data:EmptyDataSet){ // Lazy Init
        
        switch data{
        case .Loading:
            emptySetViewSearch?.discriptionLabel.text = "Loading..."
            self.tableView.backgroundView = emptySetViewSearch
        case .Search:
            emptySetViewSearch?.discriptionLabel.text = "NO_SEARCH_RESULTS".localized
            self.tableView.backgroundView = emptySetViewSearch
        case .Empty:
            emptySetView?.discriptionLabel.text = "NO_USERS_FOUND".localized
            self.tableView.backgroundView = emptySetView

        }
        self.tableView.backgroundView?.isHidden = false
    }
    
    private func hideEmptyDataSet(){
        self.tableView.backgroundView?.isHidden = true
        
    }
    
    
    // MARK: -  //////////////////////////////////////////////////////   TEXTFIELD  DELEGATES  ///////////////////////////////////////////////////////////////////

    func textFieldDidChange(_ textField: UITextField) {
        if let searchTerm = textField.text{
            loadData(searchTerm: searchTerm)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        if let searchTerm = textField.text{
            loadData(searchTerm: searchTerm)
        }
        return false
    }
    
//    func goToNextScreen(messageConversation:MessageConversation?, cell:AddFriendTableViewCell?){
        func goToNextScreen(messageConversation:MessageConversation?){

        if let messageConversation = messageConversation{
            
            let messagingVC:MessagingViewController = MessagingViewController.instanceFromStoryboard("Messaging")
            messagingVC.messageConversation = messageConversation
            //messagingVC.receiverProfilePicture = cell.userImageView.image
           // messagingVC.receiverName = cell.user?.fullName ?? ""
            if self.searchTextField.isEditing{
                self.searchTextField.resignFirstResponder()
            }
            self.navigationController?.pushViewController( messagingVC, animated: true)
            
        }
    }
    
}

// MARK: -  //////////////////////////////////////////////////////////////////////// TABLE DATA SOURCE & DELEGATE ////////////////////////////////////////////////////////////////////////

extension MessagingNewMessageViewController: UITableViewDelegate, UITableViewDataSource {
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
         if indexPath.row == currentPage-1 && !isEndOfPage{
            loadData(searchTerm: self.searchTextField.text)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as! UserTableViewCell
        cell.user = self.users[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        if let _ = tableView.cellForRow(at: indexPath) as? AddFriendTableViewCell{
        if  users.count >= indexPath.row{
            let cell = tableView.cellForRow(at: indexPath)
            cell?.isUserInteractionEnabled = false
            if let userId = self.users[indexPath.row].objectId{
                
                MessagingManager.getChatWithUser(userId, completion: {[weak vc = self] (chatId) in
                    if let chatId = chatId{
                        let messageConversation = MessageConversation(user: self.users[indexPath.row], chatId: chatId, userLastMessage: nil, date: nil,read:nil)
                        vc?.goToNextScreen(messageConversation: messageConversation)
                        
                    }else{
                        let messageConversation = MessageConversation(user: self.users[indexPath.row], chatId: nil,userLastMessage: nil, date: nil,read:nil)
                        vc?.goToNextScreen(messageConversation: messageConversation)
                        
                    }
                    cell?.isUserInteractionEnabled = true

                })
            }
        }
//        }
    }
}



