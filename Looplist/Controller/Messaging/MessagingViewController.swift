//
//  MessagingViewController.swift
//  Looplist
//
//  Created by Arun Sivakumar on 8/28/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class MessagingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UIGestureRecognizerDelegate {
    
    
    // MARK: -  //////////////////////////////////////////////////////    VARIABLES    //////////////////////////////////////////////////////////////////////////

    var preFilledMessage:String?
    
    var senderId:String?
    var senderProfilePicture:UIImage?
    
    var receiverProfilePicture:UIImage?
    var receiverName = ""
    
    var currentPage : Int = 0
    var limitNumber : Int = 20
    
    
    var messageConversation:MessageConversation?
    var messages = [Message]()
    
    var keyboardAppeared = false

    var initialLoad = true
    var refreshing = false
    
    var keyboardNotificationHandler: KeyboardNotificationHandler?
    fileprivate var fakeNavBar = FakeNavBar()
    
    fileprivate var indicator = CustomActivityIndicator()

    
    
    // MARK: -  //////////////////////////////////////////////////////     OUTLETS     //////////////////////////////////////////////////////////////////////////
    
    var refreshControl: UIRefreshControl = UIRefreshControl()
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
           
            tableView.estimatedRowHeight = 280 // max height
             tableView.rowHeight = UITableViewAutomaticDimension
        }
    }
    
    @IBOutlet weak var inputToolbar: UITextView!{
        didSet{
           
            if let inputToolbar = inputToolbar{
             inputToolbar.delegate = self
             inputToolbar.layer.cornerRadius = 5.0
            }
        }
    }
    
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!{
        didSet{
            sendButton.isEnabled = false
        }
    }
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var toolbarBottomSpace: NSLayoutConstraint!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!{ // for send message
        didSet{
            spinner.stopAnimating()
        }
    }
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    // MARK: -  ////////////////////////////////////////////////    APPLICATION LIFE CYCLE    ////////////////////////////////////////////////////////////////////
    
    override func viewDidAppear(_ animated: Bool) {
        MessagingViewController.startTimer()
        
        if let navigationController = self.navigationController {
            MessagingViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
        self.fakeNavBar.removeFakeNavBarView("White")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let chatId = messageConversation?.chatId{
            MessagingViewController.stopTimer(["chat":chatId], withActionType:"View Screen")
        } else {
            MessagingViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.sendButton.setTitle("SEND".localized, for: .normal)
        self.inputToolbar.text = "MESSAGE_PLACEHOLDER".localized
        inputToolBarPlaceholderColor()
        self.tabBarController?.tabBar.isHidden = true
        
        // Do any additional setup after loading the view.
        self.setUp()
        self.setUpUI()
        self.configureTableView()
        
        self.indicator.showActivityIndicator(uiView: self.view, offset: 0.0)
        Timer.scheduledTimer(timeInterval: ConstantsTime.indicatorMinimumTime, target: self, selector: #selector(self.loadInitialData), userInfo: nil, repeats: false)
        
        
        self.markMessagesAsRead()
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openUserProfile))
//        self.navigationController?.view.addGestureRecognizer(tapGesture)
        
//        if let productId = self.messageConversation?.product?.objectId{
//            sendMessage(nil,listingId:nil,productId: productId)
//        }
    }
    
    @objc func loadInitialData(){
        self.getMessages()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = receiverName
        
        if let navigationController = self.navigationController {
            MessagingViewController.self.setNavigationBarStatusAndFont(.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar: .default)
        }
        self.fakeNavBar.addFakeNavBarView("White", uView: self.view, offset: 0.0)
        
        keyboardNotificationHandler = KeyboardNotificationHandler()
        
        keyboardNotificationHandler!.keyboardWillBeHiddenHandler = { (height: CGFloat) in
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.toolbarBottomSpace.constant = 0
                self.view.layoutIfNeeded()
                
                self.keyboardAppeared = false
            })
        }
        
        keyboardNotificationHandler!.keyboardWillBeShownHandler = { (height: CGFloat) in
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.toolbarBottomSpace.constant = height
                self.view.layoutIfNeeded()
                self.scrollToBottom()
                self.keyboardAppeared = true
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        keyboardNotificationHandler = nil
        self.fakeNavBar.removeFakeNavBarView("White")
    }
    
    // MARK: -  ///////////////////////////////////////////////////    CONFIGURATION    /////////////////////////////////////////////////////////////////////////
    
    func setUp(){
        
        // setup user
        if let currentUser =  CurrentUser.sharedInstance.currentUser, let objectId = currentUser.objectId, let messagingConversation = self.messageConversation{
            self.senderId = objectId
            // recevier profile image
            if let user = messagingConversation.user,let receiverName = user.fullName{
                if let profileUrl = user.profileImageUrl{
                    
                    if let url = URL(string: profileUrl){
                        let sampleImageView = UIImageView()
                        sampleImageView.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cache, url) in
                            self.receiverProfilePicture = image

                        })
                    }
                }else{
                    self.receiverProfilePicture = UIImage(named: Constants.defaultMaleImage)
                }
                
                self.receiverName = receiverName
            }else{ // Maven
                if let image = UIImage(named: ConstantPlaceHolders.messagingReceiverMavenImage){
                    self.receiverProfilePicture  = image
                    self.receiverName = ConstantPlaceHolders.maven
                }
            }

            
        }else{
            //DBHelper.logoutAndClearDefaults()
            // change error handling ###
        }
    }
 
    
    func setUpUI(){
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 0.05)
        refreshControl.tintColor = UIColor.black
        refreshControl.addTarget(self, action: #selector(MessagingViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshControl)
        self.inputToolbar.layer.cornerRadius = self.inputToolbar.frame.height / 2.0
        self.inputToolbar.textContainerInset = UIEdgeInsets(top: 8, left: 10, bottom: 0, right: 8)
        
        if let navigationController = self.navigationController {
            MessagingViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "BeginConversationCell", bundle: nil), forCellReuseIdentifier: "BeginConversationCell")
        tableView.register(UINib(nibName: "SenderTextCell", bundle: nil), forCellReuseIdentifier: "SenderTextCell")
        tableView.register(UINib(nibName: "ReceiverTextCell", bundle: nil), forCellReuseIdentifier: "ReceiverTextCell")
        
        tableView.register(UINib(nibName: "SenderTextDateCell", bundle: nil), forCellReuseIdentifier: "SenderTextDateCell")
        tableView.register(UINib(nibName: "ReceiverTextDateCell", bundle: nil), forCellReuseIdentifier: "ReceiverTextDateCell")
        
        tableView.register(UINib(nibName: "ReceiverListingCell", bundle: nil), forCellReuseIdentifier: "ReceiverListingCell")
        tableView.register(UINib(nibName: "SenderProductCell", bundle: nil), forCellReuseIdentifier: "SenderProductCell")
        tableView.register(UINib(nibName: "ReceiverProductCell", bundle: nil), forCellReuseIdentifier: "ReceiverProductCell")

        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        

        if let _ = self.preFilledMessage{
            self.preFilledMessage = nil
        } else {
            inputToolbar.text = ""
        }
        inputToolBarMessageColor()
    }
    
    
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if self.indicator.isLoading{
            return false
        }
        
        return true
    }
    
    func inputToolBarMessageColor(){
        inputToolbar.textColor = ConstantsColor.looplistGreyColor
    }
    
    func inputToolBarPlaceholderColor(){
        inputToolbar.textColor = ConstantsColor.messagingInputToolbarPlaceholderColor
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.characters.count == 0{
            inputToolbar.text = "MESSAGE_PLACEHOLDER".localized
            inputToolBarPlaceholderColor()
        }
    }
    
    // MARK: -  /////////////////////////////////////////////////////    INITIAL LOAD    ////////////////////////////////////////////////////////////////////////
    
    
    func getMessages(){
        
        if let chatId = self.messageConversation?.chatId{
//            if self.currentPage == 0{
//                self.indicator.showActivityIndicator(uiView: self.view, offset: 0.0)
//            }
            MessagingManager.getMessages(self.currentPage,limit:self.limitNumber, chatId:chatId ,messageId: nil) { (messages,messageCount) in
                if self.currentPage == 0 && self.indicator.isLoading{
                    self.indicator.hideActivityIndicator()
                }
                if let messages = messages, let messageCount = messageCount, messages.count > 0{
                    
                    self.refreshControl.endRefreshing()
                    
                    if self.initialLoad{
                        
                        for message in messages{
                            self.messages.append(message)
                        }
                        
                        self.initialLoad = false
                        
                        if messageCount < self.limitNumber{
                            self.addHeaderMessage()
                            self.refreshControl.removeFromSuperview()
                        }
                        
                        self.reloadTableAndScrollToBottom()
//                        MessagingManager.markMessageAsRead(chatId, completion: { (count) in
//                            if let _ = count{
//                                
//                                
//                            }else{
//                                
//                            }
//                        })
                    }else{
                        for message in messages.reversed(){
                            self.messages.insert(message, at: 0)
                        }
                        if messageCount < self.limitNumber{
                            self.addHeaderMessage()
                            self.refreshControl.removeFromSuperview()
                        }
                        self.reloadTableAndScrollToTop()
                        
                    }
                    
                    self.currentPage += self.limitNumber
                }else{
                    self.refreshControl.endRefreshing()
                    self.refreshControl.removeFromSuperview()
                }
            }
        }else{
            self.indicator.hideActivityIndicator()

        }
    }
    
    func markMessagesAsRead(){
        
        if let chatId = self.messageConversation?.chatId{
            MessagingManager.markMessageAsRead(chatId, completion: { (count) in
                
                if let count = count, count > 0{
//                    self.tabBarController?.tabBar.items?[1].badgeValue = "\(count)"
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "showMessagingNotificationBadge"), object:nil)

                }else{
//                    self.tabBarController?.tabBar.items?[1].badgeValue = nil
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "hideMessagingNotificationBadge"), object:nil)

                }
            })
        }
    }
    
    
    // MARK: -  ///////////////////////////////////////////////////    FUNCTIONS    ////////////////////////////////////////////////////////////////////////
    
    func handleIncomingMessages(_ message:Message){
   
        self.messages.append(message)
        reloadTableAndScrollToBottom()
        self.markMessagesAsRead()
    }
    
    // MARK: -  /////////////////////////////////////////////////   TABLE FUNCTIONS    //////////////////////////////////////////////////////////////////////

    func reloadTableAndScrollToBottom(){
        self.tableView.reloadData()
        self.scrollToBottom()
        if let preFilledMessage = self.preFilledMessage{
            self.inputToolbar.text = preFilledMessage
        }else{
//            self.inputToolbar.text = "MESSAGE_PLACEHOLDER".localized
        }
        self.sendButton.isEnabled = true

    }
    
    func reloadTableAndScrollToTop(){
        self.tableView.reloadData()
        self.scrollToTop()
        
    }
    
    func refresh(_ sender:AnyObject) {
        self.getMessages()
        
    }
    
    func setInputToolbarToInitialState(){
        inputToolbar.text = "ASK_MAVEN".localized
        inputToolbar.textColor = ConstantsColor.looplistGreyColor
    }
    
//    func setInputToolbarToEditingState(){
//        inputToolbar.text = ""
//        inputToolbar.textColor = UIColor.black
//    }
    
    func scrollToBottom() {
        if self.messages.count > 0 {
            let lastRowIndexPath = IndexPath(row: self.messages.count - 1, section: 0)
            self.tableView.scrollToRow(at: lastRowIndexPath, at: UITableViewScrollPosition.bottom, animated: false)
        }
    }
    
    func scrollToTop(){
        self.tableView.scrollToRow(at: IndexPath(row: (messages.count - 1 ), section: 0), at: .top, animated: false)
        
    }
    
    func openUserProfile(){
        
        let guestVC:UserProfileViewController = UserProfileViewController.instanceFromStoryboard("Main")
        if let user = self.messageConversation?.user{
            user.updateUser { (success) in
                guestVC.currentUser = user
                self.navigationController?.pushViewController(guestVC, animated: true)
            }
        }
    }
    
    func goToUserProfile(_ sender: UIButton) {
        self.openUserProfile()
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if keyboardAppeared{
            self.inputToolbar.endEditing(true)
        }
    }

    func textViewDidChange(_ textView: UITextView) {
        if inputToolbar.text.characters.count > 0{
            
//            sendButton.titleLabel?.textColor = ConstantsColor.messagingInputToolbarPlaceholderColor
            sendButton.isEnabled = true
            
            
        }else if inputToolbar.text.characters.count == 0{
            //            setInputToolbarToInitialState()
//            sendButton.titleLabel?.textColor = ConstantsColor.looplistGreyColor
            sendButton.isEnabled = false
        }
    }
    
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text.characters.count > 1500{
            return false
        }
        return true
    }
    
    // MARK: -  ///////////////////////////////////////////////////    CHAT FUNCTIONS    ////////////////////////////////////////////////////////////////////////
    
    func addHeaderMessage(){
        let message = Message(fromId: nil, chatId: nil, textMessage: nil, listing: nil, product: nil, date: nil)
        let previousMessage = messages[0]
        if let fromId = previousMessage.fromId, let senderId = self.senderId, fromId == senderId{
            message.setMessageBasedOnSender(true,receiverName:self.receiverName)
        }else{
            message.setMessageBasedOnSender(false,receiverName:self.receiverName)
        }
        self.messages.insert(message, at: 0)
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        // send message
//        self.sendButton.isEnabled = false
        
        
        let text = inputToolbar.text
        
        let newText = text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if let count = newText?.characters.count, count > 0,newText != "MESSAGE_PLACEHOLDER".localized{
            self.sendButton.isHidden = true
            spinner.startAnimating()
            sendMessage(newText,listingId:nil,productId: nil)
        }
    }
    
    func sendMessage(_ text:String?,listingId:String?,productId:String?){
        if let messageConversation = self.messageConversation, let chatId = messageConversation.chatId{
            MessagingManager.sendMessage(chatId, message: text,listingId: listingId,productId: productId, completion: { (message) in
                if let message = message{
                    self.preFilledMessage = nil
                    self.messages.append(message)
                    if let _ = message.textMessage{
                        self.inputToolbar.text = ""
//                        self.sendButton.enabled = false
                    }else if message.product == nil, let product = messageConversation.product{
                        message.product = product
                    }
                    
                    
                    self.spinner.stopAnimating()
                    self.sendButton.isHidden = false
                    self.reloadTableAndScrollToBottom()
                    
                }else{
                    
                    self.spinner.stopAnimating()
                    self.sendButton.isHidden = false
                }
            })
            
            // create chat for first time
        }else if let messageConversation = self.messageConversation, let userId = messageConversation.user?.objectId{
            
            MessagingManager.createChat(userId, message: text, listingId: listingId, productId: productId, completion: { (chatId) in
                
                if let chatId = chatId{
                    self.messageConversation?.chatId = chatId
                    if let _ = text{
                        self.inputToolbar.text = ""
                        self.spinner.stopAnimating()
                        self.sendButton.isHidden = false
                    }
                    self.getMessages()
                }else{
                    self.spinner.stopAnimating()
                    self.sendButton.isHidden = false

                }
            })
        }
    }
    
    // MARK: -  ///////////////////////////////////////////////    TABLE VIEW CONFIGURATION   ///////////////////////////////////////////////////////////////////
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 8))
        footerView.backgroundColor = UIColor.clear
        
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 8.0
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let message = messages[indexPath.row]
        
        let dateHeight = CGFloat(30.0)
        
        if let _ = message.chatId{
            if let _ = message.textMessage{ // text message
                return UITableViewAutomaticDimension
            }else if let _ = message.chatId{ // structured message
                if indexPath.row == 0 || (indexPath.row == 1 && messages[0].chatId == nil){
                    return CGFloat(250.0 + dateHeight)
                }else if (indexPath.row > 0){
//                    return UITableViewAutomaticDimension
                    let previousMessage = self.messages[indexPath.row - 1]
                    if let messageDate = message.date, let previousMessageDate = previousMessage.date{
                        if(!HelperDate.messagingCompareTwoDates(messageDate,date2: previousMessageDate)) {
                            return CGFloat(250.0 + dateHeight)
                        }else{
                            return CGFloat(250.0)
                        }
                    }
                }
            }
        }else{
//            return CGFloat(40.0)
        }
        return  UITableViewAutomaticDimension
        
    }
    
    // MARK: -  ///////////////////////////////////////////////    TABLE VIEW DATA SOURCE    //////////////////////////////////////////////////////////////////

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
    
       
        if message.chatId == nil{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BeginConversationCell", for: indexPath) as! BeginConversationCell
            cell.message = message
            return cell
            
        // SENDER
        }else{
            if let fromId = message.fromId, let senderId = self.senderId, fromId == senderId{
                
                if let textMessage = message.textMessage{
                    
                    if let date = message.date{
                        if indexPath.row == 0 || (indexPath.row == 1 && messages[0].chatId == nil){
                            let cell = tableView.dequeueReusableCell(withIdentifier: "SenderTextDateCell", for: indexPath) as! SenderTextDateCell
                            cell.textMessageLabel.text = textMessage
                            cell.dateLabel.text = HelperDate.getTimeInString(date)
                            return cell
                        }else{
                            let previousMessage = self.messages[indexPath.item - 1];
                            if let previousMessageDate = previousMessage.date{
                                if(!HelperDate.messagingCompareTwoDates(date,date2: previousMessageDate)) {
                                    let cell = tableView.dequeueReusableCell(withIdentifier: "SenderTextDateCell", for: indexPath) as! SenderTextDateCell
                                    cell.textMessageLabel.text = textMessage
                                    cell.dateLabel.text = HelperDate.getTimeInString(date)
                                    return cell
                                } else {
                                    let cell = tableView.dequeueReusableCell(withIdentifier: "SenderTextCell", for: indexPath) as! SenderTextCell
                                    cell.textMessageLabel.text = textMessage
                                    return cell
                                    
                                }
                                
                            }
                        }
                    }
                    
                    
                    //                return cell
                }else if let product = message.product, let images = product.images, images.count > 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "SenderProductCell", for: indexPath) as! SenderProductCell
                    cell.product = product
                    
                    if let date = message.date{
                        
                        if indexPath.row == 0 || (indexPath.row == 1 && messages[0].chatId == nil){
                            cell.dateLabel.text = HelperDate.getTimeInString(date)
                        
                            cell.dateLabel.isHidden = false
                            cell.leftView.isHidden = false
                            cell.rightView.isHidden = false
                        }else{
                            let previousMessage = self.messages[indexPath.item - 1];
                            if let previousMessageDate = previousMessage.date{
                                if cell.dateLabel != nil{
                                    if(!HelperDate.messagingCompareTwoDates(date,date2: previousMessageDate)) {
                                        cell.dateLabel.text = HelperDate.getTimeInString(date)
                                        //                                    cell.dateLabel.hidden = false
                                        cell.dateLabel.isHidden = false
                                        cell.leftView.isHidden = false
                                        cell.rightView.isHidden = false
                                    } else {
                                        cell.dateLabel.isHidden = true
                                        cell.leftView.isHidden = true
                                        cell.rightView.isHidden = true
                                    }
                                }
                            }
                        }
                    }
                    
                    return cell
                }
            // RECEIVER
            }else {
                
                if let textMessage = message.textMessage{
                    
                    if let date = message.date{
                        if indexPath.row == 0 || (indexPath.row == 1 && messages[0].chatId == nil){
                            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverTextDateCell", for: indexPath) as! ReceiverTextDateCell
                            cell.textMessageLabel.text = textMessage
                            cell.profilePicture.image = self.receiverProfilePicture
                            
                            cell.dateLabel.text = HelperDate.getTimeInString(date)
                            if message.fromId != nil{
                                
                                cell.profilePictureButton.addTarget(self, action: #selector(self.goToUserProfile), for: .touchUpInside)

                            }
                            return cell
                        }else{
                            let previousMessage = self.messages[indexPath.item - 1];
                            if let previousMessageDate = previousMessage.date{
                                
                                if(!HelperDate.messagingCompareTwoDates(date,date2: previousMessageDate)) {
                                    let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverTextDateCell", for: indexPath) as! ReceiverTextDateCell
                                    cell.textMessageLabel.text = textMessage
                                    cell.profilePicture.image = self.receiverProfilePicture
                                    
                                    cell.dateLabel.text = HelperDate.getTimeInString(date)
                                    if message.fromId != nil{
                                        
                                        cell.profilePictureButton.addTarget(self, action: #selector(self.goToUserProfile), for: .touchUpInside)
                                    }
                                    return cell
                                } else {
                                    
                                    let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverTextCell", for: indexPath) as! ReceiverTextCell
                                    cell.textMessageLabel.text = textMessage
                                    cell.profilePicture.image = self.receiverProfilePicture
                                    if message.fromId != nil{
                                        
                                        cell.profilePictureButton.addTarget(self, action: #selector(self.goToUserProfile), for: .touchUpInside)
                                    }
                                    return cell
                                }
                            }
                        }
                    }
                }else if let listing = message.listing, let images = listing.images, images.count > 0{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverListingCell", for: indexPath) as! ReceiverListingCell
                    cell.profilePicture.image = self.receiverProfilePicture
                    cell.listing = listing
                    if let date = message.date{
                        cell.dateLabel.text = HelperDate.getTimeInString(date)
                        if indexPath.row == 0 || (indexPath.row == 1 && messages[0].chatId == nil){
                            cell.dateLabel.text = HelperDate.getTimeInString(date)
                            cell.dateLabel.isHidden = false
                            cell.leftView.isHidden = false
                            cell.rightView.isHidden = false
                        }else{
                            let previousMessage = self.messages[indexPath.item - 1];
                            if let previousMessageDate = previousMessage.date{
                                if(!HelperDate.messagingCompareTwoDates(date,date2: previousMessageDate)) {
                                    
                                    cell.dateLabel.text = HelperDate.getTimeInString(date)
                                    cell.dateLabel.isHidden = false
                                    cell.leftView.isHidden = false
                                    cell.rightView.isHidden = false
                                } else {
                                    if cell.dateLabel != nil{
                                        cell.dateLabel.isHidden = true
                                        cell.leftView.isHidden = true
                                        cell.rightView.isHidden = true
                                    }
                                }
                            }
                        }
                    }
                    
                    return cell
                    
                }else if let product = message.product, let images = product.images, images.count > 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverProductCell", for: indexPath) as! ReceiverProductCell
                    cell.product = product
                    cell.profilePicture.image = self.receiverProfilePicture
                    
                    cell.dateLabel.text = ""
                    if let date = message.date{
                        if indexPath.row == 0 || (indexPath.row == 1 && messages[0].chatId == nil){
                            cell.dateLabel.text = HelperDate.getTimeInString(date)
                            cell.dateLabel.isHidden = false
                            cell.leftView.isHidden = false
                            cell.rightView.isHidden = false
                        }else{
                            let previousMessage = self.messages[indexPath.item - 1];
                            if let previousMessageDate = previousMessage.date{
                                if cell.dateLabel != nil{
                                    if(!HelperDate.messagingCompareTwoDates(date,date2: previousMessageDate)) {
                                        cell.dateLabel.text = HelperDate.getTimeInString(date)
                                        cell.dateLabel.isHidden = false
                                        cell.leftView.isHidden = false
                                        cell.rightView.isHidden = false
                                    } else {
                                        cell.dateLabel.isHidden = true
                                        cell.leftView.isHidden = true
                                        cell.rightView.isHidden = true
                                    }
                                }
                            }
                        }
                    }
                    if message.fromId != nil{
                        cell.profilePictureButton.addTarget(self, action: #selector(self.goToUserProfile), for: .touchUpInside)
                    }
                    return cell
                }
            }
            
        }
        
        // DEFAULT
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SenderTextCell", for: indexPath) as! SenderTextCell
        cell.textMessageLabel.textAlignment = NSTextAlignment.right
        cell.textMessageLabel.text = " "
        return cell
    }
    
    // MARK: -  //////////////////////////////////////////////////    TABLE VIEW DELEGATE   /////////////////////////////////////////////////////////////////////
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let message = messages[indexPath.row]
        if let listing = message.listing{
            
            let vc:ProductBuyRetailerTableViewController = ProductBuyRetailerTableViewController.instanceFromStoryboard("Product")
            vc.listing = listing
            self.navigationController?.pushViewController(vc, animated: true)
        }else if let product = message.product{
            let vc:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
            vc.product = product
            self.navigationController?.pushViewController(vc, animated: true)
        }else if let _ = message.textMessage{
            
            let copyOptionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            // 1
            let copyAction = UIAlertAction(title: "COPY".localized, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                UIPasteboard.general.string = message.textMessage
                ErrorHandling.customSuccessHandler(message: "TEXT_COPIED_TO_CLIPBOARD".localized)
            })
            
            // 2
            let cancelAction = UIAlertAction(title: "CANCEL".localized, style: .destructive, handler: nil)
            
            copyOptionMenu.addAction(copyAction)
            copyOptionMenu.addAction(cancelAction)
            
            self.present(copyOptionMenu, animated: true, completion: nil)
        }
    }

}
