//
//  AddProductWebViewController.swift
//  Looplist
//
//  Created by Arun Sivakumar on 10/20/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation

//protocol dismissAddProductFinishProtocol: class{
//    func removeScript()
//    func showProductScreen(product:Product)
//}

protocol AddProductSelectProtocol: class{
    func dismissVC(product:Product?)
 
}

class AddProductFinishViewController:UIViewController {
    
//    weak var delegate:dismissAddProductFinishProtocol?
    
    weak var delegate:AddProductSelectProtocol?
    
//    var productUrl:String?
//    var productTitle:String?
//    var productImageUrl:String?
//    var productPrice:Int?
    
    var webProduct:WebProduct?
    
    
    var createdProduct:Product?
    
    weak var timer = Timer()
    
    @IBOutlet weak var finishButton: UIButton!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var selectProductLabel: UILabel!
    fileprivate var indicator = CustomActivityIndicator()
    
    override func viewDidAppear(_ animated: Bool) {
        AddProductWebViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        AddProductWebViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.finishButton.setTitle("FINISH".localized, for: .normal)
        self.selectProductLabel.text = "SELECT_AN_IMAGE".localized

        if let finishButton = self.finishButton{
            finishButton.addTarget(self, action: #selector(self.addProduct), for: .touchUpInside)
        }
        
        loadUI()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    fileprivate func loadUI(){
        
        self.productImageView.layer.cornerRadius = 3.0
        self.productImageView.clipsToBounds = true
        self.productImageView.setNeedsDisplay()
        
        self.finishButton.layer.cornerRadius = 25.0
        self.finishButton.clipsToBounds = true
        
        if let webProduct = webProduct{
        if let productTitle = webProduct.productTitle{
            productTitleLabel.text = productTitle
        }
        
        if let productImageUrl = webProduct.productImageUrl,let url = URL(string: productImageUrl){
     
            self.productImageView.kf.setImage(with:url)
        }
        
        if let productPrice = webProduct.productPrice{
            
            let remainder = productPrice%100
            var remainderString = "00"
            if remainder != 0{
                remainderString = "\(remainder)"
            }
            productPriceLabel.text = "$ \(productPrice/100).\(remainderString)"
        }
        }
    }
    
    @IBAction func goBackToAddProductSelect(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func close(_ sender: UIButton) {
        delegate?.dismissVC(product: nil)
    }
    @objc fileprivate func addProduct(){
        self.finishButton.isEnabled = false
        
        //self.activityIndicator.startAnimating()
        self.indicator.showActivityIndicator(uiView: self.view, offset: 0.0)
        
            if let webProduct = self.webProduct{
            
                APIHelperProduct.addProduct(webProduct: webProduct){[weak vc = self]product,error in
                    if let product = product{
                        if let url = webProduct.productUrl, let objectId = product.objectId{
                            Analytics.sharedInstance.actionsArray.append(Action(context:["addProductFromWeb": true, "url":url, "productId":objectId], forScreen:nil, withActivityType:"Added Product From Web"))

                        }
                        vc?.createdProduct = product
                        vc?.timer = Timer.scheduledTimer(timeInterval: ConstantsObjects.spinnerTime, target: self, selector: #selector(self.productCreationSuccess), userInfo: nil, repeats: false)
                        
                    }else {
                        vc?.timer = Timer.scheduledTimer(timeInterval: ConstantsObjects.spinnerTime, target: self, selector: #selector(self.productCreationFailure), userInfo: nil, repeats: false)
                        // Error product cannot be added
                    }
                }
                
            }else{
                // init error
                Timer.scheduledTimer(timeInterval: ConstantsObjects.spinnerTime, target: self, selector: #selector(self.productCreationFailure), userInfo: nil, repeats: false)
            }
    }

//    fileprivate func addToCollection(){
//        
//        if let product = self.createdProduct, let productObjectId = product.objectId, let objectId = CurrentUser.sharedInstance.currentUser?.objectId{
//            
//            // CASE 1 -- Check IF user has atleast one collection
//            //              ELSE create a collection and add product
//            
//            if let collections = CurrentUser.sharedInstance.currentUser?.collections, collections.count > 0{
//                
//                var addedProductsCollection:Collection? = nil
//                
//                for collection in collections{
//                    
//                     if collection.name == "Added Products"{
//                        addedProductsCollection = collection
//                        break
//                     }
//                    
//                }
//                
//                if let addedProductsCollection = addedProductsCollection{ // added products exist
//                    
//                    var productObjectIds = [String]()
//                    if let products = addedProductsCollection.products{
//                        for product in products{
//                            if let objectId = product.objectId{
//                                productObjectIds.append(objectId)
//                            }
//                        }
//                    }
//                    productObjectIds.append(productObjectId) // for user added product
//                    
//                    DBHelper.callAPIPut("collections/\(addedProductsCollection.objectId!)", params: ["name": "Added Products" as AnyObject, "description": "Products from Web" as AnyObject, "products":productObjectIds as AnyObject], completion: { (result, error) in
//                        if error == nil{
//                            CurrentUser.sharedInstance.currentUser?.getCurrentUserCollections(true, skip: 0, limit: 1000, completion: { (success) in
//                                if let collectionObjectId = result?["_id"] as? String{
//                                    Analytics.sharedInstance.actionsArray.append(Action(context: ["collection":collectionObjectId], forScreen: "Add Product User Selection", withActivityType:"Add Product To Collection"))
//                                }
//                                
//                                ErrorHandling.customSuccessHandler(message:"Successfully created \(product.brandName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!")
//                                
//                                self.goBack(product)
//                                return
//                            })
//                        } else {
//                            
//                        }
//                    })
//
//                    
//                }else{ // Added products doesnt exist
//                    
//                    let parameters:[String : AnyObject?] = ["name": "Added Products" as ImplicitlyUnwrappedOptional<AnyObject>, "owner": objectId as Optional<AnyObject>, "products": [productObjectId] as AnyObject, "users": [objectId] as AnyObject]
//                    DBHelper.callAPIPost("collections", params: parameters, completion: { (result, error) in
//                        if result != nil {
//                            if let collectionObjectId = result?["_id"] as? String{
//                                Analytics.sharedInstance.actionsArray.append(Action(context: ["collection":collectionObjectId], forScreen: "Add Product User Selection", withActivityType:"Create Collection"))
//                            }
//                            ErrorHandling.customSuccessHandler(message:"SUCCESSFULLY_CREATED".localized + " \(product.brandName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!")
//
//                            self.goBack(product)
//                        }
//                    })
//                }
//                
//            }else{
//                
//                let parameters:[String : AnyObject?] = ["name": "Added Products" as ImplicitlyUnwrappedOptional<AnyObject>, "owner": objectId as Optional<AnyObject>, "products": [productObjectId] as AnyObject, "users": [objectId] as AnyObject]
//                DBHelper.callAPIPost("collections", params: parameters, completion: { (result, error) in
//                    if error == nil{
//                        if let collectionObjectId = result?["_id"] as? String{
//                            Analytics.sharedInstance.actionsArray.append(Action(context: ["collection":collectionObjectId], forScreen: "Add Product User Selection", withActivityType:"Create Collection"))
//                        }
//                        ErrorHandling.customSuccessHandler(message:"SUCCESSFULLY_CREATED".localized + " \(product.brandName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!")
//
//
//                        self.goBack(product)
//                        
//                    } else {
//                        
//                    }
//                })
//            }
//            
//        }
////    }

//    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "ShowAddProductWeb"{
//            if let vc = segue.destination as? AddProductWebViewController{
//                vc.showProductScreen(product: self.createdProduct!)
//            }
//        }
    }
    func goBack(){
        //self.activityIndicator.stopAnimating()
        self.indicator.hideActivityIndicator()
//        self.delegate?.removeScript()
        
        if let product = self.createdProduct{
            delegate?.dismissVC(product:product)
            
//            self.navigationController?.dismissPopupViewController(animated: true, completion: nil)
//              self.dismissPopupViewController(animated: true, completion: nil)
//            self.dismissPopupViewController(animated: true, completion: nil)
//            performSegue(withIdentifier: "ShowAddProductWeb", sender: self)
//            self.dismissPopupViewController(animated: true, completion: nil)
            
//            self.dismiss(animated: true, completion: { 
//                self.delegate?.showProductScreen(product: product)
//            })
//            self.delegate?.showProductScreen(product: product)
//            self.navigationController?.showProductScreen(product)
        }else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc fileprivate func productCreationSuccess(){
        
        self.finishButton.setTitle("PRODUCT_ADDED".localized, for: UIControlState())
        self.finishButton.titleLabel!.font = UIFont(name:ConstantsFont.fontDefault,size:18)
        self.finishButton.backgroundColor = ConstantsColor.looplistGreyColor
        self.goBack()
//        self.addToCollection()
    }
    
    @objc fileprivate func productCreationFailure(){
        //self.activityIndicator.stopAnimating()
        self.indicator.hideActivityIndicator()
        ErrorHandling.customErrorHandler(message: "COULDNT_ADD_PRODUCT".localized)
        self.finishButton.isEnabled = true
    }
}
