//
//  AddProductViewController.swift
//  Looplist
//
//  Created by Carson Aberle on 8/25/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import WebKit

//@IBDesignable


class AddProductWebViewController:UIViewController,WKScriptMessageHandler, WKNavigationDelegate,UITextFieldDelegate,dismissAddProductFinishProtocol,dismissOnBoardingProtocol{
    
    // MARK: -  //////////////////////////////////////////////////////  VARIABLES  //////////////////////////////////////////////////////
    var userEnteredURL:String?
    
    private let contentController = WKUserContentController();

    
//    private var backButtonForOverlay:UIBarButtonItem = UIBarButtonItem()
//    private var closeButton:UIBarButtonItem = UIBarButtonItem()
    
    //-------Bottom view----------
    
    private var addProductButton:UIButton?
    private var navBackButton = UIButton(type: UIButtonType.custom)
    private var navForwardButton = UIButton(type: UIButtonType.custom)
    private var shareButton = UIButton(type: UIButtonType.custom)
    private var blurEffectView = UIVisualEffectView()
    
    private var addProductOnboardingView:AddProductOnboardingView?
    
//    private enum PageState{
//        case web
//        case select
//        case finish
//    }
//    
//    private var currentPageState = PageState.web
    
    // MARK: -  //////////////////////////////////////////////////////  OUTLETS  //////////////////////////////////////////////////////
    
    @IBOutlet weak var progressView: UIProgressView!

//    @IBOutlet weak var containerCollectionView: UIView!
//    @IBOutlet weak var addProductView: UIView!
//    @IBOutlet weak var addProductButton: UIButton!
//    @IBOutlet weak var pasteURLButton: UIButton!
//    @IBOutlet weak var findURLButton: UIButton!
    
    @IBOutlet weak var searchTextField: UITextField!{
        didSet{
            searchTextField.delegate = self
        }
    }
//    @IBOutlet weak var goButton: UIBarButtonItem!
    
//    @IBOutlet var containerView : UIView!
    
//    @IBInspectable
    private var webView: WKWebView?{
        didSet{
            if let webView = webView{
                webView.navigationDelegate = self
                webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
            }
        }
    }

    private var selectedProductImageUrl:[Dictionary<String,AnyObject>] = [Dictionary<String,AnyObject>]()
    
    // MARK: -  //////////////////////////////////////////////////////  VIEW CONTROLLERS  //////////////////////////////////////////////////////

//    
//    private  var addProductSelectVC = AddProductSelectViewController()
//    private  var addProductFinishVC = AddProductFinishViewController()
    
    
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   ////////////////////////////////////////////////////////////////

    
    deinit {
        print("deinit")
    }
    override func loadView() {
        super.loadView()
         self.configureWebView()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchTextField.placeholder = "ADD_PRODUCT_URL_PLACEHOLDER".localized
        self.tabBarController?.tabBar.isHidden = true
        
        loadURL()
//        self.progressView.setProgress(1.0, animated: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        AddProductWebViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        AddProductWebViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
    }
    
    override func viewWillAppear(_ animated: Bool) {

        self.tabBarController?.tabBar.isHidden = false
//        self.view.frame.origin.y = 0.0
        
        if let navigationController = self.navigationController{
            AddProductWebViewController.self.setNavigationBar(UIColor.darkGray, font: ConstantsFont.navBarFont, navigationController: navigationController, statusBar:.default, backgroundColor:NavigationBarBackgroundColor.whiteOpaque)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        self.removeUserScript()
    }
    
    // MARK: -  //////////////////////////////////////////////////////   NAVIGATION   ///////////////////////////////////////////////////////////////////
    
    
//    @IBAction func dismissScreen(_ sender: UIBarButtonItem) {
//         _ = self.navigationController?.popToRootViewController(animated: true)
//    }
//    
    @IBAction func dismissAddProduct(_ sender: UIBarButtonItem) {
        self.removeScript()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func dismissViewController(){
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "ShowAddProductSelect"{
            if let addProductSelectVC = segue.destination as? AddProductSelectViewController{
                addProductSelectVC.delegate = self
                addProductSelectVC.productUrl = self.webView?.url?.absoluteString
                addProductSelectVC.productTitle = self.webView?.title
                addProductSelectVC.productImageUrl = self.selectedProductImageUrl
            }
        }
    }
    
   
//    @objc internal func popViewController(){
//        self.searchTextField.endEditing(true)
////        self.removeUserScript()
//        _ = self.navigationController?.popToRootViewController(animated: false)
//    }
    
    @IBAction func UnwindToAddProductWeb(_ segue: UIStoryboardSegue) {
        
        
//        if segue.identifier == "ShowAddProductWeb"{
//            if let vc = segue.source as? AddProductFinishViewController{
//                if let product = vc.createdProduct{
//                    self.showProductScreen(product: product)
//                }
//            }
//        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   WEB VIEW   ///////////////////////////////////////////////////////////////////

    

    func configureWebView(){
        
        let fetchTableOfContentsScriptString = try! String(contentsOfFile: Bundle.main.path(forResource: "FetchImage", ofType: "js")!, encoding: String.Encoding.utf8)
        
        let userScript = WKUserScript(
            source: fetchTableOfContentsScriptString,
            injectionTime: .atDocumentEnd,
            forMainFrameOnly: true
        )
        contentController.addUserScript(userScript)
        contentController.add(
            self,
            name: "callbackHandler"
        )
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        
        
        self.webView = WKWebView(
            frame: self.self.view.bounds,
            configuration: config
            
        )
        
        self.webView?.frame = view.bounds
        if let webView = self.webView{
            self.view.addSubview(webView)
        }
        
        self.webView?.scrollView.keyboardDismissMode = .onDrag
        self.view.bringSubview(toFront: progressView)
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if(message.name == "callbackHandler") {
            print(message.body)
          
             self.selectedProductImageUrl = [Dictionary<String,AnyObject>]()
             self.selectedProductImageUrl = (message.body as? [Dictionary<String,AnyObject>])!
            
//             if !self.addProductButtonAdded && self.selectedProductImageUrl.count > 0 {
            if addProductButton == nil{
                createAndAddProductButton()
            }
            
            checkStateOfAddProductButton()
        }
    }
    

    
    // MARK: -  //////////////////////////////////////////////////////   WEB VIEW  DELEGATES  ///////////////////////////////////////////////////////////////////
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            if let webView = webView, let progressView = progressView{
                
                
                progressView.progress = Float(webView.estimatedProgress)
                
//                if webView.estimatedProgress == 1.0{
//                    Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.removeProgressBar), userInfo: nil, repeats: true)
//                }
            }
        }
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
                changeStateOfProgressView(completed: false)

        if let webViewURL = webView.url?.absoluteString{
            self.searchTextField.text = "\(webViewURL)"
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
 
        checkStateOfWebViewNavigationButtons()
//        changeStateOfProgressView(completed: true)
//        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.removeProgressBar), userInfo: nil, repeats: true)


//        saveUrlToUser()
        
//        if currentPageState == .finish{
//            dismissToAddProductScreenFromFinish()
//        }else if currentPageState == .select{
//            dismissToAddProductScreenFromSelect()
//        }
        
    }
    
    func removeProgressBar(){
        changeStateOfProgressView(completed: true)
    }
    

    func changeStateOfProgressView(completed:Bool){
        if completed{
//            self.progressView.progress = Float(0.0)
//            self.progressView.isHidden = false

            self.progressView.progressTintColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1.0)
        }else{
            self.progressView.progressTintColor = ConstantsColor.looplistColor
//            self.progressView.isHidden = true

        }
    }
//    private func saveUrlToUser(){
//        if let lastVisitedUrl = self.webView?.url?.absoluteString{
//            RealmHelper.assignLastVisitedUrl(lastVisitedUrl)
//        }
//    }
    
    private func showOnboarding(){
        if let view = AddProductOnboardingView.instanceFromNib(){
            self.addProductOnboardingView = view
            view.frame = self.view.frame
            view.delegate = self
            self.view.addSubview(view)
            UIView.animate(withDuration: 0.50, animations: {
                view.bringToScreen()
            })
        }
    }
    // MARK: -  //////////////////////////////////////////////////////   TEXTFIELD  DELEGATES  ///////////////////////////////////////////////////////////////////

    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//
//    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if let _ = addProductOnboardingView{
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        findURL()
        return true
    }
    
    
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
////        self.dismissOverlay()
//    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == ""{
            self.searchTextField.text = self.webView?.url?.absoluteString
        }
    }

    
    // MARK: -  //////////////////////////////////////////////////////   CHANGE UI   ///////////////////////////////////////////////////////////////////
    
//    fileprivate func configureUI(){
////         let fakeButton = UIBarButtonItem(image: UIImage(), style: .plain, target: self, action: nil)
////         self.navigationItem.leftBarButtonItem = fakeButton
////        self.navigationItem.rightBarButtonItem = fakeButton
////        closeButton = UIBarButtonItem(image: UIImage(named: Constants.closeLargeImage), style: .plain, target: self, action: #selector(self.popViewController))
////
////        searchTextField.layer.cornerRadius = self.searchTextField.frame.height / 2.0
////        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGray
////        self.navigationItem.leftBarButtonItem = closeButton
//    }

    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
        internal func removeUserScript(){
            self.webView?.configuration.userContentController.removeScriptMessageHandler(forName: "callbackHandler")
    
        }
    
    
    func showProductScreen(product: Product) {
//        self.dismissPopupViewController(animated: true, completion: nil)
//        self.dismiss
//        self.dismissPopupViewController(animated: true, completion: nil)
//        self.presentingViewController?.dismiss(animated: true, completion: nil)
        self.navigationController?.showProductScreen(product)
        
    }
    
    internal func dismissOnBoarding() {
        
        RealmHelper.setUserSeenAddProductOnboarding()
        
        if let addProductOnboardingView = self.addProductOnboardingView{
            UIView.animate(withDuration: 0.25, animations: { 
                addProductOnboardingView.removeFromScreen()
            }, completion: { (true) in
                 addProductOnboardingView.removeFromSuperview()
            })
           
            self.addProductOnboardingView = nil
        }
        
        if let userEnteredURL = self.userEnteredURL{
            self.searchTextField.text = userEnteredURL
            findURL()
        }
    }
    
    
    internal func removeScript(){
        //        self.removeUserScript()
        self.webView?.configuration.userContentController.removeScriptMessageHandler(forName: "callbackHandler")

        self.webView?.removeObserver(self, forKeyPath:  #keyPath(WKWebView.estimatedProgress))
        
    }
    
    @objc private func showAddProductSelectScreen(){
        
        performSegue(withIdentifier: "ShowAddProductSelect", sender: self)
    }
    
    @objc private func goBack(){
        _ = self.webView?.goBack()
    }
    
    @objc private func goForward(){
        _ = self.webView?.goForward()
    }
    
    @objc private func goToSafari(){
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "CANCEL".localized, style: .cancel) { (action) in }
        alertController.addAction(cancelAction)
        
        let safariAction = UIAlertAction(title: "OPEN_IN_SAFARI".localized, style: .default) { (action) in
            if let urlString = self.searchTextField.text{
                let url = URL(string: urlString)!
                UIApplication.shared.openURL(url)
            }
        }
        alertController.addAction(safariAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func createAndAddProductButton(){
        
        let addProductBackgroundView = UIView() 
        addProductBackgroundView.frame = CGRect(x: 0,y: self.view.frame.height - 49.0, width: self.view.frame.width, height: 49.0)
        addProductBackgroundView.backgroundColor = UIColor.white
        addProductBackgroundView.alpha = 0.0
        self.view.addSubview(addProductBackgroundView)
        
        addProductButton = UIButton()
        if let addProductButton = addProductButton{
            
            addProductButton.frame = CGRect(x: (self.view.frame.width - (self.view.frame.width / 4.0))/2.0,y: 10.0, width: self.view.frame.width / 4.0, height: 30)
            addProductButton.backgroundColor = UIColor.clear
            addProductButton.setTitle("ADD_TO_LOOPLIST".localized, for: UIControlState())
            addProductButton.addTarget(self, action: #selector(self.showAddProductSelectScreen), for: .touchUpInside)
            addProductButton.layer.cornerRadius = 4
            addProductButton.layer.borderColor = UIColor.darkGray.cgColor
            addProductButton.layer.borderWidth = 1
            addProductButton.titleLabel?.font = UIFont(name: ConstantsFont.fontDefault, size: 11.0)
            addProductButton.setTitleColor(UIColor.darkGray, for: UIControlState())
            addProductButton.alpha = 0.0
            addProductButton.isUserInteractionEnabled = false
            addProductBackgroundView.addSubview(addProductButton)
        }
        let image1 = UIImage(named: Constants.WebBackImageActive)
        navBackButton.frame = CGRect(x: 8, y: 4.0, width: self.view.frame.width / 8.0, height: 41)
        navBackButton.setImage(image1, for: UIControlState())
        navBackButton.addTarget(self, action: #selector(self.goBack), for:.touchUpInside)
        navBackButton.alpha = 0
//        navBackButton.isEnabled = false
        addProductBackgroundView.addSubview(navBackButton)
        
        let image2 = UIImage(named: Constants.WebForwardImageActive)
        navForwardButton.frame = CGRect(x: self.view.frame.width / 8.0 + 10, y: 4.0, width: self.view.frame.width / 8.0, height: 41)
        navForwardButton.setImage(image2, for: UIControlState())
        navForwardButton.addTarget(self, action: #selector(self.goForward), for:.touchUpInside)
        navForwardButton.alpha = 0
//         navForwardButton.isEnabled = false
        addProductBackgroundView.addSubview(navForwardButton)
        
        let shareImage = UIImage(named: Constants.WebShareImage)
        shareButton.frame = CGRect(x: self.view.frame.width - self.view.frame.width/7 - 10, y: 0.0, width: self.view.frame.width / 8.0, height: 49)
        shareButton.setImage(shareImage, for: UIControlState())
        shareButton.addTarget(self, action: #selector(self.goToSafari), for:.touchUpInside)
        shareButton.alpha = 0
        addProductBackgroundView.addSubview(shareButton)
        
//        self.addProductButtonAdded = true
        
        UIView.animate(withDuration: 0.50, animations: {
            addProductBackgroundView.alpha = 0.98
            self.addProductButton?.alpha = 1.0
            self.navBackButton.alpha = 0.5
            self.navForwardButton.alpha = 0.5
            self.shareButton.alpha = 0.5
        })
    }
    
    private func checkStateOfAddProductButton(){
        
        if selectedProductImageUrl.count > 0{
            
            self.addProductButton?.alpha = 1.0
            self.addProductButton?.isUserInteractionEnabled = true
        }else{
            self.addProductButton?.alpha = 0.5
            self.addProductButton?.isUserInteractionEnabled = false
        }
    }
    
    
    private func checkStateOfWebViewNavigationButtons(){
        //self.navigationItem.leftBarButtonItem = self.closeButton
        if let webView = self.webView{
            if webView.canGoBack {
                 self.navBackButton.alpha = 1.0
                self.navBackButton.isUserInteractionEnabled = true
            }else {
                self.navBackButton.alpha = 0.5
                self.navBackButton.isUserInteractionEnabled = false
            }
            
            if webView.canGoForward {
                self.navForwardButton.alpha = 1.0
                 self.navForwardButton.isUserInteractionEnabled = true
            }else{
                self.navForwardButton.alpha = 0.5
                self.navForwardButton.isUserInteractionEnabled = false
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   WEB VIEW FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    private func loadURL(){
        
        if let status = RealmHelper.getOnBoardingStatus(), status == false{
            showOnboarding()
            loadDefaultURL()
        }else if let userEnteredURL = self.userEnteredURL{
            self.searchTextField.text = userEnteredURL
            findURL()
        }else{
            loadDefaultURL() // fallback
        }
        
        
//        if let userEnteredURL = self.userEnteredURL
//        {
//            self.searchTextField.text = userEnteredURL
//            findURL()
//        }else
//            
//        if let userLastVisitedURL = RealmHelper.getLastVisitedUrl()
//        {
//            self.searchTextField.text = userLastVisitedURL
//            findURL()
//        }else
//        {
//            loadDefaultURL()
//        }
    }
    
    
    private func loadDefaultURL(){
        if let url =  URL(string: "http://www.google.com/"), let webView = self.webView{
            webView.load(URLRequest(url: url))
        }
        
    }
    
    private func verifyUrl (_ urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = URL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url)
            }
        }
        return false
    }
    
    private func findURL(){
        
        self.searchTextField.endEditing(true)
        
        // 1. Search term exists
        // 2. No search Term
        
        if var userEnteredURL = self.searchTextField.text {
            
            // 1. add http/https
            
            if userEnteredURL.hasPrefix("www.") && !userEnteredURL.hasPrefix("http://") && !userEnteredURL.hasPrefix("https://")   {
                let alteredURL = "http://" + userEnteredURL
                userEnteredURL = alteredURL
            }
            
            // 2. Verify & Load URL
            // a. Verify & Load
            // b. If verification Fails Load from Google
            
            if self.verifyUrl(userEnteredURL){ // Verify
                if let webView =  self.webView,let url =  URL(string: userEnteredURL){
                        webView.load(URLRequest(url: url))
                }
                
            }else if userEnteredURL != ""{ //user entered search term in google
                let newString = userEnteredURL.replacingOccurrences(of: " ", with: "%20")
                if let webView =  self.webView,let url =  URL(string: "https://www.google.com/search?q=\(newString)"){
                   
                    webView.load(URLRequest(url: url))
                }
            }
        }else{
            loadDefaultURL()
        }
    }
    

    

}
