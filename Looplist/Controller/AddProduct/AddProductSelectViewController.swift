//
//  AddProductDisplayViewController.swift
//  Looplist
//
//  Created by Arun Sivakumar on 10/26/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import WebKit

//
//protocol dismissAddProductSelectProtocol: class{
//    func showAddProductFinishScreen(webProduct:WebProduct?)
//}

protocol dismissAddProductFinishProtocol: class{
    func removeScript()
    func showProductScreen(product:Product)
}

var count = 0
class AddProductSelectViewController: UIViewController,AddProductSelectProtocol{
    
    // MARK: -  //////////////////////////////////////////////////////  PUBLIC VARIABLES  //////////////////////////////////////////////////////
    
    //    weak var delegate:dismissAddProductSelectProtocol?
        weak var delegate:dismissAddProductFinishProtocol?

    
    var productUrl:String?
    var productTitle:String?
    var productImageUrl:[Dictionary<String,AnyObject>]?
    
    //    fileprivate var selectedProductImageUrl:String?
    //    fileprivate var selectedProductPrice:Int?
    
    // MARK: -  //////////////////////////////////////////////////////  PRIVATE VARIABLES  //////////////////////////////////////////////////////
    
    fileprivate var selectedWebProduct:WebProduct?
    fileprivate var selectedCell:AddProductCollectionViewCell?{
        didSet{
            
            if let oldValue = oldValue{ // clear the color
              
                oldValue.productImageView.layer.borderColor = UIColor.clear.cgColor
            }
            if let selectedCell = selectedCell{
                selectedCell.productImageView.layer.borderColor = ConstantsColor.looplistColor.cgColor
            }else if selectedCell == nil{
            }
        }

    }
    //    fileprivate var selectedProductPrice:Int?
    
    // MARK: -  //////////////////////////////////////////////////////  OUTLETS  //////////////////////////////////////////////////////
    
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
        }
    }
    //    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   ////////////////////////////////////////////////////////////////

    override func viewDidLoad() {
        super.viewDidLoad()
        count += 1
    }
    
    override func viewDidAppear(_ animated: Bool) {
        AddProductSelectViewController.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        AddProductSelectViewController.stopTimer(NSDictionary(), withActionType:"View Screen")
        
        if let selectedCell = selectedCell{
            selectedCell.productImageView.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func UnwindToAddProductSelect(_ segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func showAddProductWeb(){
        //        delegate?.showAddProductFinishScreen(webProduct:selectedWebProduct)
        performSegue(withIdentifier: "ShowAddProductFinish", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "ShowAddProductFinish"{
            if let addProductFinishVC = segue.destination as? AddProductFinishViewController{
                addProductFinishVC.delegate = self
                addProductFinishVC.webProduct = selectedWebProduct
            }
        }
    }
    
    func dismissVC(product:Product?){
        self.view.alpha = 0.0
        self.dismiss(animated: true){
            //        self.dismiss(animated: false, completion: nil)
            self.dismiss(animated: false) {
                if let product = product{
                    self.delegate?.showProductScreen(product: product)
                }
            }
        }
        
    }
}

// MARK: -  //////////////////////////////////////////////////////  COLLECTION VIEW   /////////////////////////////////////////////////////////////////////////

extension AddProductSelectViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = self.productImageUrl?.count{
            return count
        }
        return 0
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddProductCell", for: indexPath) as! AddProductCollectionViewCell
        if let productImageUrl = productImageUrl?[indexPath.row]{
            if let imageUrl = productImageUrl["image"] as? String{
                cell.productImageUrl = imageUrl
            }
        }
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView{
        
        if (kind == UICollectionElementKindSectionHeader) {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AddProductHeaderView", for: indexPath) as! AddProductHeaderCollectionReusableView
        
        return headerView
            
        }else {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AddProductFooterView", for: indexPath) as! AddProductFooterCollectionReusableView
            
            return footerView

        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let dimension = self.view.frame.width/3
        return CGSize(width: dimension, height: dimension)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return self.view.frame.width/11
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,insetForSectionAt section: Int) -> UIEdgeInsets{
        return UIEdgeInsets(top: 0, left: self.view.frame.width/8.5, bottom: self.view.frame.width/11, right: self.view.frame.width/8.5)
    }
    
    // MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! AddProductCollectionViewCell
        
        cell.productImageView.layer.borderColor = ConstantsColor.looplistColor.cgColor
        cell.productImageView.layer.borderWidth = 3.0
        
        if let productImageUrl = self.productImageUrl?[indexPath.row]{
            guard let productImage = productImageUrl["image"] as? String,let productUrl = self.productUrl,let productTitle = self.productTitle else{ return}
            
            var productPrice:Int? = nil
            
            if let price =  productImageUrl["price"] as? Int{
                productPrice = price
            }
            
            if let webProduct = WebProduct(productUrl: productUrl, productTitle: productTitle, productImageUrl: productImage, productPrice: productPrice){
                
                selectedWebProduct = webProduct
                
                Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.showAddProductWeb), userInfo: nil, repeats: false)
            }
        }
        selectedCell = cell
    }
}
