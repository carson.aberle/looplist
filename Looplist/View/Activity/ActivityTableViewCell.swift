//
//  ActivityTableView swift
//  Looplist
//
//  Created by Carson Aberle on 8/25/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class ActivityTableViewCell:UITableViewCell{
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var activityTypeImage: UIImageView!
    @IBOutlet weak var activityUsername: UILabel!
    @IBOutlet weak var activityDate: UILabel!
    @IBOutlet weak var activityDescription: UILabel!
    @IBOutlet weak var activityImage: UIImageView!
    
    var callingClass:UIViewController!
    var activity:Activity?{
        didSet{
            if let activity = activity{
                if let user = activity.user{

                    if let activityType = activity.activityType{
                        switch activityType{
                            
                        case .likeProduct,.likedYourRepostedProduct:
                            
                            activityTypeImage.image = UIImage(named: Constants.notificationLikeImage)

                            activityImage.layer.cornerRadius = 2
                            activityImage.clipsToBounds = true
                            guard let count = activity.product?.images?.count,count > 0,let productImageUrl = activity.product?.images?[0] else{ activityImage.image = UIImage(named:Constants.placeholder); return}
                            activityImage.kf.setImage(with:(URL(string:productImageUrl)))
                            
                            
                        case .repostProduct:
                            
                            activityTypeImage.image = UIImage(named: Constants.notificationRepostImage)

                            
                            activityImage.layer.cornerRadius = 2
                            activityImage.clipsToBounds = true
                            guard let count = activity.product?.images?.count,count > 0,let productImageUrl = activity.product?.images?[0] else{ activityImage.image = UIImage(named:Constants.placeholder); return}
                            activityImage.kf.setImage(with:(URL(string:productImageUrl)))
                            

                        case .follow,.followRequest,.acceptedFollowRequest:
                            
                            activityTypeImage.image = UIImage(named: Constants.notificationFollowImage)

                            
                            activityImage.layer.cornerRadius =  activityImage.frame.height / 2.0
                            activityImage.clipsToBounds = true
                             ObjectStateHelper.getUserProfileImage(user: user, imageView: activityImage)
                        
                        
                        case .someoneAddedYourProduct:
                             activityTypeImage.image = UIImage(named: Constants.notificationAddProductImage)
                             
                             activityImage.layer.cornerRadius = 2
                             activityImage.clipsToBounds = true
                             guard let count = activity.product?.images?.count,count > 0,let productImageUrl = activity.product?.images?[0] else{ activityImage.image = UIImage(named:Constants.placeholder); return}
                             activityImage.kf.setImage(with:(URL(string:productImageUrl)))
                            
                            
                        }

                        activityDescription.text = activityType.getDescription(callingClass: callingClass)

                        
                         activityUsername.text = user.userName!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                        if let date = activity.date{
                             activityDate.text = HelperDate.getTimeStringInbox(date)
                        }
//                        if let activityRead = activity.read, activityRead == true{
//                             backgroundColor = UIColor(red:0.97, green:0.99, blue:1.00, alpha:1.0)
//                        } else {
//                             backgroundColor = UIColor.white
//                        }
                        backgroundColor = UIColor.white

                    }
                }
            }
        }
    }
    override func awakeFromNib() {
//        activityImage.layer.cornerRadius =  activityImage.frame.height / 2.0
    }
}
