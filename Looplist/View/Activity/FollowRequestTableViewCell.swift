//
//  FollowRequestTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 9/29/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class FollowRequestTableViewCell:UITableViewCell{

    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var requestLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var approveOrDenyLabel: UILabel!
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTONS   /////////////////////////////////////////////////////////////////////////

    override func awakeFromNib() {
        self.approveOrDenyLabel.text = "APPROVE_IGNORE_REQUEST".localized
    }
}
