//
//  NewFollowRequestsTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 9/30/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class NewFollowRequestsTableViewCell:UITableViewCell {
    
    @IBOutlet weak var firstUserImage: UIImageView!
    @IBOutlet weak var requestCountLabel: UILabel!
    
    @IBOutlet weak var followRequestsTitle: UILabel!
    @IBOutlet weak var newFollowRequestsTitle: UILabel!
    var user:User?{
        didSet{
            if let user = user{
                if let userImageURL = user.profileImageUrl{
                    firstUserImage.kf.setImage(with: URL(string:userImageURL))
                }
            }
            
        }
    }
    
    var count:Int?{
        didSet{
            if let count = count {
                requestCountLabel.text = count < 101 ? "\(count)" : "100+"
            }
        }
    }
    override func awakeFromNib() {
        self.followRequestsTitle.text = "FOLLOW_REQUESTS".localized
        self.newFollowRequestsTitle.text = "NEW_FOLLOW_REQUESTS_TITLE".localized
    }
}
