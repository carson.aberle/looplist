//
//  CollectionLikesCollectionViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 8/17/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class CollectionLikesCollectionViewCell:UICollectionViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var productImage: UIImageView!
    
    var indexPath: IndexPath!
    
    var currentCollection: Collection?{
        didSet{
            if let collection = self.currentCollection{
                if let products = collection.products{
                    if let index = self.indexPath{
                        if products.count > index.row{
                            let collectionProduct = collection.products![index.row]
                            if  (collectionProduct.images?.count)! > 0 {
                                if let img = collectionProduct.images?[0], img != ""{
                                    self.productImage.kf.setImage(with:URL(string:img ))
                                }else{
                                    self.productImage.image = UIImage(named: Constants.placeholder)
                                }
                            }else{
                                self.productImage.image = UIImage(named: Constants.placeholder)
                            }
                        }
                    }
                }
            }
        }
    }
}
