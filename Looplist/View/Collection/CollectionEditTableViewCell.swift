//
//  CollectionEditTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 8/19/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class CollectionEditTableViewCell:UITableViewCell{
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var clearProductImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productIdentifier: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    
    
    var indexPath:IndexPath? = nil
    var currentProduct: Product?{
        didSet{
            if let product = self.currentProduct{
                self.productImage.kf.setImage(with: URL(string: product.images![0]))
                self.productTitle.text = product.brandName
                self.productIdentifier.text = product.keywords
            }
        }
    }
}
