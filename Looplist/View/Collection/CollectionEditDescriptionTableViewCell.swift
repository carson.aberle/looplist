//
//  CollectionEditDescriptionTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 8/19/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class CollectionEditDescriptionTableViewCell:UITableViewCell{
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var deleteCollectionButton: UIButton!
    @IBOutlet weak var collectionTitle: UITextField!
    @IBOutlet weak var collectionDescription: UITextView!
    @IBOutlet weak var privateSwitch: UISwitch!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var collectionNameLabel: UILabel!
    @IBOutlet weak var privateLabel: UILabel!
    
    var currentCollection: Collection?{
        didSet{
            if let collection = self.currentCollection{
                
                self.collectionTitle.text = collection.name ?? ""
                
                if let description = collection.description{
                    if description == ""{
                        self.collectionDescription.text = "COLLECTION_DESCRIPTION_PLACEHOLDER".localized
                        self.collectionDescription.textColor = UIColor(red:0.81, green:0.81, blue:0.81, alpha:1.0)
                    } else {
                        self.collectionDescription.text = description
                        self.collectionDescription.textColor = ConstantsColor.looplistGreyColor
//                        self.collectionDescription.textColor = UIColor.black
                    }
                    
                } else {
                    self.collectionDescription.text! = "COLLECTION_DESCRIPTION_PLACEHOLDER".localized
                    self.collectionDescription.textColor = UIColor(red:0.81, green:0.81, blue:0.81, alpha:1.0)
                }
                
                if let isPrivate = collection.isPrivate{
                    self.privateSwitch.setOn(isPrivate, animated: false)
                }
    
                if collection.isAddedProductCollection == true{
                    self.collectionTitle.isUserInteractionEnabled = false
                    self.deleteCollectionButton.isHidden = true
                }
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTION  /////////////////////////////////////////////////////////////////////////
    
    override func awakeFromNib() {
        self.collectionTitle.placeholder = "COLLECTION_NAME_PLACEHOLDER".localized
        self.collectionDescription.text = "COLLECTION_DESCRIPTION_PLACEHOLDER".localized
        self.deleteCollectionButton.setTitle("DELETE_COLLECTION".localized, for: .normal)
        self.descriptionLabel.text = "COLLECTION_DESCRIPTION".localized
        self.collectionNameLabel.text = "COLLECTION_NAME".localized
        self.privateLabel.text = "PRIVATE".localized
        
        self.deleteCollectionButton.layer.borderColor = UIColor(red:0.96, green:0.40, blue:0.39, alpha:1.0).cgColor
        self.deleteCollectionButton.layer.borderWidth = 1
        self.deleteCollectionButton.layer.cornerRadius = 6
        
        self.collectionDescription.layer.borderColor = UIColor(red:0.81, green:0.81, blue:0.81, alpha:1.0).cgColor
        self.collectionDescription.layer.borderWidth = 0.6
        self.collectionDescription.layer.cornerRadius = 6
        
        self.separatorInset = UIEdgeInsets(top: 0, left: self.frame.width, bottom: 0, right: 0)
    }
}
