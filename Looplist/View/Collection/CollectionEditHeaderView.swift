//
//  CollectionEditHeaderView.swift
//  Looplist
//
//  Created by Carson Aberle on 8/19/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class CollectionEditHeaderView: UITableViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var collectionOwner: UILabel!
    @IBOutlet weak var collectionItemsCount: UILabel!
    @IBOutlet weak var collectionHeaderImage: UIImageView!
    
    var isHeaderImageSet = false
    
    var currentCollection: Collection?{
        didSet{
            if let collection = self.currentCollection{
                if !isHeaderImageSet{
                    if let count = collection.products?.count{
                        let random = arc4random_uniform(UInt32(count)) + 0
                        if count > Int(random){
                            if let image = collection.products?[Int(random)].images?[0]{
                                self.collectionHeaderImage.kf.setImage(with: URL(string: image))
                                isHeaderImageSet = true
                            }
                        }
                    }
                }
                
                self.collectionItemsCount.text! = "\(collection.collectionItemsCount)"
                
                if let username = collection.owner?.userName{
                    let usernameString = "@" + username
                    let usernameAttribute = [ NSForegroundColorAttributeName: UIColor.white,NSFontAttributeName: UIFont(name: ConstantsFont.fontDefaultBold, size: 11)!]
                    let usernameAttributedString = NSAttributedString(string: usernameString, attributes: usernameAttribute)
                    let finalAttributedString = NSMutableAttributedString(string:"A_COLLECTION_BY".localized)
                    finalAttributedString.append(usernameAttributedString)
                    self.collectionOwner.attributedText = finalAttributedString
                    self.collectionOwner.isUserInteractionEnabled = true
                }
            }
        }
    }
}
