//
//  CollectionTitleTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 8/15/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class CollectionNameTableViewCell:UITableViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var selectionIndicator: UIImageView!
    @IBOutlet weak var collectionName: UILabel!
    internal var popupVC:AddToCollectionViewController!
    @IBOutlet weak var firstProductImage: UIImageView!
}
