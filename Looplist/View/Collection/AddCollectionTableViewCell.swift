//
//  AddCollectionTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 8/15/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class AddCollectionTableViewCell:UITableViewCell{

    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    var callingClass:UIViewController?
    
    @IBOutlet weak var addCollectionButton: UILabel!
    @IBOutlet weak var createNewButton: UIButton!
    @IBAction func showCreateNewCollectionScreen(_ sender: UIButton) {
        if let addToCollectionVC = self.callingClass as? AddToCollectionViewController{
            let newCollectionVC:AddCollectionViewController = AddCollectionViewController.instanceFromStoryboard("Main")
            if let product = addToCollectionVC.product{
                newCollectionVC.product = product
            }
            addToCollectionVC.navigationController?.pushViewController(newCollectionVC, animated: true)
        } else if let productVC = self.callingClass as? ProductTableViewController{
            let newCollectionVC:AddCollectionViewController = AddCollectionViewController.instanceFromStoryboard("Main")
            if let product = productVC.product{
                newCollectionVC.product = product
            }
            productVC.navigationController?.pushViewController(newCollectionVC, animated: true)
        }
    }
}
