//
//  CollectionLikesHeaderView.swift
//  Looplist
//
//  Created by Carson Aberle on 8/17/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class CollectionLikesHeaderView: UICollectionReusableView {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var collectionOwner: UILabel!
    @IBOutlet weak var collectionItemsCount: UILabel!
    @IBOutlet weak var collectionDescription: UILabel!
    @IBOutlet weak var collectionHeaderImage: UIImageView!
    
    var isHeaderImageSet = false

    var currentCollection: Collection?{
        didSet{
            if let collection = self.currentCollection{
                
                if !isHeaderImageSet{
                    if let count = collection.products?.count{
                        let random = arc4random_uniform(UInt32(count)) + 0
                        if count > Int(random){
                            if let productHeaderImage = collection.products?[Int(random)].images?[0]{
                                self.collectionHeaderImage.kf.setImage(with: URL(string: productHeaderImage))
                                isHeaderImageSet = true
                            }
                        }
                    }
                }
                
                self.collectionItemsCount.text = "\(collection.getCollectionItemsCount())"
                
                if let ownerName = collection.owner?.userName{
                    let usernameString = "@" + ownerName
                    let usernameAttribute = [ NSForegroundColorAttributeName: UIColor.white,NSFontAttributeName: UIFont(name: ConstantsFont.fontDefaultBold, size: 11)!]
                    let usernameAttributedString = NSAttributedString(string: usernameString, attributes: usernameAttribute)
                    let finalAttributedString = NSMutableAttributedString(string:"A_COLLECTION_BY".localized)
                    finalAttributedString.append(usernameAttributedString)
                    self.collectionOwner.attributedText = finalAttributedString
                }
                
                if let collectionDescription = collection.description{
                    if collectionDescription == ""{
                        self.collectionDescription.isHidden = true
                    } else {
                        self.collectionDescription.text = collectionDescription
                        self.collectionDescription.isHidden = false
 
                    }
                } else {
                    self.collectionDescription.isHidden = true
                }
            }
        }
    }

}
