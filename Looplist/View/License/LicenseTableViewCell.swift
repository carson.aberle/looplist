//
//  LicenseTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 8/12/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

open class LicenseTableViewCell:UITableViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var projectTitle: UILabel!
    @IBOutlet weak var licenseBody: UITextView!
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////

    override open func awakeFromNib() {
    }
}
