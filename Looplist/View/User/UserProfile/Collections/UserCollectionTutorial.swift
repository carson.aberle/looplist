//
//  UserCollectionTutorial.swift
//  Looplist
//
//  Created by Roman Wendelboe on 1/30/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class UserCollectionTutorial: UIView {

    @IBOutlet weak var bottomDiscriptionLabel: UILabel!
    
    class func instanceFromNib() -> UserCollectionTutorial {
        return UINib(nibName: "UserProfileCollectionsTutorial", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UserCollectionTutorial
    }
    
    override func awakeFromNib() {
        self.bottomDiscriptionLabel.text = "USER_COLLECTION_TUTORIAL_TOP".localized
    }
}
