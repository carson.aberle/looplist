//
//  UserProfileCollectionsViewCell.swift
//  Looplist
//
//  Created by Roman Wendelboe on 8/18/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class UserProfileCollectionsViewCell: UICollectionViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var collectionPlaceHolderImageView1: UIImageView!
    @IBOutlet weak var collectionPlaceHolderImageView2: UIImageView!
    @IBOutlet weak var collectionPlaceHolderImageView3: UIImageView!
    @IBOutlet weak var collectionPlaceHolderImageView4: UIImageView!
    @IBOutlet weak var collectionNameLabel: UILabel!
    @IBOutlet weak var numberOfCollectionItemsLabel: UILabel!
    @IBOutlet weak var privateCollectionImage: UIImageView!
}
