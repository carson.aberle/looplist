//
//  UserProfileCollectionWithTwoItemTableViewCell.swift
//  Looplist
//
//  Created by Roman Wendelboe on 12/19/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class UserProfileCollectionWithTwoItemTableViewCell: UITableViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var imageViewLeft: UIImageView!
    @IBOutlet weak var imageViewRight: UIImageView!
    @IBOutlet weak var collectionNameLabel: UILabel!
    @IBOutlet weak var collectionOwnerLabel: UILabel!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var ownerInfoLabel: UILabel!
    @IBOutlet weak var aCollectionByLabel: UILabel!
    
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////
    
    var collection:Collection?{
        didSet{
            if let collection = collection{
                //                if let value = collection.isPrivate, value == true{
                //                    cell.privateCollectionImage.isHidden = false
                //                }else{
                //                    cell.privateCollectionImage.isHidden = true
                //                }
                if let name = collection.name{
                    collectionNameLabel?.text = name
                }
                
                if let owner = collection.owner?.userName{
                    collectionOwnerLabel?.text = "@\(owner)"
                }
                
                if let products = collection.products{
                    if products.count >= 1 && (products[0].images?.count)! > 0{
                        if let img1 = products[0].images?[0], img1 != "",let nsurl = URL(string:(img1)){
                            imageViewLeft.kf.setImage(with: nsurl, placeholder: UIImage(named: Constants.placeholder), options: nil, progressBlock: nil, completionHandler: nil)
                        } else {
                            imageViewLeft.image = UIImage(named: Constants.collectionsPlaceholderImage)
                        }
                    }  else {
                        imageViewLeft.image = UIImage(named: Constants.collectionsPlaceholderImage)
                    }
                    
                    if products.count >= 2 && (products[1].images?.count)! > 0{
                        if let img1 = products[1].images?[0], img1 != "",let nsurl = URL(string:(img1)){
                            imageViewRight.kf.setImage(with: nsurl, placeholder: UIImage(named: Constants.placeholder), options: nil, progressBlock: nil, completionHandler: nil)
                        } else {
                            imageViewRight.image = UIImage(named: Constants.collectionsPlaceholderImage)
                        }
                    }  else {
                        imageViewRight.image = UIImage(named: Constants.collectionsPlaceholderImage)
                    }
                }
            }
        }
    }

    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.aCollectionByLabel.text = "A_COLLECTION_BY".localized

        self.infoView.layer.shadowColor = UIColor.black.cgColor
        self.infoView.layer.shadowOpacity = 0.5
        self.infoView.layer.shadowOffset = CGSize.zero
        self.infoView.layer.shadowRadius = 4
        self.infoView.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
        self.infoView.layer.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
