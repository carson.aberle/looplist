//
//  UserProfileCollectionWithMultipleItemTableViewCell.swift
//  Looplist
//
//  Created by Roman Wendelboe on 12/19/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class UserProfileCollectionWithMultipleItemTableViewCell: UITableViewCell {

    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var imageView3: UIImageView!
    @IBOutlet weak var imageView4: UIImageView!
    @IBOutlet weak var imageView5: UIImageView!
    @IBOutlet weak var imageView6: UIImageView!
    @IBOutlet weak var imageView7: UIImageView!
    @IBOutlet weak var imageView8: UIImageView!
    
    @IBOutlet weak var collectionNameLabel: UILabel!
    @IBOutlet weak var collectionOwnerLabel: UILabel!
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var aCollectionByLabel: UILabel!
    
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////
    
    var collection:Collection?{
        didSet{
            if let collection = collection{
                //                if let value = collection.isPrivate, value == true{
                //                    cell.privateCollectionImage.isHidden = false
                //                }else{
                //                    cell.privateCollectionImage.isHidden = true
                //                }
                if let name = collection.name{
                    collectionNameLabel?.text = name
                }
                
                if let owner = collection.owner?.userName{
                    collectionOwnerLabel?.text = "@\(owner)"
                }
                
                if let products = collection.products{
                    if products.count >= 1 && (products[0].images?.count)! > 0{
                        if let img1 = products[0].images?[0], img1 != "",let nsurl = URL(string:(img1)){
                            imageView1.kf.setImage(with: nsurl, placeholder: UIImage(named: Constants.placeholder), options: nil, progressBlock: nil, completionHandler: nil)
                        } else {
                            imageView1.image = UIImage(named: Constants.collectionsPlaceholderImage)
                        }
                    }  else {
                        imageView1.image = UIImage(named: Constants.collectionsPlaceholderImage)
                    }
                    
                    if products.count >= 2 && (products[1].images?.count)! > 0{
                        if let img1 = products[1].images?[0], img1 != "",let nsurl = URL(string:(img1)){
                            imageView2.kf.setImage(with: nsurl, placeholder: UIImage(named: Constants.placeholder), options: nil, progressBlock: nil, completionHandler: nil)
                        } else {
                            imageView2.image = UIImage(named: Constants.collectionsPlaceholderImage)
                        }
                    }  else {
                        imageView2.image = UIImage(named: Constants.collectionsPlaceholderImage)
                    }
                    
                    if products.count >= 3 && (products[2].images?.count)! > 0{
                        if let img1 = products[2].images?[0], img1 != "",let nsurl = URL(string:(img1)){
                            imageView3.kf.setImage(with: nsurl, placeholder: UIImage(named: Constants.placeholder), options: nil, progressBlock: nil, completionHandler: nil)
                        } else {
                            imageView3.image = UIImage(named: Constants.collectionsPlaceholderImage)
                        }
                    }  else {
                        imageView3.image = UIImage(named: Constants.collectionsPlaceholderImage)
                    }
                    
                    if products.count >= 4 && (products[3].images?.count)! > 0{
                        if let img1 = products[3].images?[0], img1 != "",let nsurl = URL(string:(img1)){
                            imageView4.kf.setImage(with: nsurl, placeholder: UIImage(named: Constants.placeholder), options: nil, progressBlock: nil, completionHandler: nil)
                        } else {
                            imageView4.image = UIImage(named: Constants.collectionsPlaceholderImage)
                        }
                    }  else {
                        imageView4.image = UIImage(named: Constants.collectionsPlaceholderImage)
                    }
                    
                    if products.count >= 5 && (products[4].images?.count)! > 0{
                        if let img1 = products[4].images?[0], img1 != "",let nsurl = URL(string:(img1)){
                            imageView5.kf.setImage(with: nsurl, placeholder: UIImage(named: Constants.placeholder), options: nil, progressBlock: nil, completionHandler: nil)
                        } else {
                            imageView5.image = UIImage(named: Constants.collectionsPlaceholderImage)
                        }
                    }  else {
                        imageView5.image = UIImage(named: Constants.collectionsPlaceholderImage)
                    }
                    
                    if products.count >= 6 && (products[5].images?.count)! > 0{
                        if let img1 = products[5].images?[0], img1 != "",let nsurl = URL(string:(img1)){
                            imageView6.kf.setImage(with: nsurl, placeholder: UIImage(named: Constants.placeholder), options: nil, progressBlock: nil, completionHandler: nil)
                        } else {
                            imageView6.image = UIImage(named: Constants.collectionsPlaceholderImage)
                        }
                    }  else {
                        imageView6.image = UIImage(named: Constants.collectionsPlaceholderImage)
                    }
                    
                    if products.count >= 7 && (products[6].images?.count)! > 0{
                        if let img1 = products[6].images?[0], img1 != "",let nsurl = URL(string:(img1)){
                            imageView7.kf.setImage(with: nsurl, placeholder: UIImage(named: Constants.placeholder), options: nil, progressBlock: nil, completionHandler: nil)
                        } else {
                            imageView7.image = UIImage(named: Constants.collectionsPlaceholderImage)
                        }
                    }  else {
                        imageView7.image = UIImage(named: Constants.collectionsPlaceholderImage)
                    }
                    
                    if products.count >= 8 && (products[7].images?.count)! > 0{
                        if let img1 = products[7].images?[0], img1 != "",let nsurl = URL(string:(img1)){
                            imageView8.kf.setImage(with: nsurl, placeholder: UIImage(named: Constants.placeholder), options: nil, progressBlock: nil, completionHandler: nil)
                        } else {
                            imageView8.image = UIImage(named: Constants.collectionsPlaceholderImage)
                        }
                    }  else {
                        imageView8.image = UIImage(named: Constants.collectionsPlaceholderImage)
                    }
                }
            }
        }
    }

    // MARK: -  //////////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////////////
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.aCollectionByLabel.text = "A_COLLECTION_BY".localized
        self.infoView.layer.shadowColor = UIColor.black.cgColor
        self.infoView.layer.shadowOpacity = 0.5
        self.infoView.layer.shadowOffset = CGSize.zero
        self.infoView.layer.shadowRadius = 4
        self.infoView.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
        self.infoView.layer.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
