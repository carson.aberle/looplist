//
//  UserProfileHeaderView.swift
//  Looplist
//
//  Created by Roman Wendelboe on 7/19/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class UserProfileHeaderView: UIView{
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    @IBOutlet weak var userProfileImageView: UIImageView!

    @IBOutlet weak var editProfileUserButton: UIButton!
    
    @IBOutlet weak var userBioLabel: UILabel!
    @IBOutlet weak var userLocationLabel: UILabel!

    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var followingCountLabel: UILabel!
    @IBOutlet weak var followersCountLabel: UILabel!
    
    @IBOutlet weak var openFollowers: UIButton!
    @IBOutlet weak var openFollowing: UIButton!
    var headerHeight: CGFloat = 0.0

    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////

    weak var user:User?{
        didSet{
            if let user = user{
                
                // USER COUNTS
                
                if let userNameLabel = userNameLabel{
                    userNameLabel.text = user.fullName
                }
                
                if let followingCountLabel = followingCountLabel,let followersCountLabel = followersCountLabel{
                    let followersCount = user.getFollowersCount()
                    followersCountLabel.text =  "\(followersCount)"
                    
                    let followingCount = user.getFollowingCount()
                    followingCountLabel.text  = "\(followingCount)"
                }
                
                if let userBioLabel = userBioLabel,let quote = user.bio{
                    userBioLabel.text = quote
                }
                
                if let userLocationLabel = userLocationLabel,let location = user.location{
                    userLocationLabel.text = location
                }
                
                // USER BUTTON TITLE
                
                ObjectStateHelper.getStateForUserProfileHeaderButton(user: user, button: editProfileUserButton)

                // USER PROFILE IMAGE
                ObjectStateHelper.getUserProfileImage(user: user, imageView: userProfileImageView)
                
                // ALPHA FOR FOLLOWING && FOLLOWERS
                if user.isProfilePrivate && !user.isCurrentUser() && (user.followRequestStatus == .request(true) || user.followRequestStatus == .request(false)) {
                    //User cannot see followers/following
                    followersLabel.alpha = 0.5
                    followersCountLabel.alpha = 0.5
                    followingLabel.alpha = 0.5
                    followingCountLabel.alpha = 0.5
                    openFollowers.isUserInteractionEnabled = false
                    openFollowing.isUserInteractionEnabled = false
                    
                } else {
                    followersLabel.alpha = 1
                    followersCountLabel.alpha = 1
                    followingLabel.alpha = 1
                    followingCountLabel.alpha = 1
                    openFollowers.isUserInteractionEnabled = true
                    openFollowing.isUserInteractionEnabled = true
                }
                
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    override func awakeFromNib() {
        self.userNameLabel?.adjustsFontSizeToFitWidth = true
        self.userLocationLabel?.adjustsFontSizeToFitWidth = true
        
        self.editProfileUserButton?.layer.borderColor = UIColor.darkGray.cgColor
        self.editProfileUserButton?.layer.borderWidth = 1
        self.editProfileUserButton?.layer.cornerRadius = 5
        
        self.followersLabel.text = "FOLLOWERS_CAPITALIZED".localized
        self.followingLabel.text = "FOLLOWING_CAPITALIZED".localized
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.userBioLabel.preferredMaxLayoutWidth = self.userBioLabel.bounds.width
    }
}
