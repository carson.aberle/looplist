//
//  UserSegmentedControlSectionHeaderView.swift
//  Looplist
//
//  Created by Roman Wendelboe on 12/18/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

// MARK: -  //////////////////////////////////////////////////////   PROTOCOL  /////////////////////////////////////////////////////////////////////////

protocol SectionHeaderSegmentedControlDelegate: class {
    func segmentedControl(selectedSegment:Int)
}

class UserSegmentedControlSectionHeaderView: UITableViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var collectionsButton: UIButton!
    @IBOutlet weak var likesButton: UIButton!
    @IBOutlet weak var activityButton: UIButton!
    @IBOutlet weak var selectorView1: UIView!
    @IBOutlet weak var selectorViewMiddle: UIView!
    @IBOutlet weak var selectorView2: UIView!
    
    // MARK: -  //////////////////////////////////////////////////////   VARIABLES   /////////////////////////////////////////////////////////////////////////
    
    weak var delegate:SectionHeaderSegmentedControlDelegate?
    
    // MARK: -  //////////////////////////////////////////////////////   ACTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func leftSegmentClicked(_ sender: UIButton) {
        let xPosition:CGFloat = collectionsButton.frame.origin.x
        let newPosition:CGPoint = CGPoint(x: xPosition, y: selectorView2!.frame.origin.y)
        let newPositionMiddle:CGPoint = CGPoint(x: xPosition, y: selectorViewMiddle!.frame.origin.y)
        
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.selectorView2.frame.origin = newPosition
            self.selectorViewMiddle.frame.origin = newPositionMiddle
        }, completion: {(true) in
            self.delegate?.segmentedControl(selectedSegment: sender.tag)
        })
    }
    @IBAction func middleSegmentClicked(sender: UIButton) {
        let xPosition:CGFloat = likesButton.frame.origin.x
        let newPositionOne:CGPoint = CGPoint(x: xPosition, y: selectorView1!.frame.origin.y)
        let newPositionTwo:CGPoint = CGPoint(x: xPosition, y: selectorView2!.frame.origin.y)

        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.selectorView1.frame.origin = newPositionOne
            self.selectorView2.frame.origin = newPositionTwo

        }, completion: {(true) in
            self.delegate?.segmentedControl(selectedSegment: sender.tag)
        })
    }
    
    @IBAction func rightSegmentClicked(_ sender: UIButton) {
        let xPosition:CGFloat = activityButton.frame.origin.x
        let newPositionOne:CGPoint = CGPoint(x: xPosition, y: selectorView1!.frame.origin.y)
        let newPositionMiddle:CGPoint = CGPoint(x: xPosition, y: selectorViewMiddle!.frame.origin.y)

        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.selectorView1.frame.origin = newPositionOne
            self.selectorViewMiddle.frame.origin = newPositionMiddle
        }, completion: {(true) in
            self.delegate?.segmentedControl(selectedSegment: sender.tag)
        })
    }
    
    // MARK: -  //////////////////////////////////////////////////////   VARIBALES   /////////////////////////////////////////////////////////////////////////
    
    var user:User?{
        didSet{
            if let user = user{
                
                collectionsButton.setTitle("COLLECTIONS".localized,for: .normal)
                collectionsButton.setTitle("COLLECTIONS".localized,for: .highlighted)
//                
//                if let collectionsCount = user.collectionsCount{
//                    if collectionsCount > 1 {
//                        collectionsButton.setTitle("\(collectionsCount) " + "COLLECTIONS".localized,for: .normal)
//                        collectionsButton.setTitle("\(collectionsCount) " + "COLLECTIONS".localized,for: .highlighted)
//                    }else if collectionsCount == 1{
//                        collectionsButton.setTitle("\(collectionsCount) " + "COLLECTION".localized,for: .normal)
//                        collectionsButton.setTitle("\(collectionsCount) " + "COLLECTION".localized,for: .highlighted)
//                    }
//                }
                
                likesButton.setTitle("LIKES".localized,for: .normal)
                likesButton.setTitle("LIKES".localized,for: .highlighted)
                
                activityButton.setTitle("ACTIVITY".localized,for: .normal)
                activityButton.setTitle("ACTIVITY".localized,for: .highlighted)
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    func selectLeftSegment(){
        self.selectorView1.alpha = 1
        self.selectorView2.alpha = 0
        self.selectorViewMiddle.alpha = 0
    }
    
    func selectMiddleSegment(){
        self.selectorView1.alpha = 0
        self.selectorView2.alpha = 0
        self.selectorViewMiddle.alpha = 1
    }
    
    func selectRightSegment(){
        self.selectorView1.alpha = 0
        self.selectorView2.alpha = 1
        self.selectorViewMiddle.alpha = 0
    }
    
    override func awakeFromNib() {
        // set up segmented controll
        self.collectionsButton.tag = 0
        self.likesButton.tag = 1
        self.activityButton.tag = 2
    }
}
