//
//  UserProfileLikesCellCollectionViewCell.swift
//  alphalooplist
//
//  Created by Arun on 2/23/16.
//  Copyright © 2016 Kade France. All rights reserved.
//

import UIKit

class UserProfileLikesCollectionViewCell: UICollectionViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var productImage: UIImageView!


    
    // MARK: -  //////////////////////////////////////////////////////   VIEW FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    override func awakeFromNib() {
        super.awakeFromNib()
        let width = UIScreen.main.bounds.width
        productImage.frame = CGRect(x: 0,y: 0,width: width / 2 , height: width / 2)
    }
}
