//
//  UserProfileLikesTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 1/25/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class UserProfileLikesTableViewCell: UITableViewCell {

    var userProfileVC:UserProfileViewController?
    var productOne:Product?
    var productTwo:Product?

    @IBOutlet weak var imageOne: UIImageView!
    @IBOutlet weak var imageTwo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func imageOneClicked(_ sender: UIButton) {
        if let product = self.productOne{
            if let userProfileVC = self.userProfileVC{
                let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
                productVC.product = product
                userProfileVC.navigationController?.pushViewController(productVC, animated: true)
            }
        }
    }
    @IBAction func imageTwoClicked(_ sender: UIButton) {
        if let product = self.productTwo{
            if let userProfileVC = self.userProfileVC{
                let productVC:ProductTableViewController = ProductTableViewController.instanceFromStoryboard("Product")
                productVC.product = product
                userProfileVC.navigationController?.pushViewController(productVC, animated: true)
            }
        }
    }
}
