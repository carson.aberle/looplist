//
//  AddFriendTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 12/13/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class LoadingCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.label.text = "LOADING_PLACEHOLDER".localized
    }


}
