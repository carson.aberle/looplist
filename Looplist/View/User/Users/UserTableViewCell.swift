//
//  AddFriendTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 12/13/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    
    
    var user:User?{
        didSet{
            if let user = user, let userImageView = userImageView, let userFullNameLabel = userFullNameLabel, let userNameLabel = userNameLabel  {
                
                userImageView.image = nil
                userFullNameLabel.text = nil
                userNameLabel.text = nil
                
                if let userName = user.userName{
                    userNameLabel.text = "@" + userName
                }
                
                if let fullName = user.fullName{
                    userFullNameLabel.text = fullName
                }
                
                
                //IMAGE
                
                ObjectStateHelper.getUserProfileImage(user: user, imageView: userImageView)
                
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
