//
//  FindFriendsTitleTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 12/13/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class FindFriendsTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
