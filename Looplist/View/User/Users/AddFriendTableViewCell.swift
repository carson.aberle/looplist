//
//  AddFriendTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 12/13/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class AddFriendTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    
    
    var user:User?{
        didSet{
            if let user = user, let userImageView = userImageView, let followButton = followButton, let userFullNameLabel = userFullNameLabel, let userNameLabel = userNameLabel  {
                
                userImageView.image = nil
                userFullNameLabel.text = nil
                userNameLabel.text = nil
                followButton.setTitle("", for: .normal)
                
                if let userName = user.userName{
                    userNameLabel.text = "@" + userName
                }
                
                if let fullName = user.fullName{
                    userFullNameLabel.text = fullName
                }
                
                
                //IMAGE
                
                    ObjectStateHelper.getUserProfileImage(user: user, imageView: userImageView)
                
                // FOLLOW BUTTON

                    ObjectStateHelper.getStateForFollowerFollowingButton(user: user, button: followButton)
            }
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.followButton.layer.borderWidth = 1
        self.followButton.layer.cornerRadius = 5
        self.followButton.layer.borderColor = ConstantsColor.looplistColor.cgColor
        
        self.followButton.isHidden = true
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
    @IBAction func follow(_ sender: UIButton) {
        
        ObjectStateHelper.userActionForFollowerFollowingButton(user: user, button: followButton, grey:false)
        
        

    }
}
