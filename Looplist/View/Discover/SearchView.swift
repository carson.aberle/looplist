//
//  SearchView.swift
//  Looplist
//
//  Created by Arun Sivakumar on 11/6/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit


//@IBDesignable
//protocol SearchOverLayActionsProtocol:class {
    //    func resignKeyboard()
//    func overLayMovedToTop()
//    func overLayMovedToBottom()
    
//    func showSearchOverlay()
//    
//    func moveCollectionViewBottom()
    //    func dismissSearchOverlay()
//}

//protocol SearchOverLayActionsProtocol:class {
//    
//    func userChangedSwitch()
//
//}


class SearchView: UIView  {
    
//    enum Switch{
//        case On
//        case Off
//    }
    
//    @IBOutlet weak var containerView: UIView!
    
//    var collision = false
    
//    @IBOutlet weak var switchButton: UIButton!
//    @IBOutlet weak var filterView: UIView!
//    @IBOutlet weak var buttonsStack: UIStackView!
//    weak var delegate:SearchOverLayActionsProtocol?
    
    
//    var animator:UIDynamicAnimator!
//    var gravity:UIGravityBehavior!
//    var container:UICollisionBehavior!
//    
//    var dynamicItem:UIDynamicItemBehavior!
//    var snap:UISnapBehavior!
//    
//    var fakeView = UIView()
//    
//    var panGestureRecognizer:UIPanGestureRecognizer!
    
//    @IBOutlet weak var switch1View: UIView!
//    @IBOutlet weak var switch1Button: UIButton!
//    
//    @IBOutlet weak var switch2View: UIView!
//    @IBOutlet weak var switch2Button: UIButton!
//    
//    
//    @IBOutlet weak var likesButton: UIButton!
//    @IBOutlet weak var relevanceButton: UIButton!
//    @IBOutlet weak var minPriceButton: UIButton!
//    @IBOutlet weak var maxPriceButton: UIButton!
    
//        var switch1Status = Switch.Off
//        var switch2Status = Switch.Off
//    


    
    override func awakeFromNib() {
//        
//        switch1View.layer.cornerRadius = switch1View.frame.height / 2.0
//        switch1Button.layer.cornerRadius = switch1Button.frame.height / 2.0
//        
//        switch2View.layer.cornerRadius = switch2View.frame.height / 2.0
//        switch2Button.layer.cornerRadius = switch2Button.frame.height / 2.0
//        
//        likesButton.addTarget(self, action: #selector(self.changeProductTypePreference), forControlEvents: .TouchUpInside)
//        relevanceButton.addTarget(self, action: #selector(self.changeProductTypePreference), forControlEvents: .TouchUpInside)
//        switch1Button.addTarget(self, action: #selector(self.changeProductTypePreference), forControlEvents: .TouchUpInside)
//        
//        minPriceButton.addTarget(self, action: #selector(self.changeProductPricePreference), forControlEvents: .TouchUpInside)
//        maxPriceButton.addTarget(self, action: #selector(self.changeProductPricePreference), forControlEvents: .TouchUpInside)
//        switch2Button.addTarget(self, action: #selector(self.changeProductPricePreference), forControlEvents: .TouchUpInside)
  
    }
    
//    func setup () {
//        
//        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(SearchView.handlePan(_:)))
//        panGestureRecognizer.cancelsTouchesInView = false
//        self.addGestureRecognizer(panGestureRecognizer)
//        
//        animator = UIDynamicAnimator(referenceView: self.superview!)
//        dynamicItem = UIDynamicItemBehavior(items: [self])
//        
//        
//        dynamicItem.allowsRotation = false
//        dynamicItem.elasticity = 0
//        
//        gravity = UIGravityBehavior(items: [self])
//        gravity.gravityDirection = CGVectorMake(0, -1)
//        
//        container = UICollisionBehavior(items: [self])
//        container.collisionDelegate = self
//        
//        configureContainer()
//        
//        animator.addBehavior(gravity)
//        animator.addBehavior(dynamicItem)
//        animator.addBehavior(container)
//        
//    }
    
//    func configureContainer(){
//        let boundaryWidth = UIScreen.mainScreen().bounds.size.width
//        let boundaryHeight = UIScreen.mainScreen().bounds.size.height
//        
//        container.addBoundaryWithIdentifier("upper", fromPoint: CGPointMake(0, -self.frame.size.height + 20), toPoint: CGPointMake(boundaryWidth, -self.frame.size.height + 20))
//        container.addBoundaryWithIdentifier("bottom", fromPoint: CGPointMake(0, boundaryHeight - 58.5), toPoint: CGPointMake(boundaryWidth, boundaryHeight - 58.5))
//        
//    }
    
//    func changeProductTypePreference(sender: UIButton) {
//        
////        delegate?.userChangedSwitch()
//        
//        if switch1Button.frame.x == 0.0{
//            self.likesButton.titleLabel?.textColor = Constants.looplistGreyColor
//            UIView.animateWithDuration(0.3, animations: { () -> Void in // on
//                self.switch1Button.frame.x = self.switch1View.frame.width - self.switch1Button.frame.width
//               
//                
//                 },completion: { (true) in
//                    self.switch1Status = .On
//                    self.handleChangeProductTypePreference()
//                
//            })
//        }else{
//            self.relevanceButton.titleLabel?.textColor = Constants.looplistGreyColor
//            
//            UIView.animateWithDuration(0.3, animations: { () -> Void in // off
//                 self.switch1Button.frame.x = self.switch1Button.frame.x - (self.switch1View.frame.width - self.switch1Button.frame.width)
//                
//                },completion: { (true) in
//                    self.switch1Status = .Off
//                    self.handleChangeProductTypePreference()
//                })
//            
//        }
//        
//    }
    
    
    
//    func changeProductPricePreference(sender: UIButton) {
//        
////        delegate?.userChangedSwitch()
//        
//        if switch2Button.frame.x == 0.0{
//            self.minPriceButton.titleLabel?.textColor = Constants.looplistGreyColor
//            UIView.animateWithDuration(0.3, animations: { () -> Void in // on
//                self.switch2Button.frame.x = self.switch2View.frame.width - self.switch2Button.frame.width
//                
//                },completion: { (true) in
//                self.switch2Status = .On
//               self.handlechangeProductPricePreference()
//                
//            })
//        }else{
//             self.maxPriceButton.titleLabel?.textColor = Constants.looplistGreyColor
//            
//            UIView.animateWithDuration(0.3, animations: { () -> Void in //off
//                self.switch2Button.frame.x = self.switch2Button.frame.x - (self.switch2View.frame.width - self.switch2Button.frame.width)
//                },completion: { (true) in
//                self.switch2Status = .Off
//                self.handlechangeProductPricePreference()
//            })
//            
//        }
//        
//    }
//    
//    
//    func handleChangeProductTypePreference(){
//        switch switch1Status{
//        case .On:
////            switch1Status = .Off
//            
//            self.relevanceButton.titleLabel?.textColor = UIColor.whiteColor()
//        case .Off:
////            switch1Status = .On
//            self.likesButton.titleLabel?.textColor = UIColor.whiteColor()
//            
//
//        }
//    }
//    
//    func handlechangeProductPricePreference(){
//        switch switch2Status{
//        case .On:
//            switch2Status = .Off
//            
//            self.maxPriceButton.titleLabel?.textColor = UIColor.whiteColor()
//            
//        case .Off:
//            switch2Status = .On
//            self.minPriceButton.titleLabel?.textColor = UIColor.whiteColor()
//           
//
//        }
//    }
    
    
//    func handlePan (pan:UIPanGestureRecognizer){
//        
//        let velocity = pan.velocityInView(self)
//        
//        if pan.state == .Ended{
//            
//            if velocity.y > 0{
//                
//                if self.frame.bottom >= 60.0{
//                    container.removeBoundaryWithIdentifier("barrier")
//                    delegate?.showSearchOverlay()
//                    
//                }else if self.frame.bottom < 50.0{
//                    container.addBoundaryWithIdentifier("barrier", forPath: UIBezierPath(rect: fakeView.frame))
////                    delegate?.moveCollectionViewBottom()
//                    snapToBottom()
//                    
//                }
//                
//            }else if velocity.y < 0{
//                snapToTop()
//            }
//            
//        }
//        
//    }
//    
//    func snapToBottom() {
//        
//        gravity.gravityDirection = CGVectorMake(0, 5.5)
//        
//    }
//    
//    func snapToTop(){
//        gravity.gravityDirection = CGVectorMake(0, -10.5)
//    }
//    
//    func collisionBehavior(behavior: UICollisionBehavior, endedContactForItem item: UIDynamicItem, withBoundaryIdentifier identifier: NSCopying?) {
//        if identifier as! String == "upper"{
//            delegate?.overLayMovedToTop()
//            
//        }else if identifier as! String == "bottom"{
//            delegate?.overLayMovedToBottom()
//            
//        }
//        
//        
//    }
    
//    func collisionBehavior(behavior: UICollisionBehavior, beganContactForItem item: UIDynamicItem, withBoundaryIdentifier identifier: NSCopying?, atPoint p: CGPoint) {
//        if identifier as! String == "upper"{
//            delegate?.overLayMovedToTop()
//            
//        }else if identifier as! String == "bottom"{
//            delegate?.overLayMovedToBottom()
//            
//        }
//    }
    
    

    
    //    func collisionBehavior(behavior: UICollisionBehavior, endedContactForItem item1: UIDynamicItem, withItem item2: UIDynamicItem) {
    //
    //
    //        if item2.description == "upper"{
    //            delegate?.overLayMovedToTop()
    //            collision = true
    //        }else if item2.description  == "bottom"{
    //            delegate?.overLayMovedToBottom()
    //            collision = true
    //        }
    //        collision = false
    //        
    //    }
    
}

   
