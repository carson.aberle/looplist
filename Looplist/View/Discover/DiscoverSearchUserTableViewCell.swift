//
//  DiscoverSearchUserTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 10/7/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class DiscoverSearchUserTableViewCell:UITableViewCell{

    @IBOutlet weak var userFollowButton: UIButton!
    @IBOutlet weak var userLocationLabel: UILabel!
    @IBOutlet weak var userFullName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func followAction(_ sender: UIButton) {
    }
}
