//
//  DiscoverCollectionViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 10/6/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
//fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
//  switch (lhs, rhs) {
//  case let (l?, r?):
//    return l < r
//  case (nil, _?):
//    return true
//  default:
//    return false
//  }
//}
//
//// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
//// Consider refactoring the code to use the non-optional operators.
//fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
//  switch (lhs, rhs) {
//  case let (l?, r?):
//    return l > r
//  default:
//    return rhs < lhs
//  }
//}


class DiscoverCollectionViewCell:UICollectionViewCell{

    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    var startTime:Date?
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productImageOverlay: UIView!
    
    @IBOutlet weak var productNameLabel: UILabel!
//    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var priceRangeLabel: UILabel!
//    @IBOutlet weak var detailsButton: UIButton!
    
    
    var product:Product?{
        didSet{
            if let product = product{
                
                if let productNameLabel = self.productNameLabel{
                    productNameLabel.text = product.brandName
                }
                
//                if let productNameLabel = self.productNameLabel, let brand = product.brand{
//                    
//                    if let name = product.name{
//                        productNameLabel.text = ""
//                        productNameLabel.text = brand + ": " + name
//                    }else{
//                        productNameLabel.text = brand
//                    }
////                    productNameLabel.text = "Hello this is a test message Hello this is a test message Hello this is a test message Hello this is a test message Hello this is a test message Hello this is a test message Hello this is a test message Hello this is a test message"
//                }else{
//                    productNameLabel.text = ""
//                }
//
//                if let productDescription = product.descriptionAttributedString{
//                    productDescriptionLabel.text = productDescription.string.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
////                    .stringByReplacingOccurrencesOfString("\n", withString: " ")
//                    //productDescriptionLabel.textAlignment = .Justified
//                }
//                if let priceRange = product.priceRange{
                    priceRangeLabel.text = product.priceRange
//                }else{
//                    priceRangeLabel.text = ""
//                }
                if let count = product.images?.count, count > 0{
                    if let featuredImage = product.images?[0]{
                        if let nsurl = URL(string:(featuredImage)){
                            productImage.kf.setImage(with: nsurl, placeholder: UIImage(named: Constants.placeholder))
                        }
                    }else{
                        productImage.image = UIImage(named: Constants.collectionsPlaceholderImage)
                    }
                }
            }
        }
    }
    
    var showProductDetails:Bool?{
        didSet{
            if let showProductDetails = showProductDetails{
                if showProductDetails{
                    showProductOverlay(true)
                }else{
                    showProductOverlay(false)
                }
            }
        }
    }

    // MARK: -  //////////////////////////////////////////////////////   VIEW FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    override func awakeFromNib() {
        super.awakeFromNib()
//        let width = UIScreen.mainScreen().bounds.width
//        productImage.frame = CGRectMake(0,0,width / 2 , width / 2)
        showProductOverlay(true)
        
//        detailsButton.layer.cornerRadius = detailsButton.frame.height / 2.0
//        detailsButton.layer.borderWidth = 1.0
//        detailsButton.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    
    fileprivate func showProductOverlay(_ show:Bool){
        if show{
            productImageOverlay.isHidden = false
            productNameLabel.isHidden = false
//            productDescriptionLabel.hidden = false
            priceRangeLabel.isHidden = false
//            detailsButton.hidden = false
    
        }else{
            productImageOverlay.isHidden = true
            productNameLabel.isHidden = true
//            productDescriptionLabel.hidden = true
            priceRangeLabel.isHidden = true
//            detailsButton.hidden = true
            
        }
    }
}
