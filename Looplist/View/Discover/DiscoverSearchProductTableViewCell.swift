//
//  DiscoverSearchProductTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 10/7/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class DiscoverSearchProductTableViewCell:UITableViewCell{
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productIdentifier: UILabel!
    @IBOutlet weak var productPriceRange: UILabel!
    @IBOutlet weak var buyNowButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func buyNow(_ sender: UIButton) {
    }
}
