//
//  GoToAddProductCollectionViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 1/25/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class DiscoverGoToAddProductCollectionViewCell: UICollectionReusableView {
    
    
    @IBOutlet weak var goToAddProductLabel: UILabel!
    override func awakeFromNib(){
        self.goToAddProductLabel.text = "GOTO_ADD_PRODUCT".localized
    }
}
