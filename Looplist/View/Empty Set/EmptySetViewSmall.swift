//
//  EmptySetViewSmall.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/19/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class EmptySetViewSmall: UIView {
    
    // MARK: -  /////////////////////////////////////////////////////////////////////////   OUTLETS  /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var emojiLabel: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    class func instanceFromNib() -> EmptySetViewSmall {
        return UINib(nibName: "EmptySetViewSmall", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! EmptySetViewSmall
    }
    
    override func awakeFromNib() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
