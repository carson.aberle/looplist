//
//  EmptySetViewSmall.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/19/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class EmptySetViewSearch: UIView {
    
    // MARK: -  /////////////////////////////////////////////////////////////////////////   OUTLETS  /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var emojiLabel: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    class func instanceFromNib() -> EmptySetViewSearch {
        return UINib(nibName: "EmptySetViewSearch", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! EmptySetViewSearch
    }
    
    override func awakeFromNib() {
        self.discriptionLabel.text = "LOADING_PLACEHOLDER".localized
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
