//
//  EmptySetView.swift
//  Looplist
//
//  Created by Roman Wendelboe on 9/27/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import Lottie

class EmptySetView: UIView {
    
    // MARK: -  /////////////////////////////////////////////////////////////////////////   OUTLETS  /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var emojiLabel: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    @IBOutlet weak var emojiImageView: UIImageView!
    @IBOutlet weak var arrowTopImage: UIImageView!
    
    var shouldShowAnimation = false{
        didSet{
            if self.shouldShowAnimation {
                self.showAnimation(aView: self.emojiImageView)
            }
        }
    }
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////

    class func instanceFromNib() -> EmptySetView {
        return UINib(nibName: "EmptySetView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! EmptySetView
    }
    
    // Function to add animation view to image view
    func showAnimation(aView: UIImageView){
        let animationViewAnimate = LAAnimationView.animationNamed("The Wink")
        animationViewAnimate?.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        aView.addSubview(animationViewAnimate!)
        
        animationViewAnimate?.play(completion: { (finished) in
            // Do Something
        })
    }
    
    override func awakeFromNib() {
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
//    
//    @IBOutlet weak var animationView: UIView!{
//        didSet{
//            
//            let animationViewAnimate = LAAnimationView.animationNamed("The Wink")
//            animationViewAnimate?.frame = animationView.frame
//            animationViewAnimate?.center = animationView.center
//            animationViewAnimate?.clipsToBounds = true
//            //            animationViewAnimate?.frame.midY = animationView.frame.midY
//            
//            animationView.addSubview(animationViewAnimate!)
//            //            animationViewAnimate?.center = self.center
//            
//            animationViewAnimate?.play(completion: { (finished) in
//                // Do Something
//            })
//        }
//    }

}
