//
//  ProductCollectionViewCell.swift
//  Looplist
//
//  Created by Arun Sivakumar on 6/12/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//


import UIKit


class ProducCollectionViewCell: UICollectionViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var listingImage : UIImageView!
    @IBOutlet weak var listingAttributesLabel: UILabel!
    @IBOutlet weak var listingLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
}
