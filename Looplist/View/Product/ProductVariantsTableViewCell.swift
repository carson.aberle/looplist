//
//  ProductVariantsTableViewCell.swift
//  Looplist
//
//  Created by Roman Wendelboe on 1/2/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class ProductVariantsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var variantsCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.variantsCollectionView.clipsToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
