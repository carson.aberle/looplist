//
//  ProductPendingTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 11/18/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import PassKit
import Stripe


class ProductPendingTableViewCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var buyinfoTextView: UITextView!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var applePayButton: UIButton!{
        didSet{
            var btnApplePay: PKPaymentButton
            
            if PKPaymentAuthorizationViewController.canMakePayments() {
                btnApplePay = PKPaymentButton(type: .buy, style: .black)
                btnApplePay.addTarget(self, action: #selector(ProductPendingBuyTableViewController.buyWithApplePay(_:)), for: .touchUpInside)
                btnApplePay.frame = CGRect(x:0,y: 0, width:applePayButton.frame.width, height:applePayButton.frame.height )
                let bounds = applePayButton.bounds
                btnApplePay.center = CGPoint(x:bounds.midX, y:bounds.midY);

                self.applePayButton.addSubview(btnApplePay)
                 self.applePayButton.bringSubview(toFront: btnApplePay)
            }
        }
    }
    @IBOutlet weak var buyWithCardButton: UIButton!
    @IBOutlet weak var pendingInfoTitle: UILabel!
    @IBOutlet weak var returnPolicyButton: UIButton!
    
    weak var pendingBuyVC:ProductPendingBuyTableViewController?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.buyinfoTextView.layer.cornerRadius = 4
        self.buyinfoTextView.clipsToBounds = true
        
        self.pendingInfoTitle.text = "PENDING_INFO_TITLE".localized
        self.returnPolicyButton.setTitle("RETURN_POLICY_TITLE".localized + " >", for: .normal)
        self.buyinfoTextView.text = "PURCHASE_NOTES_TITLE".localized
        self.buyWithCardButton.setTitle("BUY_WITH_CARD".localized, for: .normal)
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func buyWithCard(_ sender: UIButton) {
        if let pendingBuyVC = self.pendingBuyVC{
            pendingBuyVC.buyWithCreditCard(sender)
        }
    }
    @IBAction func buyWithApplePay(_ sender: UIButton) {
        if let pendingBuyVC = self.pendingBuyVC{
            pendingBuyVC.buyWithApplePay(sender)
        }
    }
    @IBAction func showReturnPolicy(_ sender: UIButton) {
        if let pendingBuyVC = self.pendingBuyVC{
            pendingBuyVC.showReturnPolicy()
        }
    }
}
