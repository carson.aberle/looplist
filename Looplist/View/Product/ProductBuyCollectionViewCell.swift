//
//  ProductBuyCollectionViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 11/6/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import PassKit
import Stripe


class ProductBuyCollectionViewCell: UICollectionViewCell {
    
    // MARK: -   -----------------------------------------------------      VARIABLES   -----------------------------------------------------
    
    var btnApplePay: PKPaymentButton = PKPaymentButton(type: .buy, style: .whiteOutline)
    
    @IBOutlet weak var listingCreditCardButton: UIButton!
    @IBOutlet weak var listingApplePayButton: UIButton!{
        
        didSet{
            
            
            if PKPaymentAuthorizationViewController.canMakePayments() {
                    btnApplePay.frame = CGRect(x:0,y: 0, width:listingApplePayButton.frame.size.width, height: listingApplePayButton.frame.size.height)
                    btnApplePay.center = listingApplePayButton.center
                    
                    let bounds = listingApplePayButton.bounds
                    btnApplePay.center = CGPoint(x:bounds.midX, y:bounds.midY);
                    
                    self.listingApplePayButton.addSubview(btnApplePay)
                    self.listingApplePayButton.bringSubview(toFront: btnApplePay)
                
            } else {
//                listingApplePayButton.removeFromSuperview()
//                setNeedsLayout()
            }
        }
    }
    
    @IBOutlet weak var listingPrice: UILabel!
    @IBOutlet weak var listingLocationText: UILabel!
    @IBOutlet weak var listingLocationImage: UIImageView!
    @IBOutlet weak var listingImage: UIImageView!
    @IBOutlet weak var listingTitle: UILabel!
    @IBOutlet weak var listingMainAttribute: UILabel!
    
    //    let index:Int = listingApplePayButton.layer.value(forKey: "index") as? Int
    //
    //    var index:Int?{
    //        didSet{
    //        }
    //    }
    var listing:Listing?{
        didSet{
            if let listing = listing{
                if let imageURL = listing.images?[0]{
                    listingImage.kf.setImage(with:URL(string: imageURL))
                    listingImage.isUserInteractionEnabled = true
                }
                
                if let price = listing.price{
                    listingPrice.text = price
                    if let listingApplePayButton = listingApplePayButton{
                        listingApplePayButton.isEnabled = true
                    }
                    listingCreditCardButton.isEnabled = true
                    
                }
                
                if let name = listing.product?.brandName{
                    listingTitle.text = name
                }
                
                if let attributes = listing.attributes{
                    listingMainAttribute.text = attributes
                }
                listingLocationText.text = listing.location
            }
        }
    }
    
    var rowNumber: Int?{
        didSet{
            if let row = rowNumber {
                //                self.listingApplePayButton.layer.setValue(row, forKey: "index")
                self.listingCreditCardButton.layer.setValue(row, forKey: "index")
                self.btnApplePay.layer.setValue(row, forKeyPath: "index")
                
            }
        }
    }
    
    // MARK: -   -----------------------------------------------------      FUNCTIONS  -----------------------------------------------------
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.listingCreditCardButton.setTitle("BUY_WITH_CARD".localized, for: .normal)
    }
}
