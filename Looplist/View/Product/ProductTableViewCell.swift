//
//  ProductTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 11/6/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var productPriceRange: UILabel!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var repostButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var variationsActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var userOne: UIImageView!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var userTwo: UIImageView!
    @IBOutlet weak var userThree: UIImageView!
    @IBOutlet weak var userFour: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var userCount: UILabel!
    @IBOutlet weak var likingUsersView: UIStackView!
    @IBOutlet weak var likingUsersButton: UIButton!
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    
    var product:Product?{
        didSet{
            if let product = product{
                self.productTitle.text = product.brandName
                self.productPriceRange.text = product.priceRange

                
                // Add to collection button
                //                if product.currentUsersCollectionsIdsIn.count > 0 {
                //                    if let addBlue = UIImage(named: "Add Blue"){
                //                        cell.addButton.setImage(addBlue, for: UIControlState())
                //                    }
                //                }else{
                //                    if let addGrey = UIImage(named: "Add"){
                //                        cell.addButton.setImage(addGrey, for: UIControlState())
                //                    }
                //                }
//                if let attributedString = product.descriptionAttributedString{
//                    if !attributedString.string.isEmpty && self.descriptionLabel != nil{
//                        self.productDescription.attributedText = product.descriptionAttributedString
//                        self.productDescription.textAlignment = .justified
//                    } else {
//                        if self.descriptionLabel != nil && self.hasLoadedVariations{
//                            self.descriptionLabel.removeFromSuperview()
//                        }
//                    }
//                } else{
//                    if self.hasLoadedVariations && self.descriptionLabel != nil{
//                        self.descriptionLabel.removeFromSuperview()
//                    }
//                }
//                
//                if self.likingUsersFirstFourURLs.count > 4{
//                    let numberFormatter = NumberFormatter()
//                    numberFormatter.numberStyle = NumberFormatter.Style.decimal
//                    self.activityIndicator.stopAnimating()
//                    if let count = product.likesCount, count  > 0{
//                        self.userCount.text = "+ " + String(numberFormatter.string(from: NSNumber(value:(count - self.likingUsersFirstFourURLs.count)))!)
//                    }
//                }
//                
//                if self.likingUsersFirstFourURLs.count > 0 && self.hasLoadedUsers{
//                    self.userOne.isHidden = false
//                    self.userTwo.isHidden = true
//                    self.userThree.isHidden = true
//                    self.userFour.isHidden = true
//                    if let urlOne = self.likingUsersFirstFourURLs[0]{
//                        self.userOne.kf.setImage(with:URL(string:urlOne))
//                    } else {
//                        self.userOne.image = UIImage(named:"Default Male")
//                    }
//                }
//                
//                if self.likingUsersFirstFourURLs.count > 1 && self.hasLoadedUsers{
//                    self.userOne.isHidden = false
//                    self.userTwo.isHidden = false
//                    self.userThree.isHidden = true
//                    self.userFour.isHidden = true
//                    if let urlTwo = self.likingUsersFirstFourURLs[1]{
//                        self.userTwo.kf.setImage(with:URL(string:urlTwo))
//                    } else {
//                        self.userTwo.image = UIImage(named:"Default Male")
//                    }
//                }
//                
//                if self.likingUsersFirstFourURLs.count > 2 && self.hasLoadedUsers{
//                    self.userOne.isHidden = false
//                    self.userTwo.isHidden = false
//                    self.userThree.isHidden = true
//                    self.userFour.isHidden = false
//                    if let urlThree = self.likingUsersFirstFourURLs[2]{
//                        self.userFour.kf.setImage(with:URL(string:urlThree))
//                    } else {
//                        self.userFour.image = UIImage(named:"Default Male")
//                    }
//                }
//                
//                if self.likingUsersFirstFourURLs.count > 3 && self.hasLoadedUsers{
//                    self.userOne.isHidden = false
//                    self.userTwo.isHidden = false
//                    self.userThree.isHidden = false
//                    self.userThree.isHidden = false
//                    if let urlFour = self.likingUsersFirstFourURLs[3]{
//                        self.userThree.kf.setImage(with:URL(string:urlFour))
//                    } else {
//                        self.userThree.image = UIImage(named:"Default Male")
//                    }
//                }
//                
//                if self.likingUsersFirstFourURLs.count == 0 && self.hasLoadedUsers && self.userOne != nil && self.userTwo != nil && self.userThree != nil && self.userFour != nil && self.likingUsersView != nil && self.likingUsersButton != nil{
//                    self.activityIndicator.isHidden = true
//                    self.userOne.isHidden = true
//                    self.userTwo.isHidden = true
//                    self.userThree.isHidden = true
//                    self.userFour.isHidden = true
//                    self.likingUsersView.removeFromSuperview()
//                    self.likingUsersButton.removeFromSuperview()
//                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
