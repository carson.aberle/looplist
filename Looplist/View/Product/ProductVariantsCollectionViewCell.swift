//
//  ProductVariantsCollectionViewCell.swift
//  Looplist
//
//  Created by Roman Wendelboe on 1/2/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class ProductVariantsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var variantsImageView: UIImageView!
    
    var product: Product? {
        didSet{
            if let product = product{
                if let count = product.images?.count, count  > 0{
                    let imageURL = URL(string:product.images![0])!
                    self.variantsImageView.image = nil
                    self.variantsImageView.kf.setImage(with:imageURL)
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.layer.borderWidth = 1.9
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.clipsToBounds = true
        self.layer.cornerRadius = 2.0
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected{
                self.layer.borderColor = ConstantsColor.looplistColor.cgColor
            } else {
                self.layer.borderColor = UIColor.lightGray.cgColor
            }
        }
    }
}
