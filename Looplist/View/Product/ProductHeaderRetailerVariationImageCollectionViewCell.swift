//
//  ProductHeaderRetailerVariationImageCollectionViewCell.swift
//  Looplist
//
//  Created by Roman Wendelboe on 1/20/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class ProductHeaderRetailerVariationImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productHeaderVariationImage: UIImageView!
}
