//
//  ProductHeaderVariationImageCollectionViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 7/18/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class ProductHeaderVariationImageCollectionViewCell: UICollectionViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var productHeaderVariationImage: UIImageView!
}
