//
//  ProductInfoTableViewCell.swift
//  Looplist
//
//  Created by Roman Wendelboe on 1/2/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class ProductInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var repostButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var pricesFromLabel: UILabel!
    
    var product: Product?{
        didSet{
            if let product = product{
                if let productTitle = productTitle {
                    productTitle.text = product.brandName
                }
                
                if let productPrice = productPrice {
                    productPrice.text = product.priceRange
                }
                
                if product.isLikedByCurrentUser == true {
                    likeButton.setImage(UIImage(named: Constants.likedHeartsActive), for: UIControlState())
                } else {

                    likeButton.setImage(UIImage(named: Constants.likedHeartsInActive), for: UIControlState())
                }
                
                if product.isRepostedByCurrentUser == true {
                    repostButton.setImage(UIImage(named: Constants.repostedImage), for: UIControlState())
                } else{
                    repostButton.setImage(UIImage(named: Constants.repostImage), for: UIControlState())
                }
                

                if product.isInUsersCollections == true {
                    if let addBlue = UIImage(named: "Add Blue"){
                        self.addButton.setImage(addBlue, for: UIControlState())
                    }
                }else{
                    if let addGrey = UIImage(named: "Add"){
                        self.addButton.setImage(addGrey, for: UIControlState())
                    }
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.pricesFromLabel.text = "PRICES_FROM".localized
        self.buyButton.setTitle("BUY".localized, for: .normal)
        
        buyButton.layer.cornerRadius = 20
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
