//
//  ProductBuyRetailerReturnPolicyLabelTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 11/11/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class ProductBuyRetailerReturnPolicyLabelTableViewCell: UITableViewCell {

    @IBOutlet weak var returnPolicyLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.returnPolicyLabel.text = "RETURN_POLICY_TITLE".localized
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
