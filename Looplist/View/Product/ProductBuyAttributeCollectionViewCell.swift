//
//  ProductBuyAttributeCollectionViewCell.swift
//  Looplist
//
//  Created by Roman Wendelboe on 1/19/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class ProductBuyAttributeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var attributeTitleLabel: UILabel!
    
    var title: String? {
        didSet{
            if let title = title{
                self.attributeTitleLabel.text = title
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.attributeTitleLabel.textColor = UIColor.darkGray
        self.layer.borderColor = UIColor.darkGray.cgColor

        self.layer.cornerRadius = 17
        self.layer.borderWidth = 1
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected{
                self.layer.borderColor = ConstantsColor.looplistColor.cgColor
                self.attributeTitleLabel.textColor = ConstantsColor.looplistColor
            } else {
                self.attributeTitleLabel.textColor = UIColor.darkGray
                self.layer.borderColor = UIColor.darkGray.cgColor
            }
        }
    }
}
