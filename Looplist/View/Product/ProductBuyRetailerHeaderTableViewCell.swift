//
//  ProductBuyRetailerHeaderTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 11/11/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class ProductBuyRetailerHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var creditCardButton: UIButton!
    @IBOutlet weak var applePayButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var listingNameLabel: UILabel!
    @IBOutlet weak var imagesPageControl: UIPageControl!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var listingDescriptionLabel: UILabel!
    var productBuyRetailerVC:ProductBuyRetailerTableViewController!
    @IBOutlet weak var productAttributesText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.creditCardButton.layer.borderColor = UIColor.darkGray.cgColor
        self.creditCardButton.setTitle("CREDIT_CARD".localized, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func buyWithApplePay(_ sender: UIButton) {
        productBuyRetailerVC.buyWithApplePay(sender)
    }
    @IBAction func buyWithCreditCard(_ sender: UIButton) {
        productBuyRetailerVC.buyWithCreditCard(sender)
    }
}
