//
//  ProductUserLikingsTableViewCell.swift
//  Looplist
//
//  Created by Roman Wendelboe on 1/2/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class ProductUserLikingsTableViewCell: UITableViewCell {

    @IBOutlet weak var userCount: UILabel!
    
    @IBOutlet weak var likingUsersButton: UIButton!
    @IBOutlet weak var userOne: UIImageView!
    @IBOutlet weak var userTwo: UIImageView!
    @IBOutlet weak var userThree: UIImageView!
    @IBOutlet weak var userFour: UIImageView!
    
    var count: Int!
    
    var likingUsersFirstFourURLs:[String?] = [String?](){
        didSet{
            if likingUsersFirstFourURLs.count > 4{
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = NumberFormatter.Style.decimal
                if let count = self.count, count  > 0{
                    self.userCount.text = "+ " + String(numberFormatter.string(from: NSNumber(value: count - 4))!)
                }
            }
            
            if self.likingUsersFirstFourURLs.count > 0{
                self.userOne.isHidden = false
                self.userTwo.isHidden = true
                self.userThree.isHidden = true
                self.userFour.isHidden = true
                if let urlOne = self.likingUsersFirstFourURLs[0]{
                    self.userOne.kf.setImage(with:URL(string:urlOne))
                } else {
                    self.userOne.image = UIImage(named:"Default Male")
                }
            }

            if self.likingUsersFirstFourURLs.count > 1{
                self.userOne.isHidden = false
                self.userTwo.isHidden = false
                self.userThree.isHidden = true
                self.userFour.isHidden = true
                if let urlTwo = self.likingUsersFirstFourURLs[1]{
                    self.userTwo.kf.setImage(with:URL(string:urlTwo))
                } else {
                    self.userTwo.image = UIImage(named:"Default Male")
                }
            }

            if self.likingUsersFirstFourURLs.count > 2{
                self.userOne.isHidden = false
                self.userTwo.isHidden = false
                self.userThree.isHidden = true
                self.userFour.isHidden = false
                if let urlThree = self.likingUsersFirstFourURLs[2]{
                    self.userFour.kf.setImage(with:URL(string:urlThree))
                } else {
                    self.userFour.image = UIImage(named:"Default Male")
                }
            }

            if self.likingUsersFirstFourURLs.count > 3{
                self.userOne.isHidden = false
                self.userTwo.isHidden = false
                self.userThree.isHidden = false
                self.userThree.isHidden = false
                if let urlFour = self.likingUsersFirstFourURLs[3]{
                    self.userThree.kf.setImage(with:URL(string:urlFour))
                } else {
                    self.userThree.image = UIImage(named:"Default Male")
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.userOne?.isHidden = true
        self.userTwo?.isHidden = true
        self.userThree?.isHidden = true
        self.userFour?.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
