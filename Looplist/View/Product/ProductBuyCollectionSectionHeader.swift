//
//  ProductBuyCollectionSectionHeader.swift
//  Looplist
//
//  Created by Roman Wendelboe on 1/16/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class ProductBuyCollectionSectionHeader: UICollectionReusableView {
    
    @IBOutlet weak var sectionNameLabel: UILabel!
    @IBOutlet weak var sectionSubtitleLabel: UILabel!
    @IBOutlet weak var sectionImageView: UIImageView!
    @IBOutlet weak var seperator: UIView!
    
    var numberOfOffers: Int? {
        didSet{
            if let count = numberOfOffers{
                self.sectionNameLabel.text =  "OFFERS_CHOICE".localized + "\(count)"
                self.sectionSubtitleLabel?.text = "BASED_ON_SELECTIONS_ABOVE".localized
                self.sectionSubtitleLabel?.isHidden = false
            }
        }
    }
    
    var optionTitle: String? {
        didSet{
            if let title = optionTitle{
                self.sectionNameLabel.text = "SELECT_SPACE".localized + "\(title.capitalizingFirstLetter())"
            }
        }
    }
    
    var subtitleText: String? {
        didSet{
            if let text = subtitleText {
                if text == "" {
                    self.sectionSubtitleLabel.isHidden = true
                } else {
                    self.sectionSubtitleLabel.isHidden = false
                }

                self.sectionSubtitleLabel.text = text
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
