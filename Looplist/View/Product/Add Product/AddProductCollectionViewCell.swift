//
//  AddProductCollectionViewCell.swift
//  Looplist
//
//  Created by Arun Sivakumar on 10/26/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class AddProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productImageView: UIImageView!
    
    var productImageUrl:String?{
        
     
        didSet{
             if let productImageView = productImageView, let productImageUrl = productImageUrl,let url = URL(string: productImageUrl){
                productImageView.layer.borderColor = UIColor.clear.cgColor
                productImageView.kf.setImage(with: url, placeholder: UIImage(named: Constants.placeholder))
             }
            
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.productImageView.layer.borderWidth = 3.0
        
        self.productImageView.layer.cornerRadius = 3.0
        self.productImageView.clipsToBounds = true
        
        self.productImageView.layer.shadowColor = UIColor.white.cgColor
        self.productImageView.layer.shadowRadius = 10.0
        self.productImageView.layer.shadowOpacity = 0.5
        self.productImageView.layer.shadowOffset = CGSize.zero
    }
}
