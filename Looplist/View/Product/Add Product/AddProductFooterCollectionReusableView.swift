//
//  AddProductFooterCollectionReusableView.swift
//  Looplist
//
//  Created by Arun Sivakumar on 10/26/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class AddProductFooterCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var finishButton: UIButton!
    
    @IBOutlet weak var infoLabel: UILabel!{
        didSet{
            infoLabel.text = "INFO_IMAGE".localized
        }
    }
    
}
