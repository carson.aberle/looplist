//
//  AddProductOnboardingView.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/10/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

protocol dismissOnBoardingProtocol:class{
    func dismissOnBoarding()
}

class AddProductOnboardingView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var arrowImage1: UIImageView!
    @IBOutlet weak var arrowImage2: UIImageView!
    @IBOutlet weak var arrowImage3: UIImageView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var gotItButton: UIButton!
    
    weak var delegate:dismissOnBoardingProtocol?
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    @IBAction func dismissOnboarding(_ sender: UIButton) {
        
        delegate?.dismissOnBoarding()
    }
    class func instanceFromNib() -> AddProductOnboardingView? {
        if let view = UINib(nibName: "AddProductOnboardingView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? AddProductOnboardingView{
        return view
        }else {return nil}
    }
    
    func bringToScreen(){
        button.alpha = 1.0
        arrowImage1.alpha = 1.0
        arrowImage2.alpha = 1.0
        arrowImage3.alpha = 1.0
        topLabel.alpha = 1.0
        bottomLabel.alpha = 1.0
    }
    
    func removeFromScreen(){
        button.alpha = 0.0
        arrowImage1.alpha = 0.0
        arrowImage2.alpha = 0.0
        arrowImage3.alpha = 0.0
        topLabel.alpha = 0.0
        bottomLabel.alpha = 0.0
    }
    
    override func awakeFromNib() {
        button.layer.cornerRadius = button.frame.height / 2.0
        removeFromScreen()
        
        self.topLabel.text = "ADD_PRODUCT_TUTORIAL_TOP".localized
        self.bottomLabel.text = "ADD_PRODUCT_TUTORIAL_BOTTOM".localized
        self.gotItButton.setTitle("GOT_IT".localized, for: .normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
