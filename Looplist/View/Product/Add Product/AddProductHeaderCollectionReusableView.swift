//
//  AddProductHeaderCollectionReusableView.swift
//  Looplist
//
//  Created by Arun Sivakumar on 10/26/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class AddProductHeaderCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var selectProductLabel: UILabel!
    
    override func awakeFromNib() {
        self.selectProductLabel.text = "SELECT_AN_IMAGE".localized
    }
}
