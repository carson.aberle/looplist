//
//  ProductDiscriptionTableViewCell.swift
//  Looplist
//
//  Created by Roman Wendelboe on 1/2/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class ProductDescriptionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var descriptionTitleLabel: UILabel!
    
    var pDescription: NSAttributedString? {
        didSet{
            if let description = pDescription{
                self.productDescription.attributedText = description
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.descriptionTitleLabel.text = "DESCRIPTION".localized
        self.productDescription.textColor = ConstantsColor.looplistGreyColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
