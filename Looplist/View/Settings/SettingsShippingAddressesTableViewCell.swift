//
//  SettingsShippingAddressesTableViewCell.swift
//  Looplist
//
//  Created by Roman Wendelboe on 9/13/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class SettingsShippingAddressesTableViewCell: UITableViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var address1Label: UILabel!
    @IBOutlet weak var address2Label: UILabel!
    @IBOutlet weak var cityStateZipLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////

    var shipping:Shipping?{
        didSet{
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
