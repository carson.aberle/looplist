//
//  SettingsExternalAccountsTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 11/28/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class SettingsExternalAccountsTableViewCell: UITableViewCell {

    @IBOutlet weak var accountNameLabel: UILabel!
    @IBOutlet weak var accountImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
