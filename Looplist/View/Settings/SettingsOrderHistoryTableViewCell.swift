//
//  SettingsOrderHistoryTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 9/6/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class SettingsOrderHistoryTableViewCell:UITableViewCell{
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productIdentifierLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var identifierTItleLabel: UILabel!
    @IBOutlet weak var dateTitleLabel: UILabel!
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////

    
    var transaction:Transaction?{
        
        didSet{
            productImageView.image = nil
            productNameLabel.text = nil
            productPriceLabel.text = nil
            productIdentifierLabel.text = nil
            dateLabel.text = nil
            
            self.dateTitleLabel.text = "DATE_TITLE".localized
            self.identifierTItleLabel.text = "IDENTIFIER_TITLE".localized
            
            if let transaction = transaction{
                
//                if let productPriceLabel = productPriceLabel{
                
                    if let total = transaction.payment?.total{ //### going to be an integer
                        productPriceLabel.text = total
                    }
//                }
                
//                if let dateLabel = dateLabel{
                    if let createdAt = transaction.createdAt{
                        dateLabel.text = HelperDate.getTimeInString(createdAt)
                    }
//                }
//                if let productNameLabel = productNameLabel{
                    if let productName = transaction.listing?.brandName{
                        productNameLabel.text = productName
                    }
//                }
                
//                if let productImageView = productImageView{
                    if let count = (transaction.listing?.images?.count), count > 0 {
                        if let image = transaction.listing?.images![0], let nsurl = URL(string:(image)){
                            productImageView.kf.setImage(with:nsurl, placeholder: UIImage(named: Constants.placeholder), options: nil, progressBlock: nil, completionHandler: nil)
                        }
                    }
//                }
//                if let productIdentifierLabel = productIdentifierLabel{
                    if let attributes = transaction.listing?.attributes {
                        productIdentifierLabel.text = attributes
                    }
//                }
                
            }
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
