

//
//  SettingsShippingAddressesTableViewCell.swift
//  Looplist
//
//  Created by Roman Wendelboe on 9/13/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class ShippingAddressesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var address1Label: UILabel!
    @IBOutlet weak var address2Label: UILabel!
    @IBOutlet weak var cityStateZipLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    @IBOutlet weak var primaryButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    var shipping:Shipping?{
        didSet{
            if let shipping = shipping{

                 if let name = shipping.name, let address1 =  shipping.address1, let address2 = shipping.address2, let phone = shipping.phone{
                    fullNameLabel.text! = name
                    
                    if address2 != ""{
                        address1Label.text! = address1 + ", "+address2
                    }else{
                       address1Label.text! = address1
                    }
                    
                    if let cityStateZip = shipping.cityStateZip{
                        cityStateZipLabel.text! = cityStateZip
                    }
                    phoneNumberLabel.text! = phone
                }
                
                if let defaultShippingAddress = CurrentUser.sharedInstance.currentUser?.defaultShippingAddressObjectId, let objectId = shipping.objectId, objectId == defaultShippingAddress{
                    primaryButton.backgroundColor = ConstantsColor.looplistColor
                    primaryButton.setTitleColor(UIColor.white, for: UIControlState())
                    primaryButton.setTitle("PRIMARY".localized, for: UIControlState())
                    primaryButton.isUserInteractionEnabled = true
                }else{
                    primaryButton.backgroundColor = ConstantsColor.shippingMakeDefaultButtonColor
                    primaryButton.setTitleColor(ConstantsColor.shippingMakeDefaultButtonTextColor, for: UIControlState())
                    primaryButton.setTitle("MAKE_PRIMARY".localized, for: UIControlState())
                    primaryButton.isUserInteractionEnabled = true
                }
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.editButton.setTitle("EDIT".localized, for: .normal)
        primaryButton.layer.cornerRadius = ConstantsObjects.defaultButtonCornerRadius
    }
}
