//
//  ShippingAddAddressTableViewCell.swift
//  Looplist
//
//  Created by Arun Sivakumar on 10/4/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class ShippingAddAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var addAddressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.addAddressLabel.text = "ADD_NEW".localized
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
