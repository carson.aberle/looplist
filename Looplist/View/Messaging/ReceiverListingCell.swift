//
//  ReceiverListingCell.swift
//  Looplist
//
//  Created by Arun Sivakumar on 9/6/16.
//  Copyright © 2016 Looplist. All rights reserved.
//


import UIKit

class ReceiverListingCell: BaseCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var backgroundOverlayView: UIView!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var listingBackgroundImageView: UIImageView!
//    @IBOutlet weak var listingVisualBlurView: UIVisualEffectView!
    @IBOutlet weak var listingImageView: UIImageView!
    @IBOutlet weak var listingNameLabel: UILabel!
    @IBOutlet weak var listingAttributesLabel: UILabel!
    @IBOutlet weak var listingPriceLabel: UILabel!
    @IBOutlet weak var rightArrowImageView: UIImageView!
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////

    var listing:Listing?
        {
        didSet{
            if let listing = listing, let brandName = listing.brandName, let listingImage = listing.images?[0], let price = listing.price,let listingImageUrl = URL(string:(listingImage)){
                self.listingNameLabel.text = brandName
                self.listingPriceLabel.text = price
                self.listingImageView.kf.setImage(with:(listingImageUrl))

                
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        profilePicture.image = UIImage(named: Constants.defaultMaleImage)
        self.backgroundOverlayView.layer.cornerRadius = 5.0
        self.listingImageView.layer.masksToBounds = true
        self.profilePicture.layer.cornerRadius = self.profilePicture.frame.width / 2
        self.profilePicture.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
