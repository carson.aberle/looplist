//
//  MessagingInboxMavenTableViewCell.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/18/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class MessagingInboxMavenTableViewCell: UITableViewCell {

    
    // MARK: -  --------------------------------------------------------   OUTLETS  -----------------------------------------------------------------------
    
    @IBOutlet weak var userProfilePictureImage: UIImageView!
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var userLastMessageLabel: UILabel!
    @IBOutlet weak var messageDateLabel: UILabel!
    @IBOutlet weak var rightArrowLabel: UIImageView!
    
    @IBOutlet weak var border: UIView!
    
    @IBOutlet weak var readUnreadView: UIView!
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    var message:MessageConversation?{
        didSet{
            // user exists / Maven
            
            // Maven
            if let image = UIImage(named: ConstantPlaceHolders.messagingReceiverMavenImage){
                userProfilePictureImage.image = image
            }
            userFullNameLabel.text = ConstantPlaceHolders.maven
            
            if let userLastMessage = message?.userLastMessage{
                userLastMessageLabel.text = userLastMessage
            }
            if let date = message?.date{
                messageDateLabel.text = HelperDate.getTimeStringInbox((date))
                
            }
            if message?.read == false{
                 readUnreadView.alpha = 1.0
            }else{
                readUnreadView.alpha = 0.0


            }
        }
    }
    
    var fallBackMavenMessage:Bool?{
        didSet{
            if let fallBackMavenMessage = fallBackMavenMessage,fallBackMavenMessage == true {
                if let image = UIImage(named: ConstantPlaceHolders.messagingReceiverMavenImage){
                    userProfilePictureImage.image = image
                }
                userFullNameLabel.text = ConstantPlaceHolders.maven
                readUnreadView.alpha = 0.0
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userProfilePictureImage.layer.cornerRadius = self.userProfilePictureImage.layer.frame.width / 2
        self.userProfilePictureImage.clipsToBounds = true
        readUnreadView.layer.cornerRadius = self.readUnreadView.layer.frame.width / 2
        readUnreadView.layer.borderWidth = 1.0
        readUnreadView.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor

    }

}
