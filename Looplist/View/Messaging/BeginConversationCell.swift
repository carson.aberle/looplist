//
//
//  SenderTextCell.swift
//  MessageUI
//
//  Created by Arun on 4/27/16.
//  Copyright © 2016 Arun. All rights reserved.
//

import UIKit

class BeginConversationCell: BaseCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var textMessageLabel: UILabel!
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    var message:Message?{
        didSet{
            if let headerMessage = message?.headerMessage{
                textMessageLabel.text = headerMessage
            }else{
                textMessageLabel.text = " "
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textMessageLabel.sizeToFit()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
