//
//
//  SenderTextCell.swift
//  MessageUI
//
//  Created by Arun on 4/27/16.
//  Copyright © 2016 Arun. All rights reserved.
//

import UIKit

class SenderTextDateCell: BaseCell {

    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var textMessageLabel: UILabel!
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         textMessageLabel.sizeToFit()
        
       // self.view.layer.shadowPath = UIBezierPath(rect: self.view.bounds).cgPath
        //self.view.layer.shadowColor = UIColor.red.cgColor
        //self.view.layer.shadowRadius = 5.0
        //self.view.layer.shadowOpacity = 1.0
        //self.view.layer.masksToBounds = false
        //self.view.layer.shadowOffset = CGSize(width: 0, height: 8)

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       self.textMessageLabel.layer.cornerRadius = 3.0
        self.textMessageLabel.clipsToBounds = true
        // Configure the view for the selected state
        
     
    }
}
