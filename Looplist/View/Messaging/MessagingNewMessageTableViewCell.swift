//
//  MessagingAddUserTableViewCell.swift
//  Looplist
//
//  Created by Arun Sivakumar on 8/15/16.
//  Copyright © 2016 Looplist. All rights reserved.
//


import UIKit

class MessagingNewMessageTableViewCell: UITableViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var userProfilePictureImage: UIImageView!{
        didSet{
            if let userProfilePictureImage = userProfilePictureImage{
                userProfilePictureImage.clipsToBounds = true
            }
        }
    }
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!

    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    var user:User?{
        didSet{
            // user exists / Maven
            if let user = user{
                        ObjectStateHelper.getUserProfileImage(user: user, imageView: userProfilePictureImage)
                
                // set user image
                /*
                if  let profileImageUrl = user.profileImageUrl,let url = URL(string: profileImageUrl){
                    
                    userProfilePictureImage.kf.setImage(with:(url), completionHandler:{ (image, error, cacheType, imageURL) -> () in
                        if let image = image{
                            self.user?.profileImage = image
                            self.userProfilePictureImage.clipsToBounds = true
                        }
                    })
                    
                }else if let gender = user.gender, gender == "Female"{
                    
                    if let image = UIImage(named: Constants.defaultFemaleImage){
                        self.user?.profileImage = image
                    }
                    // default
                }else{
                    
                    if let image = UIImage(named: Constants.defaultMaleImage){
                        userProfilePictureImage.image = image
                        self.user?.profileImage = image
                    }
                    
                }
                 */
                
                // set user full name
                if let fullName = user.fullName {
                    userFullNameLabel.text = fullName
                }
                
                // set username
                if let userName = user.userName{
                    userNameLabel.text = "@" + userName
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userProfilePictureImage.layoutIfNeeded()
        self.userProfilePictureImage.layer.cornerRadius = self.userProfilePictureImage.layer.frame.width / 2
        self.userProfilePictureImage.clipsToBounds = true
    }
}
