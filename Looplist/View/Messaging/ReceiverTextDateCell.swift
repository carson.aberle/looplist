//
//
//  ReceiverTextCell.swift
//  MessageUI
//
//  Created by Arun on 4/27/16.
//  Copyright © 2016 Arun. All rights reserved.
//

import UIKit

class ReceiverTextDateCell: BaseCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var textMessageLabel: UILabel!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var profilePictureButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profilePicture.image = UIImage(named: Constants.defaultMaleImage)
        self.profilePicture.layer.cornerRadius = self.profilePicture.frame.width / 2
        self.profilePicture.clipsToBounds = true

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.textMessageLabel.layer.cornerRadius = 3.0
        self.textMessageLabel.clipsToBounds = true
        // Configure the view for the selected state
    }
}
