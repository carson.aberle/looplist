//
//  product.swiftReceiverProductCell
//  Looplist
//
//  Created by Arun Sivakumar on 8/29/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class ReceiverProductCell: BaseCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var rightArrowImageView: UIImageView!
    @IBOutlet weak var backgroundOverlayView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var profilePictureButton: UIButton!
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    var product:Product?
        {
        didSet{
            productImageView.image = nil
            if let product = product{
                if let productImage = product.images?[0],let productImageUrl = URL(string:(productImage)){
                    self.productNameLabel.text = product.brandName
                    self.productImageView.kf.setImage(with:(productImageUrl))
                }
                
//                if let priceRange = product.priceRange{
                    priceLabel.text = product.priceRange
//                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profilePicture.image = UIImage(named: Constants.defaultMaleImage)
        self.backgroundOverlayView.layer.cornerRadius = 5.0
        self.productImageView.layer.masksToBounds = true
        
        self.profilePicture.layer.cornerRadius = self.profilePicture.frame.width / 2
        self.profilePicture.clipsToBounds = true
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        
    }
}
