//
//  MessagingInboxTableViewCell.swift
//  Looplist
//
//  Created by Arun Sivakumar on 8/12/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class MessagingInboxTableViewCell: UITableViewCell {
    
    // MARK: -  --------------------------------------------------------   OUTLETS  -----------------------------------------------------------------------

    @IBOutlet weak var userProfilePictureImage: UIImageView!
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var userLastMessageLabel: UILabel!
    @IBOutlet weak var messageDateLabel: UILabel!
    @IBOutlet weak var rightArrowImage: UIImageView!
    
    @IBOutlet weak var readUnreadView: UIView!

    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    var message:MessageConversation?{
        didSet{
            // user exists / Maven
            
            if let user = message?.user{
                // set user image
                ObjectStateHelper.getUserProfileImage(user: user, imageView: userProfilePictureImage)
                if let fullName = user.fullName{
                    userFullNameLabel.text = fullName
                }
                
            }else{
                 userProfilePictureImage = nil
                 userFullNameLabel.text = ""
            }
            if let userLastMessage = message?.userLastMessage{
                userLastMessageLabel.text = userLastMessage
            }else{
                 userLastMessageLabel.text = ""
            }
            if let date = message?.date{
                messageDateLabel.text = HelperDate.getTimeStringInbox((date))
            }else{
                 messageDateLabel.text =  ""
            }
            
            if message?.read == false{
                readUnreadView.alpha = 1.0
            }else{
                readUnreadView.alpha = 0.0
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userProfilePictureImage.layer.cornerRadius = self.userProfilePictureImage.layer.frame.width / 2
        self.userProfilePictureImage.clipsToBounds = true
        readUnreadView.layer.cornerRadius = self.readUnreadView.layer.frame.width / 2
        readUnreadView.layer.borderWidth = 1.0
        readUnreadView.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
        
    }
}
