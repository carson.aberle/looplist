//
//  CreditCardTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 10/3/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class CCAddTableViewCell:UITableViewCell{
    
    @IBOutlet weak var addAddressLabel: UILabel!
    override func awakeFromNib(){
        self.addAddressLabel.text = "ADD_NEW".localized
    }
}
