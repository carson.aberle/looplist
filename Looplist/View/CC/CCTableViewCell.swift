//
//  CreditCardTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 10/3/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class CCTableViewCell:UITableViewCell{
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var last4Label: UILabel!
    
    @IBOutlet weak var primaryButton: UIButton!

    
    var cc:CC?{
        
        didSet{
            if let cc = cc{
                
                if let name = cc.name{
                     self.brandLabel.text = name
                }else if let brand = cc.brand{
                    self.brandLabel.text = brand
                }
                
                
                if let last4 = cc.last4{
                     self.last4Label.text =  "●●●● " + last4
                }
   
                if let defaultPaymentMethod = CurrentUser.sharedInstance.currentUser?.defaultPaymentMethodObjectId, let objectId = cc.objectId, objectId == defaultPaymentMethod{
                    primaryButton.backgroundColor = ConstantsColor.looplistColor
                    primaryButton.setTitleColor(UIColor.white, for: UIControlState())
                    primaryButton.setTitle("PRIMARY".localized, for: UIControlState())
                    primaryButton.isUserInteractionEnabled = true
                }else{
                    primaryButton.backgroundColor = ConstantsColor.shippingMakeDefaultButtonColor
                    primaryButton.setTitleColor(ConstantsColor.shippingMakeDefaultButtonTextColor, for: UIControlState())
                    primaryButton.setTitle("MAKE_PRIMARY".localized, for: UIControlState())
                    primaryButton.isUserInteractionEnabled = true
                }
            }
        
        }
    
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        primaryButton.layer.cornerRadius = ConstantsObjects.defaultButtonCornerRadius
    }
  
}
