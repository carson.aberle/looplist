//
//  TutorialCollectionViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 7/21/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class TutorialCollectionViewCell: UICollectionViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var phoneScreen: UIImageView!
//    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var pageDescription: UILabel!
    @IBOutlet weak var pageTitle: UILabel!
    
}
