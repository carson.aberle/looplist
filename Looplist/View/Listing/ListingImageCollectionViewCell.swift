//
//  ListingImageCollectionViewCell.swift
//  Looplist
//
//  Created by Arun on 5/27/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import UIKit

class ListingImageCollectionViewCell: UICollectionViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var listingImage : UIImageView!
}
