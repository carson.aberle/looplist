//
//  AddCuratorTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 12/29/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

class AddCuratorTableViewCell: UITableViewCell {
    
    var user:User?
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var interestsText: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var imageOne: UIImageView!
    @IBOutlet weak var imageTwo: UIImageView!
    @IBOutlet weak var imageThree: UIImageView!
    @IBOutlet weak var imageFour: UIImageView!
    @IBOutlet weak var imageFive: UIImageView!
    @IBOutlet weak var imageSix: UIImageView!
    @IBOutlet weak var imageSeven: UIImageView!
    @IBOutlet weak var imageEight: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.followButton.layer.borderWidth = 1
        self.followButton.layer.cornerRadius = 5
        self.followButton.layer.borderColor = ConstantsColor.looplistColor.cgColor
        
    }
    @IBAction func follow(_ sender: UIButton) {
        if let user = self.user{
            ObjectStateHelper.userActionForFollowerFollowingButton(user: user, button: followButton, grey:false)
        }
    }
}
