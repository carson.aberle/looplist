//
//  OnBoardCollectionViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 6/16/16.
//  Copyright © 2016 Looplist Inc. All rights reserved.
//

import UIKit

class OnBoardCollectionViewCell : UICollectionViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////

    @IBOutlet weak var onBoardLabel: UILabel!
}
