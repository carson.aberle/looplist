//
//  OnBoardTitleTextTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 12/28/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class OnBoardTitleTextTableViewCell : UICollectionReusableView {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var amountToPickText: UILabel!
    @IBOutlet weak var selectInterestsTitleText: UILabel!
    
    
}
