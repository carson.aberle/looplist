//
//  RepostPopupCollectionViewController.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/13/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

protocol TransformPopUpProtocol:class{
    func showMessageUI()
    func hideMessagingUI()
}

class RepostPopupNewMessageCollectionViewController: UICollectionViewController,UICollectionViewDelegateFlowLayout,UITextFieldDelegate {
    
    // MARK: -  -----------------------------------------------------   PUBLIC API VARIABLES   -----------------------------------------------------

    weak var delegate:TransformPopUpProtocol?
    weak var repostPopUpView:UIView?
    
    var selectedUser:User?{
        didSet{
            if let _ = selectedUser{
                self.delegate?.showMessageUI()
            }else{
                self.delegate?.hideMessagingUI()
            }
        }
    }
    var searchTerm:String?{
        didSet{
            if let searchTerm = searchTerm{
                currentPage = 0
                loadUserFollowers(searchTerm: searchTerm)
            }
        }
    }
    
    var selectedCell:RepostPopupSendNewMessageCollectionViewCell?{
        didSet{
            if let oldValue = oldValue{ // clear the color
                oldValue.stateOfSelectedCell(isSelected: false)
            }
        }
        willSet{
            if let newValue = newValue{
                newValue.stateOfSelectedCell(isSelected: true)
            }
        }
        
    }
    
    // MARK: -  -----------------------------------------------------   PRIVATE VARIABLES   -----------------------------------------------------

    private var currentPage = 0
    private let limitNumber = 25
    private var isEndOfPage = false
    
    private let reuseIdentifier = "RepostPopupSendNewMessageCollectionViewCell"
    
    private var keyboardAppeared = false
    
    private var users:[User] = [User](){
        didSet{
            self.hideEmptyDataSet()
            self.collectionView?.reloadData()
        }
    }
    
    private var keyboardNotificationHandler: KeyboardNotificationHandler?

    // MARK: -  ////////////////////////////////////////////////   APPLICATION LIFE CYCLE   /////////////////////////////////////////////////////////////////


    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib
//        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
////        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
//        layout.itemSize = CGSize(width: self.view.frame.width / 4.5, height: self.view.frame.height )
//        self.collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
//        collectionView!.register(RepostPopupSendNewMessageCollectionViewCell(), forCellWithReuseIdentifier: reuseIdentifier)
//
//        collectionView!.collectionViewLayout = layout

        
//        initializeEmptySet()
        loadUserFollowers(searchTerm: nil)
        registerKeyboardHandler()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        keyboardNotificationHandler = nil
    }
    
    // MARK: -  //////////////////////////////////////////////////////   CONFIGURATION   /////////////////////////////////////////////////////////////////////////

    
    func registerKeyboardHandler(){
        keyboardNotificationHandler = KeyboardNotificationHandler()
        
        keyboardNotificationHandler!.keyboardWillBeHiddenHandler = { (height: CGFloat) in
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                let down = CGAffineTransform(translationX: 0, y: -ConstantsObjects.REPOST_POPUP_TRANSLATION_HEIGHT)
                self.repostPopUpView?.transform = down
                self.view.layoutIfNeeded()
                
                self.keyboardAppeared = false
            })
        }
        
        keyboardNotificationHandler!.keyboardWillBeShownHandler = { (height: CGFloat) in
            if !self.keyboardAppeared{
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    let up = CGAffineTransform(translationX: 0, y: -ConstantsObjects.REPOST_POPUP_TRANSLATION_HEIGHT-82)
                    self.repostPopUpView?.transform = up
                    self.view.layoutIfNeeded()
                    self.keyboardAppeared = true
                })
            }
        }
    }
    
    
    private func showEmptyDataSet(data:EmptyDataSet){ // Lazy Init
        
        switch data{
        case .Loading:
            break
        case .Search:
            emptySetView?.discriptionLabel.text = "NO_USERS_FOUND".localized
            self.collectionView?.backgroundView = emptySetView
        case .Empty:
            emptySetView?.discriptionLabel.text = "NO_FOLLOWERS_YET".localized
            self.collectionView?.backgroundView = emptySetView
            
        }
        self.collectionView?.backgroundView?.isHidden = false
    }

    
    func hideEmptyDataSet(){
        self.collectionView?.backgroundView?.isHidden = true

    }
    
    var emptySetView: EmptySetViewSmall? = EmptySetViewSmall.instanceFromNib()
    
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////

    
    private func loadUserFollowers(searchTerm:String?){
        if let user = CurrentUser.sharedInstance.currentUser{
            APIHelperAnyUser.getUserFollowers(false,user:user,searchFollower:searchTerm, skip: currentPage, limit: limitNumber) {[weak vc = self] (users) in
                if let users = users,searchTerm == vc?.searchTerm{
                    if let limitNumber = vc?.limitNumber, users.count < limitNumber{
                        self.isEndOfPage = true // stop pagination
                    }else{
                        self.isEndOfPage = false
                    }
                    
                    if vc?.currentPage == 0{
                        
                        vc?.users = users // initial load
                        
                        if users.count == 0 { // configure empty data set
                            searchTerm == "" ? vc?.showEmptyDataSet(data: .Empty) : vc?.showEmptyDataSet(data: .Search)
                        }else{
                            vc?.hideEmptyDataSet()
                        }
                    }else{
                        vc?.users += users // pagination
                        
                    }

                }
            }
        }
    }
    

    // MARK: -  //////////////////////////////////////////////////////////////////////// Collection View DATA SOURCE & DELEGATE ////////////////////////////////////////////////////////////////////////


    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.users.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if indexPath.row == currentPage - 1 && !isEndOfPage{
            self.loadUserFollowers(searchTerm: self.searchTerm)
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! RepostPopupSendNewMessageCollectionViewCell
        cell.user = self.users[indexPath.row]
        
        if cell.user?.objectId == selectedUser?.objectId{
            cell.stateOfSelectedCell(isSelected: true)
        }else{
            cell.stateOfSelectedCell(isSelected: false)
        }
        return cell
        
    }
    

    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width:self.view.frame.width / 4.5, height:self.view.frame.height)
    
    }
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let cell = collectionView.cellForItem(at: indexPath) as? RepostPopupSendNewMessageCollectionViewCell{
            
            let user = self.users[indexPath.row]
            selectedCell = cell
            selectedUser = user
        }
        
    }

}
