 //
 //  RepostPopupView.swift
 //  Looplist
 //
 //  Created by Carson Aberle on 12/8/16.
 //  Copyright © 2016 Looplist. All rights reserved.
 //
 
 import UIKit
 import FacebookCore
 import FacebookLogin
 import TwitterKit
 import FacebookShare
 class RepostPopupView: UIView,UITextFieldDelegate,TransformPopUpProtocol {
    
    weak var homeFeedVC:HomeFeedViewController?
    weak var productVC:ProductTableViewController?
    weak var product:Product?
    var keyboardHeight:CGFloat = 224
    
    @IBOutlet weak var messageTextField: SearchTextField!{
        didSet{
            messageTextField.delegate = self
            messageTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        }
    }
    @IBOutlet weak var sendAsMessageButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var copyLinkButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var copyLinkLabel: UILabel!
    @IBOutlet weak var moreLabel: UILabel!
    @IBOutlet weak var sendAsMessageLabel: UILabel!
    @IBOutlet weak var searchTextField: SearchTextField!{
        didSet{
            searchTextField.delegate = self
            searchTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        }
    }
    @IBOutlet weak var searchView: UIView!{
        didSet{
            configureCollectionView()
        }
    }
    
    @IBOutlet weak var sendButton: UIButton!
    private var newMessageVC = RepostPopupNewMessageCollectionViewController()
    
    @IBOutlet weak var stackViewTextMessage: UIStackView!{
        didSet{
            stackViewTextMessage.isHidden = true
        }
    }
    
    
    func showTextMessage(){
        stackViewTextMessage.isHidden = false
    }
    
    func setKeyboardHeight(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        let textField = UITextField()
        self.addSubview(textField)
        textField.becomeFirstResponder()
        textField.resignFirstResponder()
        textField.removeFromSuperview()
    }
    
    override func awakeFromNib() {
        self.setKeyboardHeight()
        
        self.moreLabel.text = "MORE".localized + "..."
        self.copyLinkLabel.text = "COPY_LINK".localized
        self.shareLabel.text = "SHARE".localized
        self.sendAsMessageLabel.text = "SEND_AS_MESSAGE_TITLE".localized
        self.searchTextField.placeholder = "SEARCH_FRIENDS_PLACEHOLDER".localized
        self.sendButton.setTitle("SEND".localized, for: .normal)
    }
    
    
    private func configureCollectionView(){

        
        
        self.newMessageVC = RepostPopupNewMessageCollectionViewController.instanceFromStoryboard("Messaging")
        self.newMessageVC.delegate = self
        self.newMessageVC.repostPopUpView = self
        self.newMessageVC.view.frame = CGRect(x: 0, y: 0, width: searchView.frame.size.width, height: searchView.frame.size.height)
        newMessageVC.view.clipsToBounds = true
        searchView.addSubview(newMessageVC.view)
        
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1{
            
            self.hideMessagingUI()
            self.newMessageVC.selectedCell = nil
            self.newMessageVC.selectedUser = nil
        }
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if textField.tag == 1{
            if let searchTerm = textField.text{
                newMessageVC.searchTerm = searchTerm
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        UIViewController.dismissRepostPopup()
//        self.closePopUp(onMessageSentSuccess: nil)
        textField.endEditing(true)
        return true
    }
    
    func hideMessagingUI(){
        if !self.stackViewTextMessage.isHidden{
            self.messageTextField.resignFirstResponder()
            UIView.animate(withDuration: 0.3, animations: {
                self.frame = CGRect(x: self.frame.origin.x, y: (UIScreen.main.bounds.size.height - ConstantsObjects.REPOST_POPUP_HEIGHT - 67), width: self.frame.size.width, height: self.frame.size.height)
                self.layoutIfNeeded()
                
            }, completion: { (true) in
                self.stackViewTextMessage.isHidden = true
            })
        }
    }
    //More accurately get keyboard height
    func keyboardWillShow(notification:NSNotification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        self.keyboardHeight = keyboardRectangle.height
    }
    
    func showMessageUI(){
        if self.stackViewTextMessage.isHidden{
            stackViewTextMessage.isHidden = false
            self.messageTextField.becomeFirstResponder()
            
            UIView.animate(withDuration: 0.3, animations: {
                
                //Set view to top of keyboard + 265.5
                let viewTop = UIScreen.main.bounds.size.height - (self.keyboardHeight + 290.5)
                self.frame = CGRect(x: self.frame.x, y: viewTop, width: self.frame.size.width, height: self.frame.size.height)
                self.layoutIfNeeded()
                
            }, completion: { (true) in
            })
        }
    }
    
    
    @IBAction func sendAsMessage(_ sender: UIButton) {
//        if let objectId = self.product?.objectId{
//            Analytics.sharedInstance.actionsArray.append(Action(context: ["productSentInMessage":objectId], forScreen: nil, withActivityType:"Share Product In Message"))
//        }
//        
//        let messageingNewVC:MessagingNewMessageViewController = MessagingNewMessageViewController.instanceFromStoryboard("Messaging")
//        messageingNewVC.product = self.product
//        UIViewController.dismissRepostPopup()
//        
//        
//        if let homeFeed = self.homeFeedVC{
//            homeFeed.navigationController?.pushViewController(messageingNewVC, animated: true)
//        } else if let productVC = self.productVC{
//            productVC.navigationController?.pushViewController(messageingNewVC, animated: true)
//        }
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        if let user = self.newMessageVC.selectedUser,let userId = user.objectId, let productId = self.product?.objectId{
            MessagingManager.getChatWithUser(userId, completion: { (chatId) in
                self.sendMessage(chatId,userId:userId,text: self.messageTextField.text,productId: productId) // send messages
            })
        }
    }
    
    func sendMessage(_ chatId:String?,userId:String?,text:String?,productId:String){ // checks either 2 messages or 1 message
    
        if let chatId = chatId{ // chat exist
            MessagingManager.sendMessage(chatId, message: nil,listingId: nil,productId: productId, completion: { [weak vc = self](message) in // send product
                if let _ = message{
                    if let text = text , text != ""{
                        vc?.sendTextMessage(chatId: chatId, text: text) // send text
                    }else{ //only product sent
                        vc?.closePopUp(onMessageSentSuccess: true)
                    }
                }else{
                    vc?.closePopUp(onMessageSentSuccess: false)
                }
            })
            
            // create chat for first time
        }else if let userId = userId{

                MessagingManager.createChat(userId, message: nil, listingId: nil, productId: productId, completion: { [weak vc = self](chatId) in // create chat & send product
                    if let chatId = chatId{
                        if let text = text , text != ""{
                            vc?.sendTextMessage(chatId: chatId, text: text) // send text
                        }else{ //only product sent
                            vc?.closePopUp(onMessageSentSuccess: true)
                        }
                    }else{
                        vc?.closePopUp(onMessageSentSuccess: false)
                    }
                })
        }else{
            self.closePopUp(onMessageSentSuccess: false)
        }
 }
    
    func sendTextMessage(chatId:String,text:String){
        MessagingManager.sendMessage(chatId, message: text,listingId: nil,productId: nil, completion: { [weak vc = self](message) in // send text
            if let _ = message{
                vc?.closePopUp(onMessageSentSuccess: true)
                
            }else{
                vc?.closePopUp(onMessageSentSuccess: false)
            }
        })
    }
 
    func closePopUp(onMessageSentSuccess:Bool?){
        self.endEditing(true)
        if let onMessageSentSuccess = onMessageSentSuccess, onMessageSentSuccess == true{
            ErrorHandling.customSuccessHandler(message: "MESSAGE_SENT_SUCCESSFULLY".localized)
            
        }else if let onMessageSentSuccess = onMessageSentSuccess, onMessageSentSuccess == false{
            ErrorHandling.customErrorHandler(message: "ERROR_MESSAGE_SEND".localized)
        }
        UIViewController.dismissRepostPopup()
    }
    
    
    @IBAction func shareOnFacebook(_ sender: UIButton) {
        UIViewController.dismissRepostPopup()
        if let _ = AccessToken.current {
            if let product = self.product{
                if let productImages = product.images{
                    if productImages.count > 0{
                        if let productId = product.objectId
                        {
                            HelperFunction.getBranchLink(title: product.brandName, forUser: nil, forProduct: productId, withInviteCode:nil, withUrl: productImages[0], andContentDescription: product.descriptionAttributedString?.string ?? "", onChannel: "Facebook", completion:{ (url, error) in
                                
                                let content = LinkShareContent(url: URL(string:url!)!, title: product.brandName, description: product.descriptionAttributedString?.string ?? "", imageURL: URL(string:productImages[0]))
                                
                                do{
                                    if let homeFeedVC = self.homeFeedVC{
                                        try ShareDialog.show(from: homeFeedVC, content: content)
                                    } else if let productVC = self.productVC{
                                        try ShareDialog.show(from: productVC, content: content)
                                    }
                                }
                                catch{}
                            })
                        }
                    }
                }
            }
        }else {
            //User is not logged in
            let loginManager = LoginManager()
            
            if let homeFeedVC = self.homeFeedVC{
                loginManager.logIn([.publicProfile, .userFriends, .email], viewController: homeFeedVC, completion: { (loginResult) in
                    switch loginResult {
                    case .failed( _):
                        break;
                    case .cancelled:
                        break;
                    case .success(grantedPermissions: _, declinedPermissions: _, token: _):
                        break;
                    }
                    
                })
            } else if let productVC = self.productVC{
                loginManager.logIn([.publicProfile, .userFriends, .email], viewController: productVC, completion: { (loginResult) in
                    switch loginResult {
                    case .failed( _):
                        break;
                    case .cancelled:
                        break;
                    case .success(grantedPermissions: _, declinedPermissions: _, token: _):
                        break;
                    }
                    
                })
            }
            
        }
    }
    
    @IBAction func shareOnTwitter(_ sender: UIButton) {
        UIViewController.dismissRepostPopup()
        let store = Twitter.sharedInstance().sessionStore
        if let _ = store.session() {
            if let product = self.product{
                if let productImages = product.images{
                    if productImages.count > 0{
                        if let productId = product.objectId{
                            HelperFunction.getBranchLink(title: product.brandName, forUser: nil, forProduct: productId, withInviteCode:nil, withUrl: productImages[0], andContentDescription: product.keywords, onChannel: "Twitter", completion: { (url, error) in
                                if error == nil{
                                    let imageView = UIImageView()
                                    
                                    imageView.kf.setImage(with: URL(string: productImages[0])!, placeholder: nil,
                                                          completionHandler: { image, error, cacheType, imageURL in
                                                            if error == nil{
                                                                let composer = TWTRComposer()
                                                                
                                                                composer.setText(product.brandName + "ON_LOOPLIST".localized)
                                                                composer.setURL(URL(string:url!))
                                                                composer.setImage(imageView.image)
                                                                // Called from a UIViewController
                                                                if let homeFeedVC = self.homeFeedVC{
                                                                    composer.show(from: homeFeedVC) { result in
                                                                        if (result == TWTRComposerResult.cancelled) {
                                                                        }
                                                                        else {
                                                                        }
                                                                    }
                                                                } else if let productVC = self.productVC{
                                                                    composer.show(from: productVC) { result in
                                                                        if (result == TWTRComposerResult.cancelled) {
                                                                        }
                                                                        else {
                                                                        }
                                                                    }
                                                                }
                                                            }
                                    })
                                    
                                }
                                
                                
                            })
                        }
                    }
                }
            }
        } else {
            //User is not logged in
            Twitter.sharedInstance().logIn {
                (session, error) -> Void in
                if (session != nil) {
                } else {
                }
            }
        }
    }
    
    @IBAction func copyLink(_ sender: UIButton) {
        if let product = self.product{
            if let productImages = product.images{
                if productImages.count > 0{
                    if  let productId = product.objectId{
                        HelperFunction.getBranchLink(title: product.brandName, forUser: nil, forProduct: productId, withInviteCode:nil, withUrl: productImages[0], andContentDescription: product.keywords, onChannel: "General", completion:{ (url, error) in
                            UIViewController.dismissRepostPopup()
                            if error == nil{
                                UIPasteboard.general.string = url
                                //                                DropDownAlert.customSuccessMessage("Link successfully copied to clipboard")
                                ErrorHandling.customSuccessHandler(message: "LINK_COPIED_TO_CLIPBOARD".localized)
                            } else {
                                //                                DropDownAlert.customErrorMessage("Error getting link")
                                ErrorHandling.customErrorHandler(message:"ERROR_LINK".localized)
                                
                            }
                        })
                    }
                }
            }
        }
    }
    
    @IBAction func moreOptions(_ sender: UIButton) {
        if let product = self.product{
            if let productImages = product.images{
                if productImages.count > 0{
                    if let productId = product.objectId{
                        HelperFunction.getBranchLink(title: product.brandName, forUser: nil, forProduct: productId, withInviteCode:nil, withUrl: productImages[0], andContentDescription: product.keywords, onChannel: "General", completion:{ (url, error) in
                            UIViewController.dismissRepostPopup()
                            if error == nil{
                                let activityViewController = UIActivityViewController(activityItems: [url!], applicationActivities: nil)
                                if let homeFeedVC = self.homeFeedVC{
                                    homeFeedVC.present(activityViewController, animated: true, completion: {})
                                } else if let productVC = self.productVC{
                                    productVC.present(activityViewController, animated: true, completion: {})
                                }
                            } else {
                                //                                DropDownAlert.customErrorMessage("Error getting link")
                                ErrorHandling.customErrorHandler(message:"ERROR_LINK".localized)
                                
                            }
                        })
                    }
                }
            }
        }
    }
 }
