//
//  RepostPopupSendNewMessageCell.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/13/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class RepostPopupSendNewMessageCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var userImageView: UIImageView!

    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    
    
    var user:User?{
        didSet{
            if let user = user, let userImageView = userImageView, let userFullNameLabel = userFullNameLabel, let userNameLabel = userNameLabel  {
                
                userImageView.image = nil
                userFullNameLabel.text = nil
                userNameLabel.text = nil
                
                if let userName = user.userName{
                    userNameLabel.text = "@" + userName
                }
                
                if let fullName = user.fullName{
                    userFullNameLabel.text = fullName
                }
                
                //IMAGE
                
                ObjectStateHelper.getUserProfileImage(user: user, imageView: userImageView)

            }
        }
    }
    
    func stateOfSelectedCell(isSelected:Bool){
        if isSelected{
            self.userImageView.layer.borderColor = ConstantsColor.looplistColor.cgColor
            self.userFullNameLabel.textColor = ConstantsColor.looplistColor
            self.userNameLabel.textColor = ConstantsColor.looplistColor
            
//             self.userImageView.frame.width = self.userImageView.frame.width * 1.15
        }else{
            self.userImageView.layer.borderColor = UIColor.clear.cgColor
            self.userFullNameLabel.textColor = ConstantsColor.looplistGreyColor
            self.userNameLabel.textColor = ConstantsColor.looplistGreyColor
//            self.userImageView.frame.width = 50.0

        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.userImageView.layer.cornerRadius = self.userImageView.frame.height / 2.0
        self.userImageView.layer.borderWidth = 2.5
        

        self.userImageView.clipsToBounds = true
    }
    
}
