//
//  HomeFeedActivityTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 10/6/16.
//  Copyright © 2016 Looplist. All rights reserved.
//
import UIKit

class HomeFeedActivityTableViewCell: UITableViewCell {
    
    // MARK: -  //////////////////////////////////////////////////////   OUTLETS   /////////////////////////////////////////////////////////////////////////
    
    
    @IBOutlet weak var likesButton: UIButton!
    @IBOutlet weak var addToCollectionButton: UIButton!
    @IBOutlet weak var repostButton: UIButton!
    @IBOutlet weak var activityButton: UIButton!
    

    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var priceRangeLabel: UILabel!
    @IBOutlet weak var activityDescription: UILabel!
    @IBOutlet weak var activityUser: UILabel!
    @IBOutlet weak var activityUserImage: UIImageView!
    
    
    @IBOutlet weak var activityView: UIView!
    
    var startTime:Date?
    
    @IBOutlet weak var parallaxHolderView: UIView!
    @IBOutlet weak var timeAgoLabel: UILabel!
    @IBOutlet weak var actionTypeImage: UIImageView!
    @IBOutlet weak var shareButton: UIButton!
    // MARK: -  //////////////////////////////////////////////////////   FUNCTIONS   /////////////////////////////////////////////////////////////////////////
    
    var activity:ActivityHomeFeed?{
        didSet{
            
            productNameLabel.text = nil
            priceRangeLabel.text = nil
            
            activityDescription.text = nil
            activityUser.text = nil
            activityUserImage.image = UIImage(named: Constants.placeholder)
            
            if let activity = activity{
                
                
                
                if let activityUser = activityUser{
                    activityUser.text = activity.getUserDescription()
//                     activityUser.text = "testtesttesttesttesttesttesttetstets"
//                    if let firstName = user.firstName{
//                        activityUser.text = firstName
//                    }
                }
                
                
                //ACTIVITY CELL
                if let activityUserImage = activityUserImage,let user = activity.getUser(){
                    ObjectStateHelper.getUserProfileImage(user: user, imageView: activityUserImage)
                }
                
        
                //ACTIVITY CELL
                if let timeAgoLabel = timeAgoLabel{
                    if let date = activity.date{
                        timeAgoLabel.text = HelperDate.homeFeedGetDate(date)
                    }
                }
                //ACTIVITY CELL
                
                
                if let activityDescription = activityDescription{
                    if activity.activityTypeHomeFeed == .aggRepostProduct{
                        activityDescription.text = "REPOSTED_THIS_PRODUCT".localized
                    }else if activity.activityTypeHomeFeed == .aggLikeProduct{
                        activityDescription.text = "LIKED_THIS_PRODUCT".localized
                    }
                }
                //ACTIVITY CELL
                if let actionTypeImage = actionTypeImage{
                    if activity.activityTypeHomeFeed == .aggRepostProduct{
                        actionTypeImage.image = UIImage(named:Constants.activityRepost)
                    }else if activity.activityTypeHomeFeed == .aggLikeProduct{
                        actionTypeImage.image = UIImage(named:Constants.activityLike)
                    }
                }
                
                
                if let productNameLabel = productNameLabel{
                    productNameLabel.text = activity.product?.brandName.capitalized
                }
                
                
                // IMAGE
                
                if let count = (activity.product?.images?.count) , count > 0{
                    if let featuredImage = activity.product?.images?[0], let nsurl = URL(string:(featuredImage)){
                        productImage.kf.setImage(with:nsurl, placeholder: UIImage(named: Constants.placeholder))
                        
                    }
                }
                
                
                if let priceRangeLabel = priceRangeLabel{
                    priceRangeLabel.text = activity.product?.priceRange
                }
                
                
                // Like Button Fill
                if let likesButton = likesButton{
                    if activity.product?.isLikedByCurrentUser == true {
                        likesButton.setImage(UIImage(named: Constants.likedHeartsActive), for: UIControlState())
                    }else{
                        likesButton.setImage(UIImage(named: Constants.likedHeartsInActive), for: UIControlState())
                    }
                }
                
                // Add to collection button
                if let addToCollectionButton = addToCollectionButton{
                    if activity.product?.isInUsersCollections == true {
                        addToCollectionButton.setImage(UIImage(named: Constants.addBlue), for: UIControlState())
                    }else{
                        addToCollectionButton.setImage(UIImage(named: Constants.addGrey), for: UIControlState())
                    }
                }
                
                // Repost
                if let repostButton = repostButton{
                    if activity.product?.isRepostedByCurrentUser == true{
                        repostButton.setImage( UIImage(named: Constants.repostedImage), for: UIControlState())
                    } else {
                        repostButton.setImage(UIImage(named: Constants.repostImage), for: UIControlState())
                    }
                }
            }
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.parallaxHolderView.clipsToBounds = true
        self.productImage.clipsToBounds = true
        
        
        
    }
    //    @IBAction func likeProduct(_ sender: UIButton) {
    //        self.homeFeedVC.getLikedProductFromButton(sender)
    //    }
    //    @IBAction func addProductToCollection(_ sender: UIButton) {
    //        self.homeFeedVC.getAddToCollectionFromButton(sender)
    //    }
    //    @IBAction func repostProduct(_ sender: UIButton) {
    //        self.homeFeedVC.getRepostProductFromButton(sender)
    //    }
    //    @IBAction func getUserFromActivity(_ sender: UIButton) {
    //        self.homeFeedVC.getUserFromActivityButton(sender)
    //    }
}
