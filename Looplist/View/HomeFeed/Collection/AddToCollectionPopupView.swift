//
//  AddToCollectionPopupView.swift
//  Looplist
//
//  Created by Carson Aberle on 12/30/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class AddToCollectionPopupView: UIView {
    
    @IBOutlet weak var noCollectionsLabel: UILabel!
    weak var homeFeedVC:HomeFeedViewController?
    weak var productVC:ProductTableViewController?
    var collections:[Collection] = [Collection]()
    var product:Product?{
        didSet{
            if let productId = self.product?.objectId{
                DBHelper.callAPIGet("collections", queryString: DBHelper.getJSONFromDictionary(["sort": "-updatedAt", "where":["owner":CurrentUser.sharedInstance.currentUser!.objectId!,"isLikesCollection":false, "isAddedProductCollection":false],"flags":["containsProduct":productId], "populate":[["path": "products"]]])) { (result, error) in
                    if error == nil{
                        if let collectionsDictionaries:NSArray = result?["collections"] as? NSArray{
                            for value in collectionsDictionaries{
                                if let dictionary = value as? [String:AnyObject]{
                                    let tempCollection = Collection.init(input:dictionary)
                                    self.collections.append(tempCollection)
                                }
                            }
                            self.setInfo()
                        }
                    }
                }
            }
        }
    }
    
    @IBOutlet weak var buttonOne: UIButton!
    @IBOutlet weak var buttonTwo: UIButton!
    @IBOutlet weak var buttonThree: UIButton!
    @IBOutlet weak var moreCollectionsButton: UIButton!
    @IBOutlet weak var createNewButton: UIButton!
    @IBOutlet weak var imageOne: UIImageView!
    @IBOutlet weak var imageTwo: UIImageView!
    @IBOutlet weak var imageThree: UIImageView!
    @IBOutlet weak var textOne: UILabel!
    @IBOutlet weak var textTwo: UILabel!
    @IBOutlet weak var textThree: UILabel!
    
    @IBOutlet weak var optionBOne: UIImageView!
    @IBOutlet weak var optionAOne: UIImageView!
    
    @IBOutlet weak var optionATwo: UIImageView!
    @IBOutlet weak var optionBTwo: UIImageView!
    
    @IBOutlet weak var optionBThree: UIImageView!
    @IBOutlet weak var optionAThree: UIImageView!
    
    @IBOutlet weak var addButtonOne: UIButton!
    @IBOutlet weak var addButtonTwo: UIButton!
    @IBOutlet weak var addButtonThree: UIButton!
    @IBOutlet weak var createNewLabel: UIButton!
    @IBOutlet weak var addToCollectionLabel: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        self.moreCollectionsButton.isHidden = true
        
        self.activityIndicator.startAnimating()
        self.moreCollectionsButton.setTitle("MORE_COLLECTIONS".localized, for: .normal)
        self.addToCollectionLabel.text = "ADD_TO_COLLECTION".localized
        self.createNewLabel.setTitle("CREATE_NEW".localized, for: .normal)
        self.moreCollectionsButton.layer.borderColor = UIColor.darkGray.cgColor
        self.noCollectionsLabel.text = "NO_COLLECTIONS".localized
    }
    
    func setInfo(){
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        
        let collections = self.collections
        if collections.count > 0{
            let collectionOne = collections[0]
            if let containsProduct = collectionOne.containsProduct{
                if containsProduct{
                    self.buttonOne.setImage(UIImage(named: "Checkmark Blue"), for: .normal)
                } else {
                    self.buttonOne.setImage(UIImage(named: "Add"), for: .normal)
                }
            } else {
                self.buttonOne.setImage(UIImage(named: "Add"), for: .normal)
            }
            if let nameOne = collectionOne.name {
                self.textOne.text = nameOne
            }
            if let productsOne = collectionOne.products{
                if productsOne.count > 0{
                    let productOne = productsOne[0]
                    if let imagesOne = productOne.images{
                        if imagesOne.count > 0{
                            let imageUrlOne = imagesOne[0]
                            self.imageOne.kf.setImage(with: URL(string:imageUrlOne))
                        } else {
                            self.imageOne.image = UIImage(named:"Placeholder")
                        }
                    }
                } else {
                    self.imageOne.image = UIImage(named:"Placeholder")
                }
            }
        } else {
            self.moreCollectionsButton.isHidden = true
            self.noCollectionsLabel.isHidden = false
        }
        if collections.count > 1{
            let collectionTwo = collections[1]
            if let containsProduct = collectionTwo.containsProduct{
                if containsProduct{
                    self.buttonTwo.setImage(UIImage(named: "Checkmark Blue"), for: .normal)
                } else {
                    self.buttonTwo.setImage(UIImage(named: "Add"), for: .normal)
                }
            } else {
                self.buttonTwo.setImage(UIImage(named: "Add"), for: .normal)
            }
            if let nameTwo = collectionTwo.name {
                self.textTwo.text = nameTwo
            }
            if let productsTwo = collectionTwo.products{
                if productsTwo.count > 0{
                    let productTwo = productsTwo[0]
                    if let imagesTwo = productTwo.images{
                        if imagesTwo.count > 0{
                            let imageUrlTwo = imagesTwo[0]
                            self.imageTwo.kf.setImage(with: URL(string:imageUrlTwo))
                        } else {
                            self.imageTwo.image = UIImage(named:"Placeholder")
                        }
                    }
                } else {
                    self.imageTwo.image = UIImage(named:"Placeholder")
                }
            }
        }
        if collections.count > 2 {
            let collectionThree = collections[2]
            if let containsProduct = collectionThree.containsProduct{
                if containsProduct{
                    self.buttonThree.setImage(UIImage(named: "Checkmark Blue"), for: .normal)
                } else {
                    self.buttonThree.setImage(UIImage(named: "Add"), for: .normal)
                }
            } else {
                self.buttonThree.setImage(UIImage(named: "Add"), for: .normal)
            }
            
            if let nameThree = collectionThree.name {
                self.textThree.text = nameThree
            }
            
            if let productsThree = collectionThree.products{
                if productsThree.count > 0{
                    let productThree = productsThree[0]
                    if let imagesThree = productThree.images{
                        if imagesThree.count > 0{
                            let imageUrlThree = imagesThree[0]
                            self.imageThree.kf.setImage(with: URL(string:imageUrlThree))
                        } else {
                            self.imageThree.image = UIImage(named:"Placeholder")
                        }
                    }
                } else {
                    self.imageThree.image = UIImage(named:"Placeholder")
                }
            }
        }
        if collections.count > 3{
            self.moreCollectionsButton.isHidden = false
        }
    }
    
    @IBAction func addToCollectionOne(_ sender: UIButton) {
        let collections = self.collections
        if let product = self.product{
            if collections.count > 0{
                let collection = collections[0]
                if !collection.isAddedProductCollection!{
                    if let containsProduct = collection.containsProduct{
                        if containsProduct{
                            //Remove From Collection
                            APIHelper.removeProductFromCollection(collection: collection, product: product, completion: { (success) in
                                if let _ = success{
                                    self.buttonOne.setImage(UIImage(named:"Add"), for: .normal)
                                    if let homeFeedVC = self.homeFeedVC{
                                        homeFeedVC.tableView.reloadData()
                                    } else if let productVC = self.productVC{
                                        productVC.tableView.reloadData()
                                    }
                                }
                            })
                        } else {
                            //Add To Collection
                            APIHelper.addProductToCollection(collection: collection, product: product, completion: { (success) in
                                if let _ = success{
                                    self.buttonOne.setImage(UIImage(named:"Checkmark Blue"), for: .normal)
                                    if let homeFeedVC = self.homeFeedVC{
                                        homeFeedVC.tableView.reloadData()
                                    } else if let productVC = self.productVC{
                                        productVC.tableView.reloadData()
                                    }
                                }
                            })
                        }
                    }
                } else {
                    DropDownAlert.customErrorMessage("CANT_ADD_TO_ADDED_PRODUCTS_COLLECTION".localized)
                }
            }
        }
    }
    
    @IBAction func addToCollectionTwo(_ sender: UIButton) {
        let collections = self.collections
        if let product = self.product{
            if collections.count > 1{
                let collection = collections[1]
                if !collection.isAddedProductCollection! {
                    if let containsProduct = collection.containsProduct{
                        if containsProduct{
                            //Remove From Collection
                            APIHelper.removeProductFromCollection(collection: collection, product: product, completion: { (success) in
                                if let _ = success{
                                    self.buttonTwo.setImage(UIImage(named:"Add"), for: .normal)
                                    if let homeFeedVC = self.homeFeedVC{
                                        homeFeedVC.tableView.reloadData()
                                    } else if let productVC = self.productVC{
                                        productVC.tableView.reloadData()
                                    }
                                }
                            })
                        } else {
                            //Add To Collection
                            APIHelper.addProductToCollection(collection: collection, product: product, completion: { (success) in
                                if let _ = success{
                                    self.buttonTwo.setImage(UIImage(named:"Checkmark Blue"), for: .normal)
                                    if let homeFeedVC = self.homeFeedVC{
                                        homeFeedVC.tableView.reloadData()
                                    } else if let productVC = self.productVC{
                                        productVC.tableView.reloadData()
                                    }
                                }
                            })
                        }
                    }
                } else {
                    DropDownAlert.customErrorMessage("CANT_ADD_TO_ADDED_PRODUCTS_COLLECTION".localized)
                }
            }
        }
    }
    
    @IBAction func addToCollectionThree(_ sender: UIButton) {
        let collections = self.collections
        if let product = self.product{
            if collections.count > 2{
                let collection = collections[2]
                if !collection.isAddedProductCollection! {
                    if let containsProduct = collection.containsProduct{
                        if containsProduct{
                            //Remove From Collection
                            APIHelper.removeProductFromCollection(collection: collection, product: product, completion: { (success) in
                                if let _ = success{
                                    self.buttonThree.setImage(UIImage(named:"Add"), for: .normal)
                                    if let homeFeedVC = self.homeFeedVC{
                                        homeFeedVC.tableView.reloadData()
                                    } else if let productVC = self.productVC{
                                        productVC.tableView.reloadData()
                                    }
                                }
                            })
                        } else {
                            //Add To Collection
                            APIHelper.addProductToCollection(collection: collection, product: product, completion: { (success) in
                                if let _ = success{
                                    self.buttonThree.setImage(UIImage(named:"Checkmark Blue"), for: .normal)
                                    if let homeFeedVC = self.homeFeedVC{
                                        homeFeedVC.tableView.reloadData()
                                    } else if let productVC = self.productVC{
                                        productVC.tableView.reloadData()
                                    }
                                }
                            })
                        }
                    }
                    
                } else {
                    DropDownAlert.customErrorMessage("CANT_ADD_TO_ADDED_PRODUCTS_COLLECTION".localized)
                }
            }
        }
    }
    
    
    @IBAction func createNewCollection(_ sender: UIButton) {
        UIViewController.dismissAddToCollectionPopup()
        let newCollectionVC:AddCollectionViewController = AddCollectionViewController.instanceFromStoryboard("Main")
        if let product = self.product{
            newCollectionVC.product = product
        }
        
        if let homeFeedVC = self.homeFeedVC{
            homeFeedVC.navigationController?.pushViewController(newCollectionVC, animated: true)
        } else if let productVC = self.productVC{
            productVC.navigationController?.pushViewController(newCollectionVC, animated: true)
        }
    }
    
    @IBAction func moreCollections(_ sender: UIButton) {
        if let homeFeedVC = self.homeFeedVC{
            homeFeedVC.expandAddToCollectionPopup(view: self)
        } else if let productVC = self.productVC{
            productVC.expandAddToCollectionPopup(view: self)
        }
    }
    
}
