//
//  FilterPopupView.swift
//  Looplist
//
//  Created by Carson Aberle on 12/6/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import Foundation
import MARKRangeSlider

public class FilterPopupView: UIView {

    @IBOutlet weak var moreFiltersButton: UIButton!
    @IBOutlet weak var priceRangeLabel: UILabel!
    @IBOutlet weak var priceRangeSlider: MARKRangeSlider!
    weak var discoverVC:DiscoverViewController?
    @IBOutlet weak var resetButton: UIButton!
    
    @IBOutlet weak var yellowButton: UIButton!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var blueButton: UIButton!
    @IBOutlet weak var greenButton: UIButton!
    @IBOutlet weak var maroonButton: UIButton!
    @IBOutlet weak var purpleButton: UIButton!
    @IBOutlet weak var pinkButton: UIButton!
    @IBOutlet weak var whiteButton: UIButton!
    @IBOutlet weak var lightGreyButton: UIButton!
    @IBOutlet weak var darkGreyButton: UIButton!
    @IBOutlet weak var blackButton: UIButton!

    @IBOutlet weak var tanButton: UIButton!
    @IBOutlet weak var brownButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    
    
    var preSelectedFilters:NSDictionary?{
        didSet{
            if let minPrice = self.preSelectedFilters?["minPrice"] as? Int, let maxPrice = self.preSelectedFilters?["maxPrice"] as? Int{
                self.priceRangeSlider.setLeftValue(floor(CGFloat(Double(minPrice/100))), rightValue: floor(CGFloat(Double(maxPrice/100))))
                
                self.leftValueLabel.text = "$" + String(Int(Double(minPrice/100)))
                if maxPrice == 100000{
                    self.rightValueLabel.text = "$1000+"
                } else {
                    self.rightValueLabel.text = "$" + String(Int(Double(maxPrice/100)))
                }
                self.filtersDictionary.setValue(minPrice, forKey: "minPrice")
                self.filtersDictionary.setValue(maxPrice, forKey: "maxPrice")
            } else {
                self.priceRangeSlider.setLeftValue(1, rightValue: 1000)
                
                self.leftValueLabel.text = "$1"
                self.rightValueLabel.text = "$1000+"
                
                self.filtersDictionary.setValue(100, forKey: "minPrice")
                self.filtersDictionary.setValue(100000, forKey: "maxPrice")
            }
            
            if let _ = self.preSelectedFilters?["colors"] as? NSArray{
                
            } else {
                self.filtersDictionary.setValue(NSArray(), forKey: "colors")
            }
            
        }
    }
    var selectedColorsTags:[Int] = [Int]()
    var filtersDictionary:NSMutableDictionary = NSMutableDictionary()
    @IBOutlet weak var rightValueLabel: UILabel!
    @IBOutlet weak var leftValueLabel: UILabel!
    
    override public func awakeFromNib() {
        self.priceLabel.text = "PRICE".localized
        self.colorLabel.text = "COLOR".localized
        self.moreFiltersButton.setTitle("MORE_FILTERS".localized, for: .normal)
        self.resetButton.setTitle("RESET_ALL".localized, for: .normal)
        
        self.yellowButton.tag = Constants.YELLOW_BUTTON_TAG
        self.redButton.tag = Constants.RED_BUTTON_TAG
        self.blueButton.tag = Constants.BLUE_BUTTON_TAG
        self.greenButton.tag = Constants.GREEN_BUTTON_TAG
        self.maroonButton.tag = Constants.ORANGE_BUTTON_TAG
        self.purpleButton.tag = Constants.PURPLE_BUTTON_TAG
        self.pinkButton.tag = Constants.PINK_BUTTON_TAG
        self.whiteButton.tag = Constants.WHITE_BUTTON_TAG
        self.lightGreyButton.tag = Constants.LIGHT_GREY_BUTTON_TAG
        self.darkGreyButton.tag = Constants.DARK_GREY_BUTTON_TAG
        self.blackButton.tag = Constants.BLACK_BUTTON_TAG
        self.tanButton.tag = Constants.TAN_BUTTON_TAG
        self.brownButton.tag = Constants.BROWN_BUTTON_TAG
        
        self.moreFiltersButton.layer.borderWidth = 1.0
        self.moreFiltersButton.layer.borderColor = UIColor(red:0.56, green:0.56, blue:0.56, alpha:1.0).cgColor
        self.moreFiltersButton.layer.cornerRadius = 17.5
        
        self.priceRangeSlider.setMinValue(1, maxValue: 1000)

        self.leftValueLabel.frame.center.x = 49
        self.rightValueLabel.frame.center.x = UIScreen.main.bounds.size.width - 41
        
        self.leftValueLabel.frame.origin.y = self.priceRangeSlider.frame.origin.y - 25
        self.rightValueLabel.frame.origin.y = self.priceRangeSlider.frame.origin.y - 25
        

    }

    @IBAction func priceRangeChanged(_ sender: MARKRangeSlider) {
        self.filtersDictionary.setValue(Int(sender.leftValue * 100), forKey: "minPrice")
        self.filtersDictionary.setValue(Int(sender.rightValue * 100), forKey: "maxPrice")
        
        self.leftValueLabel.text = "$\(Int(sender.leftValue))"
        if sender.rightValue == 1000{
            self.rightValueLabel.text = "$\(Int(sender.rightValue))+"
        } else {
            self.rightValueLabel.text = "$\(Int(sender.rightValue))"
        }
        
        self.leftValueLabel.frame.center.x = self.priceRangeSlider.leftThumbView.frame.center.x + 27
        self.rightValueLabel.frame.center.x = self.priceRangeSlider.rightThumbView.frame.center.x + 15
    }
    
    @IBAction func selectColor(_ sender: UIButton) {
        let tag = sender.tag
        self.selectColorWithTag(tag: tag, sender: sender)
        
        var colorsHexArray = [String]()
        for tag in self.selectedColorsTags{
            colorsHexArray.append(self.getHexValueFromTag(tag: tag))
        }
        self.filtersDictionary.setValue(colorsHexArray, forKey: "colors")
    }
    
    func getHexValueFromTag(tag:Int) -> String{
        switch (tag - 100){
        case Constants.YELLOW_BUTTON_TAG:
            return ConstantsColor.FILTERS_YELLOW_HEX
        case Constants.RED_BUTTON_TAG:
            return ConstantsColor.FILTERS_RED_HEX
        case Constants.BLUE_BUTTON_TAG:
            return ConstantsColor.FILTERS_BLUE_HEX
        case Constants.GREEN_BUTTON_TAG:
            return ConstantsColor.FILTERS_GREEN_HEX
        case Constants.ORANGE_BUTTON_TAG:
            return ConstantsColor.FILTERS_ORANGE_HEX
        case Constants.PURPLE_BUTTON_TAG:
            return ConstantsColor.FILTERS_PURPLE_HEX
        case Constants.PINK_BUTTON_TAG:
            return ConstantsColor.FILTERS_PINK_HEX
        case Constants.WHITE_BUTTON_TAG:
            return ConstantsColor.FILTERS_WHITE_HEX
        case Constants.BLACK_BUTTON_TAG:
            return ConstantsColor.FILTERS_BLACK_HEX
        case Constants.LIGHT_GREY_BUTTON_TAG:
            return ConstantsColor.FILTERS_LIGHT_GREY_HEX
        case Constants.DARK_GREY_BUTTON_TAG:
            return ConstantsColor.FILTERS_DARK_GREY_HEX
        case Constants.TAN_BUTTON_TAG:
            return ConstantsColor.FILTERS_TAN_HEX
        case Constants.BROWN_BUTTON_TAG:
            return ConstantsColor.FILTERS_BROWN_HEX
        default:
            return ConstantsColor.FILTERS_WHITE_HEX

        }
    }
    
    func getTagFromHexValue(hex:String) -> Int{
        switch (hex){
        case ConstantsColor.FILTERS_YELLOW_HEX:
            return Constants.YELLOW_BUTTON_TAG
            
        case ConstantsColor.FILTERS_RED_HEX:
            return Constants.RED_BUTTON_TAG
        case ConstantsColor.FILTERS_BLUE_HEX:
            return Constants.BLUE_BUTTON_TAG
        case ConstantsColor.FILTERS_GREEN_HEX:
            return Constants.GREEN_BUTTON_TAG
        case ConstantsColor.FILTERS_ORANGE_HEX:
            return Constants.ORANGE_BUTTON_TAG
        case ConstantsColor.FILTERS_PURPLE_HEX:
            return Constants.PURPLE_BUTTON_TAG
        case ConstantsColor.FILTERS_PINK_HEX:
            return Constants.PINK_BUTTON_TAG
        case ConstantsColor.FILTERS_WHITE_HEX:
            return Constants.WHITE_BUTTON_TAG
        case ConstantsColor.FILTERS_BLACK_HEX:
            return Constants.BLACK_BUTTON_TAG
        case ConstantsColor.FILTERS_LIGHT_GREY_HEX:
            return Constants.LIGHT_GREY_BUTTON_TAG
        case ConstantsColor.FILTERS_DARK_GREY_HEX:
            return Constants.DARK_GREY_BUTTON_TAG
        case ConstantsColor.FILTERS_TAN_HEX:
            return Constants.TAN_BUTTON_TAG
        case ConstantsColor.FILTERS_BROWN_HEX:
            return Constants.BROWN_BUTTON_TAG
        default:
            return Constants.WHITE_BUTTON_TAG
            
        }
    }
    
    func getButtonFromTag(tag:Int) -> UIButton{
        switch (tag){
        case Constants.YELLOW_BUTTON_TAG:
            return self.yellowButton
        case Constants.RED_BUTTON_TAG:
            return self.redButton
        case Constants.BLUE_BUTTON_TAG:
            return self.blueButton
        case Constants.GREEN_BUTTON_TAG:
            return self.greenButton
        case Constants.ORANGE_BUTTON_TAG:
            return self.maroonButton
        case Constants.PURPLE_BUTTON_TAG:
            return self.purpleButton
        case Constants.PINK_BUTTON_TAG:
            return self.pinkButton
        case Constants.WHITE_BUTTON_TAG:
            return self.whiteButton
        case Constants.BLACK_BUTTON_TAG:
            return self.blackButton
        case Constants.LIGHT_GREY_BUTTON_TAG:
            return self.lightGreyButton
        case Constants.DARK_GREY_BUTTON_TAG:
            return self.darkGreyButton
        case Constants.TAN_BUTTON_TAG:
            return self.tanButton
        case Constants.BROWN_BUTTON_TAG:
            return self.brownButton
        default:
            return self.whiteButton
        }
    }
    
    func selectColorWithTag(tag:Int, sender: UIButton){
        if !self.selectedColorsTags.contains((tag + 100)){
            //Add Color to selected filters
            var color:UIColor
            switch(tag){
            case Constants.YELLOW_BUTTON_TAG:
                color = ConstantsColor.FILTERS_YELLOW_COLOR
                break;
            case Constants.RED_BUTTON_TAG:
                color = ConstantsColor.FILTERS_RED_COLOR
                break;
            case Constants.BLUE_BUTTON_TAG:
                color = ConstantsColor.FILTERS_BLUE_COLOR
                break;
            case Constants.GREEN_BUTTON_TAG:
                color = ConstantsColor.FILTERS_GREEN_COLOR
                break;
            case Constants.ORANGE_BUTTON_TAG:
                color = ConstantsColor.FILTERS_ORANGE_COLOR
                break;
            case Constants.PURPLE_BUTTON_TAG:
                color = ConstantsColor.FILTERS_PURPLE_COLOR
                break;
            case Constants.PINK_BUTTON_TAG:
                color = ConstantsColor.FILTERS_PINK_COLOR
                break;
            case Constants.WHITE_BUTTON_TAG:
                color = ConstantsColor.FILTERS_WHITE_COLOR
                break;
            case Constants.BLACK_BUTTON_TAG:
                color = ConstantsColor.FILTERS_BLACK_COLOR
                break;
            case Constants.LIGHT_GREY_BUTTON_TAG:
                color = ConstantsColor.FILTERS_LIGHT_GREY_COLOR
                break;
            case Constants.DARK_GREY_BUTTON_TAG:
                color = ConstantsColor.FILTERS_DARK_GREY_COLOR
                break;
            case Constants.TAN_BUTTON_TAG:
                color = ConstantsColor.FILTERS_TAN_COLOR
                break;
            case Constants.BROWN_BUTTON_TAG:
                color = ConstantsColor.FILTERS_BROWN_COLOR
                break;
            default:
                color = ConstantsColor.FILTERS_WHITE_COLOR
                break;
            }
            
            if let superView = sender.superview{
                let frame = superView.convert(sender.frame, to:self)
                let view = UIView(frame: CGRect(x: frame.center.x - 19, y: frame.center.y - 19, width: 38, height:38))
                view.layer.borderColor = color.cgColor
                view.layer.borderWidth = 2
                view.backgroundColor = UIColor.clear
                view.clipsToBounds = true
                view.tag = tag + 100
                view.layer.cornerRadius = 19
                self.insertSubview(view, at: 0)
                self.selectedColorsTags.append(tag + 100)
            }
        } else {
            //Remove color from selected filters
            if let view = self.viewWithTag(tag + 100), let index = self.selectedColorsTags.index(of: tag + 100){
                self.selectedColorsTags.remove(at: index)
                view.removeFromSuperview()
            }
        }
    }
}
