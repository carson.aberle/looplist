//
//  FiltersTextTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 11/7/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class FiltersTextTableViewCell: UITableViewCell {

    @IBOutlet weak var caretImage: UIImageView!
    @IBOutlet weak var filterTitleText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
