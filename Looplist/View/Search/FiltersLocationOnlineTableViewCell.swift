//
//  FiltersLocationTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 11/7/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class FiltersLocationOnlineTableViewCell: UITableViewCell {

    @IBOutlet weak var sliderText: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var addressSearchBar: UITextField!
    @IBOutlet weak var addressFindImage: UIImageView!
    @IBOutlet weak var onlineRadioImage: UIImageView!
    @IBOutlet weak var localRadioImage: UIImageView!
    @IBOutlet weak var bothRadioImage: UIImageView!
    @IBOutlet weak var fiveMilesLabel: UILabel!
    @IBOutlet weak var fiveHundredMilesLabel: UILabel!
    @IBOutlet weak var locationNeighborhoodLabel: UILabel!
    @IBOutlet weak var onlineText: UILabel!
    @IBOutlet weak var localText: UILabel!
    @IBOutlet weak var bothText: UILabel!
    var filtersVC:FiltersViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func updateBubblePosition(_ sender:UISlider){
        DispatchQueue.main.async(execute: {
            self.sliderText.text = String(Int(sender.value)) + "mi"
            let trackRect = self.slider.trackRect(forBounds: self.slider.bounds)
            let thumbRect = self.slider.thumbRect(forBounds: self.slider.bounds, trackRect: trackRect, value: sender.value)
            let xPosition = thumbRect.center.x + self.slider.frame.origin.x
            self.sliderText.center.x = xPosition
            self.sliderText.setNeedsDisplay()
        })
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
