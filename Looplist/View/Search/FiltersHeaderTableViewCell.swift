//
//  FiltersHeaderTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 12/12/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import MARKRangeSlider

class FiltersHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var rangeSlider: MARKRangeSlider!
    @IBOutlet weak var sliderText: UILabel!

    @IBOutlet weak var yellowButton: UIButton!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var blueButton: UIButton!
    @IBOutlet weak var greenButton: UIButton!
    @IBOutlet weak var maroonButton: UIButton!
    @IBOutlet weak var purpleButton: UIButton!
    @IBOutlet weak var pinkButton: UIButton!
    @IBOutlet weak var whiteButton: UIButton!
    @IBOutlet weak var lightGreyButton: UIButton!
    @IBOutlet weak var darkGreyButton: UIButton!
    @IBOutlet weak var blackButton: UIButton!
    @IBOutlet weak var tanButton: UIButton!
    @IBOutlet weak var brownButton: UIButton!

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var resetButton: UIButton!

    var selectedColorsTags:[Int] = [Int]()

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.priceLabel.text = "PRICE".localized
        self.resetButton.setTitle("RESET".localized, for: .normal)
        self.colorLabel.text = "COLOR".localized
        
        // Initialization code
        self.yellowButton.tag = Constants.YELLOW_BUTTON_TAG
        self.redButton.tag = Constants.RED_BUTTON_TAG
        self.blueButton.tag = Constants.BLUE_BUTTON_TAG
        self.greenButton.tag = Constants.GREEN_BUTTON_TAG
        self.maroonButton.tag = Constants.ORANGE_BUTTON_TAG
        self.purpleButton.tag = Constants.PURPLE_BUTTON_TAG
        self.pinkButton.tag = Constants.PINK_BUTTON_TAG
        self.whiteButton.tag = Constants.WHITE_BUTTON_TAG
        self.lightGreyButton.tag = Constants.LIGHT_GREY_BUTTON_TAG
        self.darkGreyButton.tag = Constants.DARK_GREY_BUTTON_TAG
        self.blackButton.tag = Constants.BLACK_BUTTON_TAG
        self.tanButton.tag = Constants.TAN_BUTTON_TAG
        self.brownButton.tag = Constants.BROWN_BUTTON_TAG
        
        self.rangeSlider.setMinValue(0, maxValue: 1000)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func valueChanged(_ sender: MARKRangeSlider) {
        //left Thumb
        let minText = "$" + String(Int(self.rangeSlider.leftValue))
        
        var maxString = "$" + String(Int(self.rangeSlider.rightValue))
        if(Int(self.rangeSlider.rightValue) == 1000){
            maxString = maxString + "+"
        }
        
        self.sliderText.text = minText + " - " + maxString
    }

    @IBAction func colorClicked(_ sender: UIButton) {
        var tag = sender.tag
        if !self.selectedColorsTags.contains((tag + 100)){
            //Add Color to selected filters
            var color:UIColor
            switch(tag){
            case Constants.YELLOW_BUTTON_TAG:
                color = ConstantsColor.FILTERS_YELLOW_COLOR
                break;
            case Constants.RED_BUTTON_TAG:
                color = ConstantsColor.FILTERS_RED_COLOR
                break;
            case Constants.BLUE_BUTTON_TAG:
                color = ConstantsColor.FILTERS_BLUE_COLOR
                break;
            case Constants.GREEN_BUTTON_TAG:
                color = ConstantsColor.FILTERS_GREEN_COLOR
                break;
            case Constants.ORANGE_BUTTON_TAG:
                color = ConstantsColor.FILTERS_ORANGE_COLOR
                break;
            case Constants.PURPLE_BUTTON_TAG:
                color = ConstantsColor.FILTERS_PURPLE_COLOR
                break;
            case Constants.PINK_BUTTON_TAG:
                color = ConstantsColor.FILTERS_PINK_COLOR
                break;
            case Constants.WHITE_BUTTON_TAG:
                color = ConstantsColor.FILTERS_WHITE_COLOR
                break;
            case Constants.BLACK_BUTTON_TAG:
                color = ConstantsColor.FILTERS_BLACK_COLOR
                break;
            case Constants.LIGHT_GREY_BUTTON_TAG:
                color = ConstantsColor.FILTERS_LIGHT_GREY_COLOR
                break;
            case Constants.DARK_GREY_BUTTON_TAG:
                color = ConstantsColor.FILTERS_DARK_GREY_COLOR
                break;
            case Constants.TAN_BUTTON_TAG:
                color = ConstantsColor.FILTERS_TAN_COLOR
                break;
            case Constants.BROWN_BUTTON_TAG:
                color = ConstantsColor.FILTERS_BROWN_COLOR
                break;
            default:
                color = ConstantsColor.FILTERS_WHITE_COLOR
                break;
            }
            
            if let superView = sender.superview{
                let frame = superView.convert(sender.frame, to:self)
                
                let view = UIView(frame: CGRect(x: frame.center.x - 19, y: frame.center.y - 19, width: 38, height:38))
                view.layer.borderColor = color.cgColor
                view.layer.borderWidth = 2
                view.backgroundColor = UIColor.clear
                view.clipsToBounds = true
                tag += 100
                view.tag = tag
                view.layer.cornerRadius = 19
                self.contentView.insertSubview(view, at: 0)
                self.selectedColorsTags.append(tag)
            }
        } else {
            //Remove color from selected filters
            tag += 100
            if let view = self.contentView.viewWithTag(tag), let index = self.selectedColorsTags.index(of: tag){
                self.selectedColorsTags.remove(at: index)
                view.removeFromSuperview()
            }
        }
    }
}
