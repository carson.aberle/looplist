//
//  SearchDisplayRecentTableViewCell.swift
//  Looplist
//
//  Created by Arun Sivakumar on 11/11/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class SearchDisplayRecentTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var productNameLabel: UILabel!
    
    @IBOutlet weak var productCountLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
