//
//  FiltersPriceRangeTableViewCell.swift
//  Looplist
//
//  Created by Carson Aberle on 11/15/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit
import MARKRangeSlider

class FiltersPriceRangeTableViewCell: UITableViewCell {

    @IBOutlet weak var rangeSlider: MARKRangeSlider!
    @IBOutlet weak var leftSliderText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func valueChanged(_ sender: MARKRangeSlider) {
        //left Thumb
        let minText = "$" + String(Int(self.rangeSlider.leftValue))
        
        var maxString = "$" + String(Int(self.rangeSlider.rightValue))
        if(Int(self.rangeSlider.rightValue) == 1000){
            maxString = maxString + "+"
        }
        
        self.leftSliderText.text = minText + " - " + maxString
    }

}
