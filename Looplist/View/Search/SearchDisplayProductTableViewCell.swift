//
//  SearchDisplayProductCellTableViewCell.swift
//  Looplist
//
//  Created by Arun Sivakumar on 1/6/17.
//  Copyright © 2017 Looplist. All rights reserved.
//

import UIKit

class SearchDisplayProductTableViewCell: UITableViewCell {
    

        
        @IBOutlet weak var productImageView: UIImageView!
        @IBOutlet weak var productBrandNameLabel: UILabel!
        @IBOutlet weak var productCountLabel: UILabel!
    
    var productBrandName:String?{
        didSet{
            guard let productBrandName = productBrandName
            else{ self.productBrandNameLabel.text = ""; return}
            productBrandNameLabel.text = productBrandName
        }
    }
    
    var productCount:Int?{
        didSet{
            guard let productCount = productCount,productCount > 0
                else{ self.productCountLabel.text = ""; return}
            productCountLabel.text = HelperFunction.getResultString(count: productCount)
        }
    }
    
        
//        var product:Product?{
//            didSet{
//                if let product = product{
//                 
//                    self.productBrandNameLabel.text = product.brandName
//                    
//                    productImageView.image = nil
//                    if (product.images?.count)! > 0{
//                    if let productImage = product.images?[0],let nsurl = URL(string:(productImage)){
//                        productImageView.kf.setImage(with: nsurl, placeholder: UIImage(named: Constants.placeholder), options: nil, progressBlock: nil, completionHandler: nil)
//                    }
//                    }
//                }
//                
//            }
//        }
        
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
//            if let productImageView = self.productImageView{
//                productImageView.layer.cornerRadius =  2.0
//                productImageView.clipsToBounds = true
//            }
        }
    }
