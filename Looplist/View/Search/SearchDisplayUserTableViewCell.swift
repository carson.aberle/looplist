//
//  SearchDisplayTableViewCell.swift
//  Looplist
//
//  Created by Arun Sivakumar on 11/8/16.
//  Copyright © 2016 Looplist. All rights reserved.
//

import UIKit

class SearchDisplayUserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    var user:User?{
        didSet{
            if let user = user{
                if let fullName = user.fullName{
                    self.userNameLabel.text = fullName
                }else{
                    self.userNameLabel.text = ""
                }
                
                userProfileImageView.image = nil
                if let profileImage = user.profileImageUrl,let nsurl = URL(string:(profileImage)){
                    userProfileImageView.kf.setImage(with: nsurl, placeholder: UIImage(named: Constants.placeholder), options: nil, progressBlock: nil, completionHandler: nil)
                }else if let gender = user.gender, gender == "Female"{
                    
                    if let image = UIImage(named: Constants.defaultFemaleImage){
                        userProfileImageView.image = image

                    }
                    // default
                }else{
                    
                    if let image = UIImage(named: Constants.defaultMaleImage){
                        userProfileImageView.image = image
                        
                    }
                    
                }
            }
            
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if let userProfileImageView = self.userProfileImageView{
             userProfileImageView.layer.cornerRadius = userProfileImageView.frame.height / 2.0
             userProfileImageView.clipsToBounds = true
        }
    }
}
